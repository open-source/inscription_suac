<?php

//@formatter:off

return [
    'schema'      => 'public',
    'name'        => 'atelier_inscription_etudiant_etudiant_id_fk',
    'table'       => 'atelier_inscription',
    'rtable'      => 'etudiant_etudiant',
    'update_rule' => 'NO ACTION',
    'delete_rule' => 'NO ACTION',
    'index'       => 'etudiant_etudiant_pk',
    'columns'     => [
        'etudiant_id' => 'id',
    ],
];

//@formatter:on

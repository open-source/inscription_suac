<?php

//@formatter:off

return [
    'schema'      => 'public',
    'name'        => 'unicaen_enquete_question_unicaen_enquete_groupe_id_fk',
    'table'       => 'unicaen_enquete_question',
    'rtable'      => 'unicaen_enquete_groupe',
    'update_rule' => 'NO ACTION',
    'delete_rule' => 'SET NULL',
    'index'       => 'unicaen_enquete_groupe_pk',
    'columns'     => [
        'groupe_id' => 'id',
    ],
];

//@formatter:on

<?php

//@formatter:off

return [
    'schema'      => 'public',
    'name'        => 'formation_session_etat_formation_session_id_fk',
    'table'       => 'atelier_session_etat',
    'rtable'      => 'atelier_session',
    'update_rule' => 'NO ACTION',
    'delete_rule' => 'CASCADE',
    'index'       => 'formation_session_pk',
    'columns'     => [
        'session_id' => 'id',
    ],
];

//@formatter:on

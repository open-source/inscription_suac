<?php

//@formatter:off

return [
    'schema'      => 'public',
    'name'        => 'etudiant_validation_etudiant_etudiant_id_fk',
    'table'       => 'etudiant_validation',
    'rtable'      => 'etudiant_etudiant',
    'update_rule' => 'NO ACTION',
    'delete_rule' => 'CASCADE',
    'index'       => 'etudiant_etudiant_pk',
    'columns'     => [
        'etudiant_id' => 'id',
    ],
];

//@formatter:on

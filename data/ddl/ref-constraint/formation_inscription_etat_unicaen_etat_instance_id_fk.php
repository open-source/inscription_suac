<?php

//@formatter:off

return [
    'schema'      => 'public',
    'name'        => 'formation_inscription_etat_unicaen_etat_instance_id_fk',
    'table'       => 'atelier_inscription_etat',
    'rtable'      => 'unicaen_etat_instance',
    'update_rule' => 'NO ACTION',
    'delete_rule' => 'CASCADE',
    'index'       => 'unicaen_etat_instance_pkey',
    'columns'     => [
        'etat_instance_id' => 'id',
    ],
];

//@formatter:on

<?php

//@formatter:off

return [
    'schema'      => 'public',
    'name'        => 'formation_intervenant_unicaen_utilisateur_user_id_fk',
    'table'       => 'atelier_intervenant',
    'rtable'      => 'unicaen_utilisateur_user',
    'update_rule' => 'NO ACTION',
    'delete_rule' => 'SET NULL',
    'index'       => 'unicaen_utilisateur_user_pkey',
    'columns'     => [
        'utilisateur_id' => 'id',
    ],
];

//@formatter:on

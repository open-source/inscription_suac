<?php

//@formatter:off

return [
    'schema'      => 'public',
    'name'        => 'atelier_atelier_fichier_fichier_id_fk',
    'table'       => 'atelier_atelier',
    'rtable'      => 'fichier_fichier',
    'update_rule' => 'NO ACTION',
    'delete_rule' => 'SET NULL',
    'index'       => 'fichier_fichier_pk',
    'columns'     => [
        'fichier_id' => 'id',
    ],
];

//@formatter:on

<?php

//@formatter:off

return [
    'schema'      => 'public',
    'name'        => 'formation_session_formation_id_fk',
    'table'       => 'atelier_session',
    'rtable'      => 'atelier_atelier',
    'update_rule' => 'NO ACTION',
    'delete_rule' => 'CASCADE',
    'index'       => 'formation_action_pk',
    'columns'     => [
        'atelier_id' => 'id',
    ],
];

//@formatter:on

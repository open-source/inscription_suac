<?php

//@formatter:off

return [
    'schema'      => 'public',
    'name'        => 'atelier_session_site_site_id_fk',
    'table'       => 'atelier_session',
    'rtable'      => 'site_site',
    'update_rule' => 'NO ACTION',
    'delete_rule' => 'NO ACTION',
    'index'       => 'dite_ville_pk',
    'columns'     => [
        'site_id' => 'id',
    ],
];

//@formatter:on

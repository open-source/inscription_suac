<?php

//@formatter:off

return [
    'schema'      => 'public',
    'name'        => 'formation_action_programme_formation_programme_id_fk',
    'table'       => 'atelier_atelier_programme',
    'rtable'      => 'atelier_programme',
    'update_rule' => 'NO ACTION',
    'delete_rule' => 'CASCADE',
    'index'       => 'formation_programme_pk',
    'columns'     => [
        'programme_id' => 'id',
    ],
];

//@formatter:on

<?php

//@formatter:off

return [
    'schema'      => 'public',
    'name'        => 'site_site_site_ville_id_fk',
    'table'       => 'site_site',
    'rtable'      => 'site_ville',
    'update_rule' => 'NO ACTION',
    'delete_rule' => 'NO ACTION',
    'index'       => 'site_site_pk',
    'columns'     => [
        'ville_id' => 'id',
    ],
];

//@formatter:on

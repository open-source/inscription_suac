<?php

//@formatter:off

return [
    'schema'      => 'public',
    'name'        => 'formation_session_intervenant_formation_session_id_fk',
    'table'       => 'atelier_session_intervenant',
    'rtable'      => 'atelier_session',
    'update_rule' => 'NO ACTION',
    'delete_rule' => 'NO ACTION',
    'index'       => 'formation_session_pk',
    'columns'     => [
        'session_id' => 'id',
    ],
];

//@formatter:on

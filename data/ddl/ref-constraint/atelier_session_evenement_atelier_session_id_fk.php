<?php

//@formatter:off

return [
    'schema'      => 'public',
    'name'        => 'atelier_session_evenement_atelier_session_id_fk',
    'table'       => 'atelier_session_evenement',
    'rtable'      => 'atelier_session',
    'update_rule' => 'NO ACTION',
    'delete_rule' => 'CASCADE',
    'index'       => 'formation_session_pk',
    'columns'     => [
        'session_id' => 'id',
    ],
];

//@formatter:on

<?php

//@formatter:off

return [
    'schema'      => 'public',
    'name'        => 'formation_inscription_formation_instance_id_fk',
    'table'       => 'atelier_inscription',
    'rtable'      => 'atelier_session',
    'update_rule' => 'NO ACTION',
    'delete_rule' => 'CASCADE',
    'index'       => 'formation_session_pk',
    'columns'     => [
        'session_id' => 'id',
    ],
];

//@formatter:on

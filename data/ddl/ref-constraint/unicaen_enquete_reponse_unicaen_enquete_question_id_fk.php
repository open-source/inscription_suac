<?php

//@formatter:off

return [
    'schema'      => 'public',
    'name'        => 'unicaen_enquete_reponse_unicaen_enquete_question_id_fk',
    'table'       => 'unicaen_enquete_reponse',
    'rtable'      => 'unicaen_enquete_question',
    'update_rule' => 'NO ACTION',
    'delete_rule' => 'CASCADE',
    'index'       => 'unicaen_enquete_question_pk',
    'columns'     => [
        'question_id' => 'id',
    ],
];

//@formatter:on

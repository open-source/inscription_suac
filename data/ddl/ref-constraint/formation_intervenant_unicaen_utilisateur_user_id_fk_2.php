<?php

//@formatter:off

return [
    'schema'      => 'public',
    'name'        => 'formation_intervenant_unicaen_utilisateur_user_id_fk_2',
    'table'       => 'atelier_intervenant',
    'rtable'      => 'unicaen_utilisateur_user',
    'update_rule' => 'NO ACTION',
    'delete_rule' => 'NO ACTION',
    'index'       => 'unicaen_utilisateur_user_pkey',
    'columns'     => [
        'histo_createur_id' => 'id',
    ],
];

//@formatter:on

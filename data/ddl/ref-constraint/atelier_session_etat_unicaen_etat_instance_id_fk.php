<?php

//@formatter:off

return [
    'schema'      => 'public',
    'name'        => 'atelier_session_etat_unicaen_etat_instance_id_fk',
    'table'       => 'atelier_session_etat',
    'rtable'      => 'unicaen_etat_instance',
    'update_rule' => 'CASCADE',
    'delete_rule' => 'NO ACTION',
    'index'       => 'unicaen_etat_instance_pkey',
    'columns'     => [
        'etat_instance_id' => 'id',
    ],
];

//@formatter:on

<?php

//@formatter:off

return [
    'schema'      => 'public',
    'name'        => 'formation_action_programme_formation_action_id_fk',
    'table'       => 'atelier_atelier_programme',
    'rtable'      => 'atelier_atelier',
    'update_rule' => 'NO ACTION',
    'delete_rule' => 'CASCADE',
    'index'       => 'formation_action_pk',
    'columns'     => [
        'atelier_id' => 'id',
    ],
];

//@formatter:on

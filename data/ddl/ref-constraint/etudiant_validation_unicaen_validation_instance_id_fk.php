<?php

//@formatter:off

return [
    'schema'      => 'public',
    'name'        => 'etudiant_validation_unicaen_validation_instance_id_fk',
    'table'       => 'etudiant_validation',
    'rtable'      => 'unicaen_validation_instance',
    'update_rule' => 'NO ACTION',
    'delete_rule' => 'CASCADE',
    'index'       => 'unicaen_validation_instance_pk',
    'columns'     => [
        'validation_instance_id' => 'id',
    ],
];

//@formatter:on

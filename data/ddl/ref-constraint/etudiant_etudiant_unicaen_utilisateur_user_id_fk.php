<?php

//@formatter:off

return [
    'schema'      => 'public',
    'name'        => 'etudiant_etudiant_unicaen_utilisateur_user_id_fk',
    'table'       => 'etudiant_etudiant',
    'rtable'      => 'unicaen_utilisateur_user',
    'update_rule' => 'NO ACTION',
    'delete_rule' => 'NO ACTION',
    'index'       => 'unicaen_utilisateur_user_pkey',
    'columns'     => [
        'utilisateur_id' => 'id',
    ],
];

//@formatter:on

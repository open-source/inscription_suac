<?php

//@formatter:off

return [
    'schema'      => 'public',
    'name'        => 'formation_session_intervenant_formation_intervenant_id_fk',
    'table'       => 'atelier_session_intervenant',
    'rtable'      => 'atelier_intervenant',
    'update_rule' => 'NO ACTION',
    'delete_rule' => 'NO ACTION',
    'index'       => 'formation_intervenant_pk',
    'columns'     => [
        'intervenant_id' => 'id',
    ],
];

//@formatter:on

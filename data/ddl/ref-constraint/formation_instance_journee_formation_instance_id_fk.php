<?php

//@formatter:off

return [
    'schema'      => 'public',
    'name'        => 'formation_instance_journee_formation_instance_id_fk',
    'table'       => 'atelier_seance',
    'rtable'      => 'atelier_session',
    'update_rule' => 'NO ACTION',
    'delete_rule' => 'CASCADE',
    'index'       => 'formation_session_pk',
    'columns'     => [
        'session_id' => 'id',
    ],
];

//@formatter:on

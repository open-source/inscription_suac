<?php

//@formatter:off

return [
    'schema'      => 'public',
    'name'        => 'formation_inscription_etat_formation_inscription_id_fk',
    'table'       => 'atelier_inscription_etat',
    'rtable'      => 'atelier_inscription',
    'update_rule' => 'NO ACTION',
    'delete_rule' => 'CASCADE',
    'index'       => 'formation_inscription_pk',
    'columns'     => [
        'inscription_id' => 'id',
    ],
];

//@formatter:on

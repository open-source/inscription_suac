<?php

//@formatter:off

return [
    'schema'  => 'public',
    'name'    => 'formation_instance_presence_pk',
    'table'   => 'atelier_presence',
    'index'   => 'formation_instance_presence_pk',
    'columns' => [
        'id',
    ],
];

//@formatter:on

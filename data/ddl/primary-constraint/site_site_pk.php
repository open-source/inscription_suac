<?php

//@formatter:off

return [
    'schema'  => 'public',
    'name'    => 'site_site_pk',
    'table'   => 'site_ville',
    'index'   => 'site_site_pk',
    'columns' => [
        'id',
    ],
];

//@formatter:on

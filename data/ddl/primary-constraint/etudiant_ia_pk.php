<?php

//@formatter:off

return [
    'schema'  => 'public',
    'name'    => 'etudiant_ia_pk',
    'table'   => 'etudiant_inscription_administrative',
    'index'   => 'etudiant_ia_pk',
    'columns' => [
        'id',
    ],
];

//@formatter:on

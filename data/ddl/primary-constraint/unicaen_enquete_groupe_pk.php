<?php

//@formatter:off

return [
    'schema'  => 'public',
    'name'    => 'unicaen_enquete_groupe_pk',
    'table'   => 'unicaen_enquete_groupe',
    'index'   => 'unicaen_enquete_groupe_pk',
    'columns' => [
        'id',
    ],
];

//@formatter:on

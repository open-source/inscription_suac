<?php

//@formatter:off

return [
    'schema'  => 'public',
    'name'    => 'formation_intervenant_pk',
    'table'   => 'atelier_intervenant',
    'index'   => 'formation_intervenant_pk',
    'columns' => [
        'id',
    ],
];

//@formatter:on

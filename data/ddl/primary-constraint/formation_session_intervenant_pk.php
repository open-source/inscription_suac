<?php

//@formatter:off

return [
    'schema'  => 'public',
    'name'    => 'formation_session_intervenant_pk',
    'table'   => 'atelier_session_intervenant',
    'index'   => 'formation_session_intervenant_pk',
    'columns' => [
        'intervenant_id',
        'session_id',
    ],
];

//@formatter:on

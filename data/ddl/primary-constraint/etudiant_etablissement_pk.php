<?php

//@formatter:off

return [
    'schema'  => 'public',
    'name'    => 'etudiant_etablissement_pk',
    'table'   => 'etudiant_etablissement',
    'index'   => 'etudiant_etablissement_pk',
    'columns' => [
        'id',
    ],
];

//@formatter:on

<?php

//@formatter:off

return [
    'schema'  => 'public',
    'name'    => 'etudiant_validation_pk',
    'table'   => 'etudiant_validation',
    'index'   => 'etudiant_validation_pk',
    'columns' => [
        'etudiant_id',
        'validation_instance_id',
    ],
];

//@formatter:on

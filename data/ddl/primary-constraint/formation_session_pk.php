<?php

//@formatter:off

return [
    'schema'  => 'public',
    'name'    => 'formation_session_pk',
    'table'   => 'atelier_session',
    'index'   => 'formation_session_pk',
    'columns' => [
        'id',
    ],
];

//@formatter:on

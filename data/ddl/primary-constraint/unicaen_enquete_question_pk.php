<?php

//@formatter:off

return [
    'schema'  => 'public',
    'name'    => 'unicaen_enquete_question_pk',
    'table'   => 'unicaen_enquete_question',
    'index'   => 'unicaen_enquete_question_pk',
    'columns' => [
        'id',
    ],
];

//@formatter:on

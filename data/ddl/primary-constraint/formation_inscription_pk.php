<?php

//@formatter:off

return [
    'schema'  => 'public',
    'name'    => 'formation_inscription_pk',
    'table'   => 'atelier_inscription',
    'index'   => 'formation_inscription_pk',
    'columns' => [
        'id',
    ],
];

//@formatter:on

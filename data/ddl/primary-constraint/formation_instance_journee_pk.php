<?php

//@formatter:off

return [
    'schema'  => 'public',
    'name'    => 'formation_instance_journee_pk',
    'table'   => 'atelier_seance',
    'index'   => 'formation_instance_journee_pk',
    'columns' => [
        'id',
    ],
];

//@formatter:on

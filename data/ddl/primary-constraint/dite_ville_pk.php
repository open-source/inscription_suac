<?php

//@formatter:off

return [
    'schema'  => 'public',
    'name'    => 'dite_ville_pk',
    'table'   => 'site_site',
    'index'   => 'dite_ville_pk',
    'columns' => [
        'id',
    ],
];

//@formatter:on

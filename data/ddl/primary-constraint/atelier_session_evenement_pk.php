<?php

//@formatter:off

return [
    'schema'  => 'public',
    'name'    => 'atelier_session_evenement_pk',
    'table'   => 'atelier_session_evenement',
    'index'   => 'atelier_session_evenement_pk',
    'columns' => [
        'evenement_instance_id',
        'session_id',
    ],
];

//@formatter:on

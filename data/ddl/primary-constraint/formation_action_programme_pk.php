<?php

//@formatter:off

return [
    'schema'  => 'public',
    'name'    => 'formation_action_programme_pk',
    'table'   => 'atelier_atelier_programme',
    'index'   => 'formation_action_programme_pk',
    'columns' => [
        'atelier_id',
        'programme_id',
    ],
];

//@formatter:on

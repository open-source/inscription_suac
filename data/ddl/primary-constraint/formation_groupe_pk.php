<?php

//@formatter:off

return [
    'schema'  => 'public',
    'name'    => 'formation_groupe_pk',
    'table'   => 'atelier_groupe',
    'index'   => 'formation_groupe_pk',
    'columns' => [
        'id',
    ],
];

//@formatter:on

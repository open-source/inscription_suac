<?php

//@formatter:off

return [
    'schema'  => 'public',
    'name'    => 'etudiant_etudiant_pk',
    'table'   => 'etudiant_etudiant',
    'index'   => 'etudiant_etudiant_pk',
    'columns' => [
        'id',
    ],
];

//@formatter:on

<?php

//@formatter:off

return [
    'schema'  => 'public',
    'name'    => 'atelier_session_etat_pk',
    'table'   => 'atelier_session_etat',
    'index'   => 'atelier_session_etat_pk',
    'columns' => [
        'etat_instance_id',
        'session_id',
    ],
];

//@formatter:on

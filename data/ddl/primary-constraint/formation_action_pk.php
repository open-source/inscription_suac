<?php

//@formatter:off

return [
    'schema'  => 'public',
    'name'    => 'formation_action_pk',
    'table'   => 'atelier_atelier',
    'index'   => 'formation_action_pk',
    'columns' => [
        'id',
    ],
];

//@formatter:on

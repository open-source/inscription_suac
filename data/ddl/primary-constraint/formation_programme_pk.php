<?php

//@formatter:off

return [
    'schema'  => 'public',
    'name'    => 'formation_programme_pk',
    'table'   => 'atelier_programme',
    'index'   => 'formation_programme_pk',
    'columns' => [
        'id',
    ],
];

//@formatter:on

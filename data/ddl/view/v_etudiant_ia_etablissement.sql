WITH tmp AS (
         SELECT DISTINCT etudiant_inscription_administrative.etablissement
           FROM etudiant_inscription_administrative
          ORDER BY etudiant_inscription_administrative.etablissement
        )
 SELECT row_number() OVER () AS id,
    tmp.etablissement
   FROM tmp
WITH tmp AS (
         SELECT DISTINCT eia.composante
           FROM etudiant_inscription_administrative eia
        )
 SELECT row_number() OVER () AS id,
    tmp.composante
   FROM tmp
  WHERE ((tmp.composante IS NOT NULL) AND (btrim((tmp.composante)::text) <> ''::text))
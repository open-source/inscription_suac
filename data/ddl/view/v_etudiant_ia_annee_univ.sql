WITH tmp AS (
         SELECT DISTINCT etudiant_inscription_administrative.annee_univ
           FROM etudiant_inscription_administrative
          ORDER BY etudiant_inscription_administrative.annee_univ
        )
 SELECT row_number() OVER () AS id,
    tmp.annee_univ
   FROM tmp
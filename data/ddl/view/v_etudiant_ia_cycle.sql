WITH tmp AS (
         SELECT DISTINCT etudiant_inscription_administrative.cycle
           FROM etudiant_inscription_administrative
        )
 SELECT row_number() OVER () AS id,
    tmp.cycle
   FROM tmp
  WHERE ((tmp.cycle IS NOT NULL) AND (btrim((tmp.cycle)::text) <> ''::text))
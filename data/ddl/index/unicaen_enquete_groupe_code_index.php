<?php

//@formatter:off

return [
    'name'    => 'unicaen_enquete_groupe_code_index',
    'unique'  => FALSE,
    'type'    => 'btree',
    'table'   => 'unicaen_enquete_groupe',
    'schema'  => 'public',
    'columns' => [
        'code',
    ],
];

//@formatter:on

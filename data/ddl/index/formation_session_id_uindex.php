<?php

//@formatter:off

return [
    'name'    => 'formation_session_id_uindex',
    'unique'  => TRUE,
    'type'    => 'btree',
    'table'   => 'atelier_session',
    'schema'  => 'public',
    'columns' => [
        'id',
    ],
];

//@formatter:on

<?php

//@formatter:off

return [
    'name'    => 'etudiant_validation_validation_instance_id_index',
    'unique'  => FALSE,
    'type'    => 'btree',
    'table'   => 'etudiant_validation',
    'schema'  => 'public',
    'columns' => [
        'validation_instance_id',
    ],
];

//@formatter:on

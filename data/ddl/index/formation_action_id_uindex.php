<?php

//@formatter:off

return [
    'name'    => 'formation_action_id_uindex',
    'unique'  => TRUE,
    'type'    => 'btree',
    'table'   => 'atelier_atelier',
    'schema'  => 'public',
    'columns' => [
        'id',
    ],
];

//@formatter:on

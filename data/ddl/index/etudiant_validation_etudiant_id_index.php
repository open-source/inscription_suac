<?php

//@formatter:off

return [
    'name'    => 'etudiant_validation_etudiant_id_index',
    'unique'  => FALSE,
    'type'    => 'btree',
    'table'   => 'etudiant_validation',
    'schema'  => 'public',
    'columns' => [
        'etudiant_id',
    ],
];

//@formatter:on

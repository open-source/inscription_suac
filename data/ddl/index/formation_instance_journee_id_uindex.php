<?php

//@formatter:off

return [
    'name'    => 'formation_instance_journee_id_uindex',
    'unique'  => TRUE,
    'type'    => 'btree',
    'table'   => 'atelier_seance',
    'schema'  => 'public',
    'columns' => [
        'id',
    ],
];

//@formatter:on

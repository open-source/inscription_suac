<?php

//@formatter:off

return [
    'name'    => 'formation_groupe_id_uindex',
    'unique'  => TRUE,
    'type'    => 'btree',
    'table'   => 'atelier_groupe',
    'schema'  => 'public',
    'columns' => [
        'id',
    ],
];

//@formatter:on

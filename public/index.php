<?php
define('REQUEST_MICROTIME', microtime(true));

/**
 * This makes our life easier when dealing with paths. Everything is relative
 * to the application root now.
 */
chdir(dirname(__DIR__));

//affichage d'erreur en prod si besoin
//error_reporting (E_ALL);
//ini_set('display_errors', true);
//ini_set('display_startup_errors',true);

// Setup autoloading
require 'init_autoloader.php';

// Run the application!
Laminas\Mvc\Application::init(require 'config/application.config.php')->run();
//echo phpinfo();

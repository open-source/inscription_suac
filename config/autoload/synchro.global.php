<?php

return [
    'data_sources' => [
        'orm_default',
        'orm_octopus',
    ],
    'synchros' => [
        'ETUDIANT' => [
            'order' => 1,
            'source' => 'OCTOPUS',
            'orm_source' => 'orm_octopus',
            'table_source' => 'v_insc_suac_etudiant',
            'orm_destination' => 'orm_default',
            'table_destination' => 'etudiant_etudiant',

            'correspondance' => [
                'ID' => 'id',
                'PRENOM' => 'prenom',
                'NOM' => 'nom',
                'NUMERO_ETUDIANT' => 'numero_etudiant',
                'EMAIL' => 'email',
                'LOGIN' => 'login',
            ],
            'id' => 'ID',
//            'separator'         => 'nom',
        ],
        'INSCRIPTION' => [
            'order' => 2,
            'source' => 'OCTOPUS',
            'orm_source' => 'orm_octopus',
            'orm_destination' => 'orm_default',
            'table_source' => 'v_insc_suac_ia',
            'table_destination' => 'etudiant_inscription_administrative',
            'correspondance' => [
                'ID' => 'id',
                'ETUDIANT_ID' => 'etudiant_id',
                'ANNEE_UNIV' => 'annee_univ',
                'FORMATION' => 'formation',
                'DIPLOME' => 'diplome',
                'CYCLE' => 'cycle',
                'COMPOSANTE' => 'composante',
                'ETABLISSEMENT' => 'etablissement',
            ],
            'id' => 'ID',
//            'separator'         => 'ANNEE_UNIV',
        ],
        'VILLE' => [
            'order' => 3,
            'source' => 'OCTOPUS',
            'orm_source' => 'orm_octopus',
            'orm_destination' => 'orm_default',
            'table_source' => 'v_insc_suac_ville',
            'table_destination' => 'site_ville',
            'correspondance' => [
                'ID' => 'id',
                'NOM' => 'nom',
            ],
            'id' => 'ID',
//            'separator'         => '',
        ],
        'SITE' => [
            'order' => 3,
            'source' => 'OCTOPUS',
            'orm_source' => 'orm_octopus',
            'orm_destination' => 'orm_default',
            'table_source' => 'v_insc_suac_site',
            'table_destination' => 'site_site',
            'correspondance' => [
                'ID' => 'id',
                'VILLE_ID' => 'ville_id',
                'LIBELLE' => 'libelle',
            ],
            'id' => 'ID',
//            'separator'         => '',
        ],
    ],
];

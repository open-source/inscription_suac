<?php

use Atelier\Event\CloturerSession\CloturerSessionEvenement;
use Atelier\Event\ConvoquerSession\ConvoquerSessionEvenement;
use Atelier\Event\RetourSession\RetourSessionEvenement;
use Atelier\Provider\Evenement\SessionEvenements;
use UnicaenEvenement\Entity\Db\Type;
use UnicaenEvenement\Service\EvenementCollection\EvenementCollectionService;

return [
    'unicaen-evenement' => [
        'service' => [
            // Évènements de base
            Type::COLLECTION => EvenementCollectionService::class,

            SessionEvenements::CONVOQUER_EVENEMENT => ConvoquerSessionEvenement::class,
            SessionEvenements::RETOUR_EVENEMENT => RetourSessionEvenement::class,
            SessionEvenements::CLOTURER_EVENEMENT => CloturerSessionEvenement::class,
        ],

        'icone' => [
//            EntretienProfessionnelEvenementProvider::RAPPEL_ENTRETIEN_PROFESSIONNEL => 'icon rappel',
        ],
    ],
];
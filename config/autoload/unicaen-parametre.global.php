<?php

namespace UnicaenParametre;

use UnicaenParametre\Provider\Privilege\ParametrecategoriePrivileges;

return [

    'navigation' => [
        'default' => [
            'home' => [
                'pages' => [
                    'administration' => [
                        'pages' => [
                            'parametre' => [
                                'label' => 'Paramètres',
                                'route' => 'parametre/index',
//                            'resource' => PrivilegeController::getResourceId(CategorieController::class, 'index'),
                                'resource' => ParametrecategoriePrivileges::getResourceId(ParametrecategoriePrivileges::PARAMETRECATEGORIE_INDEX),
                                'order' => 7020,
                                'pages' => [],
                                'icon' => 'fas fa-angle-right',
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],
];
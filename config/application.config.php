<?php

$modules = [
    'Laminas\Cache',
    'Laminas\Filter',
    'Laminas\Form',
    'Laminas\Hydrator',
    'Laminas\I18n',
    'Laminas\InputFilter',
    'Laminas\Log',
    'Laminas\Mail',
    'Laminas\Mvc\I18n',
    'Laminas\Mvc\Plugin\FlashMessenger',
    'Laminas\Mvc\Plugin\Prg',
    'Laminas\Navigation',
    'Laminas\Paginator',
    'Laminas\Router',
    'Laminas\Session',
    'Laminas\Validator',
    'DoctrineModule',
    'DoctrineORMModule',
    'ZfcUser',
    'BjyAuthorize',

    'UnicaenApp',
    'UnicaenLdap',
    'UnicaenAuthentification',
    'UnicaenPrivilege',
    'UnicaenMail',
    'UnicaenUtilisateurLdapAdapter',
    'UnicaenUtilisateur',
    'UnicaenEvenement',
    'UnicaenRenderer',
    'UnicaenIndicateur',
    'UnicaenEtat',
    'UnicaenValidation',
    'UnicaenParametre',
    'Unicaen\Console',
    'UnicaenSynchro',
    'UnicaenParametre',
    'UnicaenEnquete',

    'Fichier',
    'Site',
    'Etudiant',
    'Atelier',
    'Application',

//    'UnicaenVerification',
];

$applicationEnv = getenv('APPLICATION_ENV') ?: 'production';
if ('development' === $applicationEnv) {
    $modules[] = 'Laminas\DeveloperTools';
//    $modules[] = 'UnicaenCode';
}

$moduleListenerOptions = [
    'config_glob_paths'    => [
        'config/autoload/{,*.}{local,global}.php',
    ],
    'module_paths' => [
        './module',
        './vendor',
    ],
];

return [
    'modules' => $modules,
    'module_listener_options' => $moduleListenerOptions,
];
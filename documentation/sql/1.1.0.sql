-- TABLE
create table atelier_session_evenement
(
    session_id            integer not null
        constraint atelier_session_evenement_atelier_session_id_fk
            references atelier_session
            on delete cascade,
    evenement_instance_id integer not null
        constraint atelier_session_evenement_unicaen_evenement_instance_id_fk
            references unicaen_evenement_instance
            on delete cascade,
    constraint atelier_session_evenement_pk
        primary key (session_id, evenement_instance_id)
);

-- PARAMETRE
insert into unicaen_parametre_categorie (code, libelle, description, ordre) values
    ('SESSION', 'Paramètres associés aux sessions', null, 30);
INSERT INTO unicaen_parametre_parametre(CATEGORIE_ID, CODE, LIBELLE, DESCRIPTION, VALEURS_POSSIBLES, VALEUR, ORDRE)
WITH d(CODE, LIBELLE, DESCRIPTION, VALEURS_POSSIBLES, VALEUR, ORDRE) AS (
    SELECT 'AUTO_CONVOCATION', '[Convocation automatique] Délai en jours avant la première séance', null, 'Number', 2, 80 UNION
    SELECT 'AUTO_RETOUR', '[Demande automatique des retours] Délai en jours après la dernière séance', null, 'Number', 2, 90 UNION
    SELECT 'AUTO_CLOTURE', '[Clôture automatique] Délai en jours après la dernière séance', null, 'Number', 15, 100
)
SELECT cp.id, d.CODE, d.LIBELLE, d.DESCRIPTION, d.VALEURS_POSSIBLES, d.VALEUR, d.ORDRE
FROM d
         JOIN unicaen_parametre_categorie cp ON cp.CODE = 'SESSION';

-- EVENT
INSERT INTO unicaen_evenement_type (code, libelle, description, parametres, recursion) VALUES
('convoquer-evenement', 'Convocation des inscrit·es d''une session d''atelier', 'Convocation automatique', null, null),
('cloturer-evenement', 'Cloturer de session d''atelier', 'Clôture automatique', null, null),
('retour-evenement', 'Demande automatique des retours de session d''atelier', 'Demande automatique des retours', null, null);


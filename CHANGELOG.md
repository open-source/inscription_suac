# CHANGELOG

## Version 1.1.0 - 14/02/2025

[NOUVEAUTÉS]
* Ajout des événements pour la gestion automatique des événements

[AMELIORATIONS]
* Ajout de bouton pour masquer les ateliers sans sessions actives sur la liste des ateliers actifs
* Ajout d'informations et de filtres sur l'écran de gestion des ateliers

[CORRECTIONS]
* Correction de la liste des états pointés par l'action "Changer l'état" d'une section
* Correction bloquant correctement l'inscription multiple de certains étudiant·es



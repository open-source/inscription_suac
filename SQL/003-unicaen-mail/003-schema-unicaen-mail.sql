create table if not exists unicaen_mail_mail
(
id serial not null constraint umail_pkey primary key,
date_envoi timestamp not null,
status_envoi varchar(256) not null,
destinataires text not null,
destinataires_initials text,
copies text,
sujet text,
corps text,
mots_clefs text,
log text
);

create unique index if not exists ummail_id_uindex on unicaen_mail_mail (id);
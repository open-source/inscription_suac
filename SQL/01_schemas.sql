-- vue étudiant (postgres)
create view public.v_suac_etudiant(id, prenom, nom, numero_etudiant, email, login) as
SELECT DISTINCT i.c_individu_chaine                  AS id,
                i.prenom,
                COALESCE(i.nom_usage, i.nom_famille) AS nom,
                i.c_etu                              AS numero_etudiant,
                ic.email,
                ic.login
FROM individu i
         JOIN individu_insc_admin iia ON i.c_individu_chaine = iia.individu_id
         JOIN individu_compte ic ON i.c_individu_chaine = ic.individu_id;

alter table public.v_suac_etudiant
    owner to ad_dummy_octopus;

-- 49 743 - octopus

select *
from v_suac_etudiant;
select *
from individu;

-- modification de la vue v_suac_etudiant
-- octo
drop view v_insc_suac_etudiant;
create view public.v_insc_suac_etudiant(id, prenom, nom, numero_etudiant, email, login) as
SELECT DISTINCT i.c_individu_chaine                  AS id,
                i.prenom,
                COALESCE(i.nom_usage, i.nom_famille) AS nom,
                i.c_etu                              AS numero_etudiant,
                ic.email,
                ic.login
FROM individu i
         JOIN individu_insc_admin iia ON i.c_individu_chaine = iia.individu_id
         JOIN individu_compte ic ON i.c_individu_chaine = ic.individu_id
         JOIN individu_compte_statut ics ON ic.statut_id = ics.id
WHERE ics.code = 'ACT'
;

-- 46 622

select *
from individu_insc_admin iia;

-- v_objectifreussite_ia
SELECT iia.id,
       iia.individu_id,
       iia.etape_id,
       iia.diplome_id,
       iia.annee_univ,
       iia.etat_inscription,
       iia.regime_sise,
       iia.t_obtenu_dip,
       iia.t_obtenu_vet,
       iia.t_a_distance,
       iia.d_premiere_inscription,
       iia.d_debut,
       iia.d_fin_validite,
       iia.statut_etudiant,
       iia.lib_regime_sise,
       iia.t_paiement,
       iia.t_pj_manquantes,
       iia.t_principale,
       iia.programme_echange
FROM individu_insc_admin iia
         JOIN v_objectifreussite_etudiant voe ON iia.individu_id = voe.octopus_id
-- WHERE (iia.individu_id IN (SELECT v_objectifreussite_etudiant.octopus_id
--                            FROM v_objectifreussite_etudiant))
;
-- 152 147 / 946 581


select *
from v_objectifreussite_etudiant;
-- 46 578 (activé avec group by et max)
-- 152 253 ( activé sans group by et max)
-- 46 579 (activé sans group by et max, distinct)
-- 49 699 (activé sans group by et max, distinct, where ics.code = 'ACT'


-- ad-dummy_octopus
-- vue inscription (postgres)
drop view v_suac_ia;
create or replace view public.v_insc_suac_ia
            (id, etudiant_id, annee_univ,
             etape_id, formation_lib_court, formation,
             diplome_id, diplome_lib_court, diplome, cycle, composante,
             etablissement_libelle_court, etablissement
                )
as

SELECT
       -- ROWNUM                                            AS ID,
       ROW_NUMBER() over ()                               AS ID,  -- postgres
       iia.individu_id                                    AS etudiant_id,
       iia.annee_univ,
       E.ETAPE_ID                                         AS etape_id,
       E.LIB_COURT                                        AS formation_lib_court,
       E.LIB_LONG                                         AS formation,
       D.DIPLOME_ID                                       AS diplome_id,
       D.LIB_COURT                                        AS diplome_lib_court,
       D.LIB_LONG                                         AS diplome,
       -- D.LIB_TYPE_DIPLOME_ETB AS DIPLOME_TYPE,
       SUBSTR(D.LIB_LONG, 1, POSITION(' ' IN D.LIB_LONG)) AS cycle, -- ad_dummy_octopus
       S.LIBELLE_LONG                                     AS composante,
       ETAB.LIBELLE_COURT                                 AS etablissement_libelle_court,
       ETAB.LIBELLE_LONG                                  AS etablissement

from individu_insc_admin iia
         join individu i on iia.INDIVIDU_ID = i.C_INDIVIDU_CHAINE
         join individu_compte ic ON iia.individu_id = ic.individu_id
         join individu_compte_statut ics ON ic.statut_id = ics.id and ics.code = 'ACT'
         join ODF_ETAPE_DIPLOME OED on iia.etape_id = OED.etape_id and iia.diplome_id = OED.diplome_id
         join ODF_ETAPE E on iia.etape_id = E.etape_id
         join ODF_DIPLOME D on iia.diplome_id = D.diplome_id
         join STRUCTURE S on E.STRUCTURE_ID = S.ID
         join STRUCTURE ETAB on ETAB.CODE = 'UNIV'
;
-- 152 000 , 38 824 (>= 2023)

select *
from v_suac_ia;

drop table public.etudiant_inscription_administrative;
create table public.etudiant_inscription_administrative
(
    id            integer                 not null
        constraint etudiant_ia_pk
            primary key,
    annee_univ    varchar(4),
    formation     varchar(1024),
    diplome       varchar(1024),
    cycle         varchar(256),
    composante    varchar(256),
    etablissement varchar(1024),
    etudiant_id   integer                 not null
        constraint etudiant_ia_etudiant_id_fk
            references public.etudiant_etudiant,
    created_on    timestamp default now() not null,
    updated_on    timestamp,
    deleted_on    timestamp,
    source_id     varchar(128)
);


-- vue v_etudiant_ia_annee
drop view public.v_etudiant_ia_annee_univ;
create view public.v_etudiant_ia_annee_univ(id, annee_univ) as
with tmp as
         (select distinct annee_univ
          from etudiant_inscription_administrative
          order by annee_univ)
select ROW_NUMBER() over () AS ID, annee_univ
from tmp
;

select *
from v_etudiant_ia_annee_univ;

-- vue v_etudiant_ia_etablissement
drop view v_etudiant_ia_etablissement;
create view public.v_etudiant_ia_etablissement(id, etablissement) as
with tmp as
         (select distinct etablissement
          from etudiant_inscription_administrative
          order by etablissement)
select ROW_NUMBER() over () AS ID, etablissement
from tmp
;

select *
from v_etudiant_ia_etablissement;


-- vue v_etudiant_ia_composante
drop view v_etudiant_ia_composante;
create view public.v_etudiant_ia_composante(id, composante) as
with tmp as
             (select distinct composante from etudiant_inscription_administrative)
select ROW_NUMBER() over () AS ID, composante
from tmp
;

select *
from v_etudiant_ia_composante;

-- vue v_etudiant_ia_cycle
drop view v_etudiant_ia_cycle;
create view public.v_etudiant_ia_cycle(id, cycle) as
with tmp as
             (select distinct cycle from etudiant_inscription_administrative)
select ROW_NUMBER() over () AS ID, cycle
from tmp
;

select *
from v_etudiant_ia_cycle;


select e.nom, e.prenom, ia.*
from etudiant_etudiant e
         join etudiant_inscription_administrative ia on e.id = ia.etudiant_id
where ia.annee_univ = '2023';

/*
 villes, sites
 */

-- sites
-- octopus
create view v_suac_site(id, ville_id, libelle) as
select im.id, isa.ville_code as ville_id, im.LIBELLE_COMMUNICATION as libelle
from immobilier_site im
         left join OCTO.IMMOBILIER_SITE_ADRESSE isa on im.id = isa.SITE_ID
where im.HISTO_DESTRUCTION is null
order by im.LIBELLE_COMMUNICATION
;

/*
9,61143,Damigny,Alençon-Damigny
10,14118,Caen,Caen Campus 1
1,14118,Caen,Caen Campus 2
6,14341,Ifs,Caen Campus 3
5,14118,Caen,Caen Campus 4
3,14118,Caen,Caen Campus 5
7,14118,Caen,Caen Horowitz
8,14118,Caen,Caen INSPÉ
13,50129,Cherbourg-en-Cotentin,Cherbourg-en-Cotentin
11,,,Hors Unicaen
2,14366,Lisieux,Lisieux
15,14384,Luc-sur-Mer,Luc-sur-Mer
14,50502,Saint-Lô,Saint-Lô
16,50502,Saint-Lô,Saint-Lô
4,14762,Vire Normandie,Vire
 */

-- ad_dummy_octopus
drop view v_insc_suac_site;
create view v_insc_suac_site(id, ville_id, libelle) as
select id, ville_id, libelle
from (values (9, 61143, 'Damigny', 'Alençon-Damigny'),
             (10, 14118, 'Caen', 'Caen Campus 1'),
             (1, 14118, 'Caen', 'Caen Campus 2'),
             (6, 14341, 'Ifs', 'Caen Campus 3'),
             (5, 14118, 'Caen', 'Caen Campus 4'),
             (3, 14118, 'Caen', 'Caen Campus 5'),
             (7, 14118, 'Caen', 'Caen Horowitz'),
             (8, 14118, 'Caen', 'Caen INSPÉ'),
             (13, 50129, 'Cherbourg-en-Cotentin', 'Cherbourg-en-Cotentin'),
             (11, null, null, 'Hors Unicaen'),
             (2, 14366, 'Lisieux', 'Lisieux'),
             (15, 14384, 'Luc-sur-Mer', 'Luc-sur-Mer'),
             (14, 50502, 'Saint-Lô', 'Saint-Lô'),
             (16, 50502, 'Saint-Lô', 'Saint-Lô'),
             (4, 14762, 'Vire Normandie', 'Vire Normandie'))
         as t(id, ville_id, ville_nom, libelle);

-- villes ocotpus
create or replace view v_suac_ville(id, nom) as
select distinct isa.ville_code as id, isa.ville_nom as nom
from OCTO.IMMOBILIER_SITE_ADRESSE isa
where isa.ville_code is not null
;

-- ad_dummy_octopus
drop view v_insc_suac_ville;
create or replace view v_insc_suac_ville(id, nom) as
select *
from (values (61143, 'Damigny'),
             (14118, 'Caen'),
             (14341, 'Ifs'),
             (50129, 'Cherbourg-en-Cotentin'),
             (14366, 'Lisieux'),
             (14384, 'Luc-sur-Mer'),
             (50502, 'Saint-Lô'),
             (14762, 'Vire Normandie'))
         as t(ville_code, ville_nom);

drop table public.site_site;
create table public.site_site
(
    id       varchar(256) not null
        constraint dite_ville_pk
            primary key,
    ville_id varchar(256)
        constraint site_site_site_ville_id_fk
            references public.site_ville,
    libelle  varchar(256),
    created_on    timestamp default now() not null,
    updated_on    timestamp,
    deleted_on    timestamp,
    source_id     varchar(128)
);

drop table public.site_ville;
create table public.site_ville
(
    id  varchar(256) not null
        constraint site_site_pk
            primary key,
    nom varchar(256),
    created_on    timestamp default now() not null,
    updated_on    timestamp,
    deleted_on    timestamp,
    source_id     varchar(128)
);

/* ajout d'un site à une session */
alter table public.atelier_session
    add site_id varchar(256);

alter table public.atelier_session
    add constraint atelier_session_site_site_id_fk
        foreign key (site_id) references public.site_site;


/* site et ville d'un programme */

select prg.id, prg.libelle, at.libelle, ats.lieu, ss.libelle, sv.nom
from atelier_programme prg
         join atelier_atelier_programme aap on prg.id = aap.programme_id
         join atelier_atelier at on aap.atelier_id = at.id
         join atelier_session ats on at.id = ats.atelier_id
         left join  site_site ss on ats.site_id = ss.id
         left join site_ville sv on ss.ville_id = sv.id
order by prg.id;
;

/* etudiant import etablissement */
/*
 "école supérieure d'arts & médias ",ESAM
 */
drop table public.etudiant_etablissement;
create table public.etudiant_etablissement
(
    id      integer default nextval('etudiant_etablissement_id_seq'::regclass) not null
        constraint etudiant_etablissement_pk
            primary key,
    libelle varchar(256)                                                       not null,
    sigle   varchar(20)                                                        not null,

    histo_createur_id     integer   not null
        constraint formation_groupe_createur_fk
            references public.unicaen_utilisateur_user,
    histo_creation        timestamp not null,
    histo_modificateur_id integer
        constraint formation_groupe_modificateur_fk
            references public.unicaen_utilisateur_user,
    histo_modification    timestamp,
    histo_destructeur_id  integer
        constraint formation_groupe_destructeur_fk
            references public.unicaen_utilisateur_user,
    histo_destruction     timestamp
);

alter table public.etudiant_etablissement
    owner to ad_suac_pp;


/* Vérification import établissement */

select * from etudiant_inscription_administrative where source_id = 'ESAM'
                                                  and etudiant_id = 'ESAM_minapinpin479@gmail.com'
                                                  --and annee_univ = '2024'
                                                  order by etudiant_id , annee_univ;

select * from etudiant_etudiant
         where
             source_id = 'ESAM'
             -- id = 'ESAM_minapinpin479@gmail.com' ;

select * from unicaen_utilisateur_user where email = 'minapinpin479@gmail.com' ;

alter table public.unicaen_mail_mail add attachment_paths text ;
INSERT INTO UNICAEN_PRIVILEGE_CATEGORIE (
    CODE,
    LIBELLE,
    NAMESPACE,
    ORDRE)
values
    ('utilisateur', 'Gestion des utilisateurs', 'UnicaenUtilisateur\Provider\Privilege', 1),
    ('role', 'Gestion des rôles', 'UnicaenUtilisateur\Provider\Privilege', 1)
    ON CONFLICT (CODE) DO
    UPDATE SET
        LIBELLE=excluded.LIBELLE,
        NAMESPACE=excluded.NAMESPACE,
        ORDRE=excluded.ORDRE;

WITH d(code, lib, ordre) AS (
    SELECT 'utilisateur_afficher', 'Consulter un utilisateur', 1 UNION
    SELECT 'utilisateur_ajouter', 'Ajouter un utilisateur', 2 UNION
    SELECT 'utilisateur_changerstatus', 'Changer le statut d''un utilisateur', 3 UNION
    SELECT 'utilisateur_modifierrole', 'Modifier les rôles attribués à un utilisateur', 4
    SELECT 'utilisateur_rechercher', 'Recherche d''un utilisateur', 10
)
INSERT INTO unicaen_privilege_privilege(CATEGORIE_ID, CODE, LIBELLE, ORDRE)
SELECT cp.id, d.code, d.lib, d.ordre
FROM d
         JOIN unicaen_privilege_categorie cp ON cp.CODE = 'utilisateur'
    ON CONFLICT (CATEGORIE_ID, CODE) DO
UPDATE SET
    LIBELLE=excluded.LIBELLE,
    ORDRE=excluded.ORDRE;

WITH d(code, lib, ordre) AS (
    SELECT 'role_afficher', 'Consulter les rôles', 1 UNION
    SELECT 'role_modifier', 'Modifier un rôle', 2 UNION
    SELECT 'role_effacer', 'Supprimer un rôle', 3
)
INSERT INTO unicaen_privilege_privilege(CATEGORIE_ID, CODE, LIBELLE, ORDRE)
SELECT cp.id, d.code, d.lib, d.ordre
FROM d
         JOIN unicaen_privilege_categorie cp ON cp.CODE = 'role'
    ON CONFLICT (CATEGORIE_ID, CODE) DO
UPDATE SET
    LIBELLE=excluded.LIBELLE,
    ORDRE=excluded.ORDRE;

INSERT INTO UNICAEN_UTILISATEUR_USER (
    ID,
    USERNAME,
    EMAIL,
    DISPLAY_NAME,
    PASSWORD)
    values (0, 'app', 'dsi.applications@unicaen.fr', 'Application', 'application')
    on conflict (USERNAME) DO
        UPDATE SET
        EMAIL=excluded.EMAIL,
        DISPLAY_NAME=excluded.DISPLAY_NAME,
        PASSWORD=excluded.PASSWORD;

INSERT INTO unicaen_utilisateur_role
    (role_id,
    libelle,
    description,
    is_default,
    is_auto,
    parent_id,
    ldap_filter,
    accessible_exterieur)
    values
        ('Admin_tech ', 'Administrateur·trice Technique', 'Administrateur Technique', false, true, null, null, true),
        ('Admin_fonc ', 'Administrateur·trice Fonctionnel·le', 'Administrateur·trice Fonctionnel·le', false, true, null, null, true),
        ('Observateur ', 'Observateur·trice', 'Observateur·trice', false, true, null, null, true)
    ON CONFLICT (role_id) DO
        UPDATE SET
        libelle=excluded.libelle,
        description=excluded.description,
        is_default=excluded.is_default,
        is_auto=excluded.is_auto,
        parent_id=excluded.parent_id,
        ldap_filter=excluded.ldap_filter,
        accessible_exterieur=excluded.accessible_exterieur;

-- INSERT INTO UNICAEN_UTILISATEUR_USER (username, email, display_name, password, state) VALUES
-- -- utilisateur demo/azerty
-- ('demo', 'demo@mail.fr', 'Demo Crite', '$2y$10$PxXnVLYnGEzEnfqPqRKJSe9AabocES2H4bBK5VzzJlzuj1rVt7Lwu', true);


-- INSERT INTO unicaen_utilisateur_role_linker
--     (role_id, user_id)
--     SELECT role.id, u.id FROM unicaen_utilisateur_role role,
--     unicaen_utilisateur_user u
--     WHERE role.role_id = 'Admin_tech'
--     AND u.username = 'demo'
--     ON CONFLICT DO NOTHING ;
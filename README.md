# Squelette d'application Unicaen

Ce squelette d'application constitue un socle minimal pour entamer rapidement le développement d'une nouvelle 
application à la mode "Unicaen".

## VueJs en Dev
1. Installer [NodeJs](https://nodejs.org/) >= 20.x
2. ```bash
   npm run dev
    ```
3. Pour que vos modifications soient prises en compte en prod :
   ```bash
   npm run build
   ```
## Création d'une nouvelle appli à partir du squelette

Imaginons que nous voulons créer une nouvelle appli PHP 8.0 dans le répertoire `./newapp`.

### Préalable

Nous aurons besoin de l'image Docker `unicaen-dev-php${PHP_VERSION}-apache` pour être dans un
environnement PHP dont on maîtrise la version. Pour obtenir puis construire cette image, reportez-vous au dépôt 
[unicaen-image](https://git.unicaen.fr/open-source/docker/unicaen-image).

### Création des sources de l'application

- Pour obtenir la branche 6.x des sources du squelette d'application :

```bash
APP_DIR=newapp

git clone git@git.unicaen.fr:lib/unicaen/skeleton-application.git ${APP_DIR}
cd ${APP_DIR}
git checkout 6.x
```

### Création du dépôt git

```bash
git init
git add .
git commit -m "Squelette de l'application"
git push --set-upstream git@git.unicaen.fr:dsi/${APP_DIR}.git master
```

*NB : la création du dépôt git de cette façon n'est possible que si vous disposez des droits suffisants dans le 
namespace gitlab spécifié.*


## Construction et lancement du container Docker

Il faut penser à modifier les fichiers composer.json et docker-compose.yml pour mettre les informations relatives à l'application (le nom de l'application, le nom du container attendu etc...)
unicaen-dev-php8.0-apache est nécéssaire pour cette étape en 6.x
```bash
docker-compose up --build
```

- Il faut ensuite accèder au container et récupérer les bibliothèques  :

```bash
docker exec -it nomDuContainer /bin/bash
composer install
chown -R ${USER}:${USER} .
composer run-script "post-create-project-cmd"
```

- Comme les commandes lancées via Docker (comme ici, `composer`) sont exécutées en `root`, il faut reprendre la main
sur les fichiers créés.

```bash
chown -R ${USER}:${USER} .
```

- Lancer le script qui va déplacer les .dist dans le dossier autoload et les script sql dans un dossier nommé SQL.
Les scripts SQL ne seront pas exécuter automatiquement.

```bash
composer run-script "post-create-project-cmd"
```

## Configuration du projet

- Si cela n'a pas déjà été fait par une des `post-create-project-cmd` du `composer.json`, 
créez le fichier de config `config/autoload/local.php` à partir du `.dist` :
```bash
cp -r config/autoload/local.php.dist config/autoload/local.php
```

- Si cela n'a pas déjà été fait par une des `post-create-project-cmd` du `composer.json`, 
créez dans `config/autoload` les fichiers de configuration locaux et globaux des bibliothèques utilisées 
à partir des `.dist` :
```bash
cp -R ./vendor/unicaen/app/config/unicaen-app.global.php.dist ./config/autoload/unicaen-app.global.php;
cp -R ./vendor/unicaen/app/config/unicaen-app.local.php.dist ./config/autoload/unicaen-app.local.php;
cp -R ./vendor/unicaen/authentification/config/unicaen-authentification.global.php.dist ./config/autoload/unicaen-authentification.global.php;
cp -R ./vendor/unicaen/authentification/config/unicaen-authentification.local.php.dist ./config/autoload/unicaen-authentification.local.php;
cp -R ./vendor/unicaen/ldap/config/unicaen-ldap.local.php.dist ./config/autoload/unicaen-ldap.local.php;
cp -R ./vendor/unicaen/mail/config/unicaen-mail.global.php.dist ./config/autoload/unicaen-mail.global.php;
cp -R ./vendor/unicaen/mail/config/unicaen-mail.local.php.dist ./config/autoload/unicaen-mail.local.php;
cp -R ./vendor/unicaen/privilege/config/unicaen-privilege.global.php.dist ./config/autoload/unicaen-privilege.global.php;
cp -R ./vendor/unicaen/utilisateur/config/unicaen-utilisateur.global.php.dist ./config/autoload/unicaen-utilisateur.global.php;
cp -R ./config/autoload/local.php.dist ./config/autoload/local.php;
```

- Le cas échéant, reportez-vous aux docs des modules concernés pour adapter ces fichiers de configuration 
à vos besoins :
  - [unicaen/app](https://git.unicaen.fr/lib/unicaen/app)
  - [unicaen/auth](https://git.unicaen.fr/lib/unicaen/auth)
  - [unicaen/privilege](https://git.unicaen.fr/lib/unicaen/privilege)
  - [unicaen/mail](https://git.unicaen.fr/lib/unicaen/mail)
  - [unicaen/ldap](https://git.unicaen.fr/lib/unicaen/ldap)

  
- Si cela n'a pas déjà été fait par une des `post-create-project-cmd` du `composer.json`, 
  récupérer les différentes requêtes sql pour instancier les premières données pour les bibliothèques :
```
mkdir -p SQL 
mkdir -p ./SQL/001-unicaen-utilisateur
cp -R ./vendor/unicaen/utilisateur/data/sql/schema_postgresql.sql ./SQL/001-unicaen-utilisateur/001-schema-unicaen-utilisateur.sql
cp -R ./vendor/unicaen/utilisateur/data/sql/data.sql ./SQL/001-unicaen-utilisateur/001-data-unicaen-utilisateur.sql
cp -R ./vendor/unicaen/privilege/data/sql/schema_postgresql.sql ./SQL/002-unicaen-privilege/002-schema-unicaen-privilege.sql	
cp -R ./vendor/unicaen/privilege/data/sql/data.sql ./SQL/002-unicaen-privilege/002-data-unicaen-privilege.sql	
cp -R ./vendor/unicaen/mail/SQL/001_tables.sql ./SQL/003-unicaen-mail/003-schema-unicaen-mail.sql
cp -R ./vendor/unicaen/mail/SQL/002_privileges.sql ./SQL/003-unicaen-mail/003-data-unicaen-mail.sql
```

- Exécuter les requêtes dans l'ordre suivant dans la base de données du container :
```
./SQL/001-unicaen-utilisateur/001-schema-unicaen-utilisateur.sql
./SQL/002-unicaen-privilege/002-schema-unicaen-privilege.sql
./SQL/003-unicaen-mail/003-schema-unicaen-mail.sql;
./SQL/001-unicaen-utilisateur/001-data-unicaen-utilisateur.sql;
./SQL/001-unicaen-utilisateur/002-data-unicaen-privilege.sql;
./SQL/003-unicaen-mail/003-data-unicaen-mail.sql;	
```

## Test de l'application 

Théoriquement, l'application devrait être accessible à l'adresse [https://localhost:8443](https://localhost:8443).
Le port utilisé dépend des redirections configurées dans le fichier [docker-compose.yml](docker-compose.yml).

## Module Demo

Le squelette d'application possède un module "démo" qui utilise une base de données PostgreSQL de démonstration
permettant d'avoir une authentification locale qui fonctionne.

Cette base de données est fournie par le service `db` (fichier de config `docker-compose.yml`).
Il y a également un service `adminer` fournissant de quoi explorer la base de données avec l'outil
["Adminer"](https://www.adminer.org) en vous rendant à l'adresse `http://localhost:9080` (sélectionner "PostgeSQL"
et utiliser les informations de connexion à la bdd présentes dans le `docker-compose.yml`).
Pour explorer/administrer la base de données *de l'extérieur du container* (avec PHPStorm par exemple),
l'adresse de la base est cette fois `localhost:8432`.

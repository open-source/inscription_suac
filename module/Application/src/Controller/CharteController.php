<?php

namespace Application\Controller;

use Application\Provider\Parametre\CharteParametres;
use Application\Provider\Validation\CharteValidations;
use Etudiant\Service\Etudiant\EtudiantServiceAwareTrait;
use Laminas\Http\Response;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\View\Model\ViewModel;
use UnicaenParametre\Service\Parametre\ParametreServiceAwareTrait;
use UnicaenRenderer\Service\Rendu\RenduServiceAwareTrait;
use UnicaenUtilisateur\Service\User\UserServiceAwareTrait;
use UnicaenValidation\Service\ValidationInstance\ValidationInstanceServiceAwareTrait;

class CharteController extends AbstractActionController {
    use EtudiantServiceAwareTrait;
    use ParametreServiceAwareTrait;
    use RenduServiceAwareTrait;
    use UserServiceAwareTrait;
    use ValidationInstanceServiceAwareTrait;

    public function afficherAction(): ViewModel
    {
        $user = $this->getUserService()->getConnectedUser();
        $etudiant = $this->getEtudiantService()->getEtudiantByLogin($user->getUsername());

        $templateCode = $this->getParametreService()->getValeurForParametre(CharteParametres::TYPE, CharteParametres::CHARTE_TEMPLATE);
        $var = ['etudiant' => $etudiant];
        $rendu = $this->getRenduService()->generateRenduByTemplateCode($templateCode, $var, false);

        $instance = $etudiant?->getValidationActiveByTypeCode(CharteValidations::CHARTE_SIGNEE);

        return new ViewModel([
            'title' => $rendu->getSujet(),
            'rendu' => $rendu,
            'instance' => $instance,
            'etudiant' => $etudiant,
        ]);
    }

    public function validerAction(): Response
    {
        $user = $this->getUserService()->getConnectedUser();
        $etudiant = $this->getEtudiantService()->getEtudiantByLogin($user->getUsername());

        $instance = $this->getValidationInstanceService()->createWithCode(CharteValidations::CHARTE_SIGNEE);
        $etudiant->addValidation($instance);
        $this->getEtudiantService()->update($etudiant);

        return $this->redirect()->toRoute('home', [], [], true);
    }
}
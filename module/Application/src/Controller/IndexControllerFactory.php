<?php

namespace Application\Controller;

use Etudiant\Service\Etudiant\EtudiantService;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;
use UnicaenParametre\Service\Parametre\ParametreService;
use UnicaenRenderer\Service\Rendu\RenduService;
use UnicaenUtilisateur\Service\User\UserService;

class IndexControllerFactory
{
    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): IndexController
    {
        /**
         * @var EtudiantService $etudiantService
         * @var ParametreService $parametreService
         * @var RenduService $renduService
         * @var RenduService $renduService
         * @var UserService $userService
         **/
        $etudiantService = $container->get(EtudiantService::class);
        $parametreService = $container->get(ParametreService::class);
        $renduService = $container->get(RenduService::class);
        $userService = $container->get(UserService::class);

        $controller = new IndexController();
        $controller->setEtudiantService($etudiantService);
        $controller->setParametreService($parametreService);
        $controller->setRenduService($renduService);
        $controller->setUserService($userService);
        return $controller;
    }
}
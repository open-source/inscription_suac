<?php

namespace Application\Controller;


use Application\Provider\Parametre\CharteParametres;
use Application\Provider\Template\TexteTemplate;
use Application\Provider\Validation\CharteValidations;
use Atelier\Controller\IntervenantController;
use Atelier\Provider\Role\RolesProvider;
use Etudiant\Provider\Role\RolesProvider as EtudiantRoleProvider;
use Etudiant\Service\Etudiant\EtudiantServiceAwareTrait;
use Laminas\Http\Response;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\View\Model\ViewModel;
use UnicaenParametre\Service\Parametre\ParametreServiceAwareTrait;
use UnicaenRenderer\Service\Rendu\RenduServiceAwareTrait;
use UnicaenUtilisateur\Service\User\UserServiceAwareTrait;

class IndexController extends AbstractActionController
{
    use EtudiantServiceAwareTrait;
    use ParametreServiceAwareTrait;
    use RenduServiceAwareTrait;
    use UserServiceAwareTrait;

    public function indexAction(): ViewModel|Response
    {
        $user = $this->getUserService()->getConnectedUser();
        $role = $this->getUserService()->getConnectedRole();

        if ($role !== null) {
            switch ($role->getRoleId()) {
                case RolesProvider::ROLE_INTERVENANT:
                    /** @see IntervenantController::mesSessionsAction() */
                    return $this->redirect()->toRoute('atelier/intervenant/mes-sessions', [], [], true);
                case EtudiantRoleProvider::ROLE_ETUDIANT:
                    $charteActive = $this->getParametreService()->getValeurForParametre(CharteParametres::TYPE, CharteParametres::CHARTE_ACTIVE);
                    if ($charteActive) {
                        $etudiant = $this->getEtudiantService()->getEtudiantByLogin($user->getUsername());
                        if ($etudiant !== null && $etudiant->getValidationActiveByTypeCode(CharteValidations::CHARTE_SIGNEE) === null) {
                            /** @see CharteController::afficherAction() */
                            return $this->redirect()->toRoute('charte/afficher', [], [], true);
                        }
                    }
            }
        }

        $rendu = $this->getRenduService()->generateRenduByTemplateCode(TexteTemplate::ACCUEIL, [], false);
        $texte = $rendu->getCorps();
        return new ViewModel([
            'texte' => $texte,
        ]);
    }

    public function indexRessourcesAction(): ViewModel
    {
        return new ViewModel();
    }

    public function indexGestionAction(): ViewModel
    {
        return new ViewModel();
    }

    public function indexAdministrationAction(): ViewModel
    {
        return new ViewModel();
    }
}



<?php

namespace Application\Command;

use Laminas\ServiceManager\ServiceManager;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;
use Symfony\Component\Console\Command\Command;
use Unicaen\BddAdmin\Bdd;

class CopyBddCommandFactory extends Command
{
    /**
     * @param ContainerInterface $container
     *
     * @return CopyBddCommand
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): CopyBddCommand
    {
        $bdd = $container->get(Bdd::class);
        $command = new CopyBddCommand();


        if (isset($container->get('Config')['doctrine']['connection']['orm_source'])) {
            $configsBdd = $container->get('Config')['doctrine']['connection']['orm_source'];

            $configsDdl = $container->get('Config')['bdd-admin'];

            $bddSource = new Bdd($configsBdd['params']);
            $bddSource->setOptions($configsDdl);
            $command->setBddSource($bddSource);
        }

        $command->setBdd($bdd);


        return $command;
    }
}
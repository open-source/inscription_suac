<?php

namespace Application\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Unicaen\BddAdmin\Bdd;

class CopyBddCommand extends Command
{
    protected static $defaultName = 'copy-bdd';

    public ?Bdd $bdd       = null;
    public ?Bdd $bddSource = null;



    public function setBdd(?Bdd $bdd): void
    {
        $this->bdd = $bdd;
    }



    public function setBddSource(?Bdd $bdd): void
    {

        $this->bddSource = $bdd;
    }



    protected function configure(): void
    {
        $this
//            ->addArgument('source', InputArgument::REQUIRED, 'Base de données source')
//            ->addArgument('desination', InputArgument::REQUIRED, 'Base de données de destination')
            ->setDescription("Copie d'une base de donnée A vers une base de donnée B");
    }



    protected function execute(InputInterface $input, OutputInterface $output): int
    {

        $io = new SymfonyStyle($input, $output);

        $io->title("Copie de la bdd");
        $io->text("Copie de la Bdd en cours");

        /**
         * @var $destination   Bdd
         * @var $source        Bdd
         */
        $destination = $this->bdd;
        $source      = $this->bddSource;

        /**
         *
         * @var $bdd Bdd
         */
        if ($destination != null && $source != null) {
            try {
                $destination->copy($source);
            } catch (\Exception $e) {
                $io->error($e->getMessage());

                return self::FAILURE;
            }

            $io->success("Copie terminée");
        }

        return self::SUCCESS;
    }
}
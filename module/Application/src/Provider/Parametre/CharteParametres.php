<?php

namespace Application\Provider\Parametre;

class CharteParametres {
    const TYPE = "CHARTE";

    const CHARTE_TEMPLATE = "CHARTE_TEMPLATE";
    const CHARTE_ACTIVE = "CHARTE_ACTIVE";
}
<?php

namespace Application\Provider\Parametre;

class GlobalParametres {
    const TYPE = "GLOBAL";
    const INSTALL_PATH = "INSTALL_PATH";
    const EMAIL_SERVICE = "EMAIL_SERVICE";
}
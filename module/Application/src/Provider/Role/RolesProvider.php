<?php

namespace Application\Provider\Role;

class RolesProvider
{
    const ROLE_ADMIN_TEC = "Administrateur·trice technique";
    const ROLE_ADMIN_FONC = "Administrateur·trice fonctionnel·le";
    const ROLE_OBSERVATEUR = "Observateur·trice";

}
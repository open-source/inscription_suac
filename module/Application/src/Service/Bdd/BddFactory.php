<?php

namespace Application\Service\Bdd;

use Interop\Container\ContainerInterface;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use Unicaen\BddAdmin\Bdd;

class BddFactory
{

    /**
     * @param ContainerInterface $container
     *
     * @return Bdd
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     *
     */
    public function __invoke(ContainerInterface $container, string $requestedName): Bdd
    {

        $configsBdd = $container->get('Config')['doctrine']['connection']['orm_default'];

        $configsDdl = $container->get('Config')['bdd-admin'];

        $bdd = new Bdd($configsBdd['params']);
        $bdd->setOptions([
            /* Facultatif, permet de spécifier une fois pour toutes le répertoire où sera renseignée la DDL de votre BDD */
            Bdd::OPTION_DDL_DIR                => $configsDdl[Bdd::OPTION_DDL_DIR],

            /* Facultatif, spécifie le répertoire où seront stockés vos scripts de migration si vous en avez */
            Bdd::OPTION_MIGRATION_DIR          => $configsDdl[Bdd::OPTION_MIGRATION_DIR],

            /* Facultatif, permet de personnaliser l'ordonnancement des colonnes dans les tables */
            Bdd::OPTION_COLUMNS_POSITIONS_FILE => $configsDdl[Bdd::OPTION_COLUMNS_POSITIONS_FILE],
        ]);

        return $bdd;
    }
}
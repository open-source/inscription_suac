<?php

namespace Formation;

use Application\Controller\CharteController;
use Application\Controller\CharteControllerFactory;
use Etudiant\Provider\Privilege\EtudiantPrivileges;
use Laminas\Router\Http\Literal;
use UnicaenPrivilege\Guard\PrivilegeController;

return [
    'bjyauthorize' => [
        'guards' => [
            PrivilegeController::class => [
                [
                    'controller' => CharteController::class,
                    'action' => [
                        'afficher',
                        'valider',
                    ],
                    'privileges' => [
                        EtudiantPrivileges::ETUDIANT_AFFICHER
                    ],
                ],
            ],
        ],
    ],

    'router' => [
        'routes' => [
            'charte' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/charte',
                ],
                'may_terminate' => false,
                'child_routes' => [
                    'afficher' => [
                        'type'  => Literal::class,
                        'options' => [
                            /** @see CharteController::afficherAction() */
                            'route'    => '/afficher',
                            'defaults' => [
                                'controller' => CharteController::class,
                                'action'     => 'afficher',
                            ],
                        ],
                    ],
                    'valider' => [
                        'type'  => Literal::class,
                        'options' => [
                            /** @see CharteController::validerAction() */
                            'route'    => '/valider',
                            'defaults' => [
                                'controller' => CharteController::class,
                                'action'     => 'valider',
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],

    'service_manager' => [
        'factories' => [
        ],
    ],
    'controllers' => [
        'factories' => [
            CharteController::class => CharteControllerFactory::class,

        ],
    ],
    'form_elements' => [
        'factories' => [],
    ],
    'hydrators' => [
        'factories' => [],
    ]

];
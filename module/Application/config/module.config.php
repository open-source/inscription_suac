<?php

namespace Application;

use Application\Controller\IndexController;
use Application\Controller\IndexControllerFactory;
use Application\Service\Bdd\BddFactory;
use Doctrine\ORM\Mapping\Driver\XmlDriver;
use Doctrine\Persistence\Mapping\Driver\MappingDriverChain;
use Etudiant\Provider\Privilege\EtudiantPrivileges;
use Laminas\Router\Http\Literal;
use Unicaen\BddAdmin\Bdd;
use UnicaenEtat\Provider\Privilege\EtatPrivileges;
use UnicaenIndicateur\Provider\Privilege\IndicateurPrivileges;
use UnicaenMail\Provider\Privilege\MailPrivileges;
use UnicaenPrivilege\Guard\PrivilegeController;
use UnicaenPrivilege\Provider\Privilege\PrivilegePrivileges;
use UnicaenRenderer\Provider\Privilege\DocumenttemplatePrivileges;
use UnicaenUtilisateur\Provider\Privilege\RolePrivileges;
use UnicaenUtilisateur\Provider\Privilege\UtilisateurPrivileges;
use UnicaenValidation\Provider\Privilege\ValidationtypePrivileges;

return [
    'bjyauthorize' => [
        'guards' => [
            PrivilegeController::class => [
                [
                    'controller' => IndexController::class,
                    'action' => [
                        'index',
                    ],
                    'roles' => 'guest'
                ],
                [
                    'controller' => IndexController::class,
                    'action' => [
                        'index-administration',
                    ],
                    'privileges' => [
                        UtilisateurPrivileges::UTILISATEUR_AFFICHER,
                        RolePrivileges::ROLE_AFFICHER,
                        PrivilegePrivileges::PRIVILEGE_VOIR,

                        DocumenttemplatePrivileges::DOCUMENTTEMPLATE_INDEX,
                        MailPrivileges::MAIL_INDEX,

                        IndicateurPrivileges::AFFICHER_INDICATEUR,
                        EtatPrivileges::ETAT_INDEX,
                        ValidationtypePrivileges::VALIDATIONTYPE_AFFICHER,
                    ],
                ],
                [
                    'controller' => IndexController::class,
                    'action' => [
                        'index-gestion',
                    ],
                    'privileges' => [
                        EtudiantPrivileges::ETUDIANT_INDEX,
                    ],
                ],
                [
                    'controller' => IndexController::class,
                    'action' => [
                        'index-ressource',
                    ],
                    'privileges' => [
                        EtudiantPrivileges::ETUDIANT_INDEX,
                    ],
                ],
            ],
        ],
    ],

    'router'          => [
        'routes' => [
            'home'        => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/',
                    'defaults' => [
                        /** @see IndexController::indexAction() */
                        'controller' => IndexController::class,
                        'action'     => 'index',
                    ],
                ],
            ],
            'administration'        => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => 'index-administration',
                    'defaults' => [
                        /** @see IndexController::indexAdministrationAction() */
                        'controller' => IndexController::class,
                        'action'     => 'index-administration',
                    ],
                ],
            ],
            'gestion'        => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => 'index-gestion',
                    'defaults' => [
                        /** @see IndexController::indexGestionAction() */
                        'controller' => IndexController::class,
                        'action'     => 'index-gestion',
                    ],
                ],
            ],
            'ressource'        => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => 'index-ressource',
                    'defaults' => [
                        /** @see IndexController::indexRessourcesAction() */
                        'controller' => IndexController::class,
                        'action'     => 'index-ressource',
                    ],
                ],
            ],
        ],
    ],

    'controllers'     => [
        'factories' => [
            IndexController::class => IndexControllerFactory::class,
        ],
    ],
    'service_manager' => [
        'factories' => [
            Bdd::class => BddFactory::class,
        ],
    ],
    'translator'      => [
        'locale'                    => 'fr_FR', // en_US
        'translation_file_patterns' => [
            [
                'type'     => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern'  => '%s.mo',
            ],
        ],
    ],
    'view_manager'    => [
        'template_map'        => [
            'layout/layout' => __DIR__ . '/../view/layout/layout.phtml',
        ],
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
    ],
    'doctrine'        => [
        'driver' => [
            'orm_default'            => [
                'class'   => MappingDriverChain::class,
                'drivers' => [
                    'Application\Entity\Db' => 'orm_default_xml_driver',
                ],
            ],
            'orm_default_xml_driver' => [
                'class' => XmlDriver::class,
                'cache' => 'array',
                'paths' => [__DIR__ . '/../src/Entity/Db/Mapping'],
            ],
        ],
    ],


    'navigation' => [
        'default' => [
            'home' => [
                'pages' => [
                    'etab'                     => [
                        'footer'   => false, // propriété maison pour inclure cette page dans le menu de pied de page
                    ],
                    'gestion' => [
                        'order' => 400,
                        'label' => 'Suivi',
                        'title' => "Suivi",
                        'route' => 'gestion',
                        'resource' => PrivilegeController::getResourceId(IndexController::class, 'index-gestion'),
                    ],
                    'ressources' => [
                        'order' => 500,
                        'label' => 'Gestion des ateliers',
                        'title' => "Gestion des ateliers",
                        'route' => 'ressource',
                        'resource' => PrivilegeController::getResourceId(IndexController::class, 'index-ressource') ,
                    ],
                    'administration' => [
                        'order' => 10000,
                        'label' => 'Administration',
                        'title' => "Administration",
                        'route' => 'administration',
                        'resource' => PrivilegeController::getResourceId(IndexController::class, 'index-administration') ,
                    ],
                ],
            ],
        ],
    ],

    'public_files' => [
        'head_scripts'          => [
            '015_jquery' => 'unistrap-1.0.0/js/jquery-3.6.1.min.js',
            '040_bootstrap' => '',
            '201_' => 'vendor/chart-2.9.4/Chart.bundle.js',
        ],
        'inline_scripts' => [
            '020_app'         => '',
            '030_util'        => '/unicaen/app/js/util.js',
            '040_unicaen'     => '/unicaen/app/js/unicaen.js',
            '050_jquery_form' => '',
            '070_bootstrap' => 'unistrap-1.0.0/lib/bootstrap-5.2.2/dist/js/bootstrap.bundle.min.js',
            '080_unistrap' => 'unistrap-1.0.0/js/unistrap.js',
            '110_' => 'vendor/DataTables-1.12.1/datatables.min.js',
            '120_bootstrap-select' => '/vendor/bootstrap-select-1.14.0-beta3/js/bootstrap-select.min.js',
            '120_bootstrap-select-fr' => '/vendor/bootstrap-select-1.14.0-beta3/js/i18n/defaults-fr_FR.js',
            '150_' => 'vendor/tinymce-6.8.2/js/tinymce/tinymce.min.js',
        ],
        'stylesheets' => [
            '040_bootstrap' => 'unistrap-1.0.0/lib/bootstrap-5.2.2/dist/css/bootstrap.min.css',
            '041_ubuntu' => 'unistrap-1.0.0/css/font-ubuntu.css',
            '042_unistrap' => 'unistrap-1.0.0/css/unistrap.css',
            '060_unicaen'             => '',
            '110_' => 'vendor/DataTables-1.12.1/datatables.min.css',
            '112_' => 'vendor/font-awesome-5.15.2/css/all.min.css',
            '120_' => '/vendor/bootstrap-select-1.14.0-beta3/css/bootstrap-select.min.css',
            '065_unicaen-icon'        => '/unicaen/app/css/unicaen-icon.css',
            '075_logos'        => 'css/logos.css',
        ],
    ],
];

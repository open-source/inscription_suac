<?php

namespace Site\Service\Ville;

use Doctrine\ORM\EntityManager;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;

class VilleServiceFactory
{
    public function __invoke(ContainerInterface $container) : VilleService
    {
        /**
         * @var EntityManager $entityManager
         */

        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        $service = new VilleService();
        $service->setObjectManager($entityManager);
        return $service;
    }
}
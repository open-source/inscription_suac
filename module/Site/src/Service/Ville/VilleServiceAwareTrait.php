<?php

namespace Site\Service\Ville;

trait VilleServiceAwareTrait
{
    private VilleService $villeService;

    public function getVilleService(): VilleService
    {
        return $this->villeService;
    }

    public function setVilleService(VilleService $villeService): void
    {
        $this->villeService = $villeService;
    }
}
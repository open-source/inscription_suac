<?php

namespace Site\Service\Ville;

use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\QueryBuilder;
use DoctrineModule\Persistence\ProvidesObjectManager;
use Site\Entity\Db\Ville;

class VilleService
{
    use ProvidesObjectManager;

    /** GESTION DES ENTITÉS */

    public function create(Ville $ville): Ville
    {
        $this->getObjectManager()->persist($ville);
        $this->getObjectManager()->flush($ville);
        return $ville;
    }

    public function update(Ville $ville): Ville
    {
        $this->getObjectManager()->flush($ville);
        return $ville;
    }

    public function delete(Ville $ville): void
    {
        $this->getObjectManager()->remove($ville);
        $this->getObjectManager()->flush($ville);
    }

    /** REQUÊTAGE */

    public function createQueryBuilder(): QueryBuilder
    {
        $qb = $this->getObjectManager()->getRepository(Ville::class)->createQueryBuilder('ville');
        return $qb;
    }

    public function getVille(?string $id): ?Ville
    {
        $qb = $this->createQueryBuilder()
            ->andWhere('ville.id = :id')
            ->setParameter('id', $id);

        try {
            return $qb->getQuery()->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }

    public function getVilles(): array
    {
        $qb = $this->createQueryBuilder();
        return $qb->getQuery()->getResult();
    }

    public function getVillesAsOptions(): array
    {
        $qb = $this->createQueryBuilder();
        $qb->select('ville.id', 'ville.nom');
        $qb->orderBy('ville.nom', 'ASC');
        $villes = $qb->getQuery()->getArrayResult();
        $options = [];
        foreach ($villes as $ville) {
            $options[$ville['id']] = $ville['nom'];
        }
        return $options;
    }
}
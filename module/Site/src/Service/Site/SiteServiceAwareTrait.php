<?php

namespace Site\Service\Site;
trait SiteServiceAwareTrait
{
    private SiteService $siteService;

    public function getSiteService(): SiteService
    {
        return $this->siteService;
    }

    public function setSiteService(SiteService $siteService): void
    {
        $this->siteService = $siteService;
    }
}
<?php

namespace Site\Service\Site;

use Doctrine\ORM\EntityManager;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;

class SiteServiceFactory
{
    public function __invoke(ContainerInterface $container) : SiteService
    {
        /**
         * @var EntityManager $entityManager
         */

        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        $service = new SiteService();
        $service->setObjectManager($entityManager);
        return $service;
    }
}
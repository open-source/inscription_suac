<?php

namespace Site\Service\Site;

use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\QueryBuilder;
use DoctrineModule\Persistence\ProvidesObjectManager;
use Site\Entity\Db\Site;

class SiteService
{
    use ProvidesObjectManager;

    /** GESTION DES ENTITÉS */

    public function create(Site $site): Site
    {
        $this->getObjectManager()->persist($site);
        $this->getObjectManager()->flush($site);
        return $site;
    }

    public function update(Site $site): Site
    {
        $this->getObjectManager()->flush($site);
        return $site;
    }

    public function delete(Site $site): void
    {
        $this->getObjectManager()->remove($site);
        $this->getObjectManager()->flush($site);
    }

    /** REQUÊTAGE */

    public function createQueryBuilder(): QueryBuilder
    {
        $qb = $this->getObjectManager()->getRepository(Site::class)->createQueryBuilder('site');
        return $qb;
    }

    public function getSite(?string $id): ?Site
    {
        $qb = $this->createQueryBuilder()
            ->andWhere('site.id = :id')
            ->setParameter('id', $id);

        try {
            return $qb->getQuery()->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }

    public function getSites(): array
    {
        $qb = $this->createQueryBuilder();
        return $qb->getQuery()->getResult();
    }

    public function getSitesAsOptions(): array
    {
        $sites = $this->getSites();
        $options = [];
        foreach ($sites as $site) {
            /** @var Site $site  */
            $options[$site->getId()] = $site->getLibelle() ; //. ' (' . $site->getVille()?->getNom() . ')';
        }
        return $options;
    }
}
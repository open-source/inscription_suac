<?php

use Doctrine\ORM\Mapping\Driver\XmlDriver;
use Doctrine\Persistence\Mapping\Driver\MappingDriverChain;

return array(
    'doctrine' => [
        'driver' => [
            'orm_default'            => [
                'class'   => MappingDriverChain::class,
                'drivers' => [
                    'Site\Entity\Db' => 'orm_default_xml_driver',
                ],
            ],
            'orm_default_xml_driver' => [
                'class' => XmlDriver::class,
                'cache' => 'apc',
                'paths' => [
                    __DIR__ . '/../src/Entity/Db/Mapping',
                ],
            ],
        ],
        'cache'  => [
            'apc' => [
                'namespace' => 'SUAC__' . __NAMESPACE__,
            ],
        ],
    ],

    'router' => [
        'routes' => [
        ],
    ],

    'service_manager' => [
        'factories' => [
        ],
    ],
    'form_elements'   => [
        'factories' => [
        ],
    ],
    'hydrators'       => [
        'factories' => [
        ],
    ],
    'controllers'     => [
        'factories' => [
        ],
    ],
    'view_helpers'    => [
        'invokables' => [
        ],
    ],

    'view_manager' => [
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
    ],
);

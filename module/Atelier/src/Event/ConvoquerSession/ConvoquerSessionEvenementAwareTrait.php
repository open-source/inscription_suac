<?php

namespace Atelier\Event\ConvoquerSession;

trait ConvoquerSessionEvenementAwareTrait
{
    private ConvoquerSessionEvenement $convoquerSessionEvenement;

    public function getConvoquerSessionEvenement(): ConvoquerSessionEvenement
    {
        return $this->convoquerSessionEvenement;
    }

    public function setConvoquerSessionEvenement(ConvoquerSessionEvenement $convoquerSessionEvenement): void
    {
        $this->convoquerSessionEvenement = $convoquerSessionEvenement;
    }

}
<?php

namespace Atelier\Event\CloturerSession;

trait CloturerSessionEvenementAwareTrait
{
    private CloturerSessionEvenement $cloturerSessionEvenement;

    public function getCloturerSessionEvenement(): CloturerSessionEvenement
    {
        return $this->cloturerSessionEvenement;
    }

    public function setCloturerSessionEvenement(CloturerSessionEvenement $cloturerSessionEvenement): void
    {
        $this->cloturerSessionEvenement = $cloturerSessionEvenement;
    }

}
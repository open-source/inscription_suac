<?php

namespace Atelier\Event\RetourSession;

trait RetourSessionEvenementAwareTrait
{
    private RetourSessionEvenement $retourSessionEvenement;

    public function getRetourSessionEvenement(): RetourSessionEvenement
    {
        return $this->retourSessionEvenement;
    }

    public function setRetourSessionEvenement(RetourSessionEvenement $retourSessionEvenement): void
    {
        $this->retourSessionEvenement = $retourSessionEvenement;
    }

}
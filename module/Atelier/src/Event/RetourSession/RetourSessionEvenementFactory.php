<?php

namespace Atelier\Event\RetourSession;

use Atelier\Service\Session\SessionService;
use Doctrine\ORM\EntityManager;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;
use UnicaenEvenement\Service\Etat\EtatService;
use UnicaenEvenement\Service\Type\TypeService;
use UnicaenParametre\Service\Parametre\ParametreService;

class RetourSessionEvenementFactory
{

    /**
     * @param ContainerInterface $container
     * @return RetourSessionEvenement
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): RetourSessionEvenement
    {
        /**
         * @var EntityManager $entityManager
         * @var EtatService $etatService
         * @var ParametreService $parametreService
         * @var SessionService $sessionService
         * @var TypeService $typeService
         */
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        $etatService = $container->get(EtatService::class);
        $parametreService = $container->get(ParametreService::class);
        $sessionService = $container->get(SessionService::class);
        $typeService = $container->get(TypeService::class);

        $event = new RetourSessionEvenement();
        $event->setEntityManager($entityManager);
        $event->setObjectManager($entityManager);
        $event->setEtatEvenementService($etatService);
        $event->setParametreService($parametreService);
        $event->setSessionService($sessionService);
        $event->setTypeService($typeService);

        return $event;
    }
}
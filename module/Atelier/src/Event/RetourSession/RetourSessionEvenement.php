<?php

namespace Atelier\Event\RetourSession;

use Atelier\Entity\Db\Session;
use Atelier\Provider\Etat\SessionEtats;
use Atelier\Provider\Evenement\SessionEvenements;
use Atelier\Provider\Parametre\SessionParametres;
use Atelier\Service\Session\SessionServiceAwareTrait;
use DateInterval;
use DateTime;
use Exception;
use RuntimeException;
use UnicaenApp\Service\EntityManagerAwareTrait;
use UnicaenEvenement\Entity\Db\Etat;
use UnicaenEvenement\Entity\Db\Evenement;
use UnicaenEvenement\Service\Evenement\EvenementService;
use UnicaenParametre\Service\Parametre\ParametreServiceAwareTrait;

class RetourSessionEvenement extends  EvenementService
{
    use EntityManagerAwareTrait;
    use ParametreServiceAwareTrait;
    use SessionServiceAwareTrait;


    public function creer(Session $session, DateTime $dateTraitement = null) : Evenement
    {
        $type = $this->getTypeService()->findByCode(SessionEvenements::RETOUR_EVENEMENT);
        $etat = $this->getEtatEvenementService()->findByCode(Etat::EN_ATTENTE);

        $parametres = [
            'session'       =>  $session->getId(),
        ];

        $evenement = $this->createEvent($type->getLibelle() . " (session #".$session->getId().")", $type->getDescription(), $etat, $type, $parametres, $dateTraitement);
        $this->ajouter($evenement);
        return $evenement;
    }

    public function traiter(Evenement $evenement) : string
    {
        try {
            $parametres = json_decode($evenement->getParametres(), true);
            /** @var Session|null $session */
            $session = $this->getSessionService()->getSession($parametres['session']);

            $etatTypeCode = $session->getEtatActif()?->getType()->getCode();
            if ($etatTypeCode === SessionEtats::ETAT_FORMATION_CONVOCATION) {
                $this->getSessionService()->envoyerConvocation($session);
                $log = "Session #" . $session->getId() . " " . $session->getAtelier()->getLibelle() . " : Retour de la session ";
            } else {
                $log = "Etat [" . ($etatTypeCode ?? "Aucun état") . "] incompatible avec le traitement";
                $evenement->setLog($log);
                $this->update($evenement);
                return Etat::ECHEC;
            }
        } catch (Exception $e) {
            $evenement->setLog($e->getMessage());
            return Etat::ECHEC;
        }
        $evenement->setLog($log);
        $this->update($evenement);
        return Etat::SUCCES;
    }

    public function updateEvent(Session $session): ?Evenement
    {
        //NETTOYAGE
        $evenements = $session->getEvenements()->toArray();
        $evenements = array_filter($evenements, function (Evenement $e) { return $e->getType()->getCode() === SessionEvenements::RETOUR_EVENEMENT;});
        foreach ($evenements as $evenement) {
            $session->removeEvenement($evenement);
            $this->getSessionService()->update($session);
            $this->delete($evenement);
        }

        //LAST
        $derniereSeance = $session->getDerniereSeance();
        if ($derniereSeance === null) return null;


        $dateTraitement = null;
        if ($dateTraitement === null) {
            $dateFin = $derniereSeance->getDateTimeFin();
            try {
                $interval = new DateInterval('P' . $this->getParametreService()->getValeurForParametre(SessionParametres::TYPE, SessionParametres::AUTO_RETOUR) . 'D');
                $dateTraitement = $dateFin->add($interval);
            } catch (Exception $e) {
                throw new RuntimeException("Un problème est survenu lors du calcul de l'interval", 0 ,$e);
            }
        }
        if (!$dateTraitement instanceof DateTime) {
            throw new RuntimeException("La date de traitement de l'evenement [".SessionEvenements::RETOUR_EVENEMENT."] n'a pas pu être déterminée.");
        }
        $event = $this->creer($session, $dateTraitement);
        $session->addEvenement($event);
        $this->getSessionService()->update($session);
        return $event;
    }
}
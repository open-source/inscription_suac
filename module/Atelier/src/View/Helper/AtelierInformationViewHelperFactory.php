<?php

namespace Atelier\View\Helper;

use Fichier\Service\Fichier\FichierService;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;

class AtelierInformationViewHelperFactory
{
    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): AtelierInformationViewHelper
    {
        /** @var FichierService $fichierService */
        $fichierService = $container->get(FichierService::class);

        $helper = new AtelierInformationViewHelper();
        $helper->setFichierService($fichierService);
        return $helper;

    }
}
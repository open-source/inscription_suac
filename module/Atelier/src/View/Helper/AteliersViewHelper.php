<?php

namespace Atelier\View\Helper;

use Atelier\Service\Atelier\AtelierServiceAwareTrait;
use Laminas\View\Helper\AbstractHelper;
use Laminas\View\Helper\Partial;
use Laminas\View\Renderer\PhpRenderer;
use Laminas\View\Resolver\TemplatePathStack;

class AteliersViewHelper extends AbstractHelper
{
    use AtelierServiceAwareTrait;

    public function __invoke(array $ateliers, array $options = []): Partial|string
    {
        /** @var PhpRenderer $view */
        $view = $this->getView();
        $view->resolver()->attach(new TemplatePathStack(['script_paths' => [__DIR__ . "/partial"]]));

        return $view->partial('ateliers', ['ateliers' => $ateliers, 'options' => $options, 'atelierService' => $this->getAtelierService()]);
    }
}
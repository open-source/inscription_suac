<?php

namespace Atelier\View\Helper;

use Atelier\Service\Atelier\AtelierService;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;

class AteliersViewHelperFactory
{
    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): AteliersViewHelper
    {
        /** @var AtelierService $atelierService */
        $atelierService = $container->get(AtelierService::class);

        $helper = new AteliersViewHelper();
        $helper->setAtelierService($atelierService);
        return $helper;

    }
}
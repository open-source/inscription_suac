<?php

namespace Atelier\View\Helper;

use Atelier\Entity\Db\Atelier;
use Exception;
use Fichier\Service\Fichier\FichierServiceAwareTrait;
use Laminas\View\Helper\AbstractHelper;
use Laminas\View\Helper\Partial;
use Laminas\View\Renderer\PhpRenderer;
use Laminas\View\Resolver\TemplatePathStack;

class AtelierInformationViewHelper extends AbstractHelper
{
    use FichierServiceAwareTrait;

    public function __invoke(Atelier $atelier, array $options = []): Partial|string
    {
        /** @var PhpRenderer $view */
        $view = $this->getView();
        $view->resolver()->attach(new TemplatePathStack(['script_paths' => [__DIR__ . "/partial"]]));

        $contenu = null;
        try {
            $contenu = $atelier->getMiniature()?$this->getFichierService()->fetchContenuFichier($atelier->getMiniature()):null;
        } catch(Exception $e) {

        }

        return $view->partial('atelier-information', ['atelier' => $atelier, 'contenu' => $contenu, 'options' => $options]);
    }
}
<?php

namespace Atelier\Controller;

use DateTime;
use Etudiant\Service\Etudiant\EtudiantServiceAwareTrait;
use Atelier\Entity\Db\Inscription;
use Atelier\Form\Inscription\InscriptionFormAwareTrait;
use Atelier\Form\Justification\JustificationFormAwareTrait;
use Atelier\Provider\Etat\InscriptionEtats;
use Atelier\Provider\Etat\SessionEtats;
use Atelier\Provider\Template\PdfTemplates;
use Atelier\Service\Inscription\InscriptionServiceAwareTrait;
use Atelier\Service\Macro\MacroServiceAwareTrait;
use Atelier\Service\Notification\NotificationServiceAwareTrait;
use Atelier\Service\Session\SessionServiceAwareTrait;
use Atelier\Service\Url\UrlServiceAwareTrait;
use Laminas\Http\Response;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\Mvc\Plugin\FlashMessenger\FlashMessenger;
use Laminas\View\Model\ViewModel;
use Laminas\View\Renderer\PhpRenderer;
use Mpdf\MpdfException;
use UnicaenEnquete\Service\Enquete\EnqueteServiceAwareTrait;
use UnicaenEnquete\Service\Instance\InstanceServiceAwareTrait;
use UnicaenEtat\Service\EtatInstance\EtatInstanceServiceAwareTrait;
use UnicaenParametre\Service\Parametre\ParametreServiceAwareTrait;
use UnicaenPdf\Exporter\PdfExporter;
use UnicaenRenderer\Service\Rendu\RenduServiceAwareTrait;
use UnicaenUtilisateur\Service\User\UserServiceAwareTrait;

/** @method FlashMessenger flashMessenger() */

class InscriptionController extends AbstractActionController
{
    use EnqueteServiceAwareTrait;
    use EtudiantServiceAwareTrait;
    use EtatInstanceServiceAwareTrait;
    use InscriptionServiceAwareTrait;
    use InstanceServiceAwareTrait;
    use MacroServiceAwareTrait;
    use NotificationServiceAwareTrait;
    use ParametreServiceAwareTrait;
    use RenduServiceAwareTrait;
    use SessionServiceAwareTrait;
    use UrlServiceAwareTrait;
    use UserServiceAwareTrait;

    use InscriptionFormAwareTrait;
    use JustificationFormAwareTrait;


    private ?PhpRenderer $renderer = null;
    public function setRenderer(PhpRenderer $renderer): void
    {
        $this->renderer = $renderer;
    }

    private ?array $etablissementInfos = null;
    public function addValeur(string $clef, ?string $valeur) :void
    {
        $this->etablissementInfos[$clef] = $valeur;
    }

    /** CRUD ******************************************************************************************************** */

    public function indexAction(): ViewModel
    {
        $inscriptions = $this->getInscriptionService()->getInscriptions('histoCreation', 'DESC', true);

        return new ViewModel([
            'inscriptions' => $inscriptions,
        ]);
    }

    public function ajouterAction(): ViewModel
    {
        $session = $this->getSessionService()->getRequestedSession($this);
        $etape = $this->params()->fromQuery('etape');

        $inscription = new Inscription();
        if ($session) $inscription->setSession($session);

        $form = $this->getInscriptionForm();
        $form->setAttribute('action', $this->url()->fromRoute('atelier/inscription/ajouter', ['session' => ($session) ? $session->getId() : null], [], true));
        $form->bind($inscription);

        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            $inscription->setEtape(Inscription::ETAPE_VALIDATION);
            $form->setData($data);
            if ($form->isValid()) {
                if ($inscription->getEtudiant() !== null) {
                    $this->getInscriptionService()->create($inscription);
                    $this->getEtatInstanceService()->setEtatActif($inscription, InscriptionEtats::INSCRIPTION_VALIDER);

                    $user = $this->getUserService()->getConnectedUser();
                    $inscription->setJustificationValidation("Inscription faite par [".(($user)?$user->getDisplayName():"Inconnu·e")."]");
                    $this->getInscriptionService()->update($inscription);
                }
            }
            exit();
        }

        $vm = new ViewModel([
            'title' => "Ajout d'une inscription",
            'form' => $form,
        ]);
        $vm->setTemplate('atelier/inscription/modifier');
        return $vm;
    }

    public function modifierAction(): ViewModel
    {
        $inscription = $this->getInscriptionService()->getRequestedInscription($this);

        $form = $this->getInscriptionForm();
        $form->setAttribute('action', $this->url()->fromRoute('atelier/inscription/modifier', ['inscription' => $inscription->getId()], [], true));
        $form->bind($inscription);

        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            $form->setData($data);
            if ($form->isValid()) {
                if ($inscription->getEtudiant() !== null) {
                    $this->getInscriptionService()->update($inscription);
                }
            }
            exit();
        }

        $vm = new ViewModel([
            'title' => "Modification de l'inscription",
            'form' => $form,
        ]);
        $vm->setTemplate('atelier/inscription/modifier');
        return $vm;
    }

    public function afficherAction(): ViewModel
    {
        $inscription = $this->getInscriptionService()->getRequestedInscription($this);

        return new ViewModel([
            'inscription' => $inscription,
        ]);
    }

    public function historiserAction(): Response
    {
        $inscrit = $this->getInscriptionService()->getRequestedInscription($this);
        $this->getInscriptionService()->historise($inscrit);
        $this->flashMessenger()->addSuccessMessage("L'inscription de <strong>" . $inscrit->getStagiaireDenomination() . "</strong> vient d'être retirée des listes.");

        $retour = $this->params()->fromQuery('retour');
        if ($retour) return $this->redirect()->toUrl($retour);
        return $this->redirect()->toRoute('atelier/session/afficher', ['session' => $inscrit->getSession()->getId()], ['fragment' => 'inscriptions'], true);
    }

    public function restaurerAction(): Response
    {
        $inscrit = $this->getInscriptionService()->getRequestedInscription($this);
        $this->getInscriptionService()->restore($inscrit);
        $this->flashMessenger()->addSuccessMessage("L'inscription de <strong>" . $inscrit->getStagiaireDenomination() . "</strong> vient d'être restaurée.");

        $retour = $this->params()->fromQuery('retour');
        if ($retour) return $this->redirect()->toUrl($retour);
        return $this->redirect()->toRoute('atelier/session/afficher', ['session' => $inscrit->getSession()->getId()], [], true);

    }

    public function supprimerAction(): ViewModel
    {
        $inscrit = $this->getInscriptionService()->getRequestedInscription($this);

        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            if ($data["reponse"] === "oui") $this->getInscriptionService()->delete($inscrit);
            exit();
        }

        $vm = new ViewModel();
        $vm->setTemplate('default/confirmation');
        if ($inscrit !== null) {
            $vm->setVariables([
                'title' => "Suppression de l'inscription de [" . $inscrit->getStagiaireDenomination() . "]",
                'text' => "La suppression est définitive êtes-vous sûr&middot;e de vouloir continuer ?",
                'action' => $this->url()->fromRoute('atelier/inscription/supprimer', ["inscription" => $inscrit->getId()], [], true),
            ]);
        }
        return $vm;
    }

    /** VALIDATION DES INSCRIPTIONS ********************************************************************************* */

    public function validerAction(): ViewModel
    {
        $inscription = $this->getInscriptionService()->getRequestedInscription($this);
        $inscription->setEtape(Inscription::ETAPE_VALIDATION);
        $instance = $inscription->getSession();

        $user = $this->getUserService()->getConnectedUser();
        $inscription->setJustificationValidation("Inscription validée par ".$user->getDisplayName());

        $form = $this->getJustificationForm();
        $form->setAttribute('action', $this->url()->fromRoute('atelier/inscription/valider', ['inscription' => $inscription->getId()], [], true));
        $form->bind($inscription);
        $form->get('internationnal')->setAttribute('class', 'hidden-element');
        $form->get('telephone')->setAttribute('class', 'hidden-element');

        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            $form->setData($data);
            if ($form->isValid()) {
                $justification = (isset($data['motivation']) && trim($data['motivation']) !== '') ? trim($data['motivation']) : null;
                if ($justification === null) {
                    $this->flashMessenger()->addErrorMessage("<span class='text-danger'><strong> Échec de la validation </strong></span> <br/> Veuillez justifier votre validation !");
                } else {
                    $inscription->setJustificationValidation($justification);
                    $this->getEtatInstanceService()->setEtatActif($inscription, InscriptionEtats::INSCRIPTION_VALIDER);
                    $this->getInscriptionService()->update($inscription);
                    $this->flashMessenger()->addSuccessMessage("Validation effectuée.");
                    $this->getNotificationService()->triggerValidation($inscription);
                }
            }
        }

        $vm = new ViewModel([
            'title' => "Validation de l'inscription de " . $inscription->getStagiaireDenomination() . " à la formation " . $instance->getLibelle() . " du " . $instance->getPeriode(),
            'inscription' => $inscription,
            'form' => $form,
        ]);
//        $vm->setTemplate('default/default-form');
        return $vm;
    }

    public function refuserAction(): ViewModel
    {
        $inscription = $this->getInscriptionService()->getRequestedInscription($this);
        $inscription->setEtape(Inscription::ETAPE_REFUS);
        $instance = $inscription->getSession();

        $form = $this->getJustificationForm();
        $form->setAttribute('action', $this->url()->fromRoute('atelier/inscription/refuser', ['inscription' => $inscription->getId()], [], true));
        $form->bind($inscription);
        $form->get('internationnal')->setAttribute('class', 'hidden-element');
        $form->get('telephone')->setAttribute('class', 'hidden-element');

        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            $form->setData($data);
            if ($form->isValid()) {
                $justification = (isset($data['motivation']) && trim($data['motivation']) !== '') ? trim($data['motivation']) : null;
                if ($justification === null) {
                    $this->flashMessenger()->addErrorMessage("<span class='text-danger'><strong> Échec du refus  </strong></span> <br/> Veuillez justifier votre refus !");
                } else {
                    $inscription->setJustificationRefus($justification);
                    $this->getEtatInstanceService()->setEtatActif($inscription, InscriptionEtats::INSCRIPTION_REFUSER);
                    $this->getInscriptionService()->historise($inscription);
                    $this->flashMessenger()->addSuccessMessage("Refus effectué.");
                    $this->getNotificationService()->triggerRefus($inscription);
                    //todo trigger reclassement
                }
            }
        }

        $vm = new ViewModel([
            'title' => "Refus de l'inscription de " . $inscription->getStagiaireDenomination() . " à l'atelier " . $instance->getAtelier()->getLibelle() . " du " . $instance->getPeriode(),
            'inscription' => $inscription,
            'form' => $form,
        ]);
        //$vm->setTemplate('default/default-form');
        $vm->setTemplate('atelier/inscription/valider');
        return $vm;
    }

    /** Gestion des listes ********************************************************************************************/

    public function envoyerListePrincipaleAction(): Response
    {
        $inscription = $this->getInscriptionService()->getRequestedInscription($this);

        $inscription->setListe(Inscription::PRINCIPALE);
        $this->getInscriptionService()->update($inscription);

//        $this->getNotificationService()->triggerListePrincipale($inscription);

        $this->flashMessenger()->addSuccessMessage("<strong>" . $inscription->getStagiaireDenomination() . "</strong> vient d'être ajouté&middot;e en liste principale.");

        $retour = $this->params()->fromRoute('retour');
        if ($retour) $this->redirect()->toUrl($retour);
        return $this->redirect()->toRoute('atelier/session/afficher', ['session' => $inscription->getSession()->getId()], [], true);
    }

    public function envoyerListeComplementaireAction(): Response
    {
        $inscription = $this->getInscriptionService()->getRequestedInscription($this);

        $inscription->setListe(Inscription::COMPLEMENTAIRE);
        $this->getInscriptionService()->update($inscription);

//        $this->getNotificationService()->triggerListeComplementaire($inscription);


        $this->flashMessenger()->addSuccessMessage("<strong>" . $inscription->getStagiaireDenomination() . "</strong> vient d'être ajouté&middot;e en liste complémentaire.");

        $retour = $this->params()->fromRoute('retour');
        if ($retour) $this->redirect()->toUrl($retour);
        return $this->redirect()->toRoute('atelier/session/afficher', ['session' => $inscription->getSession()->getId()], [], true);
    }

    public function retirerListeAction(): Response
    {
        $inscription = $this->getInscriptionService()->getRequestedInscription($this);

        $inscription->setListe(null);
        $this->getInscriptionService()->update($inscription);

//        $this->getNotificationService()->triggerRetraitListe($inscription);


        $this->flashMessenger()->addSuccessMessage("<strong>" . $inscription->getStagiaireDenomination() . "</strong> vient d'être retiré·e des listes.");

        return $this->redirect()->toRoute('atelier/session/afficher', ['session' => $inscription->getSession()->getId()], [], true);
    }

    public function classerAction(): Response
    {
        $inscription = $this->getInscriptionService()->getRequestedInscription($this);

        $this->getSessionService()->classerInscription($inscription);

        switch ($inscription->getListe()) {
            case Inscription::PRINCIPALE :
                $this->flashMessenger()->addSuccessMessage("Classement de l'inscription en liste principale.");
                break;
            case Inscription::COMPLEMENTAIRE :
                $this->flashMessenger()->addWarningMessage("Liste principale complète. <br/> Classement de l'inscription en liste complémentaire.");
                break;
            default :
                $this->flashMessenger()->addErrorMessage("Plus de place en liste principale et complémentaire. <br/> Échec du classement de l'inscription.");
                break;
        }

        $retour = $this->params()->fromQuery('retour');
        if ($retour) return $this->redirect()->toUrl($retour);

        $session = $inscription->getSession();
        /** @see SessionController::afficherAction() */
        return $this->redirect()->toRoute('atelier/session/afficher', ['session' => $session->getId()], ['fragment' => 'inscriptions'], true);
    }

    public function desinscriptionAction(): ViewModel
    {
        $inscription = $this->getInscriptionService()->getRequestedInscription($this);
        $inscription->setEtape(Inscription::ETAPE_REFUS);
        $session = $inscription->getSession();
        $etudiant = $inscription->getEtudiant();


        $form = $this->getJustificationForm();
        $form->setAttribute('action', $this->url()->fromRoute('atelier/inscription/desinscription', ['inscription' => $inscription->getId(), 'agent' => $etudiant->getId()], [], true));
        $form->bind($inscription);
        $form->get('internationnal')->setAttribute('class', 'hidden-element');
        $form->get('telephone')->setAttribute('class', 'hidden-element');

        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            $form->setData($data);
            if ($form->isValid()) {
//                $justification = (isset($data['motivation']) && trim($data['motivation']) !== '') ? trim($data['motivation']) : null;
//                if ($justification === null) {
//                    $this->flashMessenger()->addErrorMessage("<span class='text-danger'><strong> Échec de la désinscription  </strong></span> <br/> Veuillez justifier votre demande de désinscription !");
//                } else {
//                    $inscription->setJustificationRefus($justification);
                    $this->getEtatInstanceService()->setEtatActif($inscription, InscriptionEtats::INSCRIPTION_REFUSER);
                    $this->getInscriptionService()->historise($inscription);
                    $this->flashMessenger()->addSuccessMessage("Désinscription faite.");
                    //todo trigger reclassement
//                }
                exit();
            }
        }

        $vm = new ViewModel([
            'title' => "Désinscription à la formation " . $session->getLibelle() . " du " . $session->getPeriode(),
            'inscription' => $inscription,
            'form' => $form,
        ]);
        $vm->setTemplate('default/default-form');
        return $vm;
    }

    /** GESTION PAR LES ÉTUDIANT·ES ***********************************************************************************/

    public function autoInscriptionAction(): ViewModel
    {
        $etudiant = $this->getEtudiantService()->getConnectedEtudiant();
        $sessions = $this->getSessionService()->getSessionsByEtat(SessionEtats::ETAT_INSCRIPTION_OUVERTE);

        return new ViewModel([
            'etudiant' => $etudiant,
            'sessions' => $sessions,
        ]);
    }

    public function inscriptionEnCoursAction(): ViewModel
    {
        $etudiant = $this->getEtudiantService()->getRequestedEtudiant($this);
        if ($etudiant === null) $etudiant = $this->getEtudiantService()->getConnectedEtudiant();

        $allInscriptions = $this->getInscriptionService()->getInscriptionsByEtudiant($etudiant);

        //filtrer les sessions par etats
        $inscriptions = [];
        foreach ($allInscriptions as $inscription) {
            $session = $inscription->getSession();
            if (in_array($session->getEtatActif()->getType()->getCode(), [SessionEtats::ETAT_INSCRIPTION_OUVERTE, SessionEtats::ETAT_INSCRIPTION_FERMEE, SessionEtats::ETAT_FORMATION_CONVOCATION])) {
                $inscriptions[] = $inscription;
            }
        }

        return new ViewModel([
            'etudiant' => $etudiant,
            'inscriptions' => $inscriptions,
        ]);
    }

    public function inscriptionHistoriqueAction(): ViewModel
    {
        $etudiant = $this->getEtudiantService()->getRequestedEtudiant($this);
        if ($etudiant === null) $etudiant = $this->getEtudiantService()->getConnectedEtudiant();

        $allInscriptions = $this->getInscriptionService()->getInscriptionsByEtudiant($etudiant);

        //filtrer les sessions par etats
        $inscriptions = [];
        foreach ($allInscriptions as $inscription) {
            $session = $inscription->getSession();
            if ($inscription->getEtatActif()->getType()->getCode() === InscriptionEtats::INSCRIPTION_VALIDER
                AND in_array($session->getEtatActif()->getType()->getCode(), [SessionEtats::ETAT_ATTENTE_RETOURS, SessionEtats::ETAT_CLOTURE_INSTANCE])) {
                $inscriptions[] = $inscription;
            }
        }

        return new ViewModel([
            'etudiant' => $etudiant,
            'inscriptions' => $inscriptions,
        ]);
    }

    /** GESTION DE L'ENQUETE ******************************************************************************************/

    public function repondreEnqueteAction(): ViewModel
    {
        $inscription = $this->getInscriptionService()->getRequestedInscription($this);
        if ($inscription->getEnquete() === null) {
            $enquete = $this->getEnqueteService()->getEnqueteByCode($this->getParametreService()->getValeurForParametre('ENQUETE', 'CODE_ENQUETE'));
            $instance = $this->getInstanceService()->createInstance($enquete);
            $inscription->setEnquete($instance);
            $this->getInscriptionService()->update($inscription);
        } else {
            $instance = $inscription->getEnquete();
        }

        $retour = $this->params()->fromQuery('retour');
        return new ViewModel([
            'inscription' => $inscription,
            'instance' => $instance,
            'retour' => $retour,
            'connectedRole' => $this->getUserService()->getConnectedRole(),
            'connectedUser' => $this->getUserService()->getConnectedUser(),
        ]);

    }

    public function validerEnqueteAction(): ViewModel
    {
        $inscription = $this->getInscriptionService()->getRequestedInscription($this);
        $inscription->getEnquete()->setValidation(new DateTime());
        $this->getInstanceService()->update($inscription->getEnquete());

        $vm = new ViewModel([
            'title' => "Validation de l'enquête de retour de l'atelier",
            'succes' => "<span class='icon icon-checked'></span> Vous venez de valider l'enquête de retour d'atelier. Vous pouvez maintenant télécharger votre attestation.",
        ]);
        $vm->setTemplate('default/reponse');
        return $vm;
    }

    /** DOCUMENTS PDF ASSOCIÉS ****************************************************************************************/

    /**
     * @throws MpdfException
     */
    public function genererConvocationAction(): ?string
    {
        $inscription = $this->getInscriptionService()->getRequestedInscription($this);

        $vars = [
            'type' => '',
            'etudiant' => $inscription->getEtudiant(),
            'inscription' => $inscription,
            'atelier' => $inscription->getSession()->getAtelier(),
            'session' => $inscription->getSession(),
            'MacroService' => $this->getMacroService(),
            'UrlService' => $this->getUrlService(),
        ];

        $rendu = $this->getRenduService()->generateRenduByTemplateCode(PdfTemplates::CONVOCATION, $vars);
        $exporter = new PdfExporter();
        $exporter->setRenderer($this->renderer);
        $exporter->getMpdf()->SetTitle($rendu->getSujet());
        $exporter->getMpdf()->SetMargins(0,0,50);
        $exporter->setHeaderScript('/document/pdf/entete-logo-ccc', null, $this->etablissementInfos);
        $exporter->setFooterScript('/document/pdf/pied-vide');
        $exporter->addBodyHtml($rendu->getCorps());
        return $exporter->export($rendu->getSujet());
    }

    /**
     * @throws MpdfException
     */
    public function genererAttestationAction(): ?string
    {
        $inscription = $this->getInscriptionService()->getRequestedInscription($this);

        $vars = [
            'type' => '',
            'etudiant' => $inscription->getEtudiant(),
            'inscription' => $inscription,
            'atelier' => $inscription->getSession()->getAtelier(),
            'session' => $inscription->getSession(),
            'MacroService' => $this->getMacroService(),
            'UrlService' => $this->getUrlService(),
        ];

        $rendu = $this->getRenduService()->generateRenduByTemplateCode(PdfTemplates::ATTESTATION, $vars);
        $exporter = new PdfExporter();
        $exporter->setRenderer($this->renderer);
        $exporter->getMpdf()->SetTitle($rendu->getSujet());
        $exporter->getMpdf()->SetMargins(0,0,50);
        $exporter->setHeaderScript('/document/pdf/entete-logo-ccc', null, $this->etablissementInfos);
        $exporter->setFooterScript('/document/pdf/pied-vide');
        $exporter->addBodyHtml($rendu->getCorps());
        return $exporter->export($rendu->getSujet());
    }

    /**
     * @throws MpdfException
     */
    public function genererHistoriqueAction(): ?string
    {
        $etudiant = $this->getEtudiantService()->getRequestedEtudiant($this);

        $vars = [
            'etudiant' => $etudiant,
            'MacroService' => $this->getMacroService(),
            'UrlService' => $this->getUrlService(),
        ];

        $rendu = $this->getRenduService()->generateRenduByTemplateCode(PdfTemplates::HISTORIQUE, $vars);
        $exporter = new PdfExporter();
        $exporter->setRenderer($this->renderer);
        $exporter->getMpdf()->SetTitle($rendu->getSujet());
        $exporter->getMpdf()->SetMargins(0,0,50);
        $exporter->setHeaderScript('/document/pdf/entete-logo-ccc', null, $this->etablissementInfos);
        $exporter->setFooterScript('/document/pdf/pied-vide');
        $exporter->addBodyHtml($rendu->getCorps());
        return $exporter->export($rendu->getSujet());
    }
}
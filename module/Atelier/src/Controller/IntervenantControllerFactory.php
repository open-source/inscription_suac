<?php

namespace Atelier\Controller;

use Atelier\Form\Intervenant\IntervenantForm;
use Atelier\Service\Intervenant\IntervenantService;
use Atelier\Service\Session\SessionService;
use Interop\Container\ContainerInterface;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use UnicaenUtilisateur\Form\User\UserForm;
use UnicaenUtilisateur\Form\User\UserRechercheForm;
use UnicaenUtilisateur\Service\User\UserService;

class IntervenantControllerFactory
{

    /**
     * @param ContainerInterface $container
     * @return IntervenantController
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): IntervenantController
    {
        /**
         * @var SessionService $sessionService
         * @var IntervenantService $intervenantService
         * @var UserService $userService
         */
        $sessionService = $container->get(SessionService::class);
        $intervenantService = $container->get(IntervenantService::class);
        $userService = $container->get(UserService::class);

        /**
         * @var IntervenantForm $intervenantForm
         * @var UserForm $userForm
         * @var UserRechercheForm $userRechercheForm
         */
        $intervenantForm = $container->get('FormElementManager')->get(IntervenantForm::class);
        $userForm = $container->get('FormElementManager')->get(UserForm::class);
        $userRechercheForm = $container->get('FormElementManager')->get(UserRechercheForm::class);

        $controller = new IntervenantController();
        $controller->setSessionService($sessionService);
        $controller->setIntervenantService($intervenantService);
        $controller->setUserService($userService);
        $controller->setIntervenantForm($intervenantForm);
        $controller->setUserForm($userForm);
        $controller->setUserRechercheForm($userRechercheForm);
        return $controller;
    }
}
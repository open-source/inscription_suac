<?php

namespace Atelier\Controller;

use Atelier\Form\Seance\SeanceForm;
use Atelier\Form\SeanceRecurrente\SeanceRecurrenteForm;
use Atelier\Service\Seance\SeanceService;
use Atelier\Service\Session\SessionService;
use Interop\Container\ContainerInterface;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use UnicaenEvenement\Service\Evenement\EvenementService;

class SeanceControllerFactory
{

    /**
     * @param ContainerInterface $container
     * @return SeanceController
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): SeanceController
    {
        /**
         * @var EvenementService $evenementService
         * @var SessionService $sessionService
         * @var SeanceService $seanceService
         * @var SeanceForm $seanceForm
         * @var SeanceRecurrenteForm $recurrenteForm
         */
        $evenementService = $container->get(EvenementService::class);
        $sessionService = $container->get(SessionService::class);
        $seanceService = $container->get(SeanceService::class);
        $seanceForm = $container->get('FormElementManager')->get(SeanceForm::class);
        $recurrenteForm = $container->get('FormElementManager')->get(SeanceRecurrenteForm::class);

        $controller = new SeanceController();
        $controller->setEvenementService($evenementService);
        $controller->setSessionService($sessionService);
        $controller->setSeanceService($seanceService);
        $controller->setSeanceForm($seanceForm);
        $controller->setSeanceRecurrenteForm($recurrenteForm);
        return $controller;
    }
}
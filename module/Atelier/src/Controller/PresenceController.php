<?php

namespace Atelier\Controller;

use Atelier\Entity\Db\Presence;
use Atelier\Service\Inscription\InscriptionServiceAwareTrait;
use Atelier\Service\Presence\PresenceServiceAwareTrait;
use Atelier\Service\Seance\SeanceServiceAwareTrait;
use Atelier\Service\Session\SessionServiceAwareTrait;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\View\Model\ViewModel;

class PresenceController extends AbstractActionController
{
    use SessionServiceAwareTrait;
    use InscriptionServiceAwareTrait;
    use PresenceServiceAwareTrait;
    use SeanceServiceAwareTrait;


    public function renseignerPresencesAction(): ViewModel
    {
        $session = $this->getSessionService()->getRequestedSession($this);
        $presences = $this->getPresenceService()->getPresencesBySession($session);

        $dictionnaire = [];
        foreach ($presences as $presence) {
            $dictionnaire[$presence->getSeance()->getId()][$presence->getInscription()->getId()] = $presence;
        }

        return new ViewModel([
            'session' => $session,
            'presences' => $dictionnaire,
        ]);
    }

    public function togglePresenceAction(): ViewModel
    {
        $seance = $this->getSeanceService()->getRequestedSeance($this);
        $inscription = $this->getInscriptionService()->getRequestedInscription($this);

        /** @var  Presence $presence */
        $presence = $this->getPresenceService()->getPresenceBySeanceAndInscription($seance, $inscription);
        if ($presence === null) {
            $presence = new Presence();
            $presence->setSeance($seance);
            $presence->setInscription($inscription);
            $presence->setStatut(Presence::PRESENCE_PRESENCE);
            $this->getPresenceService()->create($presence);
        } else {
            $presence->tooglePresence();
            $this->getPresenceService()->update($presence);
        }

        $vm = new ViewModel();
        $vm->setTemplate('default/reponse');
        $vm->setVariables([
            'reponse' => $presence->getStatut(),
        ]);
        return $vm;
    }

    public function togglePresencesAction(): ViewModel
    {
        $mode = $this->params()->fromRoute('mode');
        $inscription = $this->getInscriptionService()->getRequestedInscription($this);

        $session = $inscription->getSession();
        $seances = $session->getSeances();

        foreach ($seances as $seance) {
            $presence = $this->getPresenceService()->getPresenceBySeanceAndInscription($seance, $inscription);
            if ($presence === null) {
                $presence = new Presence();
                $presence->setSeance($seance);
                $presence->setInscription($inscription);
                $presence->setStatut($mode);
                $this->getPresenceService()->create($presence);
            } else {
                $presence->setStatut($mode);
                $this->getPresenceService()->update($presence);
            }
        }

        $vm = new ViewModel();
        $vm->setTemplate('default/reponse');
        $vm->setVariables([
            'reponse' => $mode,
        ]);
        return $vm;
    }

    public function toggleToutesPresencesAction(): ViewModel
    {
        $mode = $this->params()->fromRoute('mode');
        $session = $this->getSessionService()->getRequestedSession($this);
        $seances = $session->getSeances();

        foreach ($session->getInscriptions() as $inscription) {
            foreach ($seances as $seance) {
                $presence = $this->getPresenceService()->getPresenceBySeanceAndInscription($seance, $inscription);
                if ($presence === null) {
                    $presence = new Presence();
                    $presence->setSeance($seance);
                    $presence->setInscription($inscription);
                    $presence->setStatut($mode);
                    $this->getPresenceService()->create($presence);
                } else {
                    $presence->setStatut($mode);
                    $this->getPresenceService()->update($presence);
                }
            }
        }

        $vm = new ViewModel();
        $vm->setTemplate('default/reponse');
        $vm->setVariables([
            'reponse' => $mode,
        ]);
        return $vm;
    }
}
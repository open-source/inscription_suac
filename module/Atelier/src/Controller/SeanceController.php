<?php

namespace Atelier\Controller;

use Atelier\Entity\Db\Seance;
use Atelier\Form\Seance\SeanceFormAwareTrait;
use Atelier\Form\SeanceRecurrente\SeanceRecurrenteFormAwareTrait;
use Atelier\Service\Seance\SeanceServiceAwareTrait;
use Atelier\Service\Session\SessionServiceAwareTrait;
use Laminas\Http\Request;
use Laminas\Http\Response;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\Mvc\Plugin\FlashMessenger\FlashMessenger;
use Laminas\View\Model\ViewModel;
use UnicaenEvenement\Service\Evenement\EvenementServiceAwareTrait;

/** @method FlashMessenger flashMessenger() */

class SeanceController extends AbstractActionController
{
    use EvenementServiceAwareTrait;
    use SessionServiceAwareTrait;
    use SeanceServiceAwareTrait;
    use SeanceFormAwareTrait;
    use SeanceRecurrenteFormAwareTrait;

    public function ajouterAction(): ViewModel
    {
        $session = $this->getSessionService()->getRequestedSession($this);

        $seance = new Seance();
        $seance->setSession($session);
        $seance->setLieu($session->getLieu());
        $seance->setType(Seance::TYPE_SEANCE);

        $form = $this->getSeanceForm();
        $form->setAttribute('action', $this->url()->fromRoute('atelier/session/ajouter-seance', ['session' => $session->getId()], [], true));
        $form->bind($seance);

        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            $form->setData($data);
            if ($form->isValid()) {
                $this->getSeanceService()->create($seance);
                $this->getSeanceService()->gererEvenement($seance, $this->flashMessenger());
            }
        }

        $vm = new ViewModel();
        $vm->setTemplate('atelier/seance/modifier');
        $vm->setVariables([
            'title' => "Ajout d'une séance",
            'form' => $form,
        ]);
        return $vm;
    }

    public function modifierAction(): ViewModel
    {
        $seance = $this->getSeanceService()->getRequestedSeance($this);

        $form = $this->getSeanceForm();
        $form->setAttribute('action', $this->url()->fromRoute('atelier/session/modifier-seance', ['seance' => $seance->getId()], [], true));
        $form->bind($seance);

        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            $form->setData($data);
            if ($form->isValid()) {
                $this->getSeanceService()->update($seance);
                $this->getSeanceService()->gererEvenement($seance, $this->flashMessenger());
            }
        }

        $vm = new ViewModel();
        $vm->setTemplate('atelier/seance/modifier');
        $vm->setVariables([
            'title' => "Modification d'une séance",
            'form' => $form,
        ]);
        return $vm;
    }

    public function historiserAction(): Response
    {
        $seance = $this->getSeanceService()->getRequestedSeance($this);
        $this->getSeanceService()->historise($seance);
        $this->getSeanceService()->gererEvenement($seance, $this->flashMessenger());

        return $this->redirect()->toRoute('atelier/session/afficher', ['session' => $seance->getSession()->getId()], [], true);
    }

    public function restaurerAction(): Response
    {
        $seance = $this->getSeanceService()->getRequestedSeance($this);
        $this->getSeanceService()->restore($seance);
        $this->getSeanceService()->gererEvenement($seance, $this->flashMessenger());

        return $this->redirect()->toRoute('atelier/session/afficher', ['session' => $seance->getSession()->getId()], [], true);
    }

    public function supprimerAction()
    {
        $seance = $this->getSeanceService()->getRequestedSeance($this);

        /** @var Request $request */
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            if ($data["reponse"] === "oui") {
                $this->getSeanceService()->delete($seance);
                if ($seance->estNonHistorise()) $this->getSeanceService()->gererEvenement($seance, $this->flashMessenger());
            }

            exit();
        }

        $vm = new ViewModel();
        if ($seance !== null) {
            $vm->setTemplate('default/confirmation');
            $vm->setVariables([
                'title' => "Suppression de la séance de formation du [" . $seance->getJour()->format('d/m/Y') . "]",
                'text' => "La suppression est définitive êtes-vous sûr&middot;e de vouloir continuer ?",
                'action' => $this->url()->fromRoute('atelier/session/supprimer-seance', ["seance" => $seance->getId()], [], true),
            ]);
        }
        return $vm;
    }

    /** Gestion des seance récurrente *********************************************************************************/

    public function ajouterSeanceRecurrenteAction(): ViewModel
    {
        $session = $this->getSessionService()->getRequestedSession($this);

        $form = $this->getSeanceRecurrenteForm();
        $form->setAttribute('action', $this->url()->fromRoute('atelier/session/ajouter-seance-recurrente', ['session' => $session->getId()], [], true));

        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            $form->setData($data);
            if ($form->isValid()) {
                $this->getSeanceService()->createRecurrente($session, $data);
                exit();
            }
        }

        $form->get('lieu')->setValue($session->getLieu());

        $vm = new ViewModel();
        $vm->setTemplate('default/default-form');
        $vm->setVariables([
            'title' => "Ajout d'une séance récurrente",
            'form' => $form,
        ]);
        return $vm;
    }
}
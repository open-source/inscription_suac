<?php

namespace Atelier\Controller;

use Atelier\Form\Notification\NotificationFormAwareTrait;
use Atelier\Service\Groupe\GroupeServiceAwareTrait;
use Etudiant\Service\Etudiant\EtudiantServiceAwareTrait;
use Atelier\Entity\Db\Inscription;
use Atelier\Entity\Db\Seance;
use Atelier\Entity\Db\Session;
use Atelier\Form\Inscription\InscriptionFormAwareTrait;
use Atelier\Form\Justification\JustificationFormAwareTrait;
use Atelier\Form\SelectionnerIntervenant\SelectionnerIntervenantFormAwareTrait;
use Atelier\Form\Session\SessionFormAwareTrait;
use Atelier\Provider\Etat\InscriptionEtats;
use Atelier\Provider\Etat\SessionEtats;
use Atelier\Provider\Template\PdfTemplates;
use Atelier\Service\Atelier\AtelierServiceAwareTrait;
use Atelier\Service\Inscription\InscriptionServiceAwareTrait;
use Atelier\Service\Intervenant\IntervenantServiceAwareTrait;
use Atelier\Service\Macro\MacroServiceAwareTrait;
use Atelier\Service\Notification\NotificationServiceAwareTrait;
use Atelier\Service\Presence\PresenceServiceAwareTrait;
use Atelier\Service\Seance\SeanceServiceAwareTrait;
use Atelier\Service\Session\SessionServiceAwareTrait;
use Atelier\Service\Url\UrlServiceAwareTrait;
use Laminas\Http\Request;
use Laminas\Http\Response;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\Mvc\Plugin\FlashMessenger\FlashMessenger;
use Laminas\View\Model\JsonModel;
use Laminas\View\Model\ViewModel;
use Laminas\View\Renderer\PhpRenderer;
use Mpdf\MpdfException;
use Site\Service\Site\SiteServiceAwareTrait;
use Site\Service\Ville\VilleServiceAwareTrait;
use UnicaenEnquete\Service\Enquete\EnqueteServiceAwareTrait;
use UnicaenEnquete\Service\Resultat\ResultatServiceAwareTrait;
use UnicaenEtat\Service\EtatInstance\EtatInstanceServiceAwareTrait;
use UnicaenEtat\Service\EtatType\EtatTypeServiceAwareTrait;
use UnicaenMail\Service\Mail\MailServiceAwareTrait;
use UnicaenParametre\Service\Parametre\ParametreServiceAwareTrait;
use UnicaenPdf\Exporter\PdfExporter;
use UnicaenRenderer\Service\Rendu\RenduServiceAwareTrait;
use UnicaenUtilisateur\Service\User\UserServiceAwareTrait;

/** @method FlashMessenger flashMessenger() */

class SessionController extends AbstractActionController
{
    use AtelierServiceAwareTrait;
    use EnqueteServiceAwareTrait;
    use EtatInstanceServiceAwareTrait;
    use EtatTypeServiceAwareTrait;
    use EtudiantServiceAwareTrait;
    use GroupeServiceAwareTrait;
    use InscriptionServiceAwareTrait;
    use IntervenantServiceAwareTrait;
    use MacroServiceAwareTrait;
    use MailServiceAwareTrait;
    use NotificationServiceAwareTrait;
    use ParametreServiceAwareTrait;
    use PresenceServiceAwareTrait;
    use RenduServiceAwareTrait;
    use ResultatServiceAwareTrait;
    use SessionServiceAwareTrait;
    use SeanceServiceAwareTrait;
    use UrlServiceAwareTrait;
    use UserServiceAwareTrait;
    use InscriptionFormAwareTrait;
    use JustificationFormAwareTrait;
    use NotificationFormAwareTrait;
    use SelectionnerIntervenantFormAwareTrait;
    use SessionFormAwareTrait;
    use VilleServiceAwareTrait;
    use SiteServiceAwareTrait;

    private ?PhpRenderer $renderer = null;
    public function setRenderer(PhpRenderer $renderer): void
    {
        $this->renderer = $renderer;
    }

    private ?array $etablissementInfos = null;
    public function addValeur(string $clef, ?string $valeur) :void
    {
        $this->etablissementInfos[$clef] = $valeur;
    }

    /** CRUD **********************************************************************************************************/

    public function indexAction(): ViewModel
    {
        $groupes = $this->getGroupeService()->getGroupes();
        $types = $this->getEtatTypeService()->getEtatsTypesByCategorieCode(SessionEtats::TYPE);
        $villeAsOptions = $this->getVilleService()->getVillesAsOptions();
        $siteAsOptions = $this->getSiteService()->getSitesAsOptions();
        $params = $this->params()->fromQuery();

        $sessions = $this->getSessionService()->getSessionsWithFiltre($params);

        return new ViewModel([
            'sessions' => $sessions,
            'params' => $params,
            'groupes' => $groupes,
            'types' => $types,
            'villeAsOptions' => $villeAsOptions,
            'siteAsOptions' => $siteAsOptions,
        ]);
    }

    public function afficherAction(): ViewModel
    {
        $session = $this->getSessionService()->getRequestedSession($this);
        $mails = $this->getMailService()->getMailsByMotClef($session->generateTag());

        $tmp = $this->getPresenceService()->getPresencesBySession($session);
        $presences = [];
        foreach ($tmp as $presence) {
            $presences[$presence->getSeance()->getId()][$presence->getInscription()->getId()] = $presence;
        }

        return new ViewModel([
            'session' => $session,
            'presences' => $presences,
            'mails' => $mails,
        ]);
    }

    public function informationsAction(): ViewModel
    {
        $session = $this->getSessionService()->getRequestedSession($this);

        return new ViewModel([
            'title' => "Information à propos de la session",
            'session' => $session,
        ]);
    }

    public function ajouterAction(): ViewModel
    {
        $atelier = $this->getAtelierService()->getRequestedAtelier($this);

        $session = new Session();
        $session->setAtelier($atelier);

        $form = $this->getSessionForm();
        $form->setAttribute('action', $this->url()->fromRoute('atelier/session/ajouter', ['atelier' => $atelier->getId()], [], true));
        $form->bind($session);

        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            $form->setData($data);

            if ($form->isValid()) {
                $this->getSessionService()->create($session);
                $this->getEtatInstanceService()->setEtatActif($session, SessionEtats::ETAT_CREATION_EN_COURS, SessionEtats::TYPE);
                exit();
            }
        }

        $vm =  new ViewModel([
            'title' => "Ajout d'une session",
            'form' => $form,
        ]);
        $vm->setTemplate('default/default-form');
        return $vm;
    }

    public function modifierAction() : ViewModel
    {
        $session = $this->getSessionService()->getRequestedSession($this);

        $form = $this->getSessionForm();
        $form->setAttribute('action', $this->url()->fromRoute('atelier/session/modifier', ['session' => $session->getId()], [], true));
        $form->bind($session);

        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            $form->setData($data);

            if ($form->isValid()) {
                $this->getSessionService()->update($session);
                exit();
            }
        }

        $vm =  new ViewModel([
            'title' => "Modification d'une session",
            'form' => $form,
        ]);
        $vm->setTemplate('default/default-form');
        return $vm;
    }

    public function historiserAction(): Response
    {
        $session = $this->getSessionService()->getRequestedSession($this);
        $this->getSessionService()->historise($session);

        $retour = $this->params()->fromQuery('retour');
        if ($retour) return $this->redirect()->toUrl($retour);
        return $this->redirect()->toRoute('atelier/session', [], [], true);
    }

    public function restaurerAction(): Response
    {
        $session = $this->getSessionService()->getRequestedSession($this);
        $this->getSessionService()->restore($session);

        $retour = $this->params()->fromQuery('retour');
        if ($retour) return $this->redirect()->toUrl($retour);
        return $this->redirect()->toRoute('atelier/session', [], [], true);
    }

    public function supprimerAction(): ViewModel
    {
        $session = $this->getSessionService()->getRequestedSession($this);

        /** @var Request $request */
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            if ($data["reponse"] === "oui") $this->getSessionService()->delete($session);
            exit();
        }

        $vm = new ViewModel();
        if ($session !== null) {
            $vm->setTemplate('default/confirmation');
            $vm->setVariables([
                'title' => "Suppression de la session",
                'text' => "La suppression est définitive êtes-vous sûr&middot;e de vouloir continuer ?",
                'action' => $this->url()->fromRoute('atelier/session/supprimer', ["session" => $session->getId()], [], true),
            ]);
        }
        return $vm;
    }

    /** GESTION DES INTERVENANT·ES ************************************************************************************/

    public function ajouterIntervenantAction() : ViewModel
    {
        $session = $this->getSessionService()->getRequestedSession($this);

        $form = $this->getSelectionnerIntervenantForm();
        $form->setAttribute('action', $this->url()->fromRoute('atelier/session/ajouter-intervenant', ['session' => $session->getId()], [], true));
        $form->bind($session);

        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            $form->setData($data);
            if ($form->isValid()) {
                $this->getSessionService()->update($session);
            }
        }

        $vm = new ViewModel([
            'title' => "Sélectionner les intervenant·es pour la session",
            'form' => $form,
        ]);
        $vm->setTemplate('default/default-form');
        return $vm;
    }

    public function retirerIntervenantAction(): ViewModel
    {
        $intervenant = $this->getIntervenantService()->getRequestedIntervenant($this);
        $session = $this->getSessionService()->getRequestedSession($this);

        $session->removeIntervenant($intervenant);
        $this->getSessionService()->update($session);

        $vm =  new ViewModel([
            'title' => "Retrait de l'intervenant·e [".$intervenant->getDenomination()."]",
            'reponse' => "Le retrait de l'intervenant·e [".$intervenant->getDenomination()."] vient d'être effectué.",
        ]);
        $vm->setTemplate('default/reponse');
        return $vm;
    }

    /** FONCTION DE RECHERCHE *****************************************************************************************/

    public function rechercherAction(): JsonModel
    {
        if (($term = $this->params()->fromQuery('term'))) {
            $sessions = $this->getSessionService()->getSessionByTerm($term);
            foreach ($sessions as $session) {
                $result[] = [
                    'id' => $session->getId(),
                    'label' => $session->getAtelier()->getLibelle(),
                    'extra' => "<span class='badge' style='background-color: slategray;'>" . $session->getPeriode() . "</span>",
                ];
            }
            usort($result, function ($a, $b) {
                return strcmp($a['label'], $b['label']);
            });

            return new JsonModel($result);
        }
        exit;
    }

    /** GESTION DE L'ÉTAT DES SESSIONS ********************************************************************************/

    public function ouvrirInscriptionAction(): Response
    {
        $session = $this->getSessionService()->getRequestedSession($this);
        $this->getSessionService()->ouvrirInscription($session);

        return $this->redirect()->toRoute('atelier/session/afficher', ['session' => $session->getId()], [], true);
    }

    public function fermerInscriptionAction(): Response
    {
        $session = $this->getSessionService()->getRequestedSession($this);
        $this->getSessionService()->fermerInscription($session);

        return $this->redirect()->toRoute('atelier/session/afficher', ['session' => $session->getId()], [], true);
    }

    public function envoyerConvocationAction(): Response
    {
        $session = $this->getSessionService()->getRequestedSession($this);
        $this->getSessionService()->envoyerConvocation($session);
//        $this->getSessionService()->envoyerEmargement($session);

        return $this->redirect()->toRoute('atelier/session/afficher', ['session' => $session->getId()], [], true);
    }

    public function demanderRetourAction(): Response
    {
        $session = $this->getSessionService()->getRequestedSession($this);
        $this->getSessionService()->demanderRetour($session);

        return $this->redirect()->toRoute('atelier/session/afficher', ['session' => $session->getId()], [], true);
    }

    public function cloturerAction(): Response
    {
        $session = $this->getSessionService()->getRequestedSession($this);
        $this->getSessionService()->cloturer($session);

        return $this->redirect()->toRoute('atelier/session/afficher', ['session' => $session->getId()], [], true);
    }

    public function annulerAction(): Response
    {
        $session = $this->getSessionService()->getRequestedSession($this);
        $this->getSessionService()->annuler($session);

        return $this->redirect()->toRoute('atelier/session/afficher', ['session' => $session->getId()], [], true);
    }

    public function reouvrirAction(): Response
    {
        $session = $this->getSessionService()->getRequestedSession($this);
        $this->getSessionService()->reouvrir($session);

        return $this->redirect()->toRoute('atelier/session/afficher', ['session' => $session->getId()], [], true);
    }

    public function changerEtatAction(): ViewModel
    {
        $session = $this->getSessionService()->getRequestedSession($this);

        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            $etat = $this->getEtatTypeService()->getEtatType($data['etat']);

            switch ($etat->getCode()) {
                case SessionEtats::ETAT_CREATION_EN_COURS :
                    $this->getSessionService()->recreation($session);
                    exit();
                case SessionEtats::ETAT_INSCRIPTION_OUVERTE :
                    $this->getSessionService()->ouvrirInscription($session);
                    exit();
                case SessionEtats::ETAT_INSCRIPTION_FERMEE :
                    $this->getSessionService()->fermerInscription($session);
                    exit();
                case SessionEtats::ETAT_FORMATION_CONVOCATION :
                    $this->getSessionService()->envoyerConvocation($session);
//                    $this->getSessionService()->envoyerEmargement($session);
                    exit();
                case SessionEtats::ETAT_ATTENTE_RETOURS :
                    $this->getSessionService()->demanderRetour($session);
                    exit();
                case SessionEtats::ETAT_CLOTURE_INSTANCE :
                    $this->getSessionService()->cloturer($session);
                    exit();
                default :
            }

            exit();
        }

        return new ViewModel([
            'title' => "Changer l'état de la session",
            'etats' => $this->getEtatTypeService()->getEtatsTypesByCategorieCode(SessionEtats::TYPE),
            'session' => $session,
        ]);
    }

    /** Ajout d'inscription *******************************************************************************************/

    public function inscriptionGestionnaireAction(): ViewModel
    {
        $session = $this->getSessionService()->getRequestedSession($this);

        $inscription = new Inscription();
        $inscription->setEtape(Inscription::ETAPE_VALIDATION);
        $inscription->setSession($session);

        $form = $this->getInscriptionForm();
        $form->setAttribute('action', $this->url()->fromRoute('atelier/session/inscription-gestionnaire', ['session' => $session->getId()], [], true));
        $form->bind($inscription);

        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            $form->setData($data);
            if ($form->isValid()) {
                if ($inscription->getEtudiant() !== null) {
                    $this->getInscriptionService()->create($inscription);
                    $this->getEtatInstanceService()->setEtatActif($inscription, InscriptionEtats::INSCRIPTION_VALIDER);

                    $user = $this->getUserService()->getConnectedUser();
                    $inscription->setJustificationValidation("Inscription faite par [".(($user)?$user->getDisplayName():"Inconnu·e")."]");
                    $this->getInscriptionService()->update($inscription);

                    $this->getSessionService()->classerInscription($inscription);
                }
            }
            exit();
        }

        $vm = new ViewModel([
            'title' => "Inscription d'un·e Étudiant·e à l'atelier",
            'form' => $form,
        ]);
        $vm->setTemplate('atelier/inscription/modifier');
        return $vm;
    }


    public function inscriptionEtudiantAction(): ViewModel
    {
        $session = $this->getSessionService()->getRequestedSession($this);
        $etudiant = $this->getEtudiantService()->getRequestedEtudiant($this);

        $inscription = new Inscription();
        $inscription->setSession($session);
        $inscription->setEtudiant($etudiant);
        $inscription->setEtape(Inscription::ETAPE_ETUDIANT);

        $form = $this->getJustificationForm();
        $form->setAttribute('action', $this->url()->fromRoute('atelier/session/inscription-etudiant', ['session' => $session->getId(), 'etudiant' => $etudiant->getId()], [], true));
        $form->bind($inscription);

        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            $form->setData($data);
            if ($form->isValid()) {
                $justification = (isset($data['motivation']) && trim($data['motivation']) !== '') ? trim($data['motivation']) : null;
                if ($justification === null) {
                    $this->flashMessenger()->addErrorMessage("<span class='text-danger'><strong> Échec de l'inscription  </strong></span> <br/> Veuillez motivier votre demande d'inscription!");
                } else {
                    $inscription->setJustificationEtudiant($justification);
                    if (!$session->hasEtudiant($etudiant)) {
                        $this->getInscriptionService()->create($inscription);
                        $this->getEtatInstanceService()->setEtatActif($inscription, InscriptionEtats::INSCRIPTION_DEMANDE);
                        $this->getInscriptionService()->update($inscription);
                        $this->flashMessenger()->addSuccessMessage("Demande d'inscription faite.");
                        $this->getNotificationService()->triggerInscriptionEtudiant($inscription);
                    } else {
                        $this->flashMessenger()->addErrorMessage("Échec de l'inscription : vous êtes déjà inscrit·e.");
                    }
                }
            }
        }

        $vm = new ViewModel([
            'title' => "Inscription à la formation " . $session->getLibelle() . " du " . $session->getPeriode(),
            'inscription' => $inscription,
            'form' => $form,
        ]);
        $vm->setTemplate('default/default-form');
        return $vm;
    }

    public function notifierInscritAction(): ViewModel
    {
        $session = $this->getSessionService()->getRequestedSession($this);

        $form = $this->getNotificationForm();
        $form->setAttribute('action', $this->url()->fromRoute('atelier/session/notifier-inscrit', ['session' => $session->getId()], [], true));
        $form->bind($session);

        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            $form->setData($data);
            if ($form->isValid()) {
                $inscriptions = match ($data['liste']) {
                    Inscription::PRINCIPALE => $session->getListePrincipale(),
                    Inscription::COMPLEMENTAIRE => $session->getListeComplementaire(),
                    'tous' => $session->getListeValide(),
                    default => [],
                };
                $sujet = trim($data['sujet']);
                $corps = trim($data['corps']);

                foreach ($inscriptions as $inscription) {
                    $adresse = $inscription->getEtudiant()->getEmail();
                    $mails[$inscription->getEtudiant()->getDenomination()] = ($adresse)?$this->getMailService()->sendMail($adresse, $sujet, $corps):null;
                    if ($mails[$inscription->getEtudiant()->getDenomination()])
                    {
                        $mails[$inscription->getEtudiant()->getDenomination()]->setMotsClefs([$session->generateTag(), $inscription->generateTag(), 'Notification interne']);
                        $this->getMailService()->update($mails[$inscription->getEtudiant()->getDenomination()]);
                        //$session->addMail($mails[$inscription->getEtudiant()->getDenomination()]);
                    }
                }
                $this->getSessionService()->update($session);

                $success = []; $probleme = [];
                foreach ($mails as $individu => $mail) {
                    if ($mail === null) $probleme[] = $individu; else $success[] = $individu;
                }
                if (!empty($success)) {
                    $successTexte = " Les stagiaires ont été notifié·es (".count($success)." courrier·s envoyé·s).";
                }
                if (!empty($probleme)) {
                    $problemeTexte = "L'opération n'a pas pu être faite pour les inscriptions de  : <ul>";
                    foreach ($probleme as $individu) $problemeTexte .= "<li>" . $individu . "</li>";
                    $problemeTexte .= "</ul>";
                }

                $vm = new ViewModel([
                    'title' => "Notification des inscrits",
                    'reponse' => "Opération effectuée",
                    'success' => $successTexte??null,
                    'error' => $problemeTexte??null,
                ]);
                $vm->setTemplate('default/reponse');
                return $vm;
            }
        }

        $vm = new ViewModel([
            'title' => "Notification des inscrits",
            'form' => $form,
        ]);
        $vm->setTemplate('default/default-form');
        return $vm;
    }

    /** Enquete *******************************************************************************************************/

    public function enqueteAction() : ViewModel
    {
        $session = $this->getSessionService()->getRequestedSession($this);

        $code_enquete = $this->getParametreService()->getValeurForParametre('ENQUETE', 'CODE_ENQUETE');
        $enquete = $this->getEnqueteService()->getEnqueteByCode($code_enquete);

        $inscriptions = $this->getInscriptionService()->getInscriptionsBySession($session);
        [$counts, $results] = $this->getResultatService()->generateResultatArray($enquete, $inscriptions);

        $vm = new ViewModel([
            'enquete' => $enquete,
            'results' => $results,
            'counts' => $counts,
            'elements' => $inscriptions,
        ]);
        $vm->setTemplate('unicaen-enquete/resultat/resultats');
        return $vm;
    }

    /** DOCUMENTS PDF ASSOCIÉS ****************************************************************************************/

    /**
     * @throws MpdfException
     */
    public function genererEmargementAction(): ?string
    {
        $session = $this->getSessionService()->getRequestedSession($this);
        $seance = $this->getSeanceService()->getRequestedSeance($this);

        if ($seance !== null) {
            $seances = [$seance];
            $title = "Emargement_Atelier_".$session->getId()."_".$seance->getId();
        } else {
            $seances = $session->getSeances();
            $seances = array_filter($seances, function (Seance $seance) {
                return $seance->estNonHistorise();
            });
            $title = "Emargement_Atelier_".$session->getId();
        }


        $rendus = [];
        foreach ($seances as $seance) {
            $vars = [
                'session' => $session,
                'seance' => $seance,
                'MacroService' => $this->getMacroService(),
                'UrlService' => $this->getUrlService(),
            ];
            $rendus[$seance->getId()] = $this->getRenduService()->generateRenduByTemplateCode(PdfTemplates::EMARGEMENT, $vars, false);
        }

        $exporter = new PdfExporter();
        $exporter->setRenderer($this->renderer);
        $exporter->getMpdf()->SetTitle($title);
        $exporter->setHeaderScript('/document/pdf/entete-logo-ccc', null, $this->etablissementInfos);
        $exporter->setFooterScript('/document/pdf/pied-vide');
        $exporter->getMpdf()->SetMargins(0, 0, 50);
        foreach ($seances as $seance) {
            $exporter->addBodyHtml($rendus[$seance->getId()]->getCorps());
        }
        return $exporter->export($title);
    }

}
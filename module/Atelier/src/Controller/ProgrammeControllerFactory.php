<?php

namespace Atelier\Controller;

use Atelier\Form\Programme\ProgrammeForm;
use Atelier\Form\SelectionnerAtelier\SelectionnerAtelierForm;
use Atelier\Service\Atelier\AtelierService;
use Atelier\Service\Programme\ProgrammeService;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;
use Site\Service\Site\SiteService;
use Site\Service\Ville\VilleService;

class ProgrammeControllerFactory {

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): ProgrammeController
    {
        /**
         * @var AtelierService $atelierService
         * @var ProgrammeService $programmeService
         * @var ProgrammeForm $programmeForm
         * @var SelectionnerAtelierForm $selectionnerAtelierForm
         * @var SiteService $siteService
         * @var VilleService $villeService
         */
        $atelierService = $container->get(AtelierService::class);
        $programmeService = $container->get(ProgrammeService::class);
        $villeService = $container->get(VilleService::class);
        $siteService = $container->get(SiteService::class);
        $programmeForm = $container->get('FormElementManager')->get(ProgrammeForm::class);
        $selectionnerAtelierForm = $container->get('FormElementManager')->get(SelectionnerAtelierForm::class);

        $controller = new ProgrammeController();
        $controller->setAtelierService($atelierService);
        $controller->setProgrammeService($programmeService);
        $controller->setProgrammeForm($programmeForm);
        $controller->setSelectionnerAtelierForm($selectionnerAtelierForm);
        $controller->setVilleService($villeService);
        $controller->setSiteService($siteService);

        return $controller;
    }
}
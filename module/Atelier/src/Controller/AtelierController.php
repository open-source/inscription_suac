<?php

namespace Atelier\Controller;

use Atelier\Entity\Db\Atelier;
use Atelier\Entity\Db\Session;
use Atelier\Form\Atelier\AtelierFormAwareTrait;
use Atelier\Provider\Etat\SessionEtats;
use Atelier\Service\Atelier\AtelierServiceAwareTrait;
use Atelier\Service\Inscription\InscriptionServiceAwareTrait;
use Atelier\Service\Session\SessionServiceAwareTrait;
use DateTime;
use Etudiant\Service\Etudiant\EtudiantServiceAwareTrait;
use UnicaenEnquete\Service\Enquete\EnqueteServiceAwareTrait;
use UnicaenEnquete\Service\Resultat\ResultatServiceAwareTrait;
use Exception;
use Fichier\Entity\Db\Fichier;
use Fichier\Form\Upload\UploadFormAwareTrait;
use Fichier\Service\Fichier\FichierServiceAwareTrait;
use Fichier\Service\Nature\NatureServiceAwareTrait;
use Laminas\Http\Request;
use Laminas\Http\Response;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\View\Model\ViewModel;
use Site\Service\Site\SiteServiceAwareTrait;
use Site\Service\Ville\VilleServiceAwareTrait;
use UnicaenParametre\Service\Parametre\ParametreServiceAwareTrait;
use UnicaenUtilisateur\Service\User\UserServiceAwareTrait;

class AtelierController extends AbstractActionController
{
    use AtelierServiceAwareTrait;
    use EnqueteServiceAwareTrait;
    use EtudiantServiceAwareTrait;
    use FichierServiceAwareTrait;
    use InscriptionServiceAwareTrait;
    use NatureServiceAwareTrait;
    use ParametreServiceAwareTrait;
    use ResultatServiceAwareTrait;
    use VilleServiceAwareTrait;
    use SessionServiceAwareTrait;
    use SiteServiceAwareTrait;
    use UserServiceAwareTrait;

    use AtelierFormAwareTrait;
    use UploadFormAwareTrait;

    public function indexAction() : ViewModel
    {
        $params = $this->params()->fromQuery();
        $villeAsOptions = $this->getVilleService()->getVillesAsOptions();
        $siteAsOptions = $this->getSiteService()->getSitesAsOptions();
        $archivageAsOptions = ['0' => 'Non historisés', '1' => 'Historisés'];

        $ateliers = $this->getAtelierService()->getAteliersWithFiltre($params, 'libelle','asc', true);

        return new ViewModel([
            'ateliers' => $ateliers,
            'params' => $params,
            'villeAsOptions' => $villeAsOptions,
            'siteAsOptions' => $siteAsOptions,
            'archivageAsOptions' => $archivageAsOptions,
        ]);
    }

    public function afficherAction() : ViewModel
    {
        $atelier = $this->getAtelierService()->getRequestedAtelier($this);

        $contenu = null;
        try {
            $contenu = $atelier->getMiniature()?$this->getFichierService()->fetchContenuFichier($atelier->getMiniature()):null;
        } catch(Exception $e) {

        }

        return new ViewModel([
            'title' => "Atelier : ".$atelier->getLibelle(),
            'atelier' => $atelier,
            'miniature' => $atelier->getMiniature(),
            'contenu' => $contenu,
        ]);
    }

    public function afficherProgrammeAction() : ViewModel
    {
        $atelier = $this->getAtelierService()->getRequestedAtelier($this);

        return new ViewModel([
            'title' => "Programme de l'atelier <strong>".$atelier->getLibelle()."</strong>",
            'atelier' => $atelier,
        ]);
    }

    public function ajouterAction() : ViewModel
    {
        $atelier = new Atelier();

        $form = $this->getAtelierForm();
        $form->setAttribute('action', $this->url()->fromRoute('atelier/atelier/ajouter', [], [], true));
        $form->bind($atelier);

        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            $form->setData($data);

            if ($form->isValid()) {
                $this->getAtelierService()->create($atelier);
                exit();
            }
        }

        $vm =  new ViewModel([
            'title' => "Ajout d'un atelier",
            'form' => $form,
        ]);
        $vm->setTemplate('default/default-form');
        return $vm;
    }

    public function modifierAction() : ViewModel
    {
        $atelier = $this->getAtelierService()->getRequestedAtelier($this);

        $form = $this->getAtelierForm();
        $form->setAttribute('action', $this->url()->fromRoute('atelier/atelier/modifier', ['atelier' => $atelier->getId()], [], true));
        $form->bind($atelier);

        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            $form->setData($data);

            if ($form->isValid()) {
                $this->getAtelierService()->update($atelier);
                exit();
            }
        }

        $vm =  new ViewModel([
            'title' => "Modification d'un atelier",
            'form' => $form,
        ]);
        $vm->setTemplate('default/default-form');
        return $vm;
    }

    public function historiserAction(): Response
    {
        $atelier = $this->getAtelierService()->getRequestedAtelier($this);
        $this->getAtelierService()->historise($atelier);

        $retour = $this->params()->fromQuery('retour');
        if ($retour) return $this->redirect()->toUrl($retour);
        return $this->redirect()->toRoute('atelier/atelier', [], [], true);
    }

    public function restaurerAction(): Response
    {
        $atelier = $this->getAtelierService()->getRequestedAtelier($this);
        $this->getAtelierService()->restore($atelier);

        $retour = $this->params()->fromQuery('retour');
        if ($retour) return $this->redirect()->toUrl($retour);
        return $this->redirect()->toRoute('atelier/atelier', [], [], true);
    }

    public function supprimerAction(): ViewModel
    {
        $atelier = $this->getAtelierService()->getRequestedAtelier($this);

        /** @var Request $request */
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            if ($data["reponse"] === "oui") $this->getAtelierService()->delete($atelier);
            exit();
        }

        $vm = new ViewModel();
        if ($atelier !== null) {
            $vm->setTemplate('default/confirmation');
            $vm->setVariables([
                'title' => "Suppression de l'atelier [" . $atelier->getLibelle() . "]",
                'text' => "La suppression est définitive êtes-vous sûr&middot;e de vouloir continuer ?",
                'action' => $this->url()->fromRoute('atelier/atelier/supprimer', ["atelier" => $atelier->getId()], [], true),
            ]);
        }
        return $vm;
    }

    /** Gestion de l'image associée à l'atelier ***********************************************************************/

    public function televerserImageAction() : ViewModel
    {
        $atelier = $this->getAtelierService()->getRequestedAtelier($this);
        $nature = $this->getNatureService()->getNatureByCode('IMAGE');

        $fichier = new Fichier();
        $fichier->setNature($nature);
        $form = $this->getUploadForm();
        $form->setAttribute('action', $this->url()->fromRoute('atelier/atelier/televerser-image', ['atelier' => $atelier->getId()], [], true));
        $form->bind($fichier);

        /** @var Request $request */
        $request = $this->getRequest();
        if ($request->isPost()) {
            $file = $request->getFiles()['fichier'];

            if ($file['name'] != '') {
                $fichier = $this->getFichierService()->createFichierFromUpload($file, $nature);
                if ($atelier->getMiniature() !== null) {
                    $this->getFichierService()->removeFichier($atelier->getMiniature());
                }
                //TODO tester sans $this->getAtelierService()->update($atelier);
                $atelier->setMiniature($fichier);
                $this->getAtelierService()->update($atelier);
                exit();
            }
        }

        $vm = new ViewModel();
        $vm->setTemplate('default/default-form');
        $vm->setVariables([
            'title' => "Téléverserment de l'image associée à l'atelier",
            'form' => $form,
            'warning' => "<span class='icon icon-attention'></span> Attention la taille de la fiche de poste ne doit pas dépaser 2 Mo."
        ]);
        return $vm;
    }

    public function supprimerImageAction() : Response
    {
        $atelier = $this->getAtelierService()->getRequestedAtelier($this);

        if ($atelier->getMiniature() !== null) {
            $this->getFichierService()->removeFichier($atelier->getMiniature());
        }
        $atelier->setMiniature(null);
        $this->getAtelierService()->update($atelier);

        return $this->redirect()->toRoute('atelier/atelier/afficher', ['atelier' => $atelier->getId()], [], true);
    }

    public function ficheAction(): ViewModel
    {
        $atelier = $this->getAtelierService()->getRequestedAtelier($this);
        $sessions = $this->getAtelierService()->getSessionsActives($atelier);

        $user = $this->getUserService()->getConnectedUser();
        $etudiant = $this->getEtudiantService()->getEtudiantByLogin($user->getUsername());

        return new ViewModel([
            'title' => "Atelier [" . $atelier->getLibelle() . "]",
            'atelier' => $atelier,
            'sessions' => $sessions,
            'etudiant' => $etudiant,
        ]);
    }

    /** Enquete *******************************************************************************************************/

    public function enqueteAction() : ViewModel
    {
        $atelier = $this->getAtelierService()->getRequestedAtelier($this);

        $code_enquete = $this->getParametreService()->getValeurForParametre('ENQUETE', 'CODE_ENQUETE');
        $enquete = $this->getEnqueteService()->getEnqueteByCode($code_enquete);

        $inscriptions = $this->getInscriptionService()->getInscriptionsByAtelier($atelier);
        [$counts, $results] = $this->getResultatService()->generateResultatArray($enquete, $inscriptions);

        $vm = new ViewModel([
            'enquete' => $enquete,
            'results' => $results,
            'counts' => $counts,
            'elements' => $inscriptions,
        ]);
        $vm->setTemplate('unicaen-enquete/resultat/resultats');
        return $vm;
    }

}
<?php

namespace Atelier\Controller;

use Etudiant\Service\Etudiant\EtudiantService;
use Atelier\Form\Inscription\InscriptionForm;
use Atelier\Form\Justification\JustificationForm;
use Atelier\Provider\Parametre\EtablissementParametres;
use Atelier\Service\Inscription\InscriptionService;
use Atelier\Service\Macro\MacroService;
use Atelier\Service\Notification\NotificationService;
use Atelier\Service\Session\SessionService;
use Atelier\Service\Url\UrlService;
use Laminas\View\Renderer\PhpRenderer;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;
use UnicaenEnquete\Service\Enquete\EnqueteService;
use UnicaenEnquete\Service\Instance\InstanceService;
use UnicaenEtat\Service\EtatInstance\EtatInstanceService;
use UnicaenParametre\Service\Parametre\ParametreService;
use UnicaenRenderer\Service\Rendu\RenduService;
use UnicaenUtilisateur\Service\User\UserService;

class InscriptionControllerFactory
{

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): InscriptionController
    {
        /**
         * @var EnqueteService $enqueteService
         * @var EtatInstanceService $etatInstanceService
         * @var EtudiantService $etudiantService
         * @var InscriptionService $inscriptionService
         * @var InstanceService $instanceService
         * @var MacroService $macroService
         * @var NotificationService $notificationService
         * @var ParametreService $parametreService
         * @var RenduService $renduService
         * @var SessionService $sessionService
         * @var UrlService $urlService
         * @var UserService $userService
         */
        $enqueteService = $container->get(EnqueteService::class);
        $etatInstanceService = $container->get(EtatInstanceService::class);
        $etudiantService = $container->get(EtudiantService::class);
        $inscriptionService = $container->get(InscriptionService::class);
        $instanceService = $container->get(InstanceService::class);
        $macroService = $container->get(MacroService::class);
        $parametresService = $container->get(ParametreService::class);
        $renduService = $container->get(RenduService::class);
        $sessionService = $container->get(SessionService::class);
        $notificationService = $container->get(NotificationService::class);
        $urlService = $container->get(UrlService::class);
        $userService = $container->get(UserService::class);

        /**
         * @var InscriptionForm $inscriptionForm
         * @var JustificationForm $justificatifForm
         */
        $inscriptionForm = $container->get('FormElementManager')->get(InscriptionForm::class);
        $justificatifForm = $container->get('FormElementManager')->get(JustificationForm::class);


        $controller = new InscriptionController();

        /* @var PhpRenderer $renderer */
        $renderer = $container->get('ViewRenderer');
        $controller->setRenderer($renderer);
        /** @var ParametreService $parametreService */
        $parametreService = $container->get(ParametreService::class);
        $controller->addValeur('image', $parametreService->getValeurForParametre(EtablissementParametres::TYPE, EtablissementParametres::LOGO));
        $controller->addValeur('libelle', $parametreService->getValeurForParametre(EtablissementParametres::TYPE, EtablissementParametres::LIBELLE));
        $controller->addValeur('souslibelle', $parametreService->getValeurForParametre(EtablissementParametres::TYPE, EtablissementParametres::SOUSLIBELLE));

        $controller->setEnqueteService($enqueteService);
        $controller->setEtatInstanceService($etatInstanceService);
        $controller->setEtudiantService($etudiantService);
        $controller->setInscriptionService($inscriptionService);
        $controller->setInstanceService($instanceService);
        $controller->setMacroService($macroService);
        $controller->setNotificationService($notificationService);
        $controller->setParametreService($parametresService);
        $controller->setRenduService($renduService);
        $controller->setSessionService($sessionService);
        $controller->setUrlService($urlService);
        $controller->setUserService($userService);

        $controller->setInscriptionForm($inscriptionForm);
        $controller->setJustificationForm($justificatifForm);

        return $controller;
    }
}
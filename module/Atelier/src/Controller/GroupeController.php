<?php

namespace Atelier\Controller;

use Atelier\Entity\Db\Groupe;
use Atelier\Form\Groupe\GroupeFormAwareTrait;
use Atelier\Service\Groupe\GroupeServiceAwareTrait;
use Laminas\Http\Request;
use Laminas\Http\Response;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\View\Model\ViewModel;

class GroupeController extends AbstractActionController
{
    use GroupeServiceAwareTrait;
    use GroupeFormAwareTrait;

    public function indexAction(): ViewModel
    {
        $historise = $this->params()->fromQuery('historise');

        $groupes = $this->getGroupeService()->getGroupes();
        if ($historise !== null) $groupes = array_filter($groupes, function (Groupe $a) use ($historise) {
            if ($historise === "1") return $a->estHistorise();
            if ($historise === "0") return $a->estNonHistorise();
            return true;
        });

        return new ViewModel([
            'groupes' => $groupes,
            'historise' => $historise,
        ]);
    }

    /** CRUD **********************************************************************************************************/

    public function afficherAction(): ViewModel
    {
        $groupe = $this->getGroupeService()->getRequestedGroupe($this);

        return new ViewModel([
            'title' => "Affichage du groupe <strong>".$groupe->getLibelle()."</strong>",
            'groupe' => $groupe,
        ]);
    }

    public function ajouterAction(): ViewModel
    {
        $groupe = new Groupe();
        $form = $this->getGroupeForm();
        $form->setAttribute('action', $this->url()->fromRoute('atelier/groupe/ajouter', [], [], true));
        $form->bind($groupe);

        /** @var Request $request */
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            $form->setData($data);
            if ($form->isValid()) {
                $this->getGroupeService()->create($groupe);
                exit();
            }
        }

        $vm = new ViewModel();
        $vm->setTemplate('default/default-form');
        $vm->setVariables([
            'title' => 'Ajouter un groupe de formation',
            'form' => $form,
        ]);
        return $vm;
    }

    public function modifierAction(): ViewModel
    {
        $groupe = $this->getGroupeService()->getRequestedGroupe($this);
        $form = $this->getGroupeForm();
        $form->setAttribute('action', $this->url()->fromRoute('atelier/groupe/modifier', ['groupe' => $groupe->getId()], [], true));
        $form->bind($groupe);

        /** @var Request $request */
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            $form->setData($data);
            if ($form->isValid()) {
                $this->getGroupeService()->update($groupe);
                exit();
            }
        }

        $vm = new ViewModel();
        $vm->setTemplate('default/default-form');
        $vm->setVariables([
            'title' => 'Modifier un groupe de formation',
            'form' => $form,
        ]);
        return $vm;
    }

    public function historiserAction(): Response
    {
        $groupe = $this->getGroupeService()->getRequestedGroupe($this);
        $this->getGroupeService()->historise($groupe);

        $retour = $this->params()->fromQuery('retour');
        if ($retour) return $this->redirect()->toUrl($retour);
        return $this->redirect()->toRoute('atelier/groupe', [], [], true);
    }

    public function restaurerAction(): Response
    {
        $groupe = $this->getGroupeService()->getRequestedGroupe($this);
        $this->getGroupeService()->restore($groupe);

        $retour = $this->params()->fromQuery('retour');
        if ($retour) return $this->redirect()->toUrl($retour);
        return $this->redirect()->toRoute('atelier/groupe', [], [], true);
    }

    public function supprimerAction(): ViewModel
    {
        $groupe = $this->getGroupeService()->getRequestedGroupe($this);

        /** @var Request $request */
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            if ($data["reponse"] === "oui") $this->getGroupeService()->delete($groupe);
            exit();
        }

        $vm = new ViewModel();
        if ($groupe !== null) {
            $vm->setTemplate('default/confirmation');
            $vm->setVariables([
                'title' => "Suppression du thème de formation [" . $groupe->getLibelle() . "]",
                'text' => "La suppression est définitive êtes-vous sûr&middot;e de vouloir continuer ?",
                'action' => $this->url()->fromRoute('atelier/groupe/supprimer', ["groupe" => $groupe->getId()], [], true),
            ]);
        }
        return $vm;
    }

}
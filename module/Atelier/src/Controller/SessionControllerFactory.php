<?php

namespace Atelier\Controller;


use Atelier\Form\Notification\NotificationForm;
use Atelier\Service\Groupe\GroupeService;
use Etudiant\Service\Etudiant\EtudiantService;
use Atelier\Form\Inscription\InscriptionForm;
use Atelier\Form\Justification\JustificationForm;
use Atelier\Form\Justification\JustificationFormAwareTrait;
use Atelier\Form\SelectionnerIntervenant\SelectionnerIntervenantForm;
use Atelier\Form\Session\SessionForm;
use Atelier\Provider\Parametre\EtablissementParametres;
use Atelier\Service\Atelier\AtelierService;
use Atelier\Service\Inscription\InscriptionService;
use Atelier\Service\Intervenant\IntervenantService;
use Atelier\Service\Macro\MacroService;
use Atelier\Service\Notification\NotificationService;
use Atelier\Service\Notification\NotificationServiceAwareTrait;
use Atelier\Service\Presence\PresenceService;
use Atelier\Service\Seance\SeanceService;
use Atelier\Service\Session\SessionService;
use Atelier\Service\Url\UrlService;
use Laminas\View\Renderer\PhpRenderer;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;
use Site\Service\Site\SiteService;
use Site\Service\Ville\VilleService;
use UnicaenEnquete\Service\Enquete\EnqueteService;
use UnicaenEnquete\Service\Resultat\ResultatService;
use UnicaenEtat\Service\EtatInstance\EtatInstanceService;
use UnicaenEtat\Service\EtatType\EtatTypeService;
use UnicaenMail\Service\Mail\MailService;
use UnicaenParametre\Service\Parametre\ParametreService;
use UnicaenRenderer\Service\Rendu\RenduService;
use UnicaenUtilisateur\Service\User\UserService;

class SessionControllerFactory
{

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): SessionController
    {
        /**
         * @var AtelierService $atelierService
         * @var EnqueteService $enqueteService
         * @var EtatInstanceService $etatInstanceService
         * @var EtatTypeService $etatTypeService
         * @var EtudiantService $etudiantService
         * @var GroupeService $groupeService
         * @var InscriptionService $inscriptionService
         * @var IntervenantService $intervenantService
         * @var MacroService $macroService
         * @var MailService $mailService
         * @var NotificationServiceAwareTrait $notificationService
         * @var ParametreService $parametreService
         * @var PresenceService $presenceService
         * @var RenduService $renduService
         * @var ResultatService $resultatService
         * @var SeanceService $seanceService
         * @var SessionService $sessionService
         * @var SiteService $siteService
         * @var UrlService $urlService
         * @var UserService $userService
         * @var VilleService $villeService
         *
         * @var InscriptionForm $inscriptionForm
         * @var JustificationFormAwareTrait $justificationForm
         * @var NotificationForm $notificationForm
         * @var SelectionnerIntervenantForm $selectionnerIntervenantForm
         * @var SessionForm $sessionForm
         */
        $atelierService = $container->get(AtelierService::class);
        $enqueteService = $container->get(EnqueteService::class);
        $etatInstanceService = $container->get(EtatInstanceService::class);
        $etatTypeService = $container->get(EtatTypeService::class);
        $etudiantService = $container->get(EtudiantService::class);
        $groupeService = $container->get(GroupeService::class);
        $inscriptionService = $container->get(InscriptionService::class);
        $intervenantService = $container->get(IntervenantService::class);
        $macroService = $container->get(MacroService::class);
        $mailService = $container->get(MailService::class);
        $notificationService = $container->get(NotificationService::class);
        $parametreService = $container->get(ParametreService::class);
        $presenceService = $container->get(PresenceService::class);
        $renduService = $container->get(RenduService::class);
        $resultatService = $container->get(ResultatService::class);
        $seanceService = $container->get(SeanceService::class);
        $sessionService = $container->get(SessionService::class);
        $urlService = $container->get(UrlService::class);
        $userService = $container->get(UserService::class);
        $villeService = $container->get(VilleService::class);
        $siteService = $container->get(SiteService::class);

        $inscriptionForm = $container->get('FormElementManager')->get(InscriptionForm::class);
        $justificationForm = $container->get('FormElementManager')->get(JustificationForm::class);
        $notificationForm = $container->get('FormElementManager')->get(NotificationForm::class);
        $selectionnerIntervenantForm = $container->get('FormElementManager')->get(SelectionnerIntervenantForm::class);
        $sessionForm = $container->get('FormElementManager')->get(SessionForm::class);

        $controller = new SessionController();

        $controller->setAtelierService($atelierService);
        $controller->setEnqueteService($enqueteService);
        $controller->setEtatInstanceService($etatInstanceService);
        $controller->setEtatTypeService($etatTypeService);
        $controller->setEtudiantService($etudiantService);
        $controller->setGroupeService($groupeService);
        $controller->setInscriptionService($inscriptionService);
        $controller->setIntervenantService($intervenantService);
        $controller->setMacroService($macroService);
        $controller->setMailService($mailService);
        $controller->setNotificationService($notificationService);
        $controller->setParametreService($parametreService);
        $controller->setPresenceService($presenceService);
        $controller->setRenduService($renduService);
        $controller->setResultatService($resultatService);
        $controller->setSeanceService($seanceService);
        $controller->setSessionService($sessionService);
        $controller->setUrlService($urlService);
        $controller->setUserService($userService);

        $controller->setInscriptionForm($inscriptionForm);
        $controller->setJustificationForm($justificationForm);
        $controller->setNotificationForm($notificationForm);
        $controller->setSelectionnerIntervenantForm($selectionnerIntervenantForm);
        $controller->setSessionForm($sessionForm);

        $controller->setVilleService($villeService);
        $controller->setSiteService($siteService);

        /* @var PhpRenderer $renderer */
        $renderer = $container->get('ViewRenderer');
        $controller->setRenderer($renderer);
        /** @var ParametreService $parametreService */
        $parametreService = $container->get(ParametreService::class);
        $controller->addValeur('image', $parametreService->getValeurForParametre(EtablissementParametres::TYPE, EtablissementParametres::LOGO));
        $controller->addValeur('libelle', $parametreService->getValeurForParametre(EtablissementParametres::TYPE, EtablissementParametres::LIBELLE));
        $controller->addValeur('souslibelle', $parametreService->getValeurForParametre(EtablissementParametres::TYPE, EtablissementParametres::SOUSLIBELLE));

        return $controller;
    }
}
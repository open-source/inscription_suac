<?php

namespace Atelier\Controller;

use Atelier\Entity\Db\Intervenant;
use Atelier\Form\Intervenant\IntervenantFormAwareTrait;
use Atelier\Provider\Etat\SessionEtats;
use Atelier\Service\Intervenant\IntervenantServiceAwareTrait;
use Atelier\Service\Session\SessionServiceAwareTrait;
use Laminas\Http\Request;
use Laminas\Http\Response;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\View\Model\JsonModel;
use Laminas\View\Model\ViewModel;
use RuntimeException;
use UnicaenUtilisateur\Entity\Db\AbstractUser;
use UnicaenUtilisateur\Entity\Db\User;
use UnicaenUtilisateur\Form\User\UserFormAwareTrait;
use UnicaenUtilisateur\Form\User\UserRechercheFormAwareTrait;
use UnicaenUtilisateur\Service\User\UserServiceAwareTrait;

class IntervenantController extends AbstractActionController
{
    use SessionServiceAwareTrait;
    use IntervenantServiceAwareTrait;
    use UserServiceAwareTrait;
    use IntervenantFormAwareTrait;
    use UserFormAwareTrait;
    use UserRechercheFormAwareTrait;

    public function indexAction(): ViewModel
    {
        $params = $this->params()->fromQuery();
        $intervenants = $this->getIntervenantService()->getIntervenantsWithFiltre($params);
        return new ViewModel([
            'intervenants' => $intervenants,
            'params' => $params,
        ]);
    }

    /** CRUD **********************************************************************************************************/

    public function afficherAction(): ViewModel
    {
        $intervenant = $this->getIntervenantService()->getRequestedIntervenant($this);
        return new ViewModel([
            'title' => "Affichage de l'intervenant·e <strong>".$intervenant->getDenomination()."</strong>",
            'intervenant' => $intervenant,
        ]);
    }

    public function ajouterAction(): ViewModel
    {
        $session = $this->getSessionService()->getRequestedSession($this);

        $intervenant = new Intervenant();

        $form = $this->getIntervenantForm();
        $form->setAttribute('action', $this->url()->fromRoute('atelier/intervenant/ajouter', ['session' => $session?->getId()], [], true));
        $form->bind($intervenant);

        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            $form->setData($data);
            if ($form->isValid()) {
                $this->getIntervenantService()->create($intervenant);
                if ($session !== null) {
                    $session->addIntervenant($intervenant);
                    $this->getSessionService()->update($session);
                }
                exit();
            }
        }

        $vm = new ViewModel();
        $vm->setTemplate('default/default-form');
        $vm->setVariables([
            'title' => "Ajout d'un·e intervenant·e",
            'form' => $form,
        ]);
        return $vm;
    }

    public function modifierAction(): ViewModel
    {

        $intervenant = $this->getIntervenantService()->getRequestedIntervenant($this);

        $form = $this->getIntervenantForm();
        $form->setAttribute('action', $this->url()->fromRoute('atelier/intervenant/modifier', ['intervenant' => $intervenant->getId()], [], true));
        $form->bind($intervenant);
        $form->setOldEmail($intervenant->getEmail());

        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            $form->setData($data);
            if ($form->isValid()) {
                $this->getIntervenantService()->update($intervenant);
                exit();
            }
        }

        $vm = new ViewModel();
        $vm->setTemplate('default/default-form');
        $vm->setVariables([
            'title' => "Modification d'un·e intervenant·e",
            'form' => $form,
        ]);
        return $vm;
    }

    public function historiserAction(): Response
    {
        $intervenant = $this->getIntervenantService()->getRequestedIntervenant($this);
        $this->getIntervenantService()->historise($intervenant);

        $retour = $this->params()->fromQuery('retour');
        if ($retour) return $this->redirect()->toUrl($retour);
        return $this->redirect()->toRoute('atelier/intervenant',[],[],true);
    }

    public function restaurerAction(): Response
    {
        $intervenant = $this->getIntervenantService()->getRequestedIntervenant($this);
        $this->getIntervenantService()->restore($intervenant);

        $retour = $this->params()->fromQuery('retour');
        if ($retour) return $this->redirect()->toUrl($retour);
        return $this->redirect()->toRoute('atelier/intervenant',[],[],true);
    }

    public function supprimerAction(): ViewModel
    {
        $intervenant = $this->getIntervenantService()->getRequestedIntervenant($this);

        /** @var Request $request */
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            if ($data["reponse"] === "oui") $this->getIntervenantService()->delete($intervenant);
            exit();
        }

        $vm = new ViewModel();
        if ($intervenant !== null) {
            $vm->setTemplate('default/confirmation');
            $vm->setVariables([
                'title' => "Suppression de l'intervenant·e [" . $intervenant->getDenomination() . "]",
                'text' => "La suppression est définitive êtes-vous sûr&middot;e de vouloir continuer ?",
                'action' => $this->url()->fromRoute('atelier/intervenant/supprimer', ["intervenant" => $intervenant->getId()], [], true),
            ]);
        }
        return $vm;
    }

    /** Gestion des comptes utilisateurs associés *********************************************************************/

    public function creerCompteAction(): ViewModel
    {
        $intervenant = $this->getIntervenantService()->getRequestedIntervenant($this);

        $user = new User();
        $user->setEmail($intervenant->getEmail());
        $user->setPassword('db');
        $user->setState(1);

        $form = $this->getUserForm();
        $form->setAttribute('action', $this->url()->fromRoute('atelier/intervenant/creer-compte', ['intervenant' => $intervenant->getId()], [], true));
        $form->bind($user);
        $form->get('prenom')->setValue($intervenant->getPrenom());
        $form->get('nom')->setValue($intervenant->getNom());

        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            $form->setData($data);
            if ($form->isValid()) {
                $this->userService->createLocal($user);
                $intervenant->setUtilisateur($user);
                $this->getIntervenantService()->update($intervenant);
                exit();
            }
        }

        $vm = new ViewModel([
            'title' => "Créer et associer un compte",
            'form' => $form,
        ]);
        $vm->setTemplate('default/default-form');
        return $vm;
    }

    public function associerCompteAction(): ViewModel
    {
        $intervenant = $this->getIntervenantService()->getRequestedIntervenant($this);
        $form = $this->getUserRechercheForm();
        $form->setAttribute('action', $this->url()->fromRoute('atelier/intervenant/associer-compte', ['intervenant' => $intervenant->getId()], [], true));

        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            $form->setData($data);
            if ($form->isValid()) {
                [$source, $id] = explode('||', $data['utilisateurId']);
                /** @var ?AbstractUser $user */
                $user = null;
                if ($source === 'app') $user = $this->getUserService()->find($id);
                if ($user !== null) {
                    $intervenant->setUtilisateur($user);
                    $this->getIntervenantService()->update($intervenant);
                }
                exit();
            }
        }

        $js =<<<EOS
$(function () {
$('#utilisateur')
            .autocompleteUnicaen({
                elementDomId: 'utilisateur-id',
                source: '/utilisateur/rechercher',
                html: true,
                minLength: 3,
                delay: 750,
                search: function (event, ui) {
                    $('#selectionner').hide();
                },
                select: function (event, ui) {
                    let id = ui.item.id;
                    let label = ui.item.label;
                    $('#utilisateur').val(label);
                    $('#utilisateur-id').val(id);
                    const t = id.split("||");
                    if (t[0] === 'app') {
                        text = '<i class="icon icon-link"></i> Associer l\'utilisateur';
                    }
                    $('#selectionner').html(text).show();

                    return false;
                },
            })
            .autocomplete("instance")._renderItem = function (ul, item) {
            let template = '<span id=\"{id}\">{label} <span class=\"extra\">{extra}</span></span>';
            let markup = template
                .replace('{id}', item.id ? item.id : '')
                .replace('{label}', item.label ? item.label : '')
                .replace('{extra}', item.extra ? item.extra : '');
            markup = '<a id="autocomplete-item-' + item.id + '">' + markup + "</a>";
            let li = $("<li></li>");
            if (item.id) {
                const t = item.id.split("||");
                if (t[0] == 'app') {
                    li.addClass('bg-success');
                }
            }
            li = li.data("item.autocomplete", item).append(markup).appendTo(ul);
            // mise en évidence du motif dans chaque résultat de recherche
            highlight($('#utilisateur').val(), li, 'sas-highlight');
            // si l'item ne possède pas d'id, on fait en sorte qu'il ne soit pas sélectionnable
            if (!item.id) {
                li.click(function () {
                    return false;
                });
            }

            return li;
        };
});
EOS;


        $vm = new ViewModel([
            'title' => "Assocation d'un compte existant",
            'form' => $form,
            'js' => $js,
        ]);
        $vm->setTemplate('default/default-form');
        return $vm;
    }

    public function deassocierCompteAction(): Response
    {
        $intervenant = $this->getIntervenantService()->getRequestedIntervenant($this);
        $intervenant->setUtilisateur(null);
        $this->getIntervenantService()->update($intervenant);

        $retour = $this->params()->fromQuery('retour');
        if ($retour) return $this->redirect()->toUrl($retour);
        return $this->redirect()->toRoute('atelier/intervenant', [], [], true);
    }

    /** Point de chute pour les intervenant·es ************************************************************************/

    public function mesSessionsAction(): ViewModel
    {
        $user = $this->getUserService()->getConnectedUser();
        $intervenants = $this->getIntervenantService()->getIntervenantsByUser($user);

        if (empty($intervenants)) {
            throw new RuntimeException("[".$user->getDisplayName()." #".$user->getId()."] est associé·e à aucun·e intervenant·e actif·tive.");
        }
        if (count($intervenants) > 1) {
            throw new RuntimeException("[".$user->getDisplayName()." #".$user->getId()."] est associé·e à plusieurs intervenant·es actif·tives.");
        }
        /** @var Intervenant $formateur */
        $intervenant = current($intervenants);
        $sessions = $intervenant->getSessions();

        $sessionsActives = [];
        $sessionsInactives = [];
        foreach ($sessions as $session) {
            if ($session->estNonHistorise()) {
                if ($session->isEtatActif(SessionEtats::ETAT_CLOTURE_INSTANCE) || $session->isEtatActif(SessionEtats::ETAT_SESSION_ANNULEE)) {
                    $sessionsInactives[] = $session;
                } else {
                    $sessionsActives[] = $session;
                }
            }
        }

        return new ViewModel([
            'intervenant' => $intervenant,
            'sessionsActives' => $sessionsActives,
            'sessionsInactives' => $sessionsInactives,
        ]);
    }

    /** Fonctions de recherche ****************************************************************************************/

    public function rechercherAction(): JsonModel
    {
        if (($term = $this->params()->fromQuery('term'))) {
            $intervenants = $this->getIntervenantService()->getIntervenantsByTerm($term);
            $result = $this->getIntervenantService()->formatIntervenantsJSON($intervenants);
            return new JsonModel($result);
        }
        exit;
    }

    public function rechercherRattachementAction(): JsonModel
    {
        if (($term = $this->params()->fromQuery('term'))) {
            $rattachements = $this->getIntervenantService()->getRattachementByTerm($term);
            $result = $this->getIntervenantService()->formatRattachementsJSON($rattachements);
            return new JsonModel($result);
        }
        exit;
    }
}
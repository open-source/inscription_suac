<?php

namespace Atelier\Controller;

use Atelier\Form\Groupe\GroupeForm;
use Atelier\Service\Groupe\GroupeService;
use Interop\Container\ContainerInterface;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;

class GroupeControllerFactory
{
    /**
     * @param ContainerInterface $container
     * @return GroupeController
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): GroupeController
    {
        /**
         * @var GroupeService $formationGroupeService
         */
        $formationGroupeService = $container->get(GroupeService::class);

        /**
         * @var GroupeForm $formationGroupeForm
         */
        $formationGroupeForm = $container->get('FormElementManager')->get(GroupeForm::class);

        $controller = new GroupeController();
        $controller->setGroupeService($formationGroupeService);
        $controller->setGroupeForm($formationGroupeForm);
        return $controller;
    }
}
<?php

namespace Atelier\Controller;

use DateTime;
use Atelier\Entity\Db\Programme;
use Atelier\Form\Programme\ProgrammeFormAwareTrait;
use Atelier\Form\SelectionnerAtelier\SelectionnerAtelierFormAwareTrait;
use Atelier\Service\Atelier\AtelierServiceAwareTrait;
use Atelier\Service\Programme\ProgrammeServiceAwareTrait;
use Laminas\Http\Request;
use Laminas\Http\Response;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\View\Model\ViewModel;
use Site\Service\Site\SiteServiceAwareTrait;
use Site\Service\Ville\VilleServiceAwareTrait;

class ProgrammeController extends AbstractActionController
{
    use AtelierServiceAwareTrait;
    use ProgrammeServiceAwareTrait;
    use ProgrammeFormAwareTrait;
    use SelectionnerAtelierFormAwareTrait;
    use VilleServiceAwareTrait;
    use SiteServiceAwareTrait;

    public function indexAction(): ViewModel
    {
        $params = $this->params()->fromQuery();
        $villeAsOptions = $this->getVilleService()->getVillesAsOptions();
        $siteAsOptions = $this->getSiteService()->getSitesAsOptions();

        $programmes = $this->getProgrammeService()->getProgrammesWithFiltre($params,'dateDebut', 'DESC', true);

        return new ViewModel([
            'programmes' => $programmes,
            'params' => $params,
            'villeAsOptions' => $villeAsOptions,
            'siteAsOptions' => $siteAsOptions,
        ]);
    }

    public function afficherAction(): ViewModel
    {
        $programme = $this->getProgrammeService()->getRequestedProgramme($this);

        return new ViewModel([
            'programme' => $programme,
        ]);
    }

    public function ajouterAction(): ViewModel
    {
        $programme = new Programme();

        $form = $this->getProgrammeForm();
        $form->setAttribute('action', $this->url()->fromRoute('atelier/programme/ajouter', [], [], true));
        $form->bind($programme);

        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            $form->setData($data);

            if ($form->isValid()) {
                $this->getProgrammeService()->create($programme);
                exit();
            }
        }

        $vm = new ViewModel([
            'title' => "Ajout d'un programme",
            'form' => $form,
        ]);
        $vm->setTemplate('default/default-form');
        return $vm;
    }

    public function modifierAction(): ViewModel
    {
        $programme = $this->getProgrammeService()->getRequestedProgramme($this);

        $form = $this->getProgrammeForm();
        $form->setAttribute('action', $this->url()->fromRoute('atelier/programme/modifier', ['programme' => $programme->getId()], [], true));
        $form->bind($programme);

        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            $form->setData($data);

            if ($form->isValid()) {
                $this->getProgrammeService()->update($programme);
                exit();
            }
        }

        $vm = new ViewModel([
            'title' => "Modification d'un programme",
            'form' => $form,
        ]);
        $vm->setTemplate('default/default-form');
        return $vm;
    }

    public function historiserAction(): Response
    {
        $programme = $this->getProgrammeService()->getRequestedProgramme($this);
        $this->getProgrammeService()->historise($programme);

        $retour = $this->params()->fromQuery('retour');
        if ($retour) return $this->redirect()->toUrl($retour);
        return $this->redirect()->toRoute('atelier/programme', [], [], true);
    }

    public function restaurerAction(): Response
    {
        $programme = $this->getProgrammeService()->getRequestedProgramme($this);
        $this->getProgrammeService()->restore($programme);

        $retour = $this->params()->fromQuery('retour');
        if ($retour) return $this->redirect()->toUrl($retour);
        return $this->redirect()->toRoute('atelier/programme', [], [], true);
    }

    public function supprimerAction(): ViewModel
    {
        $programme = $this->getProgrammeService()->getRequestedProgramme($this);

        /** @var Request $request */
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            if ($data["reponse"] === "oui") $this->getProgrammeService()->delete($programme);
            exit();
        }

        $vm = new ViewModel();
        if ($programme !== null) {
            $vm->setTemplate('default/confirmation');
            $vm->setVariables([
                'title' => "Suppression du programme [" . $programme->getLibelle() . "]",
                'text' => "La suppression est définitive êtes-vous sûr&middot;e de vouloir continuer ?",
                'action' => $this->url()->fromRoute('atelier/programme/supprimer', ["programme" => $programme->getId()], [], true),
            ]);
        }
        return $vm;
    }

    /** GESTION PROGRAMME <-> ACTION ***********************************************************/

    public function ajouterProgrammeAction(): Response
    {
        $atelier = $this->getAtelierService()->getRequestedAtelier($this);
        $programme = $this->getProgrammeService()->getRequestedProgramme($this);
        $programme->addAtelier($atelier);
        $this->getProgrammeService()->update($programme);

        $retour = $this->params()->fromQuery('retour');
        if ($retour) return $this->redirect()->toUrl($retour);
        return $this->redirect()->toRoute('atelier/atelier/afficher', ['atelier' => $atelier->getId()], [], true);
    }

    public function retirerProgrammeAction(): Response
    {
        $atelier = $this->getAtelierService()->getRequestedAtelier($this);
        $programme = $this->getProgrammeService()->getRequestedProgramme($this);
        $programme->removeAtelier($atelier);
        $this->getProgrammeService()->update($programme);

        $retour = $this->params()->fromQuery('retour');
        if ($retour) return $this->redirect()->toUrl($retour);
        return $this->redirect()->toRoute('atelier/atelier/afficher', ['atelier' => $atelier->getId()], [], true);
    }


    public function gererAteliersAction() : ViewModel
    {
        $programme = $this->getProgrammeService()->getRequestedProgramme($this);

        $form = $this->getSelectionnerAtelierForm();
        $form->setAttribute('action', $this->url()->fromRoute('atelier/programme/gerer-ateliers', ['programme' => $programme->getId()], [], true));
        $form->bind($programme);

        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            $form->setData($data);
            if ($form->isValid()) {
                $this->getProgrammeService()->update($programme);
                exit();
            }
        }

        $vm = new ViewModel([
            'title' => "Sélection des ateliers du programme [".$programme->getLibelle()."]",
            'form' => $form,
        ]);
        $vm->setTemplate('default/default-form');
        return $vm;
    }

    public function courantAction(): ViewModel
    {
        $params = $this->params()->fromQuery();
        $villeAsOptions = $this->getVilleService()->getVillesAsOptions();
        $siteAsOptions = $this->getSiteService()->getSitesAsOptions();

        $programmes = $this->getProgrammeService()->getProgrammesActifsWithFiltre($params);

        $ateliers = [];
        foreach ($programmes as $programme) {
            foreach ($programme->getAteliers() as $atelier) {
                $ateliers[$atelier->getId()] = $atelier;
            }
        }

        return new ViewModel([
            'programmes' => $programmes,
            'ateliers' => $ateliers,
            'params' => $params,
            'villeAsOptions' => $villeAsOptions,
            'siteAsOptions' => $siteAsOptions,
        ]);
    }
}
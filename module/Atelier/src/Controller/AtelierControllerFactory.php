<?php

namespace Atelier\Controller;

use Atelier\Form\Atelier\AtelierForm;
use Atelier\Service\Atelier\AtelierService;
use Atelier\Service\Inscription\InscriptionService;
use Atelier\Service\Session\SessionService;
use DoctrineORMModule\Proxy\__CG__\Enquete\Entity\Db\Enquete;
use Etudiant\Controller\EtudiantController;
use Etudiant\Service\Etudiant\EtudiantService;
use Fichier\Form\Upload\UploadForm;
use Fichier\Service\Fichier\FichierService;
use Fichier\Service\Nature\NatureService;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;
use Site\Service\Site\SiteService;
use Site\Service\Ville\VilleService;
use UnicaenEnquete\Service\Enquete\EnqueteService;
use UnicaenEnquete\Service\Resultat\ResultatService;
use UnicaenParametre\Service\Parametre\ParametreService;
use UnicaenUtilisateur\Service\User\UserService;

class AtelierControllerFactory
{
    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): AtelierController
    {
        /**
         * @var AtelierService $atelierService
         * @var EnqueteService $enqueteService
         * @var EtudiantService $etudiantService
         * @var FichierService $fichierService
         * @var InscriptionService $inscriptionService
         * @var NatureService $natureService
         * @var ParametreService $parametreService
         * @var ResultatService $resultatService
         * @var SessionService $sessionService
         * @var SiteService $siteService
         * @var UserService $userService
         * @var VilleService $villeService
         *
         *
         * @var AtelierForm $atelierForm
         * @var UploadForm $uploadForm
         */
        $atelierService = $container->get(AtelierService::class);
        $enqueteService = $container->get(EnqueteService::class);
        $etudiantService = $container->get(EtudiantService::class);
        $fichierService = $container->get(FichierService::class);
        $inscriptionService = $container->get(InscriptionService::class);
        $natureService = $container->get(NatureService::class);
        $parametreService = $container->get(ParametreService::class);
        $resultatService = $container->get(ResultatService::class);
        $sessionService = $container->get(SessionService::class);
        $siteService = $container->get(SiteService::class);
        $userService = $container->get(UserService::class);
        $villeService = $container->get(VilleService::class);
        $atelierForm = $container->get('FormElementManager')->get(AtelierForm::class);
        $uploadForm = $container->get('FormElementManager')->get(UploadForm::class);

        $controller = new AtelierController();
        $controller->setAtelierService($atelierService);
        $controller->setEnqueteService($enqueteService);
        $controller->setEtudiantService($etudiantService);
        $controller->setFichierService($fichierService);
        $controller->setInscriptionService($inscriptionService);
        $controller->setNatureService($natureService);
        $controller->setParametreService($parametreService);
        $controller->setResultatService($resultatService);
        $controller->setSessionService($sessionService);
        $controller->setSiteService($siteService);
        $controller->setUserService($userService);
        $controller->setVilleService($villeService);
        $controller->setAtelierForm($atelierForm);
        $controller->setUploadForm($uploadForm);



        return $controller;
    }
}
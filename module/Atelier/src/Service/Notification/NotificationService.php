<?php

namespace Atelier\Service\Notification;

use Application\Provider\Parametre\GlobalParametres;
use Atelier\Entity\Db\Inscription;
use Atelier\Provider\Template\MailTemplates;
use Atelier\Service\Macro\MacroServiceAwareTrait;
use Atelier\Service\Url\UrlServiceAwareTrait;
use RuntimeException;
use UnicaenMail\Entity\Db\Mail;
use UnicaenMail\Service\Mail\MailServiceAwareTrait;
use UnicaenParametre\Service\Parametre\ParametreServiceAwareTrait;
use UnicaenRenderer\Service\Rendu\RenduServiceAwareTrait;

class NotificationService
{
    use MacroServiceAwareTrait;
    use UrlServiceAwareTrait;
    use MailServiceAwareTrait;
    use ParametreServiceAwareTrait;
    use RenduServiceAwareTrait;

    /** Recupération des mails *******************************************************************************/

    public function getMailInscrit(Inscription $inscription) : ?string
    {
        $etudiant = $inscription->getEtudiant();
        if ($etudiant === null) throw new RuntimeException("Aucun·e étudiant·e associé·e à l'inscription #".$inscription->getId());
        return $etudiant->getEmail();
    }

    public function getMailForService(): ?string
    {
        $email = $this->getParametreService()->getValeurForParametre(GlobalParametres::TYPE, GlobalParametres::EMAIL_SERVICE);
        if ($email === null) throw new RuntimeException("Aucune valeur pour le paramètre [".GlobalParametres::TYPE . "|". GlobalParametres::EMAIL_SERVICE."]");
        return $email;
    }

    /** Tableau de variables **********************************************************************************/

    public function prepVariables(Inscription $inscription) : array
    {
        $session = $inscription->getSession();
        $etudiant = $inscription->getEtudiant();

        $vars = [
            'etudiant' => $etudiant,
            'session' => $session,
            'inscription' => $inscription,
            'MacroService' => $this->getMacroService(),
        ];

        $urlService = $this->getUrlService();
        $urlService->setVariables($vars);

        $vars['UrlService'] = $this->getUrlService();

        return $vars;
    }


    /** Mails liés aux changements d'états ********************************************************************/

    public function triggerListePrincipale(Inscription $inscription) : ?Mail
    {
        $session = $inscription->getSession();
        $vars = $this->prepVariables($inscription);

        $rendu = $this->getRenduService()->generateRenduByTemplateCode(MailTemplates::SESSION_LISTE_PRINCIPALE, $vars);
        $mail = $this->getMailService()->sendMail($this->getMailInscrit($inscription), $rendu->getSujet(), $rendu->getCorps());
        $mail->setMotsClefs([$session->generateTag(), $inscription->generateTag(), $rendu->getTemplate()->generateTag()]);
        $this->getMailService()->update($mail);

        return $mail;
    }

    public function triggerListeComplementaire(Inscription $inscription) : ?Mail
    {
        $session = $inscription->getSession();
        $vars = $this->prepVariables($inscription);

        $rendu = $this->getRenduService()->generateRenduByTemplateCode(MailTemplates::SESSION_LISTE_COMPLEMENTAIRE, $vars);
        $mail = $this->getMailService()->sendMail($this->getMailInscrit($inscription), $rendu->getSujet(), $rendu->getCorps());
        $mail->setMotsClefs([$session->generateTag(), $rendu->getTemplate()->generateTag()]);
        $this->getMailService()->update($mail);

        return $mail;
    }

    public function triggerConvocation(Inscription $inscription) : ?Mail
    {
        $session = $inscription->getSession();
        $vars = $this->prepVariables($inscription);

        $rendu = $this->getRenduService()->generateRenduByTemplateCode(MailTemplates::SESSION_CONVOCATION, $vars);
        $mail = $this->getMailService()->sendMail($this->getMailInscrit($inscription), $rendu->getSujet(), $rendu->getCorps());
        $mail->setMotsClefs([$session->generateTag(), $rendu->getTemplate()->generateTag()]);
        $this->getMailService()->update($mail);

        return $mail;
    }

    public function triggerDemanderRetour(Inscription $inscription) : ?Mail
    {
        $session = $inscription->getSession();
        $vars = $this->prepVariables($inscription);

        $rendu = $this->getRenduService()->generateRenduByTemplateCode(MailTemplates::SESSION_DEMANDE_RETOUR, $vars);
        $mail = $this->getMailService()->sendMail($this->getMailInscrit($inscription), $rendu->getSujet(), $rendu->getCorps());
        $mail->setMotsClefs([$session->generateTag(), $rendu->getTemplate()->generateTag()]);
        $this->getMailService()->update($mail);

        return $mail;
    }

    public function triggerSessionAnnulee(Inscription $inscription) : ?Mail
    {
        $session = $inscription->getSession();
        $vars = $this->prepVariables($inscription);

        $rendu = $this->getRenduService()->generateRenduByTemplateCode(MailTemplates::SESSION_ANNULEE, $vars);
        $mail = $this->getMailService()->sendMail($this->getMailInscrit($inscription), $rendu->getSujet(), $rendu->getCorps());
        $mail->setMotsClefs([$session->generateTag(), $rendu->getTemplate()->generateTag()]);
        $this->getMailService()->update($mail);

        return $mail;
    }

    public function triggerInscriptionEtudiant(Inscription $inscription) : ?Mail
    {
        $session = $inscription->getSession();
        $vars = $this->prepVariables($inscription);

        $rendu = $this->getRenduService()->generateRenduByTemplateCode(MailTemplates::INSCRIPTION_DEMANDE, $vars);
        $mail = $this->getMailService()->sendMail($this->getMailForService(), $rendu->getSujet(), $rendu->getCorps());
        $mail->setMotsClefs([$session->generateTag(), $rendu->getTemplate()->generateTag()]);
        $this->getMailService()->update($mail);

        return $mail;
    }

    public function triggerValidation(Inscription $inscription) : ?Mail
    {
        $session = $inscription->getSession();
        $vars = $this->prepVariables($inscription);

        $rendu = $this->getRenduService()->generateRenduByTemplateCode(MailTemplates::INSCRIPTION_VALIDER, $vars);
        $mail = $this->getMailService()->sendMail($this->getMailInscrit($inscription), $rendu->getSujet(), $rendu->getCorps());
        $mail->setMotsClefs([$session->generateTag(), $rendu->getTemplate()->generateTag()]);
        $this->getMailService()->update($mail);

        return $mail;
    }

    public function triggerRefus(Inscription $inscription) : ?Mail
    {
        $session = $inscription->getSession();
        $vars = $this->prepVariables($inscription);

        $rendu = $this->getRenduService()->generateRenduByTemplateCode(MailTemplates::INSCRIPTION_REFUSER, $vars);
        $mail = $this->getMailService()->sendMail($this->getMailInscrit($inscription), $rendu->getSujet(), $rendu->getCorps());
        $mail->setMotsClefs([$session->generateTag(), $rendu->getTemplate()->generateTag()]);
        $this->getMailService()->update($mail);

        return $mail;
    }
}
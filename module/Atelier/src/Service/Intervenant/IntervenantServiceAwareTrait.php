<?php

namespace Atelier\Service\Intervenant;

trait IntervenantServiceAwareTrait
{
    private IntervenantService $intervenantService;

    public function getIntervenantService(): IntervenantService
    {
        return $this->intervenantService;
    }

    public function setIntervenantService(IntervenantService $intervenantService): void
    {
        $this->intervenantService = $intervenantService;
    }

}
<?php

namespace Atelier\Service\Intervenant;

use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\QueryBuilder;
use DoctrineModule\Persistence\ProvidesObjectManager;
use Atelier\Entity\Db\Intervenant;
use UnicaenApp\Exception\RuntimeException;
use Laminas\Mvc\Controller\AbstractActionController;
use UnicaenUtilisateur\Entity\Db\User;

class IntervenantService
{
    use ProvidesObjectManager;

    /** GESTION DES ENTITES *******************************************************************************************/

    public function create(Intervenant $intervenant) : Intervenant
    {
        $this->getObjectManager()->persist($intervenant);
        $this->getObjectManager()->flush($intervenant);
        return $intervenant;
    }

    public function update(Intervenant $intervenant) : Intervenant
    {
        $this->getObjectManager()->flush($intervenant);
        return $intervenant;
    }

    public function historise(Intervenant $intervenant) : Intervenant
    {
        $intervenant->historiser();
        $this->getObjectManager()->flush($intervenant);
        return $intervenant;
    }

    public function restore(Intervenant $intervenant) : Intervenant
    {
        $intervenant->dehistoriser();
        $this->getObjectManager()->flush($intervenant);
        return $intervenant;
    }

    public function delete(Intervenant $intervenant) : Intervenant
    {
        $this->getObjectManager()->remove($intervenant);
        $this->getObjectManager()->flush($intervenant);
        return $intervenant;
    }

    /** REQUETAGE *****************************************************************************************************/

    public function createQueryBuilder() : QueryBuilder
    {
        $qb = $this->getObjectManager()->getRepository(Intervenant::class)->createQueryBuilder('intervenant')
            ->addSelect('session')->leftjoin('intervenant.sessions', 'session')
            ->addSelect('utilisateur')->leftjoin('intervenant.utilisateur', 'utilisateur')
        ;
        return $qb;
    }

    /**
     * @param int|null $id
     * @return Intervenant|null
     */
    public function getIntervenant(?int $id) : ?Intervenant
    {
        $qb = $this->createQueryBuilder()
            ->andWhere('intervenant.id = :id')
            ->setParameter('id', $id);
        try {
            $result = $qb->getQuery()->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            throw new RuntimeException("Plusieurs Intervenant partagent le même id [" . $id . "]",0,$e);
        }
        return $result;
    }

    /**
     * @param AbstractActionController $controller
     * @param string $param
     * @return Intervenant|null
     */
    public function getRequestedIntervenant(AbstractActionController $controller, string $param = 'intervenant') : ?Intervenant
    {
        $id = $controller->params()->fromRoute($param);
        return $this->getIntervenant($id);
    }

    /** @return Intervenant[] */
    public function getIntervenants(bool $withHisto = false): array
    {
        $qb = $this->createQueryBuilder();
        if (!$withHisto) $qb = $qb->andWhere('intervenant.histoDestruction IS NULL');

        $result = $qb->getQuery()->getResult();
        return $result;
    }

    public function getIntervenantsAsOptions(bool $withHisto = false): array
    {
        $result = $this->getIntervenants($withHisto);
        $options = [];
        foreach ($result as $intervenant) {
            $options[$intervenant->getId()] = $intervenant->getDenomination();
        }
        return $options;
    }

    /** @return Intervenant[] */
    public function getIntervenantsWithFiltre(array $params): array
    {
        $type = $params['type']??null;
        $intervenantId = isset($params['intervenant'])?$params['intervenant']['id']:null;
        $rattachement = isset($params['rattachement'])?$params['rattachement']['label']:null;
        $compte = (isset($params['compte']) && $params['compte'] !== '')?($params['compte'] === '1'):null;

        $qb = $this->createQueryBuilder();
        if ($type) $qb = $qb->andWhere('intervenant.type = :type')->setParameter('type', $type);
        if ($intervenantId) $qb = $qb->andWhere('intervenant.id = :intervenantId')->setParameter('intervenantId', $intervenantId);
        if ($rattachement) $qb = $qb->andWhere('intervenant.attachement = :rattachement')->setParameter('rattachement', $rattachement);
        if ($compte === true) $qb = $qb->andWhere('intervenant.utilisateur IS NOT NULL');
        if ($compte === false) $qb = $qb->andWhere('intervenant.utilisateur IS NULL');

        $result = $qb->getQuery()->getResult();
        return $result;
    }

    /** Fonctions liées aux recherches ********************************************************************************/

    /** @return Intervenant[] */
    public function getIntervenantsByTerm(?string $term): array
    {
        $qb = $this->getObjectManager()->getRepository(Intervenant::class)->createQueryBuilder('intervenant')
            ->andWhere("LOWER(CONCAT(intervenant.nom, ' ', intervenant.prenom)) like :search OR LOWER(CONCAT(intervenant.prenom, ' ', intervenant.nom)) like :search")
            ->setParameter('search', '%'.strtolower($term).'%')
            ->orderBy("concat(intervenant.nom, ' ', intervenant.prenom)", 'ASC')
        ;
        $result = $qb->getQuery()->getResult();
        return $result;
    }

    public function formatIntervenantsJSON(array $intervenants): array
    {
        $result = [];
        /** @var Intervenant[] $intervenants */
        foreach ($intervenants as $intervenant) {
            $extra = ($intervenant->getEmail()) ?? "Aucune adresse électronique connue";
            $result[] = array(
                'id' => $intervenant->getId(),
                'label' => $intervenant->getDenomination(),
                'extra' => "<span class='badge' style='background-color: slategray;'>" . $extra . "</span>",
            );
        }
        usort($result, function ($a, $b) {
            return strcmp($a['label'], $b['label']);
        });
        return $result;
    }

    /** @return Intervenant[] */
    public function getRattachementByTerm(?string $term): array
    {
        $qb = $this->getObjectManager()->getRepository(Intervenant::class)->createQueryBuilder('intervenant')
            ->andWhere("LOWER(intervenant.rattachement) like :search")
            ->setParameter('search', '%'.strtolower($term).'%')
            ->orderBy('intervenant.rattachement', 'ASC')
        ;
        $result = $qb->getQuery()->getResult();
        return $result;
    }

    public function formatRattachementsJSON(array $intervenants): array
    {
        $result = [];
        /** @var Intervenant[] $intervenants */
        foreach ($intervenants as $intervenant) {
//            $extra = ($intervenant->getEmail()) ?? "Aucune adresse électronique connue";
            $result[] = array(
                'id' => $intervenant->getId(),
                'label' => $intervenant->getRattachement(),
//                'extra' => "<span class='badge' style='background-color: slategray;'>" . $extra . "</span>",
            );
        }
        usort($result, function ($a, $b) {
            return strcmp($a['label'], $b['label']);
        });
        return $result;
    }

    /** Gestion pour les rôles automatiques ***************************************************************************/

    public function getUsersInIntervenant() : array
    {
        $qb = $this->createQueryBuilder();
        $result = $qb->getQuery()->getResult();

        $users = [];
        /** @var Intervenant $item */
        foreach ($result as $item) {
            $users[] = $item->getUtilisateur();
        }
        return $users;
    }

    /** @return Intervenant[] */
    public function getIntervenantsByUser(?User $user) : array
    {
        if ($user === null) return [];

        $qb = $this->createQueryBuilder()
            ->andWhere('intervenant.histoDestruction IS NULL')
            ->andWhere('intervenant.utilisateur = :user')
            ->setParameter('user', $user)
        ;
        $result = $qb->getQuery()->getResult();
        return $result;
    }

    public function getIntervenantsByEmail(?string $email) : array
    {
        if ($email === null) return [];

        $qb = $this->createQueryBuilder()
            ->andWhere('intervenant.histoDestruction IS NULL')
            ->andWhere('intervenant.email = :email')
            ->setParameter('email', $email)
        ;
        $result = $qb->getQuery()->getResult();
        return $result;
    }


}
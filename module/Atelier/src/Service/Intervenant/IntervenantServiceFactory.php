<?php

namespace Atelier\Service\Intervenant;

use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;

class IntervenantServiceFactory
{

    /**
     * @param ContainerInterface $container
     * @return IntervenantService
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container) : IntervenantService
    {
        /**
         * @var EntityManager $entityManager
         */
        $entityManager = $container->get('doctrine.entitymanager.orm_default');

        $service = new IntervenantService();
        $service->setObjectManager($entityManager);
        return $service;
    }
}
<?php

namespace Atelier\Service\Programme;

use Doctrine\ORM\EntityManager;
use Psr\Container\ContainerInterface;

class ProgrammeServiceFactory {

    public function __invoke(ContainerInterface $container): ProgrammeService
    {
        /** @var EntityManager $entityManager */
        $entityManager = $container->get('doctrine.entitymanager.orm_default');

        $service = new ProgrammeService();
        $service->setObjectManager($entityManager);
        return $service;
    }
}
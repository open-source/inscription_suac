<?php

namespace Atelier\Service\Programme;

use DateTime;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\QueryBuilder;
use DoctrineModule\Persistence\ProvidesObjectManager;
use Atelier\Entity\Db\Programme;
use Laminas\Mvc\Controller\AbstractActionController;
use RuntimeException;

class ProgrammeService {
    use ProvidesObjectManager;

    /**  GESTION ENTITY ***********************************************************************************************/

    public function create(Programme $programme): Programme
    {
        $this->getObjectManager()->persist($programme);
        $this->getObjectManager()->flush($programme);
        return $programme;
    }

    public function update(Programme $programme): Programme
    {
        $this->getObjectManager()->flush($programme);
        return $programme;
    }

    public function historise(Programme $programme): Programme
    {
        $programme->historiser();
        $this->getObjectManager()->flush($programme);
        return $programme;
    }

    public function restore(Programme $programme): Programme
    {
        $programme->dehistoriser();
        $this->getObjectManager()->flush($programme);
        return $programme;
    }

    public function delete(Programme $programme): Programme
    {
        $this->getObjectManager()->remove($programme);
        $this->getObjectManager()->flush($programme);
        return $programme;
    }

    /** REQUETAGE  **********************************************************************************************/

    public function createQueryBuilder(): QueryBuilder
    {
        return $this->getObjectManager()->getRepository(Programme::class)->createQueryBuilder('programme')
            ->leftJoin('programme.ateliers', 'atelier')->addSelect('atelier');
    }

    public function getProgramme(?int $id): ?Programme
    {
        $qb = $this->createQueryBuilder()
            ->andWhere('programme.id = :id')->setParameter('id', $id);
        try {
            $result = $qb->getQuery()->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            throw new RuntimeException("Plusieurs [".Programme::class."] partagent le même id [".$id."]",0,$e);
        }
        return $result;
    }

    public function getRequestedProgramme(AbstractActionController $controller, string $param='programme'): ?Programme
    {
        $id = $controller->params()->fromRoute($param);
        return $this->getProgramme($id);
    }

    /** @return Programme[] */
    public function getProgrammes(string $champ = 'dateDebut', string $ordre = 'DESC', bool $withHisto = false): array
    {
        $qb = $this->createQueryBuilder()
            ->orderBy('programme.' . $champ, $ordre);

        if (!$withHisto) {
            $qb = $qb->andWhere('programme.histoDestruction IS NULL');
        }

        return $qb->getQuery()->getResult();
    }

     public function createQueryBuilderWithFiltre(array $params = [],): QueryBuilder
    {
        foreach ($params as $key => $value) {
            if ($value == null) {
                unset($params[$key]);
            }
        }

        $qb = $this->createQueryBuilder();

        $qb->leftjoin('atelier.sessions', 'session');
        $qb->leftjoin('session.site', 'site');

        if(array_key_exists('site', $params) ) {
            $qb = $qb->andWhere('site.id = :site')->setParameter('site', $params['site']);
        }

        if(array_key_exists('ville',$params)){
            $qb = $qb->join('site.ville', 'ville');
            $qb = $qb->andWhere('ville.id = :ville')->setParameter('ville', $params['ville']);
        }

        return $qb;
    }

    /** @return Programme[] */
    public function getProgrammesWithFiltre(array $params = [], string $champ = 'dateDebut', string $ordre = 'DESC', bool $withHisto = false): array
    {
        $qb = $this->createQueryBuilderWithFiltre($params)
            ->orderBy('programme.' . $champ, $ordre);

        if (!$withHisto) {
            $qb = $qb->andWhere('programme.histoDestruction IS NULL');
        }

        return $qb->getQuery()->getResult();
    }

   /** @return Programme[] */
    public function getProgrammesActifsWithFiltre(array $params = [], ?DateTime $date = null, string $champ = 'dateDebut', string $ordre = 'DESC', bool $withHisto = false): array
    {
        $qb = $this->createQueryBuilderWithFiltre($params)
            ->orderBy('programme.' . $champ, $ordre);

        if (!$withHisto) {
            $qb = $qb->andWhere('programme.histoDestruction IS NULL');
        }

        if ($date === null) $date = new DateTime();

        $qb = $qb->andWhere('programme.dateDebut IS NULL or programme.dateDebut <= :date')
            ->andWhere('programme.dateFin IS NULL or programme.dateFin >= :date')
            ->setParameter('date', $date)
            ->andWhere('programme.histoDestruction IS NULL')
        ;

        return $qb->getQuery()->getResult();
    }
}
<?php

namespace Atelier\Service\Programme;

trait ProgrammeServiceAwareTrait
{
    private ProgrammeService $programmeService;

    public function getProgrammeService(): ProgrammeService
    {
        return $this->programmeService;
    }

    public function setProgrammeService(ProgrammeService $programmeService): void
    {
        $this->programmeService = $programmeService;
    }

}
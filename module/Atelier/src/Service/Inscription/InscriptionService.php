<?php

namespace Atelier\Service\Inscription;

use Atelier\Entity\Db\Atelier;
use Atelier\Entity\Db\Session;
use Etudiant\Entity\Db\Etudiant;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\QueryBuilder;
use DoctrineModule\Persistence\ProvidesObjectManager;
use Atelier\Entity\Db\Inscription;
use Atelier\Provider\Etat\SessionEtats;
use Laminas\Mvc\Controller\AbstractActionController;
use RuntimeException;
use UnicaenUtilisateur\Entity\Db\UserInterface;

class InscriptionService
{
    use ProvidesObjectManager;

    /** Gestion des  entités ******************************************************************************************/

    public function create(Inscription $inscription): Inscription
    {
        $this->getObjectManager()->persist($inscription);
        $this->getObjectManager()->flush($inscription);
        return $inscription;
    }

    public function update(Inscription $inscription): Inscription
    {
        $this->getObjectManager()->flush($inscription);
        return $inscription;
    }

    public function historise(Inscription $inscription): Inscription
    {
        $inscription->historiser();
        $this->getObjectManager()->flush($inscription);
        return $inscription;
    }

    public function restore(Inscription $inscription): Inscription
    {
        $inscription->dehistoriser();
        $this->getObjectManager()->flush($inscription);
        return $inscription;
    }

    public function delete(Inscription $inscription): Inscription
    {
        $this->getObjectManager()->remove($inscription);
        $this->getObjectManager()->flush($inscription);
        return $inscription;
    }

    /** REQUETAGE *****************************************************************************************************/

    public function createQueryBuilder(): QueryBuilder
    {
        return $this->getObjectManager()->getRepository(Inscription::class)->createQueryBuilder('inscription')
            ->leftJoin('inscription.session', 'session')->addSelect('session')
            ->leftJoin('session.atelier', 'atelier')->addSelect('atelier')
            ->leftJoin('inscription.etudiant', 'etudiant')->addSelect('etudiant')
        ;
    }

    /** @return Inscription[] */
    public function getInscriptions(string $champ = 'histoCreation', string $ordre = 'DESC', bool $withHisto = false): array
    {
        $qb = $this->createQueryBuilder()
            ->orderBy('inscription.' . $champ, $ordre);
        if (!$withHisto) $qb = $qb->andWhere('inscription.histoDestruction IS NULL');

        return $qb->getQuery()->getResult();
    }

    public function getInscription(?int $id): ?Inscription
    {
        $qb = $this->createQueryBuilder()
            ->andWhere('inscription.id = :id')->setParameter('id', $id);

        try {
            $result = $qb->getQuery()->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            throw new RuntimeException("Plusieurs [" . Inscription::class . "] partagent le même id [" . $id . "]", 0, $e);
        }
        return $result;
    }

    public function getRequestedInscription(AbstractActionController $controller, string $param = 'inscription'): ?Inscription
    {
        $id = $controller->params()->fromRoute($param);
        return $this->getInscription($id);
    }

    public function getInscriptionByUser(UserInterface $user): array
    {
        $qb = $this->createQueryBuilder()
            ->andWhere('etudiant.login = :login')
            ->setParameter('login', $user->getUsername())
            ->andWhere('inscription.histoDestruction IS NULL')
            ->leftJoin('session.atelier', 'atelier')->addSelect('atelier')
            ->orderBy('formation.libelle', 'ASC');

        $qb = $qb
            ->leftJoin('session.etats', 'etat')->addSelect('etat')
            ->leftJoin('etat.type', 'etype')->addSelect('etype')
            ->andWhere('etat.histoDestruction IS NULL')
            ->andWhere('etype.code <> :code')->setParameter('code', SessionEtats::ETAT_CLOTURE_INSTANCE);

        return $qb->getQuery()->getResult();
    }

    /** @return  Inscription[] */
    public function getInscriptionsByEtudiant(Etudiant $etudiant, ?array $etatsCodes = null): array
    {
        $qb = $this->createQueryBuilder()
            ->andWhere('inscription.etudiant = :etudiant')->setParameter('etudiant', $etudiant)
            ->andWhere('inscription.histoDestruction IS NULL')
            ->orderBy('atelier.libelle', 'ASC');

        if ($etatsCodes !== null) {
            $qb = $qb
                ->leftJoin('session.etats', 'etat')->addSelect('etat')
                ->leftJoin('etat.type', 'etype')->addSelect('etype')
                ->andWhere('etat.histoDestruction IS NULL')
                ->andWhere('etype.code in (:code)')->setParameter('code', $etatsCodes);
        }
        return $qb->getQuery()->getResult();
    }

    /**
     * @var Etudiant[] $etudiants
     * @return  Inscription[]
     */
    public function getInscriptionsByEtudiants(array $etudiants): array
    {
        $qb = $this->createQueryBuilder()
            ->andWhere('inscription.etudiant in (:etudiants)')->setParameter('etudiants', $etudiants)
            ->andWhere('inscription.histoDestruction IS NULL')
            ->leftJoin('session.atelier', 'atelier')->addSelect('atelier')
            ->orderBy('atelier.libelle', 'ASC');

        $qb = $qb
            ->leftJoin('session.etats', 'etat')->addSelect('etat')
            ->leftJoin('etat.type', 'etype')->addSelect('etype')
            ->andWhere('etat.histoDestruction IS NULL')
            ->andWhere('etype.code <> :code')->setParameter('code', SessionEtats::ETAT_CLOTURE_INSTANCE);

        return $qb->getQuery()->getResult();
    }

//    public function getInscriptionsWithFiltre(array $params)
//    {
//        $qb = $this->createQueryBuilder()->orderBy('inscription.histoCreation', 'asc');
//
//        if (isset($params['etat'])) {
//            $qb = Inscription::decorateWithEtatsCodes($qb, 'inscription', [$params['etat']]);
//        }
//        if (isset($params['historise'])) {
//            if ($params['historise'] === '1') $qb = $qb->andWhere('inscription.histoDestruction IS NOT NULL');
//            if ($params['historise'] === '0') $qb = $qb->andWhere('inscription.histoDestruction IS NULL');
//        }
//        if (isset($params['annee']) and $params['annee'] !== '') {
//            $annee = (int)$params['annee'];
//            $debut = DateTime::createFromFormat('d/m/Y', '01/09/' . $annee);
//            $fin = DateTime::createFromFormat('d/m/Y', '31/08/' . ($annee + 1));
//            $qb = $qb
//                ->leftJoin('session.journees', 'journee')->addSelect('journee')
//                ->andWhere('journee.jour >= :debut')->setParameter('debut', $debut)
//                ->andWhere('journee.jour <= :fin')->setParameter('fin', $fin);
//        }
//
//        $result = $qb->getQuery()->getResult();
//        return $result;
//    }

    /** @return Inscription[] */
    public function getInscriptionsByAtelier(Atelier $atelier): array
    {
        $qb = $this->createQueryBuilder()
            ->andWhere('session.atelier = :atelier')->setParameter('atelier', $atelier)
            ->andWhere('session.histoDestruction IS NULL')
            ->andWhere('inscription.histoDestruction IS NULL')
        ;
        $result = $qb->getQuery()->getResult();
        return $result;
    }

    public function getInscriptionsBySession(?Session $session): array
    {
        $qb = $this->createQueryBuilder()
            ->andWhere('inscription.session = :session')->setParameter('session', $session)
            ->andWhere('inscription.histoDestruction IS NULL')
        ;
        $result = $qb->getQuery()->getResult();
        return $result;
    }

    /** FACADE ********************************************************************************************************/


}
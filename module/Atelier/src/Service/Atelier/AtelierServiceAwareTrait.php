<?php

namespace Atelier\Service\Atelier;

trait AtelierServiceAwareTrait
{
    private AtelierService $atelierService;

    public function getAtelierService() : AtelierService
    {
        return $this->atelierService;
    }

    public function setAtelierService(AtelierService $atelierService) : void
    {
        $this->atelierService = $atelierService;
    }
}
<?php

namespace Atelier\Service\Atelier;

use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;

class AtelierServiceFactory
{

    /**
     * @param ContainerInterface $container
     * @return AtelierService
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container) : AtelierService
    {
        /**
         * @var EntityManager $entityManager
         */
        $entityManager = $container->get('doctrine.entitymanager.orm_default');

        $service = new AtelierService();
        $service->setObjectManager($entityManager);
        return $service;
    }
}

<?php

namespace Atelier\Service\Atelier;

use Atelier\Entity\Db\Session;
use Atelier\Provider\Etat\SessionEtats;
use DateTime;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\QueryBuilder;
use DoctrineModule\Persistence\ProvidesObjectManager;
use Atelier\Entity\Db\Atelier;
use Atelier\Entity\Db\Groupe;
use Laminas\Mvc\Controller\AbstractActionController;
use UnicaenApp\Exception\RuntimeException;

class AtelierService
{
    use ProvidesObjectManager;

    /** GESTION DES ENTITÉS *******************************************************************************************/

    public function create(Atelier $atelier): Atelier
    {
        $this->getObjectManager()->persist($atelier);
        $this->getObjectManager()->flush($atelier);
        return $atelier;
    }

    public function update(Atelier $atelier): Atelier
    {
        $this->getObjectManager()->flush($atelier);
        return $atelier;
    }

    public function historise(Atelier $atelier): Atelier
    {
        $atelier->historiser();
        $this->getObjectManager()->flush($atelier);
        return $atelier;
    }

    public function restore(Atelier $atelier): Atelier
    {
        $atelier->dehistoriser();
        $this->getObjectManager()->flush($atelier);
        return $atelier;
    }

    public function delete(Atelier $atelier): Atelier
    {
        $this->getObjectManager()->remove($atelier);
        $this->getObjectManager()->flush($atelier);
        return $atelier;
    }

    /** REQUÊTAGES ****************************************************************************************************/

    public function createQueryBuilder(): QueryBuilder
    {
        return $this->getObjectManager()->getRepository(Atelier::class)->createQueryBuilder('atelier')
            ->addSelect('groupe')->leftJoin('atelier.groupe', 'groupe')
        ;
    }
    public function createQueryBuilderWithFiltre(array $params = [],): QueryBuilder
    {
        foreach ($params as $key => $value) {
            if ($value == null) {
                unset($params[$key]);
            }
        }

        $qb = $this->createQueryBuilder();

        $qb->leftjoin('atelier.sessions', 'session');
        $qb->leftjoin('session.site', 'site');

        if(array_key_exists('site', $params) ) {
            $qb = $qb->andWhere('site.id = :site')->setParameter('site', $params['site']);
        }

        if(array_key_exists('ville',$params)){
            $qb = $qb->join('site.ville', 'ville');
            $qb = $qb->andWhere('ville.id = :ville')->setParameter('ville', $params['ville']);
        }

        if(array_key_exists('archivage',$params)){
            if($params['archivage'] == '0') {
                $qb = $qb->andWhere('atelier.histoDestruction IS NULL');
            }else{
                $qb = $qb->andWhere('atelier.histoDestruction IS NOT NULL');
            }
        }

        return $qb;
    }


    /** @return Atelier[] */
    public function getAteliers(string $champ = 'libelle', string $ordre = 'ASC', bool $withHisto=false): array
    {
        $qb = $this->createQueryBuilder()
            ->orderBy('groupe.libelle, atelier.' . $champ, $ordre);
        if (!$withHisto) $qb = $qb->andWhere('atelier.histoDestruction IS NULL');
        $result = $qb->getQuery()->getResult();
        return $result;
    }

    /** @return Atelier[] */
    public function getAteliersWithFiltre(array $params = [], string $champ = 'libelle', string $ordre = 'DESC', bool $withHisto = false): array
    {
        $qb = $this->createQueryBuilderWithFiltre($params)
            ->orderBy('groupe.libelle, atelier.' . $champ, $ordre);

        if (!$withHisto) {
            $qb = $qb->andWhere('programme.histoDestruction IS NULL');
        }

        return $qb->getQuery()->getResult();
    }


    /** @return Atelier[] */
    public function getAteliersByGroupe(?Groupe $groupe, string $champ = 'libelle', string $ordre = 'ASC'): array
    {
        $qb = $this->createQueryBuilder()
            ->orderBy('groupe.libelle, atelier.' . $champ, $ordre);

        if ($groupe !== null) {
            $qb = $qb->andWhere('atelier.groupe = :groupe')
                ->setParameter('groupe', $groupe);
        }

        $result = $qb->getQuery()->getResult();
        return $result;
    }

    public function getAtelier(?int $id): ?Atelier
    {
        if ($id === null) return null;
        $qb = $this->createQueryBuilder()
            ->andWhere('atelier.id = :id')
            ->setParameter('id', $id);

        try {
            $result = $qb->getQuery()->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            throw new RuntimeException("Plusieurs [".Atelier::class."] portent le même identifiant [" . $id . "]", $e);
        }
        return $result;
    }

    public function getRequestedAtelier(AbstractActionController $controller, string $paramName = 'atelier'): ?Atelier
    {
        $id = $controller->params()->fromRoute($paramName);
        return $this->getAtelier($id);
    }


    /**
     * @return array
     */
    public function getAtelierAsOptions(): array
    {
        $ateliers = $this->getAteliers();

        $result = [];
        foreach ($ateliers as $atelier) {
            $result[$atelier->getId()] = $atelier->getLibelle();
        }
        return $result;
    }

    /**
     * @param Atelier[] $formationsAlreadyUsed
     * @return Atelier[]
     */
    public function getFormationsDisponiblesAsOptions(array $formationsAlreadyUsed = []): array
    {
        $formations = $this->getAteliers();

        $result = [];
        foreach ($formations as $formation) {
            $found = false;
            if ($formationsAlreadyUsed !== null) {
                foreach ($formationsAlreadyUsed as $used) {
                    if ($used->getId() === $formation->getId()) {
                        $found = true;
                        break;
                    }
                }
            }
            if (!$found) $result[] = $formation;
        }

        return Atelier::generateOptions($result);
    }



    /** RECHERCHES ****************************************************************************************************/

    /**
     * @param string $texte
     * @return Atelier[]
     */
    public function findAtelierByTerm(string $texte): array
    {
        $qb = $this->createQueryBuilder()
            ->andWhere("LOWER(CONCAT(atelier.libelle, ' ', groupe.libelle)) like :search OR LOWER(CONCAT(groupe.libelle, ' ', atelier.libelle)) like :search")
            ->setParameter('search', '%' . strtolower($texte) . '%');
        $result = $qb->getQuery()->getResult();

        return $result;
    }

    /**
     * @param Atelier[] $ateliers
     * @return array
     */
    public function formatAtelierJSON(array $ateliers): array
    {
        $result = [];
        foreach ($ateliers as $atelier) {
            $groupe = $atelier->getGroupe();
            $result[] = array(
                'id' => $atelier->getId(),
                'label' => (($groupe !== null) ? $groupe->getLibelle() : "Sans groupe ") . " > " . $atelier->getLibelle(),
            );
        }
        usort($result, function ($a, $b) {
            return strcmp($a['label'], $b['label']);
        });
        return $result;
    }

    /** FACADE *************************************************************************************/

    public function createAtelier(string $libelle, ?Groupe $groupe): Atelier
    {
        $atelier = new Atelier();
        $atelier->setLibelle($libelle);
        $atelier->setGroupe($groupe);
        $this->create($atelier);
        return $atelier;
    }

    /** @return Session[] */
    public function getSessionsActives(Atelier $atelier, ?DateTime $date = null): array
    {
        if ($date === null) $date = new DateTime();
        $sessions = $atelier->getSessions();
        $sessions = array_filter($sessions, function(Session $s) use($date) {
            return $s->estNonHistorise()
                AND in_array($s->getEtatActif()->getType()->getCode(), [SessionEtats::ETAT_INSCRIPTION_OUVERTE, SessionEtats::PREPARATION])
                AND $s->getDerniereSeance()->getDateTimeFin() > $date
                ; });
        return $sessions;
    }
}

<?php

namespace Atelier\Service\Session;

use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\QueryBuilder;
use DoctrineModule\Persistence\ProvidesObjectManager;
use Atelier\Entity\Db\Atelier;
use Atelier\Entity\Db\Session;
use Atelier\Entity\Db\Inscription;
use Atelier\Provider\Etat\SessionEtats;
use Atelier\Service\Notification\NotificationServiceAwareTrait;
use Laminas\Mvc\Controller\AbstractActionController;
use UnicaenApp\Exception\RuntimeException;
use UnicaenEtat\Service\EtatInstance\EtatInstanceServiceAwareTrait;
use UnicaenEtat\Service\EtatType\EtatTypeServiceAwareTrait;
use UnicaenParametre\Service\Parametre\ParametreServiceAwareTrait;

class SessionService
{
    use ProvidesObjectManager;
    use EtatTypeServiceAwareTrait;
    use EtatInstanceServiceAwareTrait;
    use NotificationServiceAwareTrait;
    use ParametreServiceAwareTrait;


    /** GESTION DES ENTITÉS *******************************************************************************************/

    public function create(Session $session): Session
    {
        $this->getObjectManager()->persist($session);
        $this->getObjectManager()->flush($session);
        return $session;
    }

    public function update(Session $session): Session
    {
        $this->getObjectManager()->flush($session);
        return $session;
    }

    public function historise(Session $session): Session
    {
        $session->historiser();
        $this->getObjectManager()->flush($session);
        return $session;
    }

    public function restore(Session $session): Session
    {
        $session->dehistoriser();
        $this->getObjectManager()->flush($session);
        return $session;
    }

    public function delete(Session $session): Session
    {
        $this->getObjectManager()->remove($session);
        $this->getObjectManager()->flush($session);
        return $session;
    }

    /** REQUÊTAGE *****************************************************************************************************/

    /**
     * @return QueryBuilder
     */
    public function createQueryBuilder(): QueryBuilder
    {
        $qb = $this->getObjectManager()->getRepository(Session::class)->createQueryBuilder('session')
            ->addSelect('atelier')->join('session.atelier', 'atelier')
            ->addSelect('groupe')->leftjoin('atelier.groupe', 'groupe')
//            ->addSelect('journee')->leftJoin('session.journees', 'journee')
            ->addSelect('inscription')->leftJoin('session.inscriptions', 'inscription')
            ->addSelect('ietat')->leftjoin('inscription.etats', 'ietat')
            ->addSelect('ietype')->leftjoin('ietat.type', 'ietype')
            ->addSelect('etat')->leftjoin('session.etats', 'etat')
            ->addSelect('etype')->leftjoin('etat.type', 'etype')
            ->addSelect('intervenant')->leftjoin('session.intervenants', 'intervenant')
            ->andWhere('etat.histoDestruction IS NULL')
            ->andWhere('ietat.histoDestruction IS NULL')
        ;
        return $qb;
    }

    /** @return Session[] */
    public function getSessionsByAtelier(Atelier $atelier): array
    {
        $qb = $this->createQueryBuilder()
            ->andWhere('atelier.id = :id')->setParameter('id', $atelier->getId());

        return $qb->getQuery()->getResult();
    }

    /** @return Session[] */
    public function getSessionsByEtat(string $etatCode): array
    {
        $qb = $this->createQueryBuilder()
            ->andWhere('etype.code = :code')
            ->setParameter('code', $etatCode);
        return $qb->getQuery()->getResult();
    }

    /**
     * @param int|null $id
     * @return Session|null
     */
    public function getSession(?int $id): ?Session
    {
        $qb = $this->createQueryBuilder()
            ->andWhere('session.id = :id')->setParameter('id', $id)
        ;

        try {
            $result = $qb->getQuery()->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            throw new RuntimeException("Plusieurs [".Session::class."] partagent le même id [" . $id . "]", 0, $e);
        }
        return $result;
    }

    /**
     * @param AbstractActionController $controller
     * @param string $param
     * @return Session|null
     */
    public function getRequestedSession(AbstractActionController $controller, string $param = 'session'): ?Session
    {
        $id = $controller->params()->fromRoute($param);
        return $this->getSession($id);
    }

    /** @return Session[] */
    public function getSessions(string $champ = 'id', string $ordre = 'ASC'): array
    {
        $qb = $this->createQueryBuilder()
            ->orderBy('session.' . $champ, $ordre);
        return $qb->getQuery()->getResult();
    }

    /** @return Session[] */
    public function getSessionsWithFiltre(array $params = [], string $champ = 'id', string $ordre = 'ASC'): array
    {
        foreach ($params as $key => $value) {
            if ($value == null) {
                unset($params[$key]);
            }
        }

        $qb = $this->createQueryBuilder()
            ->orderBy('session.' . $champ, $ordre);

        $intervenantId = isset($params['intervenant'])?$params['intervenant']['id']:null;
        $typeId = $params['etat'] ?? null;
        $groupeId = $params['groupe'] ?? null;

        if ($intervenantId) $qb = $qb->andWhere('intervenant.id = :intervenantId')->setParameter('intervenantId', $intervenantId);
        if ($typeId) $qb = $qb->andWhere('etype.id = :etype')->setParameter('etype', $typeId);
        if ($groupeId) $qb = $qb->andWhere('groupe.id = :groupe')->setParameter('groupe', $groupeId);

        $qb->leftjoin('session.site', 'site');
        if(array_key_exists('site', $params) ) {
            $qb = $qb->andWhere('site.id = :site')->setParameter('site', $params['site']);
        }

        if(array_key_exists('ville',$params)){
            $qb = $qb->join('site.ville', 'ville');
            $qb = $qb->andWhere('ville.id = :ville')->setParameter('ville', $params['ville']);
        }

        return $qb->getQuery()->getResult();
    }

    /** FACADE  *******************************************************************************************************/

    public function createSession(Atelier $atelier): Session
    {
        $session = new Session();
        $session->setAutoInscription(true);
//        $session->setNbPlacePrincipale($this->getParametreService()->getParametreByCode(FormationParametres::TYPE, FormationParametres::NB_PLACE_PRINCIPALE)->getValeur());
//        $session->setNbPlaceComplementaire($this->getParametreService()->getParametreByCode(FormationParametres::TYPE, FormationParametres::NB_PLACE_COMPLEMENTAIRE)->getValeur());
        $session->setAtelier($atelier);
        $this->create($session);
        $this->getEtatInstanceService()->setEtatActif($session, SessionEtats::ETAT_CREATION_EN_COURS, SessionEtats::TYPE);
        $this->update($session);

        return $session;
    }

    /** Fonction de gestion des états de la session *******************************************************************/

    public function recreation(Session $session): Session
    {
        $this->getEtatInstanceService()->setEtatActif($session, SessionEtats::ETAT_CREATION_EN_COURS);
        $this->update($session);

        return $session;
    }

    public function ouvrirInscription(Session $session): Session
    {
        $this->getEtatInstanceService()->setEtatActif($session, SessionEtats::ETAT_INSCRIPTION_OUVERTE);
        $this->update($session);

        //notification abonnement
//        $abonnements = $instance->getFormation()->getAbonnements();
//        foreach ($abonnements as $abonnement) {
//            if ($abonnement->estNonHistorise()) $this->getNotificationService()->triggerNouvelleSession($instance, $abonnement);
//        }

        return $session;
    }

    public function fermerInscription(Session $session): Session
    {
        $this->getEtatInstanceService()->setEtatActif($session, SessionEtats::ETAT_INSCRIPTION_FERMEE);
        $this->update($session);

/*      désactiver l'envoi de mail pour les étudiants sur liste principale
        foreach ($session->getListePrincipale() as $inscription) {
            $this->getNotificationService()->triggerListePrincipale($inscription);
            //TODO ABONN
//            $etudiant = $inscription->getEtudiant();
//            $action = $inscription->getSession()->getAction();
//            $abonnement = $this->getAbonnementService()->getAbonnementByAgentAndFormation($etudiant, $action);
//            if ($abonnement !== null) $this->getAbonnementService()->retirerAbonnement($etudiant, $action);
        }
*/
        foreach ($session->getListeComplementaire() as $inscription) {
            $this->getNotificationService()->triggerListeComplementaire($inscription);
            //TODO ABONN
//            $etudiant = $inscription->getEtudiant();
//            $action = $inscription->getSession()->getAction();
//            $abonnement = $this->getAbonnementService()->getAbonnementByAgentAndFormation($etudiant, $action);
//            if ($abonnement === null) $this->getAbonnementService()->ajouterAbonnement($etudiant, $action);
        }

        //TODO RAPPEL
//        $debut = $session->getDebut();
//        if ($debut !== null) {
//            $dateRappel = DateTime::createFromFormat('d/m/Y H:i', $session->getDebut() . " 08:00");
//            $dateRappel->sub(new DateInterval('P4D'));
//            $this->getRappelAgentAvantFormationService()->creer($session, $dateRappel);
//        } else {
//            throw new RuntimeException("Aucun événement/rappel ne peut être créé sans au moins une séance de planifiée", 0);
//        }

        return $session;
    }

    public function envoyerConvocation(Session $session): Session
    {
        $this->getEtatInstanceService()->setEtatActif($session, SessionEtats::ETAT_FORMATION_CONVOCATION);
        $this->update($session);
        foreach ($session->getListePrincipale() as $inscription) {
            if ($inscription->estNonHistorise()) $this->getNotificationService()->triggerConvocation($inscription);
        }
        return $session;
    }

//    public function envoyerEmargement(Session $session): Session
//    {
//        $this->update($session);
//        $this->getNotificationService()->triggerLienPourEmargement($session);
//        return $session;
//    }

    public function demanderRetour(Session $session): Session
    {
        $this->getEtatInstanceService()->setEtatActif($session, SessionEtats::ETAT_ATTENTE_RETOURS);
        $this->update($session);
        foreach ($session->getListePrincipale() as $inscription) {
            $this->getNotificationService()->triggerDemanderRetour($inscription);
        }
        return $session;
    }

    public function cloturer(Session $session): Session
    {
        $this->getEtatInstanceService()->setEtatActif($session, SessionEtats::ETAT_CLOTURE_INSTANCE);
        $this->update($session);
        return $session;
    }

    public function annuler(Session $session): Session
    {
        $this->getEtatInstanceService()->setEtatActif($session, SessionEtats::ETAT_SESSION_ANNULEE);
        $this->update($session);
        foreach ($session->getInscriptions() as $inscription) {
            $this->getNotificationService()->triggerSessionAnnulee($inscription);

            //TODO ABONNEMENT
//            $etudiant = $inscription->getEtudiant();
//            $action = $inscription->getSession()->getAction();
//            $abonnement = $this->getAbonnementService()->getAbonnementByAgentAndFormation($etudiant, $action);
//            if ($abonnement === null) $this->getAbonnementService()->ajouterAbonnement($etudiant, $action);
        }
        return $session;
    }

    public function reouvrir(Session $session): Session
    {
        $this->getEtatInstanceService()->setEtatActif($session, SessionEtats::ETAT_CREATION_EN_COURS);
        $this->update($session);
        return $session;
    }

    /** Fonction de classement des inscriptions ***********************************************************************/

    public function classerInscription(Inscription $inscription): Inscription
    {
        $session = $inscription->getSession();
        $placePrincipale = $session->getPlaceDisponible(Inscription::PRINCIPALE);
        if ($session->getNbPlacePrincipale() > $placePrincipale) {
            $inscription->setListe(Inscription::PRINCIPALE);
            $this->getObjectManager()->flush($inscription);
            return $inscription;
        }
        $placeComplementaire = $session->getPlaceDisponible(Inscription::COMPLEMENTAIRE);
        if ($session->getNbPlaceComplementaire() > $placeComplementaire) {
            $inscription->setListe(Inscription::COMPLEMENTAIRE);
            $this->getObjectManager()->flush($inscription);
            return $inscription;
        }
        return $inscription;
    }

    /** @return Session[] */
    public function getSessionByTerm(mixed $term): array
    {
        $qb = $this->createQueryBuilder();
        $qb = $qb->andWhere("LOWER(formation.libelle) like :search")
            ->setParameter('search', '%' . strtolower($term) . '%');

        return $qb->getQuery()->getResult();
    }

    public function formatSessionJSON(array $sessions): array
    {
        $result = [];
        /** @var Session[] $sessions */
        foreach ($sessions as $session) {
            $formation = $session->getAtelier();
            $groupe = $formation->getGroupe();
            $result[] = array(
                'id' => $session->getId(),
                'label' => ($groupe ? $groupe->getLibelle() : "Acucun groupe") . " > " . $session->getAtelier()->getLibelle(),
                'extra' => "<span class='badge' style='background-color: slategray;'>" . $session->getPeriode() . "</span>",
            );
        }
        usort($result, function ($a, $b) {
            return strcmp($a['label'], $b['label']);
        });
        return $result;
    }



}
<?php

namespace Atelier\Service\Session;

trait SessionServiceAwareTrait
{

    private SessionService $sessionService;

    public function getSessionService(): SessionService
    {
        return $this->sessionService;
    }

    public function setSessionService(SessionService $sessionService): void
    {
        $this->sessionService = $sessionService;
    }
}
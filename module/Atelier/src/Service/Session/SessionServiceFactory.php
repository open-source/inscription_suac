<?php

namespace Atelier\Service\Session;

use Doctrine\ORM\EntityManager;
use Atelier\Service\Notification\NotificationService;
use Interop\Container\ContainerInterface;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use UnicaenEtat\Service\EtatInstance\EtatInstanceService;
use UnicaenEtat\Service\EtatType\EtatTypeService;
use UnicaenParametre\Service\Parametre\ParametreService;

class SessionServiceFactory
{

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): SessionService
    {
        /**
         * @var EntityManager $entityManager
         * @var EtatInstanceService $etatInstanceService
         * @var EtatTypeService $etatTypeService
         * @var NotificationService $notificationService
         * @var ParametreService $parametreService
         */
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        $etatInstanceService = $container->get(EtatInstanceService::class);
        $etatTypeService = $container->get(EtatTypeService::class);
        $notificationService = $container->get(NotificationService::class);
        $parametreService = $container->get(ParametreService::class);

        $service = new SessionService();
        $service->setObjectManager($entityManager);
        $service->setEtatInstanceService($etatInstanceService);
        $service->setEtatTypeService($etatTypeService);
        $service->setNotificationService($notificationService);
        $service->setParametreService($parametreService);
        return $service;
    }
}
<?php

namespace Atelier\Service\Presence;

use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\QueryBuilder;
use DoctrineModule\Persistence\ProvidesObjectManager;
use Atelier\Entity\Db\Session;
use Atelier\Entity\Db\Inscription;
use Atelier\Entity\Db\Presence;
use Atelier\Entity\Db\Seance;
use Laminas\Mvc\Controller\AbstractActionController;
use UnicaenApp\Exception\RuntimeException;

class PresenceService
{
    use ProvidesObjectManager;

    /** GESTION DES ENTITÉS *******************************************************************************************/

    public function create(Presence $presence): Presence
    {
        $this->getObjectManager()->persist($presence);
        $this->getObjectManager()->flush($presence);
        return $presence;
    }

    public function update(Presence $presence): Presence
    {
        $this->getObjectManager()->flush($presence);
        return $presence;
    }

    public function historise(Presence $presence): Presence
    {
        $presence->historiser();
        $this->getObjectManager()->flush($presence);
        return $presence;
    }

    public function restore(Presence $presence): Presence
    {
        $presence->dehistoriser();
        $this->getObjectManager()->flush($presence);
        return $presence;
    }

    public function delete(Presence $presence): Presence
    {
        $this->getObjectManager()->remove($presence);
        $this->getObjectManager()->flush($presence);
        return $presence;
    }

    /** REQUETAGE *****************************************************************************************************/

    /**
     * @return QueryBuilder
     */
    public function createQueryBuilder(): QueryBuilder
    {
        return $this->getObjectManager()->getRepository(Presence::class)->createQueryBuilder('presence')
            ->addSelect('seance')->join('presence.seance', 'seance')
            ->addSelect('session')->join('seance.session', 'session')
            ->addSelect('atelier')->join('session.atelier', 'atelier')
            ->addSelect('inscription')->join('presence.inscription', 'inscription');
    }

    public function getPresence(?int $id): ?Presence
    {
        $qb = $this->createQueryBuilder()
            ->andWhere('presence.id = :id')
            ->setParameter('id', $id);

        try {
            $result = $qb->getQuery()->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            throw new RuntimeException("Plusieurs Presence partagent le même id [" . $id . "].", 0, $e);
        }
        return $result;
    }

    public function getRequestedPresence(AbstractActionController $controller, string $param = 'presence'): ?Presence
    {
        $id = $controller->params()->fromRoute($param);
        return $this->getPresence($id);
    }

    public function getPresenceBySeanceAndInscription(Seance $seance, Inscription $inscription): ?Presence
    {
        $qb = $this->createQueryBuilder()
            ->andWhere('presence.seance = :seance')
            ->andWhere('presence.inscription = :inscription')
            ->setParameter('seance', $seance)
            ->setParameter('inscription', $inscription)
            ->andWhere('presence.histoDestruction IS NULL');

        try {
            $result = $qb->getQuery()->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            throw new RuntimeException("Plusieurs Presence (non historisées) partagent la même journée [" . $seance->getId() . "] et le même inscrit [" . $inscription->getId() . "]", 0, $e);
        }
        return $result;
    }

    /** @return Presence[] */
    public function getPresencesBySession(Session $session): array
    {
        $qb = $this->createQueryBuilder()
            ->andWhere('seance.session = :session')
            ->setParameter('session', $session);

        return $qb->getQuery()->getResult();
    }

    /** @return Presence[] */
    public function getPresences(): array
    {
        $qb = $this->getObjectManager()->getRepository(Presence::class)->createQueryBuilder('presence');
        return $qb->getQuery()->getResult();
    }


}
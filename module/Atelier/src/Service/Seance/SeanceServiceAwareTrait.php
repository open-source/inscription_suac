<?php

namespace Atelier\Service\Seance;


trait SeanceServiceAwareTrait
{

    private SeanceService $seanceService;

    public function getSeanceService(): SeanceService
    {
        return $this->seanceService;
    }

    public function setSeanceService(SeanceService $seanceService): void
    {
        $this->seanceService = $seanceService;
    }

}
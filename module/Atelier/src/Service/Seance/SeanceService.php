<?php

namespace Atelier\Service\Seance;

use Atelier\Entity\Db\Session;
use Atelier\Event\CloturerSession\CloturerSessionEvenementAwareTrait;
use Atelier\Event\ConvoquerSession\ConvoquerSessionEvenementAwareTrait;
use Atelier\Event\RetourSession\RetourSessionEvenementAwareTrait;
use DateInterval;
use DateTime;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\QueryBuilder;
use DoctrineModule\Persistence\ProvidesObjectManager;
use Atelier\Entity\Db\Seance;
use Laminas\Mvc\Controller\AbstractActionController;
use UnicaenApp\Exception\RuntimeException;
use Laminas\Mvc\Plugin\FlashMessenger\FlashMessenger;

class SeanceService
{
    use ProvidesObjectManager;
    use CloturerSessionEvenementAwareTrait;
    use ConvoquerSessionEvenementAwareTrait;
    use RetourSessionEvenementAwareTrait;


    /**  GESTION ENTITY ***********************************************************************************************/

    public function create(Seance $seance): Seance
    {
        $this->getObjectManager()->persist($seance);
        $this->getObjectManager()->flush($seance);
        return $seance;
    }

    public function update(Seance $seance): Seance
    {
        $this->getObjectManager()->flush($seance);
        return $seance;
    }

    public function historise(Seance $seance): Seance
    {
        $seance->historiser();
        $this->getObjectManager()->flush($seance);
        return $seance;
    }

    public function restore(Seance $seance): Seance
    {
        $seance->dehistoriser();
        $this->getObjectManager()->flush($seance);
        return $seance;
    }

    public function delete(Seance $seance): Seance
    {
        $this->getObjectManager()->remove($seance);
        $this->getObjectManager()->flush($seance);
        return $seance;
    }

    /** REQUÊTAGE *****************************************************************************************************/

    public function createQueryBuilder(): QueryBuilder
    {
        return $this->getObjectManager()->getRepository(Seance::class)->createQueryBuilder('seance')
            ->addSelect('session')->join('seance.session', 'session')
            ->addSelect('atelier')->join('session.atelier', 'atelier');
    }

    /** @return Seance[] */
    public function getSeances(string $champ = 'id', string $ordre = 'ASC'): array
    {
        $qb = $this->createQueryBuilder()
            ->orderBy('seance.' . $champ, $ordre);
        return $qb->getQuery()->getResult();
    }

    public function getSeance(?int $id): ?Seance
    {
        $qb = $this->createQueryBuilder()
            ->andWhere('seance.id = :id')
            ->setParameter('id', $id);
        try {
            $result = $qb->getQuery()->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            throw new RuntimeException("Plusieurs [".Seance::class."] partagent le même id [" . $id . "]", 0, $e);
        }
        return $result;
    }

    public function getRequestedSeance(AbstractActionController $controller, string $param = 'seance'): ?Seance
    {
        $id = $controller->params()->fromRoute($param);
        return $this->getSeance($id);
    }

    /** FACADE ********************************************************************************************************/

    public function createSeanceWith(Session $session, DateTime $jour, string $debut, string $fin, string $lieu): Seance
    {
        $seance = new Seance();
        $seance->setType(Seance::TYPE_SEANCE);
        $seance->setJour($jour);
        $seance->setDebut($debut);
        $seance->setFin($fin);
        $seance->setLieu($lieu);
        $seance->setSession($session);
        $this->create($seance);
        return $seance;
    }


    public function createRecurrente(Session $session, mixed $formData): array
    {
        $seances = [];

        $jourString = $formData['jour'];
        $debut = $formData['debut'];
        $fin = $formData['fin'];
        $lieu = $formData['lieu'];
        $typeRecurrence = $formData['type_recurrence'];
        $finRecurrenceString = $formData['fin_recurrence'];

        $jour = DateTime::createFromFormat('Y-m-d', $jourString);
        if (!$jour instanceof DateTime) {
            throw new RuntimeException("Une erreur est survenu lors de la cration de la date à partir de l'entrée [".$jourString."]");
        }
        $finRecurrence = DateTime::createFromFormat('Y-m-d', $finRecurrenceString);
        if (!$finRecurrence instanceof DateTime) {
            throw new RuntimeException("Une erreur est survenu lors de la cration de la date à partir de l'entrée [".$finRecurrenceString."]");
        }

        $seance = $this->createSeanceWith($session, $jour, $debut, $fin, $lieu);
        $seances[] = $seance;

        $current = DateTime::createFromFormat('Y-m-d', $jourString);
        while(true) {
            $current = DateTime::createFromFormat('Y-m-d', $current->format('Y-m-d'));
            if (!$current instanceof DateTime) {
                throw new RuntimeException("Une erreur est survenu lors de la cration de la date à partir de l'entrée [".$current->format('d/m/Y')."]");
            }
            switch ($typeRecurrence) {
                case 'journalière' :
                    $current->add(new DateInterval('P1D'));
                    break;
                case 'hebdomadaire' :
                    $current->add(new DateInterval('P7D'));
                    break;
                default :
                    throw new RuntimeException("Le type de récurrence [" . $finRecurrenceString . "] n'est pas géré.");
            }
            if ($current > $finRecurrence) break;
            $seance = $this->createSeanceWith($session, $current, $debut, $fin, $lieu);
            $seances[] = $seance;
        }

        return $seances;
    }

    public function gererEvenement(Seance $seance, ?FlashMessenger $flashMessenger = null): void
    {
        $message = "";
        $session = $seance->getSession();

        if ($seance->isPremiereSeance() OR $seance->isDerniereSeance()) $message .= "<strong>Modification des événements </strong><br><br>";
        if ($seance->isPremiereSeance()) {
            $message .= "<strong>Modification de la première séance</strong><br>";
            $event = $this->getConvoquerSessionEvenement()->updateEvent($session);
            if ($event) {
                $message .= "Nouvelle date de convocation automatique : " . $event->getDatePlanification()->format('d/m/Y à H:i');
            } else {
                $message .= "Plus de séance : retrait de la convocation automatique";
            }
            $message .= "<br>";
        }
        //}
        if ($seance->isDerniereSeance()) {
            $message .= "<strong>Modification de la dernière séance</strong><br>";
            $event = $this->getRetourSessionEvenement()->updateEvent($session);
            if ($event) {
                $message .= "Nouvelle date de demande automatique des retours : " . $event->getDatePlanification()->format('d/m/Y à H:i');
            } else {
                $message .= "Plus de séance : retrait de la demande automatique des retours";
            }
            $message .= "<br>";
            $event = $this->getCloturerSessionEvenement()->updateEvent($session);
            if ($event) {
                $message .= "Nouvelle date de cloture automatique : " . $event->getDatePlanification()->format('d/m/Y à H:i');
            } else {
                $message .= "Plus de séance : retrait de la cloture automatique";
            }
            $message .= "<br>";

        }
        if ($seance->isPremiereSeance() OR $seance->isDerniereSeance()) $flashMessenger?->addInfoMessage($message);
    }

}
<?php

namespace Atelier\Service\Seance;

use Atelier\Event\CloturerSession\CloturerSessionEvenement;
use Atelier\Event\ConvoquerSession\ConvoquerSessionEvenement;
use Atelier\Event\RetourSession\RetourSessionEvenement;
use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;

class SeanceServiceFactory
{
    /**
     * @param ContainerInterface $container
     * @return SeanceService
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): SeanceService
    {
        /** @var EntityManager $entityManager */
        $entityManager = $container->get('doctrine.entitymanager.orm_default');

        /**
         * @var CloturerSessionEvenement $cloturerSessionEvenement
         * @var ConvoquerSessionEvenement $convoquerSessionEvenement
         * @var RetourSessionEvenement $retourSessionEvenement
         */
        $cloturerSessionEvenement = $container->get(CloturerSessionEvenement::class);
        $convoquerSessionEvenement = $container->get(ConvoquerSessionEvenement::class);
        $retourSessionEvenement = $container->get(RetourSessionEvenement::class);

        $service = new SeanceService();
        $service->setObjectManager($entityManager);
        $service->setCloturerSessionEvenement($cloturerSessionEvenement);
        $service->setConvoquerSessionEvenement($convoquerSessionEvenement);
        $service->setRetourSessionEvenement($retourSessionEvenement);
        return $service;
    }
}
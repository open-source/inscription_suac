<?php

namespace Atelier\Service\Groupe;

trait GroupeServiceAwareTrait
{
    private GroupeService $groupeService;

    public function getGroupeService() : GroupeService
    {
        return $this->groupeService;
    }

    public function setGroupeService(GroupeService $groupeService) : void
    {
        $this->groupeService = $groupeService;
    }

}
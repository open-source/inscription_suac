<?php

namespace Atelier\Service\Groupe;

use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\QueryBuilder;
use DoctrineModule\Persistence\ProvidesObjectManager;
use Atelier\Entity\Db\Groupe;
use UnicaenApp\Exception\RuntimeException;
use Laminas\Mvc\Controller\AbstractActionController;

class GroupeService
{
    use ProvidesObjectManager;

    /** GESTION DES ENTITÉS *******************************************************************************************/

    public function create(Groupe $groupe) : Groupe
    {
        $this->getObjectManager()->persist($groupe);
        $this->getObjectManager()->flush($groupe);
        return $groupe;
    }

    public function update(Groupe $groupe) : Groupe
    {
        $this->getObjectManager()->flush($groupe);
        return $groupe;
    }

    public function historise(Groupe $groupe) : Groupe
    {
        $groupe->historiser();
        $this->getObjectManager()->flush($groupe);
        return $groupe;
    }

    public function restore(Groupe $groupe) : Groupe
    {
        $groupe->dehistoriser();
        $this->getObjectManager()->flush($groupe);
        return $groupe;
    }

    public function delete(Groupe $groupe) : Groupe
    {
        $this->getObjectManager()->remove($groupe);
        $this->getObjectManager()->flush($groupe);
        return $groupe;
    }

    /** REQUÉTAGE *****************************************************************************************************/

    /**
     * @return QueryBuilder
     */
    public function createQueryBuilder() : QueryBuilder
    {
        return $this->getObjectManager()->getRepository(Groupe::class)->createQueryBuilder('groupe')
            ->leftJoin('groupe.ateliers', 'atelier')->addSelect('atelier')
        ;
    }

    /** @return Groupe[] */
    public function getGroupes(string $champ = 'ordre', string $ordre = 'ASC') : array
    {
        $qb = $this->createQueryBuilder()
            ->orderBy('groupe.' . $champ, $ordre);

        return $qb->getQuery()->getResult();
    }

    public function optionify(Groupe $groupe) : array
    {
        $this_option = [
            'value' => $groupe->getId(),
            'label' => $groupe->getLibelle(),
        ];
        return $this_option;
    }

    public function getGroupesAsOption(string $champ = 'libelle', string $ordre = 'ASC') : array
    {
        $groupes = $this->getGroupes($champ, $ordre);
        $array = [];
        foreach ($groupes as $groupe) {
            $option = $this->optionify($groupe);
            $array[$groupe->getId()] = $option;
        }
        return $array;
    }

    /**
     * @param int|null $id
     * @return Groupe|null
     */
    public function getGroupe(?int $id) : ?Groupe
    {
        $qb = $this->createQueryBuilder()
            ->andWhere('groupe.id = :id')
            ->setParameter('id', $id);
        try {
            $result = $qb->getQuery()->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            throw new RuntimeException("Plusieurs Groupe partagent le même id [" . $id . "]",0,$e);
        }
        return $result;
    }

    public function getRequestedGroupe(AbstractActionController $controller, string  $param = 'groupe') : ?Groupe
    {
        $id = $controller->params()->fromRoute($param);
        return $this->getGroupe($id);
    }

    /** Façade ********************************************************************************************/

    public function createGroupe(string $libelle): Groupe
    {
        $theme = new Groupe();
        $theme->setLibelle($libelle);
        $this->create($theme);
        return $theme;
    }

}
<?php

namespace Atelier\Entity\Db;

use Atelier\Provider\Etat\InscriptionEtats;
use Atelier\Provider\Etat\SessionEtats;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Etudiant\Entity\Db\Etudiant;
use Laminas\Permissions\Acl\Resource\ResourceInterface;
use UnicaenEnquete\Entity\HasEnqueteInterface;
use UnicaenEnquete\Entity\HasEnqueteTrait;
use UnicaenEtat\Entity\Db\HasEtatsInterface;
use UnicaenEtat\Entity\Db\HasEtatsTrait;
use UnicaenUtilisateur\Entity\Db\HistoriqueAwareInterface;
use UnicaenUtilisateur\Entity\Db\HistoriqueAwareTrait;
use UnicaenValidation\Entity\HasValidationsInterface;
use UnicaenValidation\Entity\HasValidationsTrait;

class Inscription implements HistoriqueAwareInterface, HasEtatsInterface, HasValidationsInterface, ResourceInterface, HasEnqueteInterface {
    use HistoriqueAwareTrait;
    use HasEtatsTrait;
    use HasValidationsTrait;

    use HasEnqueteTrait;

    const PRINCIPALE = 'principale';
    const COMPLEMENTAIRE = 'complementaire';

    const ETAPE_ETUDIANT = 'ETAPE_ETUDIANT';
    const ETAPE_VALIDATION = 'ETAPE_VALIDATION';
    const ETAPE_REFUS = 'ETAPE_REFUS';

    private ?int $id = null;
    private ?Etudiant $etudiant = null;
    private ?Session $session = null;

    private ?string $liste = null;

    private ?string $etape = null;
    private ?string $justificationEtudiant = null;
    private ?string $justificationValidation = null;
    private ?string $justificationRefus = null;
    private ?bool $international = null;
    private ?string $telephone = null;

    private Collection $presences;



    public function __construct()
    {
        $this->etats = new ArrayCollection();
        $this->validations = new ArrayCollection();
        $this->presences = new ArrayCollection();
    }

    public function getResourceId(): string
    {
        return 'Inscription';
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEtudiant(): ?Etudiant
    {
        return $this->etudiant;
    }

    public function setEtudiant(?Etudiant $etudiant): void
    {
        $this->etudiant = $etudiant;
    }

    public function getSession(): ?Session
    {
        return $this->session;
    }

    public function setSession(?Session $session): void
    {
        $this->session = $session;
    }

    public function getListe(): ?string
    {
        return $this->liste;
    }

    public function setListe(?string $liste): void
    {
        $this->liste = $liste;
    }

    public function getEtape(): ?string
    {
        return $this->etape;
    }

    public function setEtape(?string $etape): void
    {
        $this->etape = $etape;
    }

    public function getJustificationEtudiant(): ?string
    {
        return $this->justificationEtudiant;
    }

    public function setJustificationEtudiant(?string $justificationEtudiant): void
    {
        $this->justificationEtudiant = $justificationEtudiant;
    }

    public function getJustificationValidation(): ?string
    {
        return $this->justificationValidation;
    }

    public function setJustificationValidation(?string $justificationValidation): void
    {
        $this->justificationValidation = $justificationValidation;
    }

    public function getJustificationRefus(): ?string
    {
        return $this->justificationRefus;
    }

    public function setJustificationRefus(?string $justificationRefus): void
    {
        $this->justificationRefus = $justificationRefus;
    }

    public function getInternational(): ?bool
    {
        return $this->international;
    }

    public function setInternational(?bool $international): void
    {
        $this->international = $international;
    }

    public function getTelephone(): ?string
    {
        return $this->telephone;
    }

    public function setTelephone(?string $telephone): void
    {
        $this->telephone = $telephone;
    }

    public function hasAllPresence(): bool
    {
        /** @var Presence $presence */
        foreach ($this->presences as $presence) {
            if ($presence->getStatut() === null || $presence->getStatut() === Presence::PRESENCE_NON_RENSEIGNEE) return false;
        }
        return true;
    }

    /** MACROS ********************************************************************************************************/

    public function generateTag(): string
    {
        return "Inscription_".$this->getId();
    }

    /** @noinspection PhpUnused */
    public function getStagiaireDenomination(bool $prenomFirst = true): string
    {
        return $this->getEtudiant()->getDenomination($prenomFirst);
    }

    /** @noinspection PhpUnused [Inscription#Duree] */
    public function toStringDureePresence(): string
    {
        $sum = DateTime::createFromFormat('d/m/Y H:i', '01/01/1970 00:00');
        /** @var Presence[] $presences */
        $presences = array_filter($this->presences->toArray(), function (Presence $a) {
            return $a->estNonHistorise();
        });
        foreach ($presences as $presence) {
            if ($presence->getStatut() === Presence::PRESENCE_PRESENCE) {
                $seance = $presence->getSeance();
                $debut = DateTime::createFromFormat('d/m/Y H:i', $seance->getJour()->format('d/m/Y') . " " . $seance->getDebut());
                $fin = DateTime::createFromFormat('d/m/Y H:i', $seance->getJour()->format('d/m/Y') . " " . $seance->getFin());
                if ($debut instanceof DateTime and $fin instanceof DateTime) {
                    $duree = $debut->diff($fin);
                    $sum->add($duree);
                }
            }
        }

        $result = $sum->diff(DateTime::createFromFormat('d/m/Y H:i', '01/01/1970 00:00'));
        $heures = ($result->d * 24 + $result->h);
        $minutes = ($result->i);
        $text = $heures . " heure" . ($heures>1?"s":"") . (($minutes !== 0) ? (" " . $minutes . " minute" . ($minutes>1?"s":"")) : "");
        return $text;
    }

    public function toStringListe(): string
    {
        if ($this->getListe() === Inscription::PRINCIPALE) return "Liste principale";
        if ($this->getListe() === Inscription::COMPLEMENTAIRE) return "Liste complémentaire";
        if (in_array($this->getEtatActif()->getType()->getCode(), SessionEtats::CONVOCATION)) return "Inscription non classée";
        return "En attente de classement";
    }

    public function toStringDateValidation(): string
    {
        $etat = $this->getEtatActif();
        if ($etat->getType()->getCode() === InscriptionEtats::INSCRIPTION_VALIDER) {
            return $etat->getHistoCreation()->format('d/m/Y');
        }
        return "Aucun date de validation";
    }


}
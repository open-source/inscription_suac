<?php

namespace Atelier\Entity\Db;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use UnicaenUtilisateur\Entity\Db\HistoriqueAwareInterface;
use UnicaenUtilisateur\Entity\Db\HistoriqueAwareTrait;

class Programme implements HistoriqueAwareInterface
{
    use HistoriqueAwareTrait;

    private ?int $id = -1;
    private ?string $libelle = null;
    private ?DateTime $dateDebut = null;
    private ?DateTime $dateFin = null;

    private Collection $ateliers;

    public function __construct()
    {
        $this->ateliers = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(?string $libelle): void
    {
        $this->libelle = $libelle;
    }

    public function getDateDebut(): ?DateTime
    {
        return $this->dateDebut;
    }

    public function setDateDebut(?DateTime $dateDebut): void
    {
        $this->dateDebut = $dateDebut;
    }

    public function getDateFin(): ?DateTime
    {
        return $this->dateFin;
    }

    public function setDateFin(?DateTime $dateFin): void
    {
        $this->dateFin = $dateFin;
    }

    public function getAteliers(): Collection
    {
        return $this->ateliers;
    }

    public function hasAtelier(Atelier $atelier): bool
    {
        return $this->ateliers->contains($atelier);
    }

    public function addAtelier(Atelier $atelier): void
    {
        if (!$this->hasAtelier($atelier)) $this->ateliers->add($atelier);
    }

    public function removeAtelier(Atelier $atelier): void
    {
        if ($this->hasAtelier($atelier)) $this->ateliers->removeElement($atelier);
    }

    public function toStringPeriode() : string
    {
        if ($this->getDateDebut() === null AND $this->getDateFin() === null) return "Aucune période de déterminée";
        return ($this->getDateDebut()?$this->getDateDebut()->format('d/m/Y'):"---"). " au ". ($this->getDateFin()?$this->getDateFin()->format('d/m/Y'):"---");
    }

}
<?php

namespace Atelier\Entity\Db;

use DateInterval;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Etudiant\Entity\Db\Etudiant;
use Exception;
use Atelier\Provider\Etat\SessionEtats;
use Laminas\Permissions\Acl\Resource\ResourceInterface;
use RuntimeException;
use Site\Entity\Db\Site;
use UnicaenEtat\Entity\Db\HasEtatsInterface;
use UnicaenEtat\Entity\Db\HasEtatsTrait;
use UnicaenEvenement\Entity\HasEvenementsInterface;
use UnicaenEvenement\Entity\HasEvenementsTrait;
use UnicaenUtilisateur\Entity\Db\HistoriqueAwareInterface;
use UnicaenUtilisateur\Entity\Db\HistoriqueAwareTrait;
use UnicaenUtilisateur\Entity\Db\UserInterface;


class Session implements HistoriqueAwareInterface, HasEtatsInterface, HasEvenementsInterface, ResourceInterface
{
    use HasEtatsTrait;
    use HasEvenementsTrait;
    use HistoriqueAwareTrait;

    public function getResourceId(): string
    {
        return 'Session';
    }

    private ?int $id = null;
    private ?Atelier $atelier = null;
    private ?string $complement = null;
    private ?bool $autoInscription = true;

    private int $nbPlacePrincipale = 0;
    private int $nbPlaceComplementaire = 0;
    private ?string $lieu = null;
    private ?string $type = null;
    private ?Site $site = null;

    private Collection $seances;
    private Collection $inscriptions;
    private Collection $intervenants;


    public function __construct()
    {
        $this->etats = new ArrayCollection();
        $this->seances = new ArrayCollection();
//        $this->inscrits = new ArrayCollection();
//        $this->inscriptions = new ArrayCollection();
        $this->intervenants = new ArrayCollection();
        $this->evenements = new ArrayCollection();

    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getAtelier(): ?Atelier
    {
        return $this->atelier;
    }

    public function setAtelier(?Atelier $atelier): void
    {
        $this->atelier = $atelier;
    }

    public function getComplement(): ?string
    {
        return $this->complement;
    }

    public function setComplement(?string $complement): void
    {
        $this->complement = $complement;
    }

    public function getAutoInscription(): ?bool
    {
        return $this->autoInscription;
    }

    public function setAutoInscription(?bool $autoInscription): void
    {
        $this->autoInscription = $autoInscription;
    }

    public function getNbPlacePrincipale(): int
    {
        return $this->nbPlacePrincipale;
    }

    public function setNbPlacePrincipale(int $nbPlacePrincipale): void
    {
        $this->nbPlacePrincipale = $nbPlacePrincipale;
    }

    public function getNbPlaceComplementaire(): int
    {
        return $this->nbPlaceComplementaire;
    }

    public function setNbPlaceComplementaire(int $nbPlaceComplementaire): void
    {
        $this->nbPlaceComplementaire = $nbPlaceComplementaire;
    }

    public function getLieu(): ?string
    {
        return $this->lieu;
    }

    public function setLieu(?string $lieu): void
    {
        $this->lieu = $lieu;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(?string $type): void
    {
        $this->type = $type;
    }


    /** SEANCES *******************************************************************************************************/

    /**
     * @return Seance[]|null
     */
    public function getSeances(): ?array
    {
        if (!isset($this->seances)) return null;
        return $this->seances->toArray();
    }

    public function getDebut(bool $datetime = false): string|DateTime|null
    {
        $minimum = null;
        /** @var Seance $seances */
        foreach ($this->seances as $seance) {
            if ($seance->estNonHistorise()) {
                if ($seance->getType() === Seance::TYPE_SEANCE) {
                    $split = explode("/", $seance->getJour()->format('d/m/Y'));
                    $reversed = $split[2] . "/" . $split[1] . "/" . $split[0];
                    if ($minimum === null or $reversed < $minimum) $minimum = $reversed;
                }
                if ($seance->getType() === Seance::TYPE_VOLUME) {
                    if ($seance->getVolumeDebut()) {
                        $debut = $seance->getVolumeDebut()->format("Y/m/d");
                        if ($minimum === null or $debut < $minimum) $minimum = $debut;
                    }
                }
            }
        }
        if ($minimum !== null) {
            $split = explode("/", $minimum);
            $minimum = $split[2] . "/" . $split[1] . "/" . $split[0];
        }
        if ($datetime === true) {
            return ($minimum) ? DateTime::createFromFormat('d/m/Y', $minimum) : null;
        }
        return $minimum;
    }

    public function getFin(bool $datetime = false): string|DateTime|null
    {
        $maximum = null;
        /** @var Seance $seance */
        foreach ($this->seances as $seance) {
            if ($seance->estNonHistorise()) {
                if ($seance->getType() === Seance::TYPE_SEANCE) {
                    $split = explode("/", $seance->getJour()->format('d/m/Y'));
                    $reversed = $split[2] . "/" . $split[1] . "/" . $split[0];
                    if ($maximum === null or $reversed > $maximum) $maximum = $reversed;
                }
                if ($seance->getType() === Seance::TYPE_VOLUME) {
                    if ($seance->getVolumeFin()) {
                        $fin = $seance->getVolumeFin()->format("Y/m/d");
                        if ($maximum === null or $fin > $maximum) $maximum = $fin;
                    }
                }
            }
        }
        if ($maximum !== null) {
            $split = explode("/", $maximum);
            $maximum = $split[2] . "/" . $split[1] . "/" . $split[0];
        }
        if ($datetime === true) {
            return ($maximum) ? DateTime::createFromFormat('d/m/Y', $maximum) : null;
        }
        return $maximum;
    }

    public function hasSeance(): bool
    {
        /** @var Seance $journee */
        foreach ($this->seances as $seance) {
            if ($seance->estNonHistorise()) return true;
        }
        return false;
    }

    public function getPremiereSeance(): ?Seance
    {
        $premiere = null;
        $seances = $this->getSeances();
        $seances = array_filter($seances, function (Seance $seance) {return $seance->estNonHistorise();});

        foreach ($seances as $seance) {
            if ($premiere === null) $premiere = $seance;
            else {
                if ($premiere->getDateTimeFin() > $seance->getDateTimeFin()) $premiere = $seance;
            }
        }

        return $premiere;
    }

    public function getDerniereSeance(): ?Seance
    {
        $derniere = null;
        $seances = $this->getSeances();
        $seances = array_filter($seances, function (Seance $seance) {return $seance->estNonHistorise();});

        foreach ($seances as $seance) {
            if ($derniere === null) $derniere = $seance;
            else {
                if ($derniere->getDateTimeFin() < $seance->getDateTimeFin()) $derniere = $seance;
            }
        }

        return $derniere;
    }


    /** INSCRIPTION ***************************************************************************************************/

    /**
     * @return Inscription[]
     */
    public function getInscriptions(): array
    {
        return $this->inscriptions->toArray();
    }

    /**
     * @return Inscription[]
     */
    public function getListeValide(): array
    {
        $array = array_filter($this->inscriptions->toArray(), function (Inscription $a) {
            return ($a->estNonHistorise());
        });
        usort($array, function (Inscription $a, Inscription $b) {
            return $a->getStagiaireDenomination() <=> $b->getStagiaireDenomination();
        });
        return $array;
    }

    /**
     * @return Inscription[]
     */
    public function getListePrincipale(): array
    {
        $array = array_filter($this->inscriptions->toArray(), function (Inscription $a) {
            return ($a->getListe() === Inscription::PRINCIPALE and $a->estNonHistorise());
        });
        usort($array, function (Inscription $a, Inscription $b) {
            return $a->getStagiaireDenomination() <=> $b->getStagiaireDenomination();
        });
        return $array;
    }

    /**
     * @return Inscription[]
     */
    public function getListeComplementaire(): array
    {
        $array = array_filter($this->inscriptions->toArray(), function (Inscription $a) {
            return ($a->getListe() === Inscription::COMPLEMENTAIRE and $a->estNonHistorise());
        });
        usort($array, function (Inscription $a, Inscription $b) {
            return $a->getStagiaireDenomination() <=> $b->getStagiaireDenomination();
        });
        return $array;
    }

    /**
     * @return Inscription[]
     */
    public function getListeHistorisee(): array
    {
        $array = array_filter($this->inscriptions->toArray(), function (Inscription $a) {
            return $a->estHistorise();
        });
        usort($array, function (Inscription $a, Inscription $b) {
            return $a->getStagiaireDenomination() <=> $b->getStagiaireDenomination();
        });
        return $array;
    }

    public function isListePrincipaleComplete(): bool
    {
        $liste = $this->getListePrincipale();
        return (count($liste) >= $this->getNbPlacePrincipale());
    }

    public function isListeComplementaireComplete(): bool
    {
        $liste = $this->getListeComplementaire();
        return (count($liste) >= $this->getNbPlaceComplementaire());
    }

    /**
     * @return string|null
     */
    public function getListeDisponible(): ?string
    {
        $liste = null;
        if ($liste === null and !$this->isListePrincipaleComplete()) $liste = Inscription::PRINCIPALE;
        if ($liste === null and !$this->isListeComplementaireComplete()) $liste = Inscription::COMPLEMENTAIRE;
        return $liste;
    }

    /** PRÉDICATS D'ETAT *********************************************************************************************/

    public function estPreparation(): bool
    {
        $etatCode = $this->getEtatActif() ? $this->getEtatActif()->getType()->getCode() : null;
        return (
            $etatCode === SessionEtats::ETAT_CREATION_EN_COURS ||
            $etatCode === SessionEtats::ETAT_INSCRIPTION_OUVERTE ||
            $etatCode === SessionEtats::ETAT_INSCRIPTION_FERMEE
        );
    }

    public function estPrete(): bool
    {
        $etatCode = $this->getEtatActif() ? $this->getEtatActif()->getType()->getCode() : null;
        return (
            $etatCode === SessionEtats::ETAT_FORMATION_CONVOCATION
        );
    }

    public function estRealisee(): bool
    {
        $etatCode = $this->getEtatActif() ? $this->getEtatActif()->getType()->getCode() : null;
        return (
            $etatCode === SessionEtats::ETAT_ATTENTE_RETOURS ||
            $etatCode === SessionEtats::ETAT_CLOTURE_INSTANCE
        );
    }

    /** Intervenants ***********************************************************************************************/

    /** @return Intervenant[] */
    public function getIntervenants(): array
    {
        return $this->intervenants->toArray();
    }

    public function hasIntervenant(Intervenant $intervenant): bool
    {
        return $this->intervenants->contains($intervenant);
    }
    public function addIntervenant(Intervenant $intervenant): void
    {
        if (!$this->hasIntervenant($intervenant)) $this->intervenants->add($intervenant);
    }

    public function removeIntervenant(Intervenant $intervenant): void
    {
        if ($this->hasIntervenant($intervenant)) $this->intervenants->removeElement($intervenant);
    }

    public function getInscription(Etudiant $etudiant): ?Inscription
    {
        foreach ($this->inscriptions as $inscription) {
            if ($inscription->getEtudiant() === $etudiant AND $inscription->estNonHistorise()) {
                return $inscription;
            }
        }
        return null;
    }

    public function getSite(): ?Site
    {
        return $this->site;
    }

    public function setSite(?Site $site): void
    {
        $this->site = $site;
    }



    /** Fonctions pour les macros **********************************************************************************/

    public function generateTag(): string
    {
        return 'Session_' . $this->getId();
    }

    /** @noinspection PhpUnused [Session#Libelle]*/
    public function getLibelle(): string
    {
        return $this->getAtelier()->getLibelle();
    }

    /** @noinspection PhpUnused [Session#Lieu]*/
    public function getLieuString(): string
    {
        return ($this->getLieu()) ? " à " . $this->getLieu() : "";
    }

    /** @noinspection PhpUnused */
    public function getInstanceCode(): string
    {
        return $this->getAtelier()->getId() . "/" . $this->getId();
    }

    /** @noinspection PhpUnused [Session#Intervenants]*/
    public function getListeIntervenants(): string
    {
        /** @var Intervenant[] $intervenants */
        $intervenants = $this->getIntervenants();
        usort($intervenants, function (Intervenant $a, Intervenant $b) {
            return ($a->getNom() . " " . $a->getPrenom()) <=> ($b->getNom() . " " . $b->getPrenom());
        });

        $text = "<table class='bordered' style='width:100%;'>";
        $text .= "<thead>";
        $text .= "<tr style='border-bottom:1px solid black;'>";
        $text .= "<th>Dénomination  </th>";
        $text .= "<th>Structure de rattachement / Organisme  </th>";
        $text .= "</tr>";
        $text .= "</thead>";
        $text .= "<tbody>";
        foreach ($intervenants as $intervenant) {
            $text .= "<tr>";
            $text .= "<td>" . $intervenant->getPrenom() . " " . $intervenant->getNom() . "</td>";
            $text .= "<td>" . $intervenant->getRattachement() . "</td>";
            $text .= "</tr>";
        }
        $text .= "</tbody>";
        $text .= "</table>";

        return $text;
    }

    /** @noinspection PhpUnused [Session#Inscrits]*/
    public function getListeInscrits(): string
    {
        /** @var Inscription[] $inscriptions */
        $inscriptions = $this->getListePrincipale();
        usort($inscriptions, function (Inscription $a, Inscription $b) {
            return $a->getEtudiant()->getDenomination(false) <=> $b->getEtudiant()->getDenomination(false);
        });

        $text = "<table class='bordered' style='width:100%;'>";
        $text .= "<thead>";
        $text .= "<tr style='border-bottom:1px solid black;'>";
        $text .= "<th>Dénomination  </th>";
        $text .= "<th>Signature  </th>";
        $text .= "</tr>";
        $text .= "</thead>";
        $text .= "<tbody>";
        foreach ($inscriptions as $inscription) {
            $text .= "<tr>";
            $text .= "<td>" . $inscription->getEtudiant()->getDenomination() . "<br>" . $inscription->getEtudiant()->getComposante() ."</td>";
            $text .= "<td></td>";
            $text .= "</tr>";
        }
        $text .= "</tbody>";
        $text .= "</table>";

        return $text;
    }

    /** @noinspection PhpUnused  [Session#Seances]*/
    public function getListeSeances(): string
    {
        /** @var Seance[] $seances */
        $seances = $this->getSeances();
        //usort($journees, function (FormationInstanceJournee $a, FormationInstanceJournee $b) { return $a <=> $b;});

        $text = "<table style='width:100%;'>";
        $text .= "<thead>";
        $text .= "<tr style='border-bottom:1px solid black;'>";
        $text .= "<th>Date  </th>";
        $text .= "<th>de  </th>";
        $text .= "<th>à  </th>";
        $text .= "<th>Lieu  </th>";
        $text .= "</tr>";
        $text .= "</thead>";
        $text .= "<tbody>";
        foreach ($seances as $journee) {
            $text .= "<tr>";
            if ($journee->getType() === Seance::TYPE_SEANCE) {
                $text .= "<td>" . $journee->getJour()->format('d/m/Y') . "</td>";
                $text .= "<td>" . $journee->getDebut() . "</td>";
                $text .= "<td>" . $journee->getFin() . "</td>";
            }
            if ($journee->getType() === Seance::TYPE_VOLUME) {
                $text .= "<td colspan='2'>Volume horaire</td>";
                $text .= "<td>" . $journee->getVolume() . " heures </td>";
            }
            $text .= "<td>" . $journee->getLieu() . "</td>";
            $text .= "</tr>";
        }
        $text .= "</tbody>";
        $text .= "</table>";

        return $text;
    }

    /** @noinspection PhpUnused [Session#Periode] */
    public function getPeriode(): string
    {
        if ($this->getDebut() === null or $this->getFin() === null) return "atelier sans date";
        if ($this->getDebut() === $this->getFin()) return $this->getDebut();
        return $this->getDebut() . " au " . $this->getFin();
    }

    /** @noinspection PhpUnused [Session#Duree]*/
    public function getDuree(): string
    {
        $sum = DateTime::createFromFormat('d/m/Y H:i', '01/01/1970 00:00');
        /** @var Seance[] $seances */
        $seances = array_filter($this->seances->toArray(), function (Seance $a) {
            return $a->estNonHistorise();
        });
        foreach ($seances as $seance) {
            if ($seance->getType() === Seance::TYPE_SEANCE) {
                $debut = DateTime::createFromFormat('d/m/Y H:i', $seance->getJour()->format('d/m/Y') . " " . $seance->getDebut());
                $fin = DateTime::createFromFormat('d/m/Y H:i', $seance->getJour()->format('d/m/Y') . " " . $seance->getFin());
                if ($debut instanceof DateTime and $fin instanceof DateTime) {
                    $duree = $debut->diff($fin);
                    $sum->add($duree);
                }
            }
            if ($seance->getType() === Seance::TYPE_VOLUME) {
                $volume = $seance->getVolume();
                if ($volume) {
                    try {
                        $temp = new DateInterval('PT' . $volume . 'H');
                    } catch (Exception $e) {
                        throw new RuntimeException("Une erreur est survenue lors de la création de l'interval", 0, $e);
                    }
                    $sum->add($temp);
                }

            }
        }

        $result = $sum->diff(DateTime::createFromFormat('d/m/Y H:i', '01/01/1970 00:00'));
        $heures = ($result->d * 24 + $result->h);
        $minutes = ($result->i);
        $text = $heures . " heure".(($heures>1)?"s":"") . (($minutes !== 0) ? (" " . $minutes . " minute".($minutes>1?"s":"")) : "");
        return $text;
    }

    /** @noinspection PhpUnused */
    public function getPlaceDisponible(string $liste): int
    {
        $inscriptions = array_filter(
            $this->getInscriptions(),
            function (Inscription $a) use ($liste) {
                return $a->estNonHistorise() and $a->getListe() === $liste;
            });
        return count($inscriptions);
    }

    public function hasIntervenantWithUser(?UserInterface $user): bool
    {
        /** @var Intervenant $intervenant */
        foreach ($this->intervenants as $intervenant) {
            if ($intervenant->estNonHistorise() && $intervenant->getUtilisateur() === $user) return true;
        }
        return false;
    }

    public function hasEtudiant(?Etudiant $etudiant): bool
    {
        /** @var Intervenant $intervenant */
        foreach ($this->getInscriptions() as $inscription) {
            if ($inscription->estNonHistorise() && $inscription->getEtudiant() === $etudiant) return true;
        }
        return false;
    }


}
<?php

namespace Atelier\Entity\Db;

use RuntimeException;
use UnicaenUtilisateur\Entity\Db\HistoriqueAwareInterface;
use UnicaenUtilisateur\Entity\Db\HistoriqueAwareTrait;

class Presence implements HistoriqueAwareInterface
{
    use HistoriqueAwareTrait;

    const PRESENCE_NON_RENSEIGNEE = "NON_RENSEIGNEE";
    const PRESENCE_PRESENCE = "PRESENCE";
    const PRESENCE_ABSENCE_JUSTIFIEE = "ABSENCE_JUSTIFIEE";
    const PRESENCE_ABSENCE_NON_JUSTIFIEE = "ABSENCE_NON_JUSTIFIEE";

    private int $id = -1;
    private ?Seance $seance = null;
    private ?Inscription $inscription = null;
    private string $statut = self::PRESENCE_NON_RENSEIGNEE;
    private ?string $commentaire = null;

    public function getId(): int
    {
        return $this->id;
    }

    public function getSeance(): ?Seance
    {
        return $this->seance;
    }

    public function setSeance(?Seance $seance): void
    {
        $this->seance = $seance;
    }

    public function getInscription(): ?Inscription
    {
        return $this->inscription;
    }

    public function setInscription(?Inscription $inscription): void
    {
        $this->inscription = $inscription;
    }

    public function isPresent(): bool
    {
        return $this->statut === Presence::PRESENCE_PRESENCE;
    }

    public function tooglePresence(): void
    {
        $this->statut = match ($this->statut) {
            Presence::PRESENCE_NON_RENSEIGNEE => Presence::PRESENCE_PRESENCE,
            Presence::PRESENCE_PRESENCE => Presence::PRESENCE_ABSENCE_JUSTIFIEE,
            Presence::PRESENCE_ABSENCE_JUSTIFIEE => Presence::PRESENCE_ABSENCE_NON_JUSTIFIEE,
            Presence::PRESENCE_ABSENCE_NON_JUSTIFIEE => Presence::PRESENCE_NON_RENSEIGNEE,
            default => throw new RuntimeException("Valeur [" . $this->statut . "] non attendue"),
        };
    }

    public function getStatut(): string
    {
        return $this->statut;
    }

    public function setStatut(string $statut): void
    {
        $this->statut = $statut;
    }


    public function getCommentaire(): ?string
    {
        return $this->commentaire;
    }

    public function setCommentaire(string $commentaire): void
    {
        $this->commentaire = $commentaire;
    }

}
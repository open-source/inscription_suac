<?php

namespace Atelier\Entity\Db;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Fichier\Entity\Db\Fichier;
use Site\Entity\Db\Ville;
use UnicaenUtilisateur\Entity\Db\HistoriqueAwareInterface;
use UnicaenUtilisateur\Entity\Db\HistoriqueAwareTrait;

class Atelier implements HistoriqueAwareInterface
{
    use HistoriqueAwareTrait;

    private ?int $id = -1;
    private ?string $libelle = null;
    private ?string $description = null;
    private ?Groupe $groupe = null;

    private ?string $objectifs = null;
    private ?string $programme = null;
    private ?string $periode = null;
    private ?Fichier $miniature = null;

    private Collection $sessions;
    private Collection $programmes;
//    private Collection $abonnements;

    public function __construct()
    {
        $this->sessions = new ArrayCollection();
        $this->programmes = new ArrayCollection();
//        $this->abonnements = new ArrayCollection();

    }

    public function getId() : int
    {
        return $this->id;
    }

    public function getLibelle() : ?string
    {
        return $this->libelle;
    }

    public function setLibelle(?string $libelle) : void
    {
        $this->libelle = $libelle;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): void
    {
        $this->description = $description;
    }

    public function getGroupe(): ?Groupe
    {
        return $this->groupe;
    }

    public function setGroupe(?Groupe $groupe): void
    {
        $this->groupe = $groupe;
    }

    /** @return Session[] */
    public function getSessions(): array
    {
        return $this->sessions->toArray();
    }

    /** @return Ville[] */
    public function getVilles(): array
    {
        $villes = new ArrayCollection();
        foreach ($this->getSessions() as $session) {
            $ville = $session->getSite()?->getVille();
            if($ville and !$villes->contains($ville)) $villes[] = $ville;
        }
        return  $villes->toArray();
    }

    public function getNomVilles(): string
    {
        $villes = $this->getVilles();
        $nomVilles = [];
        foreach ($villes as $ville) {
            $nomVilles[] = $ville->getNom();
        }
        $nomVilles = array_unique($nomVilles);
        //supprimer les valeurs null
        $nomVilles = array_filter($nomVilles);
        return implode(', ', $nomVilles);
    }

    public function getSites(): string
    {
        $sites = [];
        foreach ($this->getSessions() as $session) {
            $sites[] = $session->getSite()?->getLibelle();
        }
        $sites = array_unique($sites);
        //supprimer les valeurs null
        $sites = array_filter($sites);
        return implode(', ', $sites);
    }

    public function getLibelleEtVilles(): string
    {
        $libelle = $this->getLibelle();

        foreach ($this->getVilles() as $ville) {
            if ($ville) $libelle .= ' ( ' . $ville->getNom() . ' )';
        }

        return $libelle;
    }

    /** @return Programme[] */
    public function getProgrammes(): array
    {
        return $this->programmes->toArray();
    }

    public static function generateOptions(array $formations) : array
    {
        $groupes = [];
        foreach ($formations as $formation) $groupes[($formation->getGroupe()) ? $formation->getGroupe()->getLibelle() : "Sans groupe"][] = $formation;

        $options = [];
        foreach ($groupes as $libelle => $liste) {
            $optionsoptions = [];
            usort($liste, function (Atelier $a, Atelier $b) {
                return $a->getLibelle() <=> $b->getLibelle();
            });
            foreach ($liste as $formation) {
                $optionsoptions[] = [
                    'value' => $formation->getId(),
                    'label' => $formation->getLibelle(),
                ];
            }
            $array = [
                'label' => $libelle,
                'options' => $optionsoptions,
            ];
            $options[] = $array;
        }
        return $options;
    }


    public function getObjectifs(): ?string
    {
        return $this->objectifs;
    }

    public function setObjectifs(?string $objectifs): void
    {
        $this->objectifs = $objectifs;
    }

    public function getProgramme(): ?string
    {
        return $this->programme;
    }

    public function setProgramme(?string $programme): void
    {
        $this->programme = $programme;
    }

    public function getPeriode(): ?string
    {
        return $this->periode;
    }

    public function setPeriode(?string $periode): void
    {
        $this->periode = $periode;
    }

    public function getMiniature(): ?Fichier
    {
        return $this->miniature;
    }

    public function setMiniature(?Fichier $miniature): void
    {
        $this->miniature = $miniature;
    }

}
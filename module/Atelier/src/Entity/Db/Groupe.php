<?php

namespace Atelier\Entity\Db;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use UnicaenUtilisateur\Entity\Db\HistoriqueAwareInterface;
use UnicaenUtilisateur\Entity\Db\HistoriqueAwareTrait;

class Groupe implements HistoriqueAwareInterface
{
    use HistoriqueAwareTrait;

    private ?int $id = -1;
    private ?string $libelle = null;
    private ?string $description = null;
    private ?int $ordre = 0;
    private Collection $ateliers;

    public function __construct()
    {
        $this->ateliers = new ArrayCollection();
    }

    public function getId() : int
    {
        return $this->id;
    }

    /** /!\ NB: utilise pour creer le groupe : sans groupe */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getLibelle() : ?string
    {
        return $this->libelle;
    }

    public function setLibelle(?string $libelle) : void
    {
        $this->libelle = $libelle;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): void
    {
        $this->description = $description;
    }

    public function getOrdre() : ?int
    {
        return $this->ordre;
    }

    public function setOrdre(?int $ordre) : void
    {
        $this->ordre = $ordre;
    }

    /**
     * @return Atelier[]
     */
    public function getAteliers() : array
    {
        return $this->ateliers->toArray();
    }

}
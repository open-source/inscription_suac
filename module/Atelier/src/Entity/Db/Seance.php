<?php

namespace Atelier\Entity\Db;

use DateTime;
use UnicaenUtilisateur\Entity\Db\HistoriqueAwareInterface;
use UnicaenUtilisateur\Entity\Db\HistoriqueAwareTrait;

class Seance implements HistoriqueAwareInterface
{
    use HistoriqueAwareTrait;

    const TYPE_SEANCE = "SEANCE";
    const TYPE_VOLUME = "VOLUME";
    const TYPES = [
        Seance::TYPE_SEANCE => "Séance",
        Seance::TYPE_VOLUME => "Volume horaire",
    ];

    private ?int $id = null;
    private ?Session $session = null;
    private string $type = self::TYPE_SEANCE;

    //seance
    private ?DateTime $jour = null;
    private ?string $debut = null;
    private ?string $fin = null;
    private ?string $lieu = null;

    //volume
    private ?float $volume = null;
    private ?DateTime $volumeDebut = null;
    private ?DateTime $volumeFin = null;

    private ?string $remarque = null;

    public function getId() : ?int
    {
        return $this->id;
    }

    public function getSession(): ?Session
    {
        return $this->session;
    }

    public function setSession(?Session $session): void
    {
        $this->session = $session;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function setType(string $type): void
    {
        $this->type = $type;
    }

    public function getJour(): ?DateTime
    {
        return $this->jour;
    }

    public function setJour(?DateTime $jour): void
    {
        $this->jour = $jour;
    }

    public function getDebut(): ?string
    {
        return $this->debut;
    }

    public function setDebut(?string $debut): void
    {
        $this->debut = $debut;
    }

    public function getFin(): ?string
    {
        return $this->fin;
    }

    public function setFin(?string $fin): void
    {
        $this->fin = $fin;
    }

    public function getLieu(): ?string
    {
        return $this->lieu;
    }

    public function setLieu(?string $lieu): void
    {
        $this->lieu = $lieu;
    }

    public function getVolume(): ?float
    {
        return $this->volume;
    }

    public function setVolume(?float $volume): void
    {
        $this->volume = $volume;
    }

    public function getVolumeDebut(): ?DateTime
    {
        return $this->volumeDebut;
    }

    public function setVolumeDebut(?DateTime $volumeDebut): void
    {
        $this->volumeDebut = $volumeDebut;
    }

    public function getVolumeFin(): ?DateTime
    {
        return $this->volumeFin;
    }

    public function setVolumeFin(?DateTime $volumeFin): void
    {
        $this->volumeFin = $volumeFin;
    }

    public function getRemarque(): ?string
    {
        return $this->remarque;
    }

    public function setRemarque(?string $remarque): void
    {
        $this->remarque = $remarque;
    }

    public function getDateTimeDebut() : ?DateTime
    {
        $date = DateTime::createFromFormat('d/m/Y H:i', $this->getJour()->format('d/m/Y') . ' '. $this->getDebut());
        return $date;
    }

    public function getDateTimeFin() : ?DateTime
    {
        $date = DateTime::createFromFormat('d/m/Y H:i', $this->getJour()->format('d/m/Y') . ' '. $this->getFin());
        return $date;

    }

    public function isPremiereSeance(): bool
    {
        $session = $this->getSession();
        $premiere = $session->getPremiereSeance();
        $res =  ($session->getSeances() === null OR $session->getSeances() === []
            OR $this->getDateTimeDebut() <= $premiere->getDateTimeDebut());
        return $res;
    }

    public function isDerniereSeance(): bool
    {
        $session = $this->getSession();
        $derniere = $session->getDerniereSeance();
        if ($derniere === null) return true;
        $res = ($session->getSeances() === null OR $session->getSeances() === []
                OR $this->getDateTimeFin() >= $derniere->getDateTimeFin());
        return $res;
    }


    /** FONCTIONS POUR MACROS  ****************************************************************************************/

    /** @noinspection PhpUnused  [Seance#Lieu] */
    public function getLieuString(): string
    {
        if ($this->getLieu() === null) return "Aucun lieu de précisé";
        return $this->getLieu();
    }

    /** @noinspection PhpUnused  [Seance#DateDebut] */
    public function getDateDebutString(): string
    {
        if ($this->getDateTimeDebut() === null) return "Aucune date de début";
        return $this->getDateTimeDebut()->format('d/m/Y à H:i');
    }

    /** @noinspection PhpUnused  [Seance#DateFin] */
    public function getDateFinString(): string
    {
        if ($this->getDateTimeFin() === null) return "Aucune date de fin";
        return $this->getDateTimeFin()->format('d/m/Y à H:i');
    }

    /** @noinspection PhpUnused  [Seance#DatePeriode] */
    public function getPeriodeString(): string
    {
        $texte = "";
        if ($this->getJour() === null) $texte .= "Aucune date de jour";
        else $texte .= $this->getJour()->format('d/m/Y');
        $texte .= " de  ";
        if ($this->getDebut() === null) $texte .= "aucune heure de début";
        else $texte .= $this->getDebut();
        $texte .= " à ";
        if ($this->getFin() === null) $texte .= "aucune heure de fin";
        else $texte .= $this->getFin();

        return $texte;
    }

}
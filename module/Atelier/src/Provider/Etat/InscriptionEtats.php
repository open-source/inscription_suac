<?php

namespace Atelier\Provider\Etat;

class InscriptionEtats {

    const TYPE = "INSCRIPTION";

    const INSCRIPTION_DEMANDE         = 'INSCRIPTION_DEMANDE';
    const INSCRIPTION_VALIDER         = 'INSCRIPTION_VALIDER';
    const INSCRIPTION_REFUSER         = 'INSCRIPTION_REFUSER';
}
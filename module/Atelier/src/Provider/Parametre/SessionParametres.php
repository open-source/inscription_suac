<?php

namespace Atelier\Provider\Parametre;

class SessionParametres {

    const TYPE = 'SESSION';

    const AUTO_CONVOCATION = 'AUTO_CONVOCATION';
    const AUTO_RETOUR = 'AUTO_RETOUR';
    const AUTO_CLOTURE = 'AUTO_CLOTURE';
}
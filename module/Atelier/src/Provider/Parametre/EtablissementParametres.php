<?php

namespace Atelier\Provider\Parametre;

class EtablissementParametres {

    const TYPE = 'ETABLISSEMENT';

    const LOGO = 'LOGO';
    const LIBELLE = 'LIBELLE';
    const SOUSLIBELLE = 'SOUSLIBELLE';
}
<?php

namespace Atelier\Provider\Privilege;

use UnicaenPrivilege\Provider\Privilege\Privileges;

class InscriptionPrivileges extends Privileges
{
    const INSCRIPTION_AUTOINSCRIPTION = 'inscription-inscription_autoinscription';
    const INSCRIPTION_ANNULER = 'inscription-inscription_annuler';

    const INSCRIPTION_GESTIONNAIRE = 'inscription-inscription_gestionnaire';
    const INSCRIPTION_ETUDIANT = 'inscription-inscription_etudiant';
    const INSCRIPTION_ENQUETE = 'inscription-inscription_enquete';


    const INSCRIPTION_CONVOCATION = 'inscription-inscription_convocation';
    const INSCRIPTION_ATTESTATION = 'inscription-inscription_attestation';
    const INSCRIPTION_HISTORIQUE = 'inscription-inscription_historique';
}

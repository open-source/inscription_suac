<?php

namespace Atelier\Provider\Privilege;

use UnicaenPrivilege\Provider\Privilege\Privileges;

class SessionPrivileges extends Privileges
{
    const SESSION_INDEX = 'session-session_index';
    const SESSION_INFORMATIONS = 'session-session_informations';
    const SESSION_AFFICHER = 'session-session_afficher';
    const SESSION_AJOUTER = 'session-session_ajouter';
    const SESSION_MODIFIER = 'session-session_modifier';
    const SESSION_HISTORISER = 'session-session_historiser';
    const SESSION_SUPPRIMER = 'session-session_supprimer';

    const SESSION_GERER_SEANCE = 'session-session_gerer_seance';
    const SESSION_GERER_INSCRIPTION = 'session-session_gerer_inscription';
    const SESSION_AFFICHER_PRESENCE = 'session-session_afficher_presence';
    const SESSION_GERER_PRESENCE = 'session-session_gerer_presence';
    const SESSION_GERER_ETAT = 'session-session_gerer_etat';

    const SESSION_INSCRIPTION_GESTIONNAIRE = 'session-session_inscription_gestionnaire';
    const SESSION_INSCRIPTION_ETUDIANT = 'session-session_inscription_etudiant';
    const SESSION_EMARGEMENT = 'session-session_emargement';


}

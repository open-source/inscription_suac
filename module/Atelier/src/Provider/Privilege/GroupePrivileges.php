<?php

namespace Atelier\Provider\Privilege;

use UnicaenPrivilege\Provider\Privilege\Privileges;

class GroupePrivileges extends Privileges
{
    const GROUPE_INDEX = 'groupe-groupe_index';
    const GROUPE_AFFICHER = 'groupe-groupe_afficher';
    const GROUPE_AJOUTER = 'groupe-groupe_ajouter';
    const GROUPE_MODIFIER = 'groupe-groupe_modifier';
    const GROUPE_HISTORISER = 'groupe-groupe_historiser';
    const GROUPE_SUPPRIMER = 'groupe-groupe_supprimer';
}
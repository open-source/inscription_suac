<?php

namespace Atelier\Provider\Privilege;

use UnicaenPrivilege\Provider\Privilege\Privileges;

class ProgrammePrivileges extends Privileges
{
    const PROGRAMME_INDEX = 'programme-programme_index';
    const PROGRAMME_AFFICHER = 'programme-programme_afficher';
    const PROGRAMME_AJOUTER = 'programme-programme_ajouter';
    const PROGRAMME_MODIFIER = 'programme-programme_modifier';
    const PROGRAMME_HISTORISER = 'programme-programme_historiser';
    const PROGRAMME_SUPPRIMER = 'programme-programme_supprimer';

    const PROGRAMME_ACTIONS_COURANTES = 'programme-programme_actions_courantes';
    const PROGRAMME_GERER_ACTION = 'programme-programme_gerer_action';
}

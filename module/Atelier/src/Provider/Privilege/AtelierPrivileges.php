<?php

namespace Atelier\Provider\Privilege;

use UnicaenPrivilege\Provider\Privilege\Privileges;

class AtelierPrivileges extends Privileges
{
    const ATELIER_INDEX = 'atelier-atelier_index';
    const ATELIER_AFFICHER = 'atelier-atelier_afficher';
    const ATELIER_AJOUTER = 'atelier-atelier_ajouter';
    const ATELIER_MODIFIER = 'atelier-atelier_modifier';
    const ATELIER_HISTORISER = 'atelier-atelier_historiser';
    const ATELIER_SUPPRIMER = 'atelier-atelier_supprimer';
}
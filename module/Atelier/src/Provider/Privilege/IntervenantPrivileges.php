<?php

namespace Atelier\Provider\Privilege;

use UnicaenPrivilege\Provider\Privilege\Privileges;

class IntervenantPrivileges extends Privileges
{
    const INTERVENANT_INDEX = 'intervenant-intervenant_index';
    const INTERVENANT_AFFICHER = 'intervenant-intervenant_afficher';
    const INTERVENANT_AJOUTER = 'intervenant-intervenant_ajouter';
    const INTERVENANT_MODIFIER = 'intervenant-intervenant_modifier';
    const INTERVENANT_HISTORISER = 'intervenant-intervenant_historiser';
    const INTERVENANT_SUPPRIMER = 'intervenant-intervenant_supprimer';
    const INTERVENANT_MES_SESSIONS = 'intervenant-intervenant_mes_sessions';
}
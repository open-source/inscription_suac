<?php

namespace Atelier\Provider\Identity;

use Atelier\Service\Intervenant\IntervenantService;
use Interop\Container\ContainerInterface;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use UnicaenUtilisateur\Service\Role\RoleService;
use UnicaenUtilisateur\Service\User\UserService;

class IdentityProviderFactory
{
    /**
     * @param ContainerInterface $container
     * @return IdentityProvider
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): IdentityProvider
    {
        /**
         * @var IntervenantService $intervenantService
         */
        $intervenantService = $container->get(IntervenantService::class);
        $roleService = $container->get(RoleService::class);
        $userService = $container->get(UserService::class);

        $service = new IdentityProvider();
        $service->setIntervenantService($intervenantService);
        $service->setRoleService($roleService);
        $service->setUserService($userService);
        return $service;
    }
}
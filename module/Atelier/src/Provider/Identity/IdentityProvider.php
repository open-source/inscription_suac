<?php

namespace Atelier\Provider\Identity;

use Atelier\Provider\Role\RolesProvider;
use Atelier\Service\Intervenant\IntervenantService;
use Atelier\Service\Intervenant\IntervenantServiceAwareTrait;
use UnicaenUtilisateur\Entity\Db\RoleInterface;
use UnicaenUtilisateur\Entity\Db\User;
use UnicaenUtilisateur\Provider\Identity\AbstractIdentityProvider;
use UnicaenUtilisateur\Service\Role\RoleServiceAwareTrait;
use UnicaenUtilisateur\Service\User\UserServiceAwareTrait;

class IdentityProvider extends AbstractIdentityProvider
{
    use IntervenantServiceAwareTrait;
    use RoleServiceAwareTrait;
    use UserServiceAwareTrait;

    /**
     * @param string $code
     * @return User[]|null
     */
    public function computeUsersAutomatiques(string $code): ?array
    {
        switch ($code)  {
            case RolesProvider::ROLE_INTERVENANT :
                $user = $this->getIntervenantService()->getUsersInIntervenant();
                return $user;

        }
        return null;
    }

    /**
     * @param User|null $user
     * @return string[]|RoleInterface[]
     */
    public function computeRolesAutomatiques(?User $user = null): array
    {
        $roles = [];

        if ($user === null) {
            $user = $this->getUserService()->getConnectedUser();
        }

        $intervenants = $this->getIntervenantService()->getIntervenantsByUser($user);
        if (!empty($intervenants)) {
            $roleIntervenant = $this->getRoleService()->findByRoleId(RolesProvider::ROLE_INTERVENANT);
            $roles[] = $roleIntervenant;
        }
        return $roles;
    }
}
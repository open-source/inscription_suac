<?php

namespace Atelier\Provider\Evenement;

class SessionEvenements {

    const CONVOQUER_EVENEMENT = 'convoquer-evenement';
    const RETOUR_EVENEMENT = 'retour-evenement';
    const CLOTURER_EVENEMENT = 'cloturer-evenement';
}
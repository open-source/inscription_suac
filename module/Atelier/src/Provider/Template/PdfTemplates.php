<?php

namespace Atelier\Provider\Template;

class PdfTemplates {

    const CONVOCATION = 'CONVOCATION';
    const ATTESTATION = 'ATTESTATION';
    const HISTORIQUE = 'HISTORIQUE';
    const EMARGEMENT = 'EMARGEMENT';

}
<?php

namespace Atelier\Form\Programme;

use Psr\Container\ContainerInterface;

class ProgrammeFormFactory
{

    public function __invoke(ContainerInterface $container): ProgrammeForm
    {
        /** @var ProgrammeHydrator $hydrator */
        $hydrator = $container->get('HydratorManager')->get(ProgrammeHydrator::class);

        $form = new ProgrammeForm();
        $form->setHydrator($hydrator);
        return $form;
    }
}
<?php

namespace Atelier\Form\Programme;

trait ProgrammeFormAwareTrait
{
    private ProgrammeForm $programmeForm;

    public function getProgrammeForm(): ProgrammeForm
    {
        return $this->programmeForm;
    }

    public function setProgrammeForm(ProgrammeForm $programmeForm): void
    {
        $this->programmeForm = $programmeForm;
    }

}
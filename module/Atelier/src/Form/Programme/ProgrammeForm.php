<?php

namespace Atelier\Form\Programme;

use Laminas\Form\Element\Button;
use Laminas\Form\Element\Text;
use Laminas\Form\Form;
use Laminas\InputFilter\Factory;
use UnicaenApp\Form\Element\Date;

class ProgrammeForm extends Form
{

    public function init() : void
    {
        //libelle
        $this->add([
            'type' => Text::class,
            'name' => 'libelle',
            'options' => [
                'label' => "Libelle <span class='icon icon-obligatoire' title='Champ obligatoire'></span> :",
                'label_options' => [ 'disable_html_escape' => true, ],
            ],
            'attributes' => [
                'id' => 'libelle',
            ],
        ]);
        //date_debut
        $this->add([
            'type' => Date::class,
            'name' => 'date_debut',
            'options' => [
                'label' => "Date de début <span class='icon icon-obligatoire' title='Champ obligatoire pour les volumes horaires' ></span> :",
                'label_options' => [ 'disable_html_escape' => true, ],
                'format' => 'd/m/Y',
            ],
            'attributes' => [
                'id' => 'date_debut',
            ],
        ]);
        //date_fin
        $this->add([
            'type' => Date::class,
            'name' => 'date_fin',
            'options' => [
                'label' => "Date de fin :",
                'label_options' => [ 'disable_html_escape' => true, ],
                'format' => 'd/m/Y',
            ],
            'attributes' => [
                'id' => 'date_fin',
            ],
        ]);
        //bouton
        $this->add([
            'type' => Button::class,
            'name' => 'bouton',
            'options' => [
                'label' => '<i class="fas fa-save"></i> Enregistrer',
                'label_options' => [
                    'disable_html_escape' => true,
                ],
            ],
            'attributes' => [
                'type' => 'submit',
                'class' => 'btn btn-primary',
            ],
        ]);
        $this->setInputFilter((new Factory())->createInputFilter([
            'libelle' => [ 'required' => true,  ],
            'date_debut' => [ 'required' => true,  ],
            'date_fin' => [ 'required' => false,  ],
        ]));
    }
}
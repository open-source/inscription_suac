<?php

namespace Atelier\Form\Programme;

use DateTime;
use Atelier\Entity\Db\Programme;
use Laminas\Hydrator\HydratorInterface;
use Psr\Container\ContainerInterface;

class ProgrammeHydratorFactory
{

    public function __invoke(ContainerInterface $container): ProgrammeHydrator
    {
        $hydrator = new ProgrammeHydrator();
        return $hydrator;
    }
}
<?php

namespace Atelier\Form\Programme;

use DateTime;
use Atelier\Entity\Db\Programme;
use Laminas\Hydrator\HydratorInterface;

class ProgrammeHydrator implements HydratorInterface
{

    public function extract(object $object): array
    {
        /** @var Programme $object */
        return [
            'libelle'  => $object->getLibelle(),
            'date_debut' => $object->getDateDebut()?->format('d/m/Y'),
            'date_fin' => $object->getDateFin()?->format('d/m/Y'),
        ];
    }

    public function hydrate(array $data, object $object): object
    {
        $libelle = (isset($data['libelle']) and trim($data['libelle']) !== '') ? trim($data['libelle']) : null;
        $dateDebut = (isset($data['date_debut'])) ? DateTime::createFromFormat('d/m/Y', $data['date_debut']) : null;
        $dateDebut = ($dateDebut === false) ? null : $dateDebut;
        $dateFin = (isset($data['date_fin'])) ? DateTime::createFromFormat('d/m/Y', $data['date_fin']) : null;
        $dateFin = ($dateFin === false) ? null : $dateFin;

        /** @var Programme $object */
        $object->setLibelle($libelle);
        $object->setDateDebut($dateDebut);
        $object->setDateFin($dateFin);
        return $object;
    }

}
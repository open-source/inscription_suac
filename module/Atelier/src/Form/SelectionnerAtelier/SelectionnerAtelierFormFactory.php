<?php

namespace Atelier\Form\SelectionnerAtelier;

use Atelier\Service\Atelier\AtelierService;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;

class SelectionnerAtelierFormFactory
{

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): SelectionnerAtelierForm
    {
        /**
         * @var AtelierService $atelierService
         * @var SelectionnerAtelierHydrator $hydrator
         */
        $atelierService = $container->get(AtelierService::class);
        $hydrator = $container->get('HydratorManager')->get(SelectionnerAtelierHydrator::class);

        $form = new SelectionnerAtelierForm();
        $form->setAtelierService($atelierService);
        $form->setHydrator($hydrator);
        return $form;

    }
}
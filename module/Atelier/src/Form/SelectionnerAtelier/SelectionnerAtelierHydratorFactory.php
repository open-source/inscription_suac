<?php

namespace Atelier\Form\SelectionnerAtelier;

use Atelier\Service\Atelier\AtelierService;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;

class SelectionnerAtelierHydratorFactory {

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): SelectionnerAtelierHydrator
    {
        /** @var AtelierService $atelierService */
        $atelierService = $container->get(AtelierService::class);

        $hydrator = new SelectionnerAtelierHydrator();
        $hydrator->setAtelierService($atelierService);
        return $hydrator;
    }
}
<?php

namespace Atelier\Form\SelectionnerAtelier;

trait SelectionnerAtelierFormAwareTrait
{
    private SelectionnerAtelierForm $selectionnerAtelierForm;

    public function getSelectionnerAtelierForm(): SelectionnerAtelierForm
    {
        return $this->selectionnerAtelierForm;
    }

    public function setSelectionnerAtelierForm(SelectionnerAtelierForm $selectionnerAtelierForm): void
    {
        $this->selectionnerAtelierForm = $selectionnerAtelierForm;
    }

}
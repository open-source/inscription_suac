<?php

namespace Atelier\Form\SelectionnerAtelier;

use Atelier\Entity\Db\Programme;
use Atelier\Service\Atelier\AtelierServiceAwareTrait;
use Laminas\Hydrator\HydratorInterface;

class SelectionnerAtelierHydrator implements HydratorInterface
{
    use AtelierServiceAwareTrait;

    public function extract(object $object): array
    {
        $ids = [];
        foreach ($object->getAteliers() as $atelier) $ids[] = $atelier->getId();

        return [
            'ateliers' => $ids,
        ];
    }

    public function hydrate(array $data, object $object): object
    {
        $ids = isset($data['ateliers']) ? $data['ateliers']: [];
        $ateliers = [];
        foreach ($ids as $id) {
            $ateliers[] = $this->getAtelierService()->getAtelier($id);
        }

        /** @var Programme $object */
        foreach ($object->getAteliers() as $atelier) {
            if (!in_array($atelier, $ateliers)) $object->removeAtelier($atelier);
        }
        foreach ($ateliers as $atelier) {
            $object->addAtelier($atelier);
        }
        return $object;
    }
}
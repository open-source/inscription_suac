<?php

namespace Atelier\Form\SelectionnerAtelier;

use Atelier\Service\Atelier\AtelierServiceAwareTrait;
use Laminas\Form\Element\Button;
use Laminas\Form\Element\Hidden;
use Laminas\Form\Element\Select;
use Laminas\Form\Form;
use Laminas\InputFilter\Factory;

class SelectionnerAtelierForm extends Form
{
    use AtelierServiceAwareTrait;

    public function init(): void
    {
        $this->add([
            'type' => Hidden::class,
            'name' => 'placeholder',
            'attributes' => [
                'value' => 'come on !',
            ],
        ]);
        //select multiple avec groupe
        $this->add([
            'type' => Select::class,
            'name' => 'ateliers',
            'options' => [
                'label' => "Ateliers associées :",
                'empty_option' => "Sélectionner les ateliers ...",
                'value_options' => $this->getAtelierService()->getAtelierAsOptions(),
            ],
            'attributes' => [
                'id' => 'ateliers',
                'class' => 'bootstrap-selectpicker show-tick',
                'data-live-search' => 'true',
                'multiple' => 'multiple',
            ],
        ]);
        //button
        $this->add([
            'type' => Button::class,
            'name' => 'enregistrer',
            'options' => [
                'label' => '<i class="fas fa-save"></i> Enregistrer',
                'label_options' => [
                    'disable_html_escape' => true,
                ],
            ],
            'attributes' => [
                'type' => 'submit',
                'class' => 'btn btn-primary',
            ],
        ]);
        //inputfilter
        $this->setInputFilter((new Factory())->createInputFilter([
            'ateliers' => ['required' => false,],
        ]));
    }
}
<?php

namespace Atelier\Form\Session;

use Laminas\Form\Element\Button;
use Laminas\Form\Element\Number;
use Laminas\Form\Element\Select;
use Laminas\Form\Element\Text;
use Laminas\Form\Element\Textarea;
use Laminas\Form\Form;
use Laminas\InputFilter\Factory;
use Site\Service\Site\SiteServiceAwareTrait;

class SessionForm extends Form
{
    use SiteServiceAwareTrait;

    public function init(): void
    {
        /** Complement */
        $this->add([
            'name' => 'description',
            'type' => Textarea::class,
            'options' => [
                'label' => 'Description :',
                'label_attributes' => [
                    'class' => 'control-label',
                ],
            ],
            'attributes' => [
                'id' => 'description',
                'class' => 'form-control tinymce',
            ],
        ]);
        /** Taille liste principale */
        $this->add([
            'type' => Number::class,
            'name' => 'principale',
            'options' => [
                'label' => "Nombre de place en liste principale <span class='icon icon-obligatoire' title='Champ obligatoire'></span> :",
                'label_options' => [ 'disable_html_escape' => true, ],
            ],
            'attributes' => [
                'id' => 'principale',
            ],
        ]);
        /** Taille liste complémentaire */
        $this->add([
            'type' => Number::class,
            'name' => 'complementaire',
            'options' => [
                'label' => "Nombre de place en liste complémentaire <span class='icon icon-obligatoire' title='Champ obligatoire'></span> :",
                'label_options' => [ 'disable_html_escape' => true, ],
            ],
            'attributes' => [
                'id' => 'complementaire',
            ],
        ]);
        /** Taille liste complementaire */
        $this->add([
            'type' => Select::class,
            'name' => 'inscription',
            'options' => [
                'label' => "Inscriptions directes par les agents <span class='icon icon-obligatoire' title='Champ obligatoire'></span> :",
                'label_options' => [ 'disable_html_escape' => true, ],
                'empty_option' => "Sélectionner un type d'inscription ...",
                'value_options' => [
                    false => "Non, les étudiant·es sont inscrit·es par les gestionnaires de formation",
                    true => "Oui, les étudiant·es peuvent s'inscrire directement dans l'application",
                ],
            ],
            'attributes' => [
                'id' => 'inscription',
            ],
        ]);
        /** lieu */
        $this->add([
            'type' => Text::class,
            'name' => 'lieu',
            'options' => [
                'label' => "Lieu <span class='icon icon-obligatoire'></span> :",
                'label_options' => [ 'disable_html_escape' => true, ],
            ],
            'attributes' => [
                'id' => 'lieu',
            ],
        ]);

        /** Site */
        $this->add([
            'type' => Select::class,
            'name' => 'site',
            'options' => [
                'label' => "Site <span class='icon ' title='Champ obligatoire'></span> :",
                'label_options' => [ 'disable_html_escape' => true, ],
                'empty_option' => "Sélectionner un site ...",
                'value_options' => $this->getSiteService()->getSitesAsOptions(),
            ],
            'attributes' => [
                'id' => 'site',
            ],
        ]);

        //button
        $this->add([
            'type' => Button::class,
            'name' => 'creer',
            'options' => [
                'label' => '<i class="fas fa-save"></i> Enregistrer',
                'label_options' => [ 'disable_html_escape' => true, ],
            ],
            'attributes' => [
                'type' => 'submit',
                'class' => 'btn btn-primary',
            ],
        ]);

        $this->setInputFilter((new Factory())->createInputFilter([
            'description' => ['required' => false,],
            'principale' => ['required' => true,],
            'complementaire' => ['required' => true,],
            'inscription' => ['required' => true,],
            'lieu' => ['required' => true,],
            'site' => ['required' => false,],
        ]));
    }
}
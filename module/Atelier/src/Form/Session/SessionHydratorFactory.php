<?php

namespace Atelier\Form\Session;

use Interop\Container\ContainerInterface;
use Site\Service\Site\SiteService;

class SessionHydratorFactory
{
    /**
     * @param ContainerInterface $container
     * @return SessionHydrator
     */
    public function __invoke(ContainerInterface $container): SessionHydrator
    {
        /** @var SiteService $siteService */
        $siteService =  $container->get(SiteService::class);

        $hydrator = new SessionHydrator();

        $hydrator->setSiteService($siteService);

        return $hydrator;
    }
}
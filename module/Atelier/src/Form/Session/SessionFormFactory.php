<?php

namespace Atelier\Form\Session;

use Interop\Container\ContainerInterface;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use Site\Service\Site\SiteService;

class SessionFormFactory
{

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): SessionForm
    {
        /** @var SessionHydrator $hydrator */
        $hydrator = $container->get('HydratorManager')->get(SessionHydrator::class);
        $serviceSite = $container->get(SiteService::class);

        $form = new SessionForm();
        $form->setHydrator($hydrator);
        $form->setSiteService($serviceSite);

        return $form;
    }
}
<?php

namespace Atelier\Form\Session;

use Atelier\Entity\Db\Session;
use Laminas\Hydrator\HydratorInterface;
use Site\Service\Site\SiteServiceAwareTrait;

class SessionHydrator implements HydratorInterface
{
    use SiteServiceAwareTrait;

    public function extract($object) : array
    {
        /** @var Session $object */
        $data = [
            'description' => $object->getComplement() ?: null,
            'principale' => ($object AND $object->getNbPlacePrincipale()) ?$object->getNbPlacePrincipale(): 0,
            'complementaire' => ($object AND $object->getNbPlaceComplementaire()) ?$object->getNbPlaceComplementaire(): 0,
            'lieu' => ($object AND $object->getLieu()) ?  $object->getLieu() : null,
            'inscription' => ($object) ? $object->getAutoInscription() : null,
            'site' => ($object AND $object->getSite()) ? $object->getSite()->getId() : null,
        ];
        return $data;
    }

    public function hydrate(array $data, $object): object
    {
        $description = (isset($data['description']) and trim($data['description']) !== "") ? trim($data['description']) : null;
        $principale = (isset($data['principale'])) ? ((int)$data['principale']) : 0;
        $complementaire = (isset($data['complementaire'])) ? ((int)$data['complementaire']) : 0;
        $lieu = (isset($data['lieu']) and trim($data['lieu']) !== "") ? trim($data['lieu']) : null;
        $inscription = (isset($data['inscription']))?$data['inscription'] : false;
        $site = (isset($data['site'])) ? $this->getSiteService()->getSite($data['site']) : null;

        /** @var Session $object */
        $object->setComplement($description);
        $object->setNbPlacePrincipale($principale);
        $object->setNbPlaceComplementaire($complementaire);
        $object->setLieu($lieu);
        $object->setAutoInscription($inscription);
        $object->setSite($site);

        return $object;
    }


}
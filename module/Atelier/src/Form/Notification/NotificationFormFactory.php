<?php

namespace Atelier\Form\Notification;

use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;

class NotificationFormFactory
{
    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): NotificationForm
    {
        $hydrator = $container->get('HydratorManager')->get(NotificationHydrator::class);
        $form = new NotificationForm();
        $form->setHydrator($hydrator);
        return $form;
    }

}
<?php

namespace Atelier\Form\Notification;

use Atelier\Entity\Db\Inscription;
use Laminas\Form\Element\Button;
use Laminas\Form\Element\Select;
use Laminas\Form\Element\Text;
use Laminas\Form\Element\Textarea;
use Laminas\Form\Form;
use Laminas\InputFilter\Factory;

class NotificationForm extends Form {

    public function init(): void
    {
        // liste
        $this->add([
            'type' => Select::class,
            'name' => 'liste',
            'options' => [
                'label' => "Liste à notifier <span class='icon icon-obligatoire' title='Obligatoire'></span>  :",
                'label_options' => [
                    'disable_html_escape' => true,
                ],
                'empty_option' => "Sélectionner une liste d'inscrit·e ...",
                'value_options' => [
                    Inscription::PRINCIPALE => 'Liste principale',
                    Inscription::COMPLEMENTAIRE => 'Liste complémentaire',
                    'tous' => 'Tou·tes les inscrit·es',
                ],
            ],
            'attributes' => [
                'id' => 'liste',
                'class'             => 'bootstrap-selectpicker show-tick',
                'data-live-search'  => 'true',
            ],
        ]);
        // libelle
        $this->add([
            'type' => Text::class,
            'name' => 'sujet',
            'options' => [
                'label' => "Sujet du courrier <span class='icon icon-obligatoire' title='Champ obligatoire'></span>:",
                'label_options' => [ 'disable_html_escape' => true, ],
            ],
            'attributes' => [
                'id' => 'sujet',
            ],
        ]);
        // corps
        $this->add([
            'name' => 'corps',
            'type' => Textarea::class,
            'options' => [
                'label' => "Corps du courrier <span class='icon icon-obligatoire' title='Champ obligatoire'></span>:",
                'label_options' => [ 'disable_html_escape' => true, ],
            ],
            'attributes' => [
                'id' => 'corps',
                'class' => 'tinymce',
            ],
        ]);
    //bouton
        $this->add([
            'type' => Button::class,
            'name' => 'creer',
            'options' => [
                'label' => '<span class="icon icon-notifier"></span> Notifier ',
                'label_options' => [
                    'disable_html_escape' => true,
                ],
            ],
            'attributes' => [
                'type' => 'submit',
                'class' => 'btn btn-success',
            ],
        ]);

        //input filter
        $this->setInputFilter((new Factory())->createInputFilter([
            'liste' => ['required' => true,],
            'sujet' => ['required' => true,],
            'corps' => ['required' => true,],
        ]));
    }
}
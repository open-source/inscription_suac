<?php

namespace Atelier\Form\Notification;

trait NotificationFormAwareTrait
{
    private NotificationForm $notificationForm;

    public function getNotificationForm(): NotificationForm
    {
        return $this->notificationForm;
    }

    public function setNotificationForm(NotificationForm $notificationForm): void
    {
        $this->notificationForm = $notificationForm;
    }

}
<?php

namespace Atelier\Form\Notification;

use Laminas\Hydrator\HydratorInterface;

class NotificationHydrator implements HydratorInterface
{
    public function extract(object $object): array
    {
        return [];
    }

    public function hydrate(array $data, object $object): object
    {
        return $object;
    }

}
<?php

namespace Atelier\Form\Notification;

use Psr\Container\ContainerInterface;

class NotificationHydratorFactory
{
    public function __invoke(ContainerInterface $container): NotificationHydrator
    {
        $hydrator = new NotificationHydrator();
        return $hydrator;
    }

}
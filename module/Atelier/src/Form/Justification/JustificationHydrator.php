<?php

namespace Atelier\Form\Justification;

use Atelier\Entity\Db\Inscription;
use Atelier\Provider\Etat\InscriptionEtats;
use Laminas\Hydrator\HydratorInterface;
use RuntimeException;

class JustificationHydrator implements HydratorInterface
{
    public function extract(object $object): array
    {
        /** @var Inscription $object */
        $motivation = match ($object->getEtape()) {
            Inscription::ETAPE_ETUDIANT => $object->getJustificationEtudiant(),
            Inscription::ETAPE_VALIDATION => $object->getJustificationValidation(),
            Inscription::ETAPE_REFUS => $object->getJustificationRefus(),
            default => throw new RuntimeException("[InscriptionExtraction] Etape d'inscription [" . $object->getEtape() . "] non gérée."),
        };

        $data = [
            'motivation' => $motivation,
            'internationnal' => $object->getInternational()??false,
            'telephone' => $object->getTelephone()??null,
        ];
        return $data;
    }

    public function hydrate(array $data, object $object): object
    {
        /** @var Inscription $object */
        $motivation = (isset($data['motivation']) and trim($data['motivation']) !== '') ? trim($data['motivation']) : null;
        $internationnal = (isset($data['internationnal']))?$data['internationnal']:false;
        $telephone = (isset($data['telephone']) AND trim($data['telephone']) !== "")?trim($data['telephone']):null;

        switch ($object->getEtape()) {
            case Inscription::ETAPE_ETUDIANT :
                $object->setJustificationEtudiant($motivation);
                $object->setInternational($internationnal);
                $object->setTelephone($telephone);
                break;
            case Inscription::ETAPE_VALIDATION :
                $object->setJustificationValidation($motivation);
                break;
            case Inscription::ETAPE_REFUS :
                $object->setJustificationRefus($motivation);
                break;
            default :
                throw new RuntimeException("[InscriptionHydration] Etape d'inscription [" . $object->getEtape() . "] non gérée.");
        }
        return $object;
    }

}
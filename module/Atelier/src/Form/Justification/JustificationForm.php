<?php

namespace Atelier\Form\Justification;

use Laminas\Form\Element\Button;
use Laminas\Form\Element\Checkbox;
use Laminas\Form\Element\Text;
use Laminas\Form\Element\Textarea;
use Laminas\Form\Form;
use Laminas\InputFilter\Factory;

class JustificationForm extends Form {

    public function init(): void
    {
        //motiviation
        $this->add([
            'type' => Textarea::class,
            'name' => 'motivation',
            'options' => [
                'label' => "Motivation <span class='icon icon-obligatoire' title='Champ obligatoire'></span> :",
                'label_options' => [ 'disable_html_escape' => true, ],
            ],
            'attributes' => [
                'id' => 'motivation',
                'class' => 'tinymce',
            ],
        ]);
        //internationnal
        $this->add([
            'type' => Checkbox::class,
            'name' => 'internationnal',
            'options' => [
                'label' => "Inscription dans le cadre d'étude internationnale <span class='icon icon-information' title='Information à but de statistique'></span>",
                'label_options' => [ 'disable_html_escape' => true, ],
            ],
            'attributes' => [
                'id' => 'internationnal',
            ],
        ]);
        //telpehone
        $this->add([
            'type' => Text::class,
            'name' => 'telephone',
            'options' => [
                'label' => "Téléphone <span class='icon icon-information' title='Ce numéro peut-être utilisé afin de vous contacter dans le cadre des ateliers'></span> :",
                'label_options' => [ 'disable_html_escape' => true, ],
            ],
            'attributes' => [
                'id' => 'telephone',
            ],
        ]);
        //bouton
        $this->add([
            'type' => Button::class,
            'name' => 'bouton',
            'options' => [
                'label' => '<i class="fas fa-save"></i> Enregistrer',
                'label_options' => [
                    'disable_html_escape' => true,
                ],
            ],
            'attributes' => [
                'type' => 'submit',
                'class' => 'btn btn-primary',
            ],
        ]);
        $this->setInputFilter((new Factory())->createInputFilter([
            'motivation' => [ 'required' => true,  ],
            'internationnal' => [ 'required' => false,  ],
            'telephone' => [ 'required' => false,  ],
        ]));
    }

}
<?php

namespace Atelier\Form\SeanceRecurrente;

use Psr\Container\ContainerInterface;

class SeanceRecurrenteHydratorFactory
{

    public function __invoke(ContainerInterface $container): SeanceRecurrenteHydrator
    {
        $hydrator = new SeanceRecurrenteHydrator();
        return $hydrator;
    }
}
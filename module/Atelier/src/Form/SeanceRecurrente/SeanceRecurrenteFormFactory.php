<?php

namespace Atelier\Form\SeanceRecurrente;

use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;

class SeanceRecurrenteFormFactory
{

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): SeanceRecurrenteForm
    {

        /**
         * @var SeanceRecurrenteHydrator $hydrator
         */
        $hydrator = $container->get('HydratorManager')->get(SeanceRecurrenteHydrator::class);

        $form = new SeanceRecurrenteForm();
        $form->setHydrator($hydrator);
        return $form;
    }

}
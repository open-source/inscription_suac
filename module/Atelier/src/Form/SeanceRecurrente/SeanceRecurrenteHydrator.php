<?php

namespace Atelier\Form\SeanceRecurrente;

use Laminas\Hydrator\HydratorInterface;

class SeanceRecurrenteHydrator implements HydratorInterface {

    public function extract(object $object): array
    {
        return [];
    }

    public function hydrate(array $data, object $object): object
    {
        return $object;
    }


}
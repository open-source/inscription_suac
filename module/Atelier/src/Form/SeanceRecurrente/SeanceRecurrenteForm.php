<?php

namespace Atelier\Form\SeanceRecurrente;

use Laminas\Form\Element\Button;
use Laminas\Form\Element\Date;
use Laminas\Form\Element\Radio;
use Laminas\Form\Element\Text;
use Laminas\Form\Element\Time;
use Laminas\Form\Form;
use Laminas\InputFilter\Factory;

class SeanceRecurrenteForm extends Form {

    public function init(): void
    {
        //jour de la premiere seance
        $this->add([
            'type' => Date::class,
            'name' => 'jour',
            'options' => [
                'label' => "Jour <span class='icon icon-obligatoire' title='Champ obligatoire' ></span> :",
                'label_options' => [ 'disable_html_escape' => true, ],
            ],
            'attributes' => [
                'id' => 'jour',
            ],
        ]);
        //debut
        $this->add([
            'type' => Time::class,
            'name' => 'debut',
            'options' => [
                'label' => "Début de la séance <span class='icon icon-obligatoire' title='Champ obligatoire' ></span> :",
                'label_options' => [ 'disable_html_escape' => true, ],
                'format' => 'H:i',
            ],
            'attributes' => [
                'id' => 'debut',
            ],
        ]);
        //fin
        $this->add([
            'type' => Time::class,
            'name' => 'fin',
            'options' => [
                'label' => "Fin de la séance <span class='icon icon-obligatoire' title='Champ obligatoire' ></span> :",
                'label_options' => [ 'disable_html_escape' => true, ],
                'format' => 'H:i'
            ],
            'attributes' => [
                'id' => 'fin',
            ],
        ]);
        //lieu
        $this->add([
            'type' => Text::class,
            'name' => 'lieu',
            'options' => [
                'label' => "Lieu  <span class='icon icon-obligatoire' title='Champ obligatoire' ></span> :",
                'label_options' => [ 'disable_html_escape' => true, ],
            ],
            'attributes' => [
                'id' => 'lieu',
            ],
        ]);

        //récurrence
        $this->add([
            'type'    => Radio::class,
            'name'    => 'type_recurrence',
            'id'         => 'type_recurrence',
            'options' => [
                'label'         => "Type de récurrence : <span class='icon icon-obligatoire'></span> ",
                'label_options' => [ 'disable_html_escape' => true, ],
                'value_options' => [
                    'journalière' => "Se répète chaque jour",
                    'hebdomadaire' => "Se répète chaque semaine",
                ],
            ],
            'attributes' => [
                'id'               => 'type_recurrence',
            ],
        ]);
        //fin de récurrence
        $this->add([
            'type' => Date::class,
            'name' => 'fin_recurrence',
            'options' => [
                'label' => "Fin de récurrence <span class='icon icon-obligatoire' title='Champ obligatoire' ></span> :",
                'label_options' => [ 'disable_html_escape' => true, ],
            ],
            'attributes' => [
                'id' => 'fin_recurrence',
            ],
        ]);
//button
        $this->add([
            'type' => Button::class,
            'name' => 'creer',
            'options' => [
                'label' => '<i class="fas fa-save"></i> Enregistrer',
                'label_options' => [ 'disable_html_escape' => true, ],
            ],
            'attributes' => [
                'type' => 'submit',
                'class' => 'btn btn-primary',
            ],
        ]);
        //input filter
        $this->setInputFilter((new Factory())->createInputFilter([
            'jour'   => [     'required' => true, ],
            'debut'   => [     'required' => true, ],
            'fin'   => [     'required' => true, ],
            'lieu'   => [     'required' => true, ],
            'type_recurrence'   => [     'required' => true, ],
            'fin_recurrence'   => [     'required' => true, ],
        ]));
    }
}
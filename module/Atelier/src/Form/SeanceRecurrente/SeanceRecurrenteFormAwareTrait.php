<?php

namespace Atelier\Form\SeanceRecurrente;

trait SeanceRecurrenteFormAwareTrait
{

    private SeanceRecurrenteForm $seanceRecurrenteForm;

    public function getSeanceRecurrenteForm(): SeanceRecurrenteForm
    {
        return $this->seanceRecurrenteForm;
    }

    public function setSeanceRecurrenteForm(SeanceRecurrenteForm $seanceRecurrenteForm): void
    {
        $this->seanceRecurrenteForm = $seanceRecurrenteForm;
    }

}
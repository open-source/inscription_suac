<?php

namespace Atelier\Form\Inscription;

use Application\Service\Agent\AgentServiceAwareTrait;
use Etudiant\Service\Etudiant\EtudiantServiceAwareTrait;
use Atelier\Entity\Db\Inscription;
use Atelier\Service\FormationInstance\FormationInstanceServiceAwareTrait;
use Atelier\Service\Session\SessionServiceAwareTrait;
use Atelier\Service\StagiaireExterne\StagiaireExterneServiceAwareTrait;
use Laminas\Hydrator\HydratorInterface;

class InscriptionHydrator implements HydratorInterface
{
    use EtudiantServiceAwareTrait;
    use SessionServiceAwareTrait;

    public function extract(object $object): array
    {
        /** @var Inscription $object */
        $data = [
            'session' => ($object->getSession()) ? ['id' => $object->getSession()->getId(), 'label' => $object->getSession()->getAtelier()->getLibelle()] : null,
            'etudiant' => ($object->getEtudiant()) ? ['id' => $object->getEtudiant()->getId(), 'label' => $object->getEtudiant()->getDenomination()] : null,
            'internationnal' => $object->getInternational()??false,
            'telephone' => $object->getTelephone()??null,
        ];
        return $data;
    }

    /** @return Inscription */
    public function hydrate(array $data, object $object): object
    {
        $session = (isset($data['session']) && $data['session']['id'] !== '') ? $this->getSessionService()->getSession((int)$data['session']['id']) : null;
        $etudiant = (isset($data['etudiant']) && $data['etudiant']['id'] !== '') ? $this->getEtudiantService()->getEtudiant($data['etudiant']['id']) : null;
        $internationnal = (isset($data['internationnal']))?$data['internationnal']:false;
        $telephone = (isset($data['telephone']) AND trim($data['telephone']) !== "")?trim($data['telephone']):null;


        /** @var Inscription $object */
        $object->setSession($session);
        $object->setEtudiant($etudiant);
        $object->setInternational($internationnal);
        $object->setTelephone($telephone);
        return $object;
    }

}
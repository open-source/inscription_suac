<?php

namespace Atelier\Form\Inscription;

use Laminas\View\Renderer\PhpRenderer;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;

class InscriptionFormFactory {

    /**
     * @param ContainerInterface $container
     * @return InscriptionForm
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container) : InscriptionForm
    {
        /**
         * @var InscriptionHydrator $hydrator
         */
        $hydrator = $container->get('HydratorManager')->get(InscriptionHydrator::class);

        /* @var PhpRenderer $renderer  */
        $renderer = $container->get('ViewRenderer');

        $form = new InscriptionForm();
        $form->setHydrator($hydrator);

        $form->sessionUrl = $renderer->url('atelier/session/rechercher', [], [], true);
        $form->agentUrl = $renderer->url('etudiant/rechercher', [], [], true);
        return $form;
    }
}
<?php

namespace Atelier\Form\Inscription;

use Laminas\Form\Element\Button;
use Laminas\Form\Element\Checkbox;
use Laminas\Form\Element\Radio;
use Laminas\Form\Element\Text;
use Laminas\Form\Form;
use Laminas\InputFilter\Factory;
use UnicaenApp\Form\Element\SearchAndSelect;

class InscriptionForm extends Form {

    public ?string $agentUrl = null;
    public ?string $sessionUrl = null;


    public function init(): void
    {
        // session
        $session = new SearchAndSelect('session', ['label' => "Session * :"]);
        $session
            ->setAutocompleteSource($this->sessionUrl)
            ->setSelectionRequired()
            ->setAttributes([
                'id' => 'session',
                'placeholder' => "Session ...",
            ]);
        $this->add($session);
        // etudiant
        $agent = new SearchAndSelect('etudiant', ['label' => "Étudiant·e associé·e à l'inscription * :"]);
        $agent
            ->setAutocompleteSource($this->agentUrl)
            ->setSelectionRequired()
            ->setAttributes([
                'id' => 'etudiant',
                'placeholder' => "Dénomination de l'étudiant·e ...",
            ]);
        $this->add($agent);
        //internationnal
        $this->add([
            'type' => Checkbox::class,
            'name' => 'internationnal',
            'options' => [
                'label' => "Inscription dans le cadre d'étude internationnale <span class='icon icon-information' title='Information à but de statistique'></span>",
                'label_options' => [ 'disable_html_escape' => true, ],
            ],
            'attributes' => [
                'id' => 'internationnal',
            ],
        ]);
        //telpehone
        $this->add([
            'type' => Text::class,
            'name' => 'telephone',
            'options' => [
                'label' => "Téléphone <span class='icon icon-information' title='Ce numéro peut-être utilisé afin de vous contacter dans le cadre des ateliers'></span> :",
                'label_options' => [ 'disable_html_escape' => true, ],
            ],
            'attributes' => [
                'id' => 'telephone',
            ],
        ]);
        // button
        $this->add([
            'type' => Button::class,
                'name' => 'valider',
                'options' => [
                    'label' => "<i class='fas fa-save'></i> Enregistrer l'inscription",
                    'label_options' => [
                        'disable_html_escape' => true,
                    ],
                ],
                'attributes' => [
                    'type' => 'submit',
                    'class' => 'btn btn-primary',
                    'id' => 'valider',
                ],
        ]);

        //inputFIlter
        $this->setInputFilter((new Factory())->createInputFilter([
            'session'         => [ 'required' => true,  ],
            'etudiant'        => [ 'required' => true,  ],
            'internationnal'  => [ 'required' => false, ],
            'telephone'       => [ 'required' => false, ],
        ]));
    }
}
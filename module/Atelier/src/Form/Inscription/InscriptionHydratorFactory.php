<?php

namespace Atelier\Form\Inscription;

use Application\Service\Agent\AgentService;
use Etudiant\Service\Etudiant\EtudiantService;
use Atelier\Service\FormationInstance\FormationInstanceService;
use Atelier\Service\Session\SessionService;
use Atelier\Service\StagiaireExterne\StagiaireExterneService;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;

class InscriptionHydratorFactory
{

    /**
     * @param ContainerInterface $container
     * @return InscriptionHydrator
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): InscriptionHydrator
    {
        /**
         * @var EtudiantService $etudiantService
         * @var SessionService $sessionService
         */
        $etudiantService = $container->get(EtudiantService::class);
        $sessionService = $container->get(SessionService::class);

        $hydrator = new InscriptionHydrator();
        $hydrator->setEtudiantService($etudiantService);
        $hydrator->setSessionService($sessionService);
        return $hydrator;
    }
}
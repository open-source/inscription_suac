<?php

namespace Atelier\Form\Intervenant;

use Atelier\Service\Intervenant\IntervenantService;
use Interop\Container\ContainerInterface;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;

class IntervenantFormFactory
{

    /**
     * @param ContainerInterface $container
     * @return IntervenantForm
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container) : IntervenantForm
    {
        /** @var IntervenantService $intervenantService */
        $intervenantService = $container->get(IntervenantService::class);
        /** @var IntervenantHydrator $hydrator */
        $hydrator = $container->get('HydratorManager')->get(IntervenantHydrator::class);

        $form = new IntervenantForm();
        $form->setIntervenantService($intervenantService);
        $form->setHydrator($hydrator);
        return $form;
    }
}
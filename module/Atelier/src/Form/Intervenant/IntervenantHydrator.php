<?php

namespace Atelier\Form\Intervenant;

use Atelier\Entity\Db\Intervenant;
use Laminas\Hydrator\HydratorInterface;

class IntervenantHydrator implements HydratorInterface
{

    public function extract($object): array
    {
        /** @var Intervenant $object */
        $data = [
            'prenom' => ($object) ? $object->getPrenom() : null,
            'nom' => ($object) ? $object->getNom() : null,
            'email' => ($object) ? $object->getEmail() : null,
            'rattachement' => ($object) ? $object->getRattachement() : null,
            'telephone' => ($object) ? $object->getTelephone() : null,

        ];
        return $data;
    }

    public function hydrate(array $data, $object) : object
    {
        $prenom = (isset($data['prenom']) and trim($data['prenom']) !== "") ? trim($data['prenom']) : null;
        $nom = (isset($data['nom']) and trim($data['nom']) !== "") ? trim($data['nom']) : null;
        $email = (isset($data['email']) and trim($data['email']) !== "") ? trim($data['email']) : null;
        $rattachement = (isset($data['rattachement']) and trim($data['rattachement']) !== "") ? trim($data['rattachement']) : null;
        $telephone = (isset($data['telephone']) and trim($data['telephone']) !== "") ? trim($data['telephone']) : null;

        /** @var Intervenant $object */
        $object->setPrenom($prenom);
        $object->setNom($nom);
        $object->setRattachement($rattachement);
        $object->setEmail($email);
        $object->setTelephone($telephone);

        return $object;
    }

}
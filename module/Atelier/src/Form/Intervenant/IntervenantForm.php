<?php

namespace Atelier\Form\Intervenant;

use Atelier\Service\Intervenant\IntervenantServiceAwareTrait;
use Laminas\Form\Element\Button;
use Laminas\Form\Element\Hidden;
use Laminas\Form\Element\Text;
use Laminas\Form\Form;
use Laminas\InputFilter\Factory;
use Laminas\Validator\Callback;

class IntervenantForm extends Form
{
    use IntervenantServiceAwareTrait;

    public function init(): void
    {
        //prenom
        $this->add([
            'type' => Text::class,
            'name' => 'prenom',
            'options' => [
                'label' => "Prénom <span class='icon icon-obligatoire' title='Champ obligatoire'></span> :",
                'label_options' => [ 'disable_html_escape' => true, ],
            ],
            'attributes' => [
                'id' => 'prenom',
            ],
        ]);
        //nom
        $this->add([
            'type' => Text::class,
            'name' => 'nom',
            'options' => [
                'label' => "Nom <span class='icon icon-obligatoire' title='Champ obligatoire'></span> :",
                'label_options' => [ 'disable_html_escape' => true, ],
            ],
            'attributes' => [
                'id' => 'nom',
            ],
        ]);
        //email
        $this->add([
            'type' => Text::class,
            'name' => 'email',
            'options' => [
                'label' => "Adresse électronique  <span class='icon icon-obligatoire' title='Champ obligatoire'></span> :",
                'label_options' => [ 'disable_html_escape' => true, ],
            ],
            'attributes' => [
                'id' => 'email',
            ],
        ]);
        $this->add([
            'name' => 'old-email',
            'type' => Hidden::class,
            'attributes' => [
                'value' => "",
            ],
        ]);
        //attachement
        $this->add([
            'type' => Text::class,
            'name' => 'rattachement',
            'options' => [
                'label' => "Structure de rattachement / Organisme  :",
            ],
            'attributes' => [
                'id' => 'rattachement',
            ],
        ]);
        //telephone
        $this->add([
            'type' => Text::class,
            'name' => 'telephone',
            'options' => [
                'label' => "Téléphone :",
            ],
            'attributes' => [
                'id' => 'telephone',
            ],
        ]);

        //button
        $this->add([
            'type' => Button::class,
            'name' => 'creer',
            'options' => [
                'label' => '<i class="fas fa-save"></i> Enregistrer',
                'label_options' => [ 'disable_html_escape' => true, ],
            ],
            'attributes' => [
                'type' => 'submit',
                'class' => 'btn btn-primary',
            ],
        ]);

        //inputfilter
        $this->setInputFilter((new Factory())->createInputFilter([
            'prenom' => ['required' => true],
            'nom' => ['required' => true],
            'email' => [
                'required' => false,
                'validators' => [
                    [
                        'name' => Callback::class,
                        'options' => [
                            'messages' => [
                                Callback::INVALID_VALUE => "Un·e intervenant·e utilise déjà cette adresse électronique",
                            ],
                            'callback' => function ($value, $context = []) {
                                if (trim($value) == $context['old-email']) return true;
                                $intervenants = $this->getIntervenantService()->getIntervenantsByEmail(trim($value));
                                return empty($intervenants);
                            },
                            //'break_chain_on_failure' => true,
                        ],
                    ],
                ],
            ],
            'old-email' => ['required' => false,],
            'rattachement' => ['required' => false],
            'telephone' => ['required' => false],
        ]));
    }

    public function setOldEmail(?string $email): void
    {
        $this->get('old-email')->setValue($email);
    }
}
<?php

namespace Atelier\Form\Intervenant;

trait IntervenantFormAwareTrait
{
    private IntervenantForm $intervenantForm;

    public function getIntervenantForm(): IntervenantForm
    {
        return $this->intervenantForm;
    }

    public function setIntervenantForm(IntervenantForm $intervenantForm): void
    {
        $this->intervenantForm = $intervenantForm;
    }
}
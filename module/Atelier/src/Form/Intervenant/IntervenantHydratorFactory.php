<?php

namespace Atelier\Form\Intervenant;

use Interop\Container\ContainerInterface;

class IntervenantHydratorFactory
{

    /**
     * @param ContainerInterface $container
     * @return IntervenantHydrator
     */
    public function __invoke(ContainerInterface $container) : IntervenantHydrator
    {
        $hydrator = new IntervenantHydrator();
        return $hydrator;
    }
}
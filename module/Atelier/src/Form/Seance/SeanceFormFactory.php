<?php

namespace Atelier\Form\Seance;

use Interop\Container\ContainerInterface;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;

class SeanceFormFactory
{

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container) : SeanceForm
    {
        /**
         * @var SeanceHydrator $hydrator
         */
        $hydrator = $container->get('HydratorManager')->get(SeanceHydrator::class);

        $form = new SeanceForm();
        $form->setHydrator($hydrator);
        return $form;
    }
}
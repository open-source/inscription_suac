<?php

namespace Atelier\Form\Seance;

use Laminas\Form\Element\Button;
use Laminas\Form\Element\Date;
use Laminas\Form\Element\Number;
use Laminas\Form\Element\Text;
use Laminas\Form\Element\Time;
use Laminas\Form\Form;
use Laminas\InputFilter\Factory;

class SeanceForm extends Form
{

    public function init(): void
    {
        //type
//        $this->add([
//                'type' => Select::class,
//                'name' => 'type',
//                'options' => [
//                    'label' => "Type de séance <span class='icon icon-obligatoire' title='Champ obligatoire pour les volumes horaires' ></span> :",
//                    'label_options' => [ 'disable_html_escape' => true, ],
//                    'value_options' => [
//                        Seance::TYPE_SEANCE => "Séance",
//                        Seance::TYPE_VOLUME => "Volume horaire",
//                    ],
//                ],
//                'attributes' => [
//                    'id' => 'type',
//                ],
//            ]
//
//        );

        /* SEANCE *********************************************************************/

        //jour
        $this->add([
            'type' => Date::class,
            'name' => 'jour',
            'options' => [
                'label' => "Jour <span class='icon icon-obligatoire' title='Champ obligatoire pour les volumes horaires' ></span> :",
                'label_options' => ['disable_html_escape' => true,],
            ],
            'attributes' => [
                'id' => 'jour',
            ],
        ]);
        //debut
        $this->add([
            'type' => Time::class,
            'name' => 'debut',
            'options' => [
                'label' => "Début de la journée <span class='icon icon-obligatoire' title='Champ obligatoire pour les volumes horaires' ></span> :",
                'label_options' => ['disable_html_escape' => true,],
                'format' => 'H:i',
            ],
            'attributes' => [
                'id' => 'debut',
            ],
        ]);
        //fin
        $this->add([
            'type' => Time::class,
            'name' => 'fin',
            'options' => [
                'label' => "Fin de la journée <span class='icon icon-obligatoire' title='Champ obligatoire pour les volumes horaires' ></span> :",
                'label_options' => ['disable_html_escape' => true,],
                'format' => 'H:i'
            ],
            'attributes' => [
                'id' => 'fin',
            ],
        ]);

        /* VOLUME *********************************************************************/

        //number
        $this->add([
            'type' => Number::class,
            'name' => "volume",
            'options' => [
                'label' => "Volume horaire de la formation <span class='icon icon-obligatoire' title='Champ obligatoire pour les volumes horaires' ></span> :",
                'label_options' => ['disable_html_escape' => true,],
            ],
            'attributes' => [
                'id' => 'volume',
            ],
        ]);
        //jour
        $this->add([
            'type' => Date::class,
            'name' => 'volume_debut',
            'options' => [
                'label' => "Date d'ouverture du volume <span class='icon icon-obligatoire' title='Champ obligatoire pour les volumes horaires' ></span> :",
                'label_options' => ['disable_html_escape' => true,],
                'format' => 'd/m/Y',
            ],
            'attributes' => [
                'id' => 'volume_debut',
            ],
        ]);
        //jour
        $this->add([
            'type' => Date::class,
            'name' => 'volume_fin',
            'options' => [
                'label' => "Date de fermeture du volume <span class='icon icon-obligatoire' title='Champ obligatoire pour les volumes horaires' ></span> :",
                'label_options' => ['disable_html_escape' => true,],
                'format' => 'd/m/Y',
            ],
            'attributes' => [
                'id' => 'volume_fin',
            ],
        ]);

        //salle
        $this->add([
            'type' => Text::class,
            'name' => "lieu",
            'label_options' => ['disable_html_escape' => true,],
            'options' => [
                'label' => "Lieu de la formation ou lien de visoconférence <span class='icon icon-obligatoire' title='Champ obligatoire pour les volumes horaires' ></span> :",
                'label_options' => ['disable_html_escape' => true,],
            ],
            'attributes' => [
                'id' => 'lieu',
            ],
        ]);
        //submit
        $this->add([
            'type' => Button::class,
            'name' => 'creer',
            'options' => [
                'label' => '<i class="fas fa-save"></i> Enregistrer',
                'label_options' => [
                    'disable_html_escape' => true,
                ],
            ],
            'attributes' => [
                'type' => 'submit',
                'class' => 'btn btn-primary',
            ],
        ]);

        //inputFilter
        $this->setInputFilter((new Factory())->createInputFilter([
//            'type' => ['required' => true,
//                'validators' => [[
//                    'name' => Callback::class,
//                    'options' => [
//                        'messages' => [
//                            Callback::INVALID_VALUE => "Une information obligatoire est manquante",
//                        ],
//                        'callback' => function ($value, $context = []) {
//                            if($context['type'] === Seance::TYPE_VOLUME) return ($context['volume'] !== '' AND $context['volume_debut'] AND $context['volume_fin'] !== '');
//                            if($context['type'] === Seance::TYPE_SEANCE) return ($context['jour'] !== '' AND $context['debut'] !== '' AND $context['fin'] !== '');
//                            return true;
//                        },
//                        //'break_chain_on_failure' => true,
//                    ],
//                ]],],
            'jour' => ['required' => true,

            ],
            'debut' => ['required' => true,],
            'fin' => ['required' => true,],
            'volume' => ['required' => false,],
            'volume_debut' => ['required' => false,],
            'volume_fin' => ['required' => false,],
            'lieu' => ['required' => true,],
        ]));
    }
}
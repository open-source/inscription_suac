<?php

namespace Atelier\Form\SelectionnerIntervenant;

use Atelier\Service\Intervenant\IntervenantService;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;

class SelectionnerIntervenantFormFactory
{

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): SelectionnerIntervenantForm
    {
        /**
         * @var IntervenantService $atelierService
         * @var SelectionnerIntervenantHydrator $hydrator
         */
        $atelierService = $container->get(IntervenantService::class);
        $hydrator = $container->get('HydratorManager')->get(SelectionnerIntervenantHydrator::class);

        $form = new SelectionnerIntervenantForm();
        $form->setIntervenantService($atelierService);
        $form->setHydrator($hydrator);
        return $form;

    }
}
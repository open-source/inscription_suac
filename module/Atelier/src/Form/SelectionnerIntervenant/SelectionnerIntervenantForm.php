<?php

namespace Atelier\Form\SelectionnerIntervenant;

use Atelier\Service\Intervenant\IntervenantServiceAwareTrait;
use Laminas\Form\Element\Button;
use Laminas\Form\Element\Hidden;
use Laminas\Form\Element\Select;
use Laminas\Form\Form;
use Laminas\InputFilter\Factory;

class SelectionnerIntervenantForm extends Form
{
    use IntervenantServiceAwareTrait;

    public function init(): void
    {
        $this->add([
            'type' => Hidden::class,
            'name' => 'placeholder',
            'attributes' => [
                'value' => 'come on !',
            ],
        ]);
        //select multiple avec groupe
        $this->add([
            'type' => Select::class,
            'name' => 'intervenants',
            'options' => [
                'label' => "Intervenant·es à associer :",
                'empty_option' => "Sélectionner les intervenant·es ...",
                'value_options' => $this->getIntervenantService()->getIntervenantsAsOptions(),
            ],
            'attributes' => [
                'id' => 'intervenants',
                'class' => 'bootstrap-selectpicker show-tick',
                'data-live-search' => 'true',
                'multiple' => 'multiple',
            ],
        ]);
        //button
        $this->add([
            'type' => Button::class,
            'name' => 'enregistrer',
            'options' => [
                'label' => '<i class="fas fa-save"></i> Enregistrer',
                'label_options' => [
                    'disable_html_escape' => true,
                ],
            ],
            'attributes' => [
                'type' => 'submit',
                'class' => 'btn btn-primary',
            ],
        ]);
        //inputfilter
        $this->setInputFilter((new Factory())->createInputFilter([
            'intervenants' => ['required' => false,],
        ]));
    }
}
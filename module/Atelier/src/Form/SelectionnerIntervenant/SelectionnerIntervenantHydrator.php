<?php

namespace Atelier\Form\SelectionnerIntervenant;

use Atelier\Entity\Db\Session;
use Atelier\Service\Intervenant\IntervenantServiceAwareTrait;
use Laminas\Hydrator\HydratorInterface;

class SelectionnerIntervenantHydrator implements HydratorInterface
{
    use IntervenantServiceAwareTrait;

    public function extract(object $object): array
    {
        $ids = [];

        /** @var Session $object */
        foreach ($object->getIntervenants() as $intervenant) $ids[] = $intervenant->getId();

        return [
            'intervenants' => $ids,
        ];
    }

    public function hydrate(array $data, object $object): object
    {
        $ids = isset($data['intervenants']) ? $data['intervenants'] : [];
        $intervenants = [];
        foreach ($ids as $id) {
            $intervenants[$id] = $this->getIntervenantService()->getIntervenant($id);
        }

        /** @var Session $object */
        foreach ($object->getIntervenants() as $intervenant) {
            if (!in_array($intervenant, $intervenants)) $object->removeIntervenant($intervenant);
        }
        foreach ($intervenants as $intervenant) {
            $object->addIntervenant($intervenant);
        }
        return $object;
    }
}
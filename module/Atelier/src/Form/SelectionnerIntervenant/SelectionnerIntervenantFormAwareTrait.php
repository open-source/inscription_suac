<?php

namespace Atelier\Form\SelectionnerIntervenant;

trait SelectionnerIntervenantFormAwareTrait
{
    private SelectionnerIntervenantForm $selectionnerIntervenantForm;

    public function getSelectionnerIntervenantForm(): SelectionnerIntervenantForm
    {
        return $this->selectionnerIntervenantForm;
    }

    public function setSelectionnerIntervenantForm(SelectionnerIntervenantForm $selectionnerIntervenantForm): void
    {
        $this->selectionnerIntervenantForm = $selectionnerIntervenantForm;
    }

}
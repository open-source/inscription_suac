<?php

namespace Atelier\Form\SelectionnerIntervenant;

use Atelier\Service\Intervenant\IntervenantService;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;

class SelectionnerIntervenantHydratorFactory {

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): SelectionnerIntervenantHydrator
    {
        /** @var IntervenantService $atelierService */
        $atelierService = $container->get(IntervenantService::class);

        $hydrator = new SelectionnerIntervenantHydrator();
        $hydrator->setIntervenantService($atelierService);
        return $hydrator;
    }
}
<?php

namespace Atelier\Form\Atelier;

trait AtelierFormAwareTrait
{

    private AtelierForm $atelierForm;

    public function getAtelierForm(): AtelierForm
    {
        return $this->atelierForm;
    }

    public function setAtelierForm(AtelierForm $atelierForm): AtelierForm
    {
        $this->atelierForm = $atelierForm;
        return $this->atelierForm;
    }

}
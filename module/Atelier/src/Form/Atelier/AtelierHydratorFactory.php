<?php

namespace Atelier\Form\Atelier;

use Atelier\Service\Groupe\GroupeService;
use Interop\Container\ContainerInterface;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;

class AtelierHydratorFactory
{

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): AtelierHydrator
    {
        /**
         * @var GroupeService $groupeService
         */
        $groupeService = $container->get(GroupeService::class);

        $hydrator = new AtelierHydrator();
        $hydrator->setGroupeService($groupeService);
        return $hydrator;
    }
}
<?php

namespace Atelier\Form\Atelier;

use Atelier\Service\Groupe\GroupeService;
use Interop\Container\ContainerInterface;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;

class AtelierFormFactory
{

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): AtelierForm
    {
        /**
         * @var GroupeService $groupeService
         */
        $groupeService = $container->get(GroupeService::class);

        /** @var AtelierHydrator $hydrator */
        $hydrator = $container->get('HydratorManager')->get(AtelierHydrator::class);

        /** @var AtelierForm $form */
        $form = new AtelierForm();
        $form->setGroupeService($groupeService);
        $form->setHydrator($hydrator);
        return $form;
    }
}
<?php

namespace Atelier\Form\Atelier;

use Atelier\Entity\Db\Atelier;
use Atelier\Service\Groupe\GroupeServiceAwareTrait;
use Laminas\Hydrator\HydratorInterface;

class AtelierHydrator implements HydratorInterface
{
    use GroupeServiceAwareTrait;

    public function extract($object): array
    {
        /** @var Atelier $object */
        $data = [
            'libelle' => $object->getLibelle(),
            'description' => $object->getDescription(),
            'groupe' => ($object->getGroupe()) ? $object->getGroupe()->getId() : null,
            'objectifs' => $object->getObjectifs(),
            'programme' => $object->getProgramme(),
            'periode' => $object->getPeriode(),
        ];

        return $data;
    }

    public function hydrate(array $data, object $object): object
    {
        $libelle = (isset($data['libelle']) && trim($data['libelle']) !== "") ? trim($data['libelle']) : null;
        $groupe = (isset($data['groupe']) && $data['groupe'] !== "") ? $this->getGroupeService()->getGroupe($data['groupe']) : null;
        $description = (isset($data['description']) && trim($data['description']) != '') ? trim($data['description']) : null;
        $objectifs = (isset($data['objectifs']) && trim($data['objectifs']) != '') ? trim($data['objectifs']) : null;
        $programme = (isset($data['programme']) && trim($data['programme']) != '') ? trim($data['programme']) : null;
        $periode = (isset($data['periode']) && trim($data['periode']) != '') ? trim($data['periode']) : null;

        /** @var Atelier $object */
        $object->setLibelle($libelle);
        $object->setDescription($description);
        $object->setGroupe($groupe);
        $object->setObjectifs($objectifs);
        $object->setProgramme($programme);
        $object->setPeriode($periode);
        return $object;
    }


}
<?php

namespace Atelier\Form\Atelier;

use Atelier\Entity\Db\Atelier;
use Atelier\Service\Groupe\GroupeServiceAwareTrait;
use Laminas\Form\Element\Button;
use Laminas\Form\Element\Radio;
use Laminas\Form\Element\Select;
use Laminas\Form\Element\Text;
use Laminas\Form\Element\Textarea;
use Laminas\Form\Form;
use Laminas\InputFilter\Factory;

class AtelierForm extends Form
{
    use GroupeServiceAwareTrait;

    public function init(): void
    {

        //libelle
        $this->add([
            'type' => Text::class,
            'name' => 'libelle',
            'options' => [
                'label' => "Libelle <span class='icon icon-obligatoire' title='Champ obligatoire'></span> :",
                'label_options' => [ 'disable_html_escape' => true, ],
            ],
            'attributes' => [
                'id' => 'libelle',
            ],
        ]);
        //groupe
        $this->add([
            'name' => 'groupe',
            'type' => Select::class,
            'options' => [
                'label' => "Groupe de l'atelier : ",
                'label_attributes' => [
                    'class' => 'control-label',
                ],
                'empty_option' => 'Sélectionner un groupe ...',
                'value_options' => $this->getGroupeService()->getGroupesAsOption(),
            ],
            'attributes' => [
                'class' => 'description form-control show-tick',
                'data-live-search'  => 'true',
                'style' => 'height:300px;',
            ]
        ]);
        //description
        $this->add([
            'type' => Textarea::class,
            'name' => 'description',
            'options' => [
                'label' => "Description :",
                'label_options' => [ 'disable_html_escape' => true, ],
            ],
            'attributes' => [
                'id' => 'description',
                'class' => 'tinymce',
            ],
        ]);

        //objectifs
        $this->add([
            'type' => Textarea::class,
            'name' => 'objectifs',
            'options' => [
                'label' => "Objectif :",
            ],
            'attributes' => [
                'id' => 'objectifs',
                'class' => 'tinymce',
            ],
        ]);
        //programme
        $this->add([
            'type' => Textarea::class,
            'name' => 'programme',
            'options' => [
                'label' => "Programme :",
            ],
            'attributes' => [
                'id' => 'programme',
                'class' => 'tinymce',
            ],
        ]);
        //periode
        $this->add([
            'type' => Textarea::class,
            'name' => 'periode',
            'options' => [
                'label' => "Période :",
            ],
            'attributes' => [
                'id' => 'periode',
                'class' => 'tinymce',
            ],
        ]);

        //submit
        $this->add([
            'type' => Button::class,
            'name' => 'creer',
            'options' => [
                'label' => '<i class="fas fa-save"></i> Enregistrer l\'atelier',
                'label_options' => [ 'disable_html_escape' => true, ],
            ],
            'attributes' => [
                'type' => 'submit',
                'class' => 'btn btn-primary',
            ],
        ]);

        //filter
        $this->setInputFilter((new Factory())->createInputFilter([
            'libelle' => ['required' => true,],
            'groupe' => ['required' => false,],
            'description' => ['required' => false,],
            'objectifs' => ['required' => false,],
            'programme' => ['required' => false,],
            'periode' => ['required' => false,],
        ]));
    }
}
<?php

namespace Atelier\Form\Groupe;

use Atelier\Entity\Db\Groupe;
use Laminas\Hydrator\HydratorInterface;

class GroupeHydrator implements HydratorInterface
{

    public function extract($object): array
    {
        /** @var Groupe $object */
        return [
            'libelle' => ($object->getLibelle()) ?: null,
            'description' => ($object->getDescription()) ?: null,
            'ordre' => ($object->getOrdre() !== null) ? $object->getOrdre(): null,
        ];
    }

    public function hydrate(array $data, $object) : object
    {
        $libelle = (isset($data['libelle']) and trim($data['libelle']) !== '') ? trim($data['libelle']) : null;
        $description = (isset($data['description']) AND  trim($data['description']) != '')?trim($data['description']):null;
        $ordre = (isset($data['ordre'])) ? $data['ordre'] : null;

        /** @var Groupe $object */
        $object->setLibelle($libelle);
        $object->setDescription($description);
        $object->setOrdre($ordre?((int) $ordre):null);
        return $object;
    }


}
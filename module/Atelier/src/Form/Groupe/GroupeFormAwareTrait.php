<?php

namespace Atelier\Form\Groupe;

trait GroupeFormAwareTrait
{
    private GroupeForm $groupeForm;

    public function getGroupeForm(): GroupeForm
    {
        return $this->groupeForm;
    }

    public function setGroupeForm(GroupeForm $groupeForm): void
    {
        $this->groupeForm = $groupeForm;
    }


}
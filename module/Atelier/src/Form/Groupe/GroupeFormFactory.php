<?php

namespace Atelier\Form\Groupe;

use Interop\Container\ContainerInterface;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;

class GroupeFormFactory
{

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): GroupeForm
    {
        /** @var GroupeHydrator $hydrator **/
        $hydrator = $container->get('HydratorManager')->get(GroupeHydrator::class);

        $form = new GroupeForm();
        $form->setHydrator($hydrator);
        return $form;
    }
}
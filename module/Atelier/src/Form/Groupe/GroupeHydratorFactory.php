<?php

namespace Atelier\Form\Groupe;

use Interop\Container\ContainerInterface;

class GroupeHydratorFactory
{
    public function __invoke(ContainerInterface $container): GroupeHydrator
    {
        $hydrator = new GroupeHydrator();
        return $hydrator;
    }
}
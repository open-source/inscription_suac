<?php

namespace Atelier\Assertion;

use Etudiant\Provider\Role\RolesProvider as EtudiantRolesProvider;
use Atelier\Entity\Db\Session;
use Atelier\Provider\Etat\SessionEtats;
use Atelier\Provider\Privilege\SessionPrivileges;
use Atelier\Provider\Role\RolesProvider;
use Atelier\Service\Session\SessionServiceAwareTrait;
use Laminas\Permissions\Acl\Resource\ResourceInterface;
use UnicaenPrivilege\Assertion\AbstractAssertion;
use UnicaenPrivilege\Service\Privilege\PrivilegeServiceAwareTrait;
use UnicaenUtilisateur\Service\User\UserServiceAwareTrait;

class SessionAssertion extends AbstractAssertion
{
    use PrivilegeServiceAwareTrait;
    use SessionServiceAwareTrait;
    use UserServiceAwareTrait;

    public function computeAssertion(Session $session, string $privilege): bool
    {
        $role = $this->getUserService()->getConnectedRole();
        $user = $this->getUserService()->getConnectedUser();
        if (!$this->getPrivilegeService()->checkPrivilege($privilege, $role)) return false;

        $etatActif = $session->getEtatActif();
        if ($etatActif === null) return false;

        $isIntervanant = false;
        if ($role->getRoleId() === RolesProvider::ROLE_INTERVENANT) {
            $isIntervanant = $session->hasIntervenantWithUser($user);
        }

        switch ($privilege) {
            case SessionPrivileges::SESSION_EMARGEMENT:
                if (in_array($etatActif->getType()->getCode(), SessionEtats::TERMINE)) return false;
                if ($role->getRoleId() === RolesProvider::ROLE_INTERVENANT) return $isIntervanant;
                return true;
            case SessionPrivileges::SESSION_INSCRIPTION_ETUDIANT:
                if ($etatActif->getType()->getCode() !== SessionEtats::ETAT_INSCRIPTION_OUVERTE) return false;
                return true;
            case SessionPrivileges::SESSION_INSCRIPTION_GESTIONNAIRE:
                if (in_array($etatActif->getType()->getCode(), SessionEtats::TERMINE)) return false;
                return true;
        }
        return true;
    }

    protected function assertEntity(ResourceInterface $entity = null, $privilege = null): bool
    {

        /** @var Session $entity */
        if (!$entity instanceof Session) {
            return false;
        }

        return $this->computeAssertion($entity, $privilege);
    }

    protected function assertController($controller, $action = null, $privilege = null): bool
    {
        //to remove copium here
        $sessionId = (($this->getMvcEvent()->getRouteMatch()->getParam('session')));
        $session = $this->getSessionService()->getSession($sessionId);

        if ($session === null) return true;
        return match ($action) {
            'inscription-gestionnaire' => $this->computeAssertion($session, SessionPrivileges::SESSION_INSCRIPTION_GESTIONNAIRE),
            'inscription-etudiant' => $this->computeAssertion($session, SessionPrivileges::SESSION_INSCRIPTION_ETUDIANT),
            default => true,
        };
    }
}
<?php

namespace Atelier\Assertion;

use Etudiant\Service\Etudiant\EtudiantService;
use Atelier\Service\Inscription\InscriptionService;
use Interop\Container\ContainerInterface;
use Laminas\Mvc\Application;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use UnicaenPrivilege\Service\Privilege\PrivilegeService;
use UnicaenUtilisateur\Service\User\UserService;

class InscriptionAssertionFactory
{
    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): InscriptionAssertion
    {
        /**
         * @var EtudiantService $etudiantService
         * @var InscriptionService $inscriptionService
         * @var PrivilegeService $privilegeService
         * @var UserService $userService
         **/
        $etudiantService = $container->get(EtudiantService::class);
        $inscriptionService = $container->get(InscriptionService::class);
        $userService = $container->get(UserService::class);
        $privilegeService = $container->get(PrivilegeService::class);

        $assertion = new InscriptionAssertion();
        $assertion->setEtudiantService($etudiantService);
        $assertion->setInscriptionService($inscriptionService);
        $assertion->setPrivilegeService($privilegeService);
        $assertion->setUserService($userService);

        /* @var $application Application */
        $application = $container->get('Application');
        $mvcEvent = $application->getMvcEvent();
        $assertion->setMvcEvent($mvcEvent);

        return $assertion;
    }
}
<?php

namespace Atelier\Assertion;

use Application\Provider\Role\RolesProvider as ApplicationRolesProvider;
use Etudiant\Provider\Role\RolesProvider as EtudiantRolesProvider;
use Etudiant\Service\Etudiant\EtudiantServiceAwareTrait;
use Atelier\Entity\Db\Inscription;
use Atelier\Provider\Etat\SessionEtats;
use Atelier\Provider\Privilege\InscriptionPrivileges;
use Atelier\Service\Inscription\InscriptionServiceAwareTrait;
use Laminas\Permissions\Acl\Resource\ResourceInterface;
use UnicaenPrivilege\Assertion\AbstractAssertion;
use UnicaenPrivilege\Service\Privilege\PrivilegeServiceAwareTrait;
use UnicaenUtilisateur\Service\User\UserServiceAwareTrait;

class InscriptionAssertion extends AbstractAssertion
{
    use EtudiantServiceAwareTrait;
    use InscriptionServiceAwareTrait;
    use PrivilegeServiceAwareTrait;
    use UserServiceAwareTrait;

    public function computeAssertion(Inscription $inscription, string $privilege): bool
    {
        $role = $this->getUserService()->getConnectedRole();
        $user = $this->getUserService()->getConnectedUser();
        $etudiant = $this->getEtudiantService()->getEtudiantByLogin($user->getUsername());
        if (!$this->getPrivilegeService()->checkPrivilege($privilege, $role)) return false;

        $session = $inscription->getSession();

        switch ($privilege) {
            case InscriptionPrivileges::INSCRIPTION_ETUDIANT:
                if ($inscription->getEtatActif()->getType()->getCode() !== SessionEtats::ETAT_INSCRIPTION_OUVERTE) return false;
                switch($role->getRoleId()) {
                    case EtudiantRolesProvider::ROLE_ETUDIANT:
                        return ($inscription->getEtudiant() === $etudiant);
                    default :
                        return true;
                }
            case InscriptionPrivileges::INSCRIPTION_GESTIONNAIRE:
                if (in_array($inscription->getEtatActif()->getType()->getCode(),SessionEtats::TERMINE)) return false;
                switch($role->getRoleId()) {
                    case EtudiantRolesProvider::ROLE_ETUDIANT:
                        return ($inscription->getEtudiant() === $etudiant);
                    default :
                        return true;
                }

            case InscriptionPrivileges::INSCRIPTION_HISTORIQUE:
                switch($role->getRoleId()) {
                    case ApplicationRolesProvider::ROLE_ADMIN_TEC:
                    case ApplicationRolesProvider::ROLE_ADMIN_FONC:
                    case ApplicationRolesProvider::ROLE_OBSERVATEUR:
                        return true;
                    case EtudiantRolesProvider::ROLE_ETUDIANT:
                        return ($inscription->getEtudiant() === $etudiant);
                }
                break;
            case InscriptionPrivileges::INSCRIPTION_CONVOCATION:
                switch($role->getRoleId()) {
                    case ApplicationRolesProvider::ROLE_ADMIN_TEC:
                    case ApplicationRolesProvider::ROLE_ADMIN_FONC:
                    case ApplicationRolesProvider::ROLE_OBSERVATEUR:
                        return true;
                    case EtudiantRolesProvider::ROLE_ETUDIANT:
                        $etudiantok = ($inscription->getEtudiant() === $etudiant);
                        $listeOk =  $inscription->getListe() === Inscription::PRINCIPALE;
                        $etatOk =  ($inscription->getSession()->getEtatActif() !== null AND in_array($inscription->getSession()->getEtatActif()->getType()->getCode(), SessionEtats::CONVOCATION));
                        return $etudiantok && $listeOk && $etatOk;

                }
                break;
            case InscriptionPrivileges::INSCRIPTION_ENQUETE:
                switch($role->getRoleId()) {
                    case ApplicationRolesProvider::ROLE_ADMIN_TEC:
                    case ApplicationRolesProvider::ROLE_ADMIN_FONC:
                    case ApplicationRolesProvider::ROLE_OBSERVATEUR:
                        return true;
                    case EtudiantRolesProvider::ROLE_ETUDIANT:
                        $etudiantOk = ($inscription->getEtudiant() === $etudiant);
                        $inscriptionOk = $inscription->getId() !== null;
                        $etatSessionOk = $session->getEtatActif() !== null && in_array($session->getEtatActif()->getType()->getCode(), SessionEtats::TERMINE);
                        $enqueteOk = $inscription->getEnquete() === null || $inscription->getEnquete()->getValidation() === null;
                        return $etudiantOk && $inscriptionOk && $etatSessionOk && $enqueteOk;
                }
                break;
            case InscriptionPrivileges::INSCRIPTION_ATTESTATION:
                switch($role->getRoleId()) {
                    case ApplicationRolesProvider::ROLE_ADMIN_TEC:
                    case ApplicationRolesProvider::ROLE_ADMIN_FONC:
                    case ApplicationRolesProvider::ROLE_OBSERVATEUR:
                        return true;
                    case EtudiantRolesProvider::ROLE_ETUDIANT:
                        $etudiantok = ($inscription->getEtudiant() === $etudiant);
                        $listeOk =  $inscription->getListe() === Inscription::PRINCIPALE;
                        $etatSessionOk =  $session->getEtatActif() !== null && in_array($session->getEtatActif()->getType()->getCode(), SessionEtats::TERMINE);
                        $enqueteOk = $inscription->getEnquete() !== null && $inscription->getEnquete()->getValidation() !== null;
                        return $etudiantok && $listeOk && $etatSessionOk && $enqueteOk;

                }
                break;
            case InscriptionPrivileges::INSCRIPTION_ANNULER:
                switch($role->getRoleId()) {
                    case ApplicationRolesProvider::ROLE_ADMIN_TEC:
                    case ApplicationRolesProvider::ROLE_ADMIN_FONC:
                    case ApplicationRolesProvider::ROLE_OBSERVATEUR:
                        return true;
                    case EtudiantRolesProvider::ROLE_ETUDIANT:
                        $etudiantOk = ($inscription->getEtudiant() === $etudiant);
                        $inscriptionOk = $inscription->getId() !== null;
                        $etatSessionOk = $session->getEtatActif() !== null && !in_array($session->getEtatActif()->getType()->getCode(), SessionEtats::TERMINE);
                        return $etudiantOk && $inscriptionOk && $etatSessionOk;
                }

            default:
                return true;
        }
        return true;
    }

    protected function assertEntity(ResourceInterface $entity = null, $privilege = null): bool
    {

        /** @var Inscription $entity */
        if (!$entity instanceof Inscription) {
            return false;
        }

        return $this->computeAssertion($entity, $privilege);
    }

    protected function assertController($controller, $action = null, $privilege = null): bool
    {
        //to remove copium here
        $inscriptionId = (($this->getMvcEvent()->getRouteMatch()->getParam('inscription')));
        $inscription = $this->getInscriptionService()->getInscription($inscriptionId);

        if ($inscription === null) return true;
        return match ($action) {
            'inscription-gestionnaire' => $this->computeAssertion($inscription, InscriptionPrivileges::INSCRIPTION_GESTIONNAIRE),
            'inscription-etudiant' => $this->computeAssertion($inscription, InscriptionPrivileges::INSCRIPTION_ETUDIANT),
            'generer-attestation' => $this->computeAssertion($inscription, InscriptionPrivileges::INSCRIPTION_ATTESTATION),
            'generer-convocation' => $this->computeAssertion($inscription, InscriptionPrivileges::INSCRIPTION_CONVOCATION),
            'generer-historique' => $this->computeAssertion($inscription, InscriptionPrivileges::INSCRIPTION_HISTORIQUE),
            'desinscription' => $this->computeAssertion($inscription, InscriptionPrivileges::INSCRIPTION_ANNULER),
            default => true,
        };
    }
}


<?php

namespace Atelier\Assertion;

use Atelier\Service\Session\SessionService;
use Interop\Container\ContainerInterface;
use Laminas\Mvc\Application;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use UnicaenPrivilege\Service\Privilege\PrivilegeService;
use UnicaenUtilisateur\Service\User\UserService;

class SessionAssertionFactory
{
    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): SessionAssertion
    {
        /**
         * @var PrivilegeService $privilegeService
         * @var SessionService $sessionService
         * @var UserService $userService
         **/
        $privilegeService = $container->get(PrivilegeService::class);
        $sessionService = $container->get(SessionService::class);
        $userService = $container->get(UserService::class);


        $assertion = new SessionAssertion();
        $assertion->setPrivilegeService($privilegeService);
        $assertion->setSessionService($sessionService);
        $assertion->setUserService($userService);

        /* @var $application Application */
        $application = $container->get('Application');
        $mvcEvent = $application->getMvcEvent();
        $assertion->setMvcEvent($mvcEvent);

        return $assertion;
    }
}
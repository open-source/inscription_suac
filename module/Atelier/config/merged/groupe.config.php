<?php

use Atelier\Controller\GroupeController;
use Atelier\Controller\GroupeControllerFactory;
use Atelier\Form\Groupe\GroupeForm;
use Atelier\Form\Groupe\GroupeFormFactory;
use Atelier\Form\Groupe\GroupeHydrator;
use Atelier\Form\Groupe\GroupeHydratorFactory;
use Atelier\Provider\Privilege\GroupePrivileges;
use Atelier\Service\Groupe\GroupeService;
use Atelier\Service\Groupe\GroupeServiceFactory;
use Laminas\Router\Http\Literal;
use Laminas\Router\Http\Segment;
use UnicaenPrivilege\Guard\PrivilegeController;


return [
    'bjyauthorize' => [

        'guards' => [
            PrivilegeController::class => [
                [
                    'controller' => GroupeController::class,
                    'action' => [
                        'index',
                    ],
                    'privileges' => [
                        GroupePrivileges::GROUPE_INDEX,
                    ],
                ],
                [
                    'controller' => GroupeController::class,
                    'action' => [
                        'afficher',
                    ],
                    'privileges' => [
                        GroupePrivileges::GROUPE_AFFICHER,
                    ],
                ],
                [
                    'controller' => GroupeController::class,
                    'action' => [
                        'ajouter',
                    ],
                    'privileges' => [
                        GroupePrivileges::GROUPE_AJOUTER,
                    ],
                ],
                [
                    'controller' => GroupeController::class,
                    'action' => [
                        'modifier',
                    ],
                    'privileges' => [
                        GroupePrivileges::GROUPE_MODIFIER,
                    ],
                ],
                [
                    'controller' => GroupeController::class,
                    'action' => [
                        'historiser',
                        'restaurer',

                    ],
                    'privileges' => [
                        GroupePrivileges::GROUPE_HISTORISER,
                    ],
                ],
                [
                    'controller' => GroupeController::class,
                    'action' => [
                        'supprimer',

                    ],
                    'privileges' => [
                        GroupePrivileges::GROUPE_SUPPRIMER,
                    ],
                ],
            ],

        ],
    ],

    'router' => [
        'routes' => [
            'atelier' => [
                'child_routes' => [
                    'groupe' => [
                        'type' => Literal::class,
                        'options' => [
                            'route' => '/groupe',
                            'defaults' => [
                                /** @see GroupeController::indexAction() */
                                'controller' => GroupeController::class,
                                'action' => 'index',
                            ],
                        ],
                        'may_terminate' => true,
                        'child_routes' => [
                            'afficher' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/afficher/:groupe',
                                    'defaults' => [
                                        /** @see GroupeController::afficherAction() */
                                        'action' => 'afficher',
                                    ],
                                ],
                            ],
                            'ajouter' => [
                                'type' => Literal::class,
                                'options' => [
                                    'route' => '/ajouter',
                                    'defaults' => [
                                        /** @see GroupeController::ajouterAction() */
                                        'action' => 'ajouter',
                                    ],
                                ],
                            ],
                            'modifier' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/modifier/:groupe',
                                    'defaults' => [
                                        /** @see GroupeController::modifierAction() */
                                        'action' => 'modifier',
                                    ],
                                ],
                            ],
                            'historiser' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/historiser/:groupe',
                                    'defaults' => [
                                        /** @see GroupeController::historiserAction() */
                                        'action' => 'historiser',
                                    ],
                                ],
                            ],
                            'restaurer' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/restaurer/:groupe',
                                    'defaults' => [
                                        /** @see GroupeController::restaurerAction() */
                                        'action' => 'restaurer',
                                    ],
                                ],
                            ],
                            'supprimer' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/supprimer/:groupe',
                                    'defaults' => [
                                        /** @see GroupeController::supprimerAction() */
                                        'action' => 'supprimer',
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],

    'navigation' => [
        'default' => [
            'home' => [
                'pages' => [
                    'ressources' => [
                        'pages' => [
                            'atelier-groupe' => [
                                'label' => "Groupe d'ateliers",
                                'route' => 'atelier/groupe',
                                'resource' => PrivilegeController::getResourceId(GroupeController::class, 'index'),
                                'order' => 1000 + 100,
                                'icon' => 'fas fa-angle-right',
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],

    'controllers' => [
        'factories' => [
            GroupeController::class => GroupeControllerFactory::class,
        ],
    ],
    'service_manager' => [
        'factories' => [
            GroupeService::class => GroupeServiceFactory::class
        ],
    ],
    'form_elements' => [
        'factories' => [
            GroupeForm::class => GroupeFormFactory::class
        ],
    ],
    'hydrators' => [
        'factories' => [
            GroupeHydrator::class => GroupeHydratorFactory::class
        ],
    ],
    'view_helpers' => [
        'invokables' => [
        ],
        'factories' => [
        ],
        'aliases' => [
        ],
    ],
];

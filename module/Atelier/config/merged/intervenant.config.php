<?php

namespace Formation;

use Atelier\Controller\IntervenantController;
use Atelier\Controller\IntervenantControllerFactory;
use Atelier\Form\Intervenant\IntervenantForm;
use Atelier\Form\Intervenant\IntervenantFormFactory;
use Atelier\Form\Intervenant\IntervenantHydrator;
use Atelier\Form\Intervenant\IntervenantHydratorFactory;
use Atelier\Form\SelectionnerIntervenant\SelectionnerIntervenantForm;
use Atelier\Form\SelectionnerIntervenant\SelectionnerIntervenantFormFactory;
use Atelier\Form\SelectionnerIntervenant\SelectionnerIntervenantHydrator;
use Atelier\Form\SelectionnerIntervenant\SelectionnerIntervenantHydratorFactory;
use Atelier\Provider\Privilege\IntervenantPrivileges;
use Atelier\Service\Intervenant\IntervenantService;
use Atelier\Service\Intervenant\IntervenantServiceFactory;
use Laminas\Router\Http\Literal;
use Laminas\Router\Http\Segment;
use UnicaenPrivilege\Guard\PrivilegeController;


return [
    'bjyauthorize' => [
        'guards' => [
            PrivilegeController::class => [
                [
                    'controller' => IntervenantController::class,
                    'action' => [
                        'index',
                        'rechercher',
                        'rechercher-rattachement',
                    ],
                    'privileges' => [
                        IntervenantPrivileges::INTERVENANT_INDEX,
                    ],
                ],
                [
                    'controller' => IntervenantController::class,
                    'action' => [
                        'afficher',
                    ],
                    'privileges' => [
                        IntervenantPrivileges::INTERVENANT_AFFICHER,
                    ],
                ],
                [
                    'controller' => IntervenantController::class,
                    'action' => [
                        'ajouter',
                    ],
                    'privileges' => [
                        IntervenantPrivileges::INTERVENANT_AJOUTER,
                    ],
                ],
                [
                    'controller' => IntervenantController::class,
                    'action' => [
                        'modifier',
                        'creer-compte',
                        'associer-compte',
                        'deassocier-compte'
                    ],
                    'privileges' => [
                        IntervenantPrivileges::INTERVENANT_MODIFIER,
                    ],
                ],
                [
                    'controller' => IntervenantController::class,
                    'action' => [
                        'historiser',
                        'restaurer',
                    ],
                    'privileges' => [
                        IntervenantPrivileges::INTERVENANT_HISTORISER,
                    ],
                ],
                [
                    'controller' => IntervenantController::class,
                    'action' => [
                        'supprimer',
                    ],
                    'privileges' => [
                        IntervenantPrivileges::INTERVENANT_SUPPRIMER,
                    ],
                ],
                [
                    'controller' => IntervenantController::class,
                    'action' => [
                        'mes-sessions',
                    ],
                    'privileges' => [
                        IntervenantPrivileges::INTERVENANT_MES_SESSIONS,
                    ],
                ],
            ],
        ],
    ],

    'router' => [
        'routes' => [
            'atelier' => [
                'child_routes' => [
                    'intervenant' => [
                        'type' => Literal::class,
                        'options' => [
                            'route' => '/intervenant',
                            'defaults' => [
                                /** @see IntervenantController::indexAction() */
                                'controller' => IntervenantController::class,
                                'action' => 'index',
                            ],
                        ],
                        'may_terminate' => true,
                        'child_routes' => [
                            'afficher' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/afficher/:intervenant',
                                    'defaults' => [
                                        /** @see IntervenantController::afficherAction() */
                                        'action' => 'afficher',
                                    ],
                                ],
                            ],
                            'ajouter' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/ajouter[/:session]',
                                    'defaults' => [
                                        /** @see IntervenantController::ajouterAction() */
                                        'action' => 'ajouter',
                                    ],
                                ],
                            ],
                            'modifier' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/modifier/:intervenant',
                                    'defaults' => [
                                        /** @see IntervenantController::modifierAction() */
                                        'action' => 'modifier',
                                    ],
                                ],
                            ],
                            'historiser' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/historiser/:intervenant',
                                    'defaults' => [
                                        /** @see IntervenantController::historiserAction() */
                                        'action' => 'historiser',
                                    ],
                                ],
                            ],
                            'restaurer' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/restaurer/:intervenant',
                                    'defaults' => [
                                        /** @see IntervenantController::restaurerAction() */
                                        'action' => 'restaurer',
                                    ],
                                ],
                            ],
                            'supprimer' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/supprimer/:intervenant',
                                    'defaults' => [
                                        /** @see IntervenantController::supprimerAction() */
                                        'action' => 'supprimer',
                                    ],
                                ],
                            ],
                            'creer-compte' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/creer-compte/:intervenant',
                                    'defaults' => [
                                        /** @see IntervenantController::creerCompteAction() */
                                        'action' => 'creer-compte',
                                    ],
                                ],
                            ],
                            'associer-compte' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/associer-compte/:intervenant',
                                    'defaults' => [
                                        /** @see IntervenantController::associerCompteAction() */
                                        'action' => 'associer-compte',
                                    ],
                                ],
                            ],
                            'deassocier-compte' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/deassocier-compte/:intervenant',
                                    'defaults' => [
                                        /** @see IntervenantController::deassocierCompteAction() */
                                        'action' => 'deassocier-compte',
                                    ],
                                ],
                            ],
                            'rechercher' => [
                                'type' => Literal::class,
                                'options' => [
                                    'route' => '/rechercher',
                                    'defaults' => [
                                        /** @see IntervenantController::rechercherAction() */
                                        'action' => 'rechercher',
                                    ],
                                ],
                            ],
                            'rechercher-rattachement' => [
                                'type' => Literal::class,
                                'options' => [
                                    'route' => '/rechercher-rattachement',
                                    'defaults' => [
                                        /** @see IntervenantController::rechercherRattachementAction() */
                                        'action' => 'rechercher-rattachement',
                                    ],
                                ],
                            ],
                            'mes-sessions' => [
                                'type' => Literal::class,
                                'options' => [
                                    'route' => '/mes-sessions',
                                    'defaults' => [
                                        /** @see IntervenantController::mesSessionsAction() */
                                        'action' => 'mes-sessions',
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],

    'navigation' => [
        'default' => [
            'home' => [
                'pages' => [
                    'ressources' => [
                        'pages' => [
                            'intervenant' => [
                                'label' => "Intervenant",
                                'route' => 'atelier/intervenant',
                                'resource' => PrivilegeController::getResourceId(IntervenantController::class, 'index'),
                                'order' => 1000 + 400,
                                'icon' => 'fas fa-angle-right',
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],

    'controllers' => [
        'factories' => [
            IntervenantController::class => IntervenantControllerFactory::class,
        ],
    ],
    'service_manager' => [
        'factories' => [
            IntervenantService::class => IntervenantServiceFactory::class,
        ],
    ],
    'form_elements' => [
        'factories' => [
            IntervenantForm::class => IntervenantFormFactory::class,
            SelectionnerIntervenantForm::class => SelectionnerIntervenantFormFactory::class,
        ],
    ],
    'hydrators' => [
        'factories' => [
            IntervenantHydrator::class => IntervenantHydratorFactory::class,
            SelectionnerIntervenantHydrator::class => SelectionnerIntervenantHydratorFactory::class,
        ],
    ],
    'view_helpers' => [
        'invokables' => [
        ],
        'factories' => [
        ],
        'aliases' => [
        ],
    ],
];

<?php

use Atelier\Assertion\InscriptionAssertion;
use Atelier\Assertion\InscriptionAssertionFactory;
use Atelier\Controller\InscriptionController;
use Atelier\Controller\InscriptionControllerFactory;
use Atelier\Form\Inscription\InscriptionForm;
use Atelier\Form\Inscription\InscriptionFormFactory;
use Atelier\Form\Inscription\InscriptionHydrator;
use Atelier\Form\Inscription\InscriptionHydratorFactory;
use Atelier\Provider\Privilege\InscriptionPrivileges;
use Atelier\Provider\Privilege\SessionPrivileges;
use Atelier\Service\Inscription\InscriptionService;
use Atelier\Service\Inscription\InscriptionServiceFactory;
use Atelier\View\Helper\InscriptionViewHelper;
use Laminas\Router\Http\Literal;
use Laminas\Router\Http\Segment;
use UnicaenPrivilege\Guard\PrivilegeController;
use UnicaenPrivilege\Provider\Rule\PrivilegeRuleProvider;


return [
    'bjyauthorize' => [
        'resource_providers' => [
            'BjyAuthorize\Provider\Resource\Config' => [
                'Inscription' => [],
            ],
        ],
        'rule_providers' => [
            PrivilegeRuleProvider::class => [
                'allow' => [
                    [
                        'privileges' => [
                            InscriptionPrivileges::INSCRIPTION_ANNULER,
                            InscriptionPrivileges::INSCRIPTION_CONVOCATION,
                            InscriptionPrivileges::INSCRIPTION_ATTESTATION,
                            InscriptionPrivileges::INSCRIPTION_HISTORIQUE,
                            InscriptionPrivileges::INSCRIPTION_ENQUETE,
                        ],
                        'resources' => ['Inscription'],
                        'assertion' => InscriptionAssertion::class
                    ],
                ],
            ],
        ],
        'guards' => [
            PrivilegeController::class => [
                [
                    'controller' => InscriptionController::class,
                    'action' => [
                        'ajouter',
                        'modifier',
                        'historiser',
                        'restaurer',
                        'supprimer',

                        'valider',
                        'refuser',
                        'classer',
                        'envoyer-liste-principale',
                        'envoyer-liste-complementaire',
                    ],
                    'privileges' => [
                        SessionPrivileges::SESSION_GERER_INSCRIPTION,
                    ],
                ],
                [
                    'controller' => InscriptionController::class,
                    'action' => [
                        'generer-convocation',
                    ],
                    'privileges' => [
                        InscriptionPrivileges::INSCRIPTION_CONVOCATION,
                    ],
                    'assertion' => InscriptionAssertion::class,
                ],
                [
                    'controller' => InscriptionController::class,
                    'action' => [
                        'generer-attestation',
                    ],
                    'privileges' => [
                        InscriptionPrivileges::INSCRIPTION_ATTESTATION,
                    ],
                    'assertion' => InscriptionAssertion::class,
                ],
                [
                    'controller' => InscriptionController::class,
                    'action' => [
                        'generer-historique',
                    ],
                    'privileges' => [
                        InscriptionPrivileges::INSCRIPTION_HISTORIQUE,
                    ],
                    'assertion' => InscriptionAssertion::class,
                ],
                [
                    'controller' => InscriptionController::class,
                    'action' => [
                        'auto-inscription',
                        'inscription-en-cours',
                        'inscription-historique',
                        'inscription',
                    ],
                    'privileges' => [
                        InscriptionPrivileges::INSCRIPTION_AUTOINSCRIPTION
                    ],
                ],
                [
                    'controller' => InscriptionController::class,
                    'action' => [
                        'repondre-enquete',
                        'valider-enquete'
                    ],
                    'privileges' => [
                        InscriptionPrivileges::INSCRIPTION_ENQUETE
                    ],
                    'assertion' => InscriptionAssertion::class,
                ],
                [
                    'controller' => InscriptionController::class,
                    'action' => [
                        'desinscription',
                    ],
                    'privileges' => [
                        InscriptionPrivileges::INSCRIPTION_ANNULER
                    ],
                    'assertion' => InscriptionAssertion::class,
                ]
            ],
        ],
    ],

    'router' => [
        'routes' => [
            'atelier' => [
                'child_routes' => [
                    'inscription' => [
                        'type' => Literal::class,
                        'options' => [
                            'route' => '/inscription',
                            'defaults' => [
                                /** @see InscriptionController::indexAction() */
                                'controller' => InscriptionController::class,
                                'action' => 'index',
                            ],
                        ],
                        'may_terminate' => true,
                        'child_routes' => [
//                            'afficher' => [
//                                'type' => Segment::class,
//                                'options' => [
//                                    'route' => '/afficher/:action-formation',
//                                    'defaults' => [
//                                        /** @see ActionController::afficherAction() */
//                                        'action' => 'afficher',
//                                    ],
//                                ],
//                            ],
                            'ajouter' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/ajouter[/:session]',
                                    'defaults' => [
                                        /** @see InscriptionController::ajouterAction() */
                                        'action' => 'ajouter',
                                    ],
                                ],
                            ],
                            'modifier' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/modifier/:inscription',
                                    'defaults' => [
                                        /** @see InscriptionController::modifierAction() */
                                        'action' => 'modifier',
                                    ],
                                ],
                            ],
                            'historiser' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/historiser/:inscription',
                                    'defaults' => [
                                        /** @see InscriptionController::historiserAction() */
                                        'action' => 'historiser',
                                    ],
                                ],
                            ],
                            'restaurer' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/restaurer/:inscription',
                                    'defaults' => [
                                        /** @see InscriptionController::restaurerAction() */
                                        'action' => 'restaurer',
                                    ],
                                ],
                            ],
                            'supprimer' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/supprimer/:inscription',
                                    'defaults' => [
                                        /** @see InscriptionController::supprimerAction() */
                                        'action' => 'supprimer',
                                    ],
                                ],
                            ],
                            'valider' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/valider/:inscription',
                                    'defaults' => [
                                        /** @see InscriptionController::validerAction() */
                                        'action' => 'valider',
                                    ],
                                ],
                            ],
                            'refuser' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/refuser/:inscription',
                                    'defaults' => [
                                        /** @see InscriptionController::refuserAction() */
                                        'action' => 'refuser',
                                    ],
                                ],
                            ],
                            'classer' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/classer/:inscription',
                                    'defaults' => [
                                        /** @see InscriptionController::classerAction() */
                                        'action' => 'classer',
                                    ],
                                ],
                            ],
                            'envoyer-liste-principale' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/envoyer-liste-principale/:inscription',
                                    'defaults' => [
                                        /** @see InscriptionController::envoyerListePrincipaleAction() */
                                        'action' => 'envoyer-liste-principale',
                                    ],
                                ],
                            ],
                            'envoyer-liste-complementaire' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/envoyer-liste-complementaire/:inscription',
                                    'defaults' => [
                                        /** @see InscriptionController::envoyerListeComplementaireAction() */
                                        'action' => 'envoyer-liste-complementaire',
                                    ],
                                ],
                            ],
                            'auto-inscription' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/auto-inscription',
                                    'defaults' => [
                                        /** @see InscriptionController::autoInscriptionAction() */
                                        'action' => 'auto-inscription',
                                    ],
                                ],
                            ],
                            'inscription-en-cours' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/inscription-en-cours[/:etudiant]',
                                    'defaults' => [
                                        /** @see InscriptionController::inscriptionEnCoursAction() */
                                        'action' => 'inscription-en-cours',
                                    ],
                                ],
                            ],
                            'inscription-historique' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/inscription-historique[/:etudiant]',
                                    'defaults' => [
                                        /** @see InscriptionController::inscriptionHistoriqueAction() */
                                        'action' => 'inscription-historique',
                                    ],
                                ],
                            ],
                            'desinscription' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/desinscription/:inscription',
                                    'defaults' => [
                                        /** @see InscriptionController::desinscriptionAction() */
                                        'action' => 'desinscription',
                                    ],
                                ],
                            ],
                            /** ENQUETE ********************************************************************************/
                            'repondre-enquete' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/repondre-enquete/:inscription',
                                    'defaults' => [
                                        /** @see InscriptionController::repondreEnqueteAction() */
                                        'action' => 'repondre-enquete',
                                    ],
                                ],
                            ],
                            'valider-enquete' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/valider-enquete/:inscription',
                                    'defaults' => [
                                        /** @see InscriptionController::repondreEnqueteAction() */
                                        'action' => 'valider-enquete',
                                    ],
                                ],
                            ],
                            /** DOCUMENTS ******************************************************************************/
                            'generer-convocation' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/generer-convocation/:inscription',
                                    'defaults' => [
                                        /** @see InscriptionController::genererConvocationAction() */
                                        'action' => 'generer-convocation',
                                    ],
                                ],
                            ],
                            'generer-attestation' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/generer-attestation/:inscription',
                                    'defaults' => [
                                        /** @see InscriptionController::genererAttestationAction() */
                                        'action' => 'generer-attestation',
                                    ],
                                ],
                            ],
                            'generer-historique' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/generer-historique/:etudiant',
                                    'defaults' => [
                                        /** @see InscriptionController::genererHistoriqueAction() */
                                        'action' => 'generer-historique',
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],

    'navigation' => [
        'default' => [
            'home' => [
                'pages' => [
                    'auto-inscription' => [
                        'label' => "S'inscrire",
                        'route' => 'atelier/inscription/auto-inscription',
                        'resource' => PrivilegeController::getResourceId(InscriptionController::class, 'auto-inscription'),
                        'order' => 1000 + 200,
                    ],
                    'mes-ateliers' => [
                        'label' => "Mes ateliers",
                        'route' => 'atelier/inscription/auto-inscription',
                        'resource' => PrivilegeController::getResourceId(InscriptionController::class, 'auto-inscription'),
                        'order' => 1000 + 300,
                        'pages' => [
                            'inscription-en-cours' => [
                                'label' => "Mes inscriptions en cours",
                                'route' => 'atelier/inscription/inscription-en-cours',
                                'resource' => PrivilegeController::getResourceId(InscriptionController::class, 'inscription-en-cours'),
                                'order' => 100,
                            ],
                            'inscription-historique' => [
                                'label' => "Mes ateliers suivis",
                                'route' => 'atelier/inscription/inscription-historique',
                                'resource' => PrivilegeController::getResourceId(InscriptionController::class, 'inscription-historique'),
                                'order' => 200,
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],

    'controllers' => [
        'factories' => [
            InscriptionController::class => InscriptionControllerFactory::class,
        ],
    ],
    'service_manager' => [
        'factories' => [
            InscriptionAssertion::class => InscriptionAssertionFactory::class,
            InscriptionService::class => InscriptionServiceFactory::class,
        ],
    ],
    'form_elements' => [
        'factories' => [
            InscriptionForm::class => InscriptionFormFactory::class,
        ],
    ],
    'hydrators' => [
        'factories' => [
            InscriptionHydrator::class => InscriptionHydratorFactory::class,
        ],
    ],
    'view_helpers' => [
        'invokables' => [
            'inscription' => InscriptionViewHelper::class,
        ],
        'factories' => [
        ],
        'aliases' => [
        ],
    ],
];

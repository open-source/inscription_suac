<?php

namespace Atelier;

use Atelier\Controller\SeanceController;
use Atelier\Controller\SeanceControllerFactory;
use Atelier\Form\Seance\SeanceForm;
use Atelier\Form\Seance\SeanceFormFactory;
use Atelier\Form\Seance\SeanceHydrator;
use Atelier\Form\Seance\SeanceHydratorFactory;
use Atelier\Form\SeanceRecurrente\SeanceRecurrenteForm;
use Atelier\Form\SeanceRecurrente\SeanceRecurrenteFormFactory;
use Atelier\Form\SeanceRecurrente\SeanceRecurrenteHydrator;
use Atelier\Form\SeanceRecurrente\SeanceRecurrenteHydratorFactory;
use Atelier\Provider\Privilege\SessionPrivileges;
use Atelier\Service\Seance\SeanceService;
use Atelier\Service\Seance\SeanceServiceFactory;
use Laminas\Router\Http\Literal;
use Laminas\Router\Http\Segment;
use UnicaenPrivilege\Guard\PrivilegeController;

return [
    'bjyauthorize' => [
        'guards' => [
            PrivilegeController::class => [
                [
                    'controller' => SeanceController::class,
                    'action' => [
                        'ajouter',
                        'ajouter-seance-recurrente',
                        'modifier',
                        'historiser',
                        'restaurer',
                        'supprimer',
                    ],
                    'privileges' => [
                        SessionPrivileges::SESSION_GERER_SEANCE,
                    ],
                ],
            ],
        ],
    ],

    'router'          => [
        'routes' => [
            'atelier' => [
                'child_routes' => [
                    'session' => [
                        'type' => Literal::class,
                        'options' => [
                            'route' => '/session',
                        ],
                        'may_terminate' => true,
                        'child_routes' => [
                            'ajouter-seance' => [
                                'type'  => Segment::class,
                                'options' => [
                                    'route'    => '/ajouter-seance/:session',
                                    'defaults' => [
                                        /** @see SeanceController::ajouterAction() */
                                        'controller' => SeanceController::class,
                                        'action'     => 'ajouter',
                                    ],
                                ],
                            ],
                            'ajouter-seance-recurrente' => [
                                'type'  => Segment::class,
                                'options' => [
                                    'route'    => '/ajouter-seance-recurrente/:session',
                                    'defaults' => [
                                        /** @see SeanceController::ajouterSeanceRecurrenteAction() */
                                        'controller' => SeanceController::class,
                                        'action'     => 'ajouter-seance-recurrente',
                                    ],
                                ],
                            ],
                            'modifier-seance' => [
                                'type'  => Segment::class,
                                'options' => [
                                    'route'    => '/modifier-seance/:seance',
                                    'defaults' => [
                                        /** @see SeanceController::modifierAction() */
                                        'controller' => SeanceController::class,
                                        'action'     => 'modifier',
                                    ],
                                ],
                            ],
                            'historiser-seance' => [
                                'type'  => Segment::class,
                                'options' => [
                                    'route'    => '/historiser-seance/:seance',
                                    'defaults' => [
                                        /** @see SeanceController::historiserAction() */
                                        'controller' => SeanceController::class,
                                        'action'     => 'historiser',
                                    ],
                                ],
                            ],
                            'restaurer-seance' => [
                                'type'  => Segment::class,
                                'options' => [
                                    'route'    => '/restaurer-seance/:seance',
                                    'defaults' => [
                                        /** @see SeanceController::restaurerAction() */
                                        'controller' => SeanceController::class,
                                        'action'     => 'restaurer',
                                    ],
                                ],
                            ],
                            'supprimer-seance' => [
                                'type'  => Segment::class,
                                'options' => [
                                    'route'    => '/supprimer-seance/:seance',
                                    'defaults' => [
                                        /** @see SeanceController::supprimerAction() */
                                        'controller' => SeanceController::class,
                                        'action'     => 'supprimer',
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],

    'service_manager' => [
        'factories' => [
            SeanceService::class => SeanceServiceFactory::class,
        ],
    ],
    'controllers'     => [
        'factories' => [
            SeanceController::class => SeanceControllerFactory::class,
        ],
    ],
    'form_elements' => [
        'factories' => [
            SeanceForm::class => SeanceFormFactory::class,
            SeanceRecurrenteForm::class => SeanceRecurrenteFormFactory::class,
        ],
    ],
    'hydrators' => [
        'factories' => [
            SeanceHydrator::class => SeanceHydratorFactory::class,
            SeanceRecurrenteHydrator::class => SeanceRecurrenteHydratorFactory::class,
        ],
    ],

];
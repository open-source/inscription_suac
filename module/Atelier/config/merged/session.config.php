<?php

use Atelier\Assertion\SessionAssertion;
use Atelier\Assertion\SessionAssertionFactory;
use Atelier\Controller\SessionController;
use Atelier\Controller\SessionControllerFactory;
use Atelier\Event\CloturerSession\CloturerSessionEvenement;
use Atelier\Event\CloturerSession\CloturerSessionEvenementFactory;
use Atelier\Event\ConvoquerSession\ConvoquerSessionEvenement;
use Atelier\Event\ConvoquerSession\ConvoquerSessionEvenementFactory;
use Atelier\Event\RetourSession\RetourSessionEvenement;
use Atelier\Event\RetourSession\RetourSessionEvenementFactory;
use Atelier\Form\Session\SessionForm;
use Atelier\Form\Session\SessionFormFactory;
use Atelier\Form\Session\SessionHydrator;
use Atelier\Form\Session\SessionHydratorFactory;
use Atelier\Provider\Privilege\SessionPrivileges;
use Atelier\Service\Session\SessionService;
use Atelier\Service\Session\SessionServiceFactory;
use Laminas\Router\Http\Literal;
use Laminas\Router\Http\Segment;
use UnicaenPrivilege\Guard\PrivilegeController;
use UnicaenPrivilege\Provider\Rule\PrivilegeRuleProvider;


return [
    'bjyauthorize' => [
        'resource_providers' => [
            'BjyAuthorize\Provider\Resource\Config' => [
                'Session' => [],
            ],
        ],
        'rule_providers' => [
            PrivilegeRuleProvider::class => [
                'allow' => [
                    [
                        'privileges' => [
                            SessionPrivileges::SESSION_INSCRIPTION_GESTIONNAIRE,
                            SessionPrivileges::SESSION_INSCRIPTION_ETUDIANT,
                            SessionPrivileges::SESSION_EMARGEMENT,
                        ],
                        'resources' => ['Session'],
                        'assertion' => SessionAssertion::class
                    ],
                ],
            ],
        ],
        'guards' => [
            PrivilegeController::class => [
                [
                    'controller' => SessionController::class,
                    'action' => [
                        'index',
                    ],
                    'privileges' => [
                        SessionPrivileges::SESSION_INDEX,
                    ],
                ],
                [
                    'controller' => SessionController::class,
                    'action' => [
                        'afficher',
                        'rechercher',
                        'enquete',
                    ],
                    'privileges' => [
                        SessionPrivileges::SESSION_AFFICHER,
                    ],
                ],
                [
                    'controller' => SessionController::class,
                    'action' => [
                        'informations',
                    ],
                    'privileges' => [
                        SessionPrivileges::SESSION_INFORMATIONS,
                    ],
                ],
                [
                    'controller' => SessionController::class,
                    'action' => [
                        'ajouter',
                        'ajouter-intervenant',
                        'retirer-intervenant',
                    ],
                    'privileges' => [
                        SessionPrivileges::SESSION_AJOUTER,
                    ],
                ],
                [
                    'controller' => SessionController::class,
                    'action' => [
                        'modifier',
                    ],
                    'privileges' => [
                        SessionPrivileges::SESSION_MODIFIER,
                    ],
                ],
                [
                    'controller' => SessionController::class,
                    'action' => [
                        'historiser',
                        'restaurer',
                    ],
                    'privileges' => [
                        SessionPrivileges::SESSION_HISTORISER,
                    ],
                ],
                [
                    'controller' => SessionController::class,
                    'action' => [
                        'supprimer',
                    ],
                    'privileges' => [
                        SessionPrivileges::SESSION_SUPPRIMER,
                    ],
                ],
                [
                    'controller' => SessionController::class,
                    'action' => [
                        'supprimer',
                    ],
                    'privileges' => [
                        SessionPrivileges::SESSION_SUPPRIMER,
                    ],
                ],
                [
                    'controller' => SessionController::class,
                    'action' => [
                        'ouvrir-inscription',
                        'fermer-inscription',
                        'envoyer-convocation',
                        'demander-retour',
                        'annuler',
                        'cloturer',
                        'reouvrir',
                        'changer-etat',
                        'notifier-inscrit',
                    ],
                    'privileges' => [
                        SessionPrivileges::SESSION_GERER_ETAT,
                    ],
                ],
                [
                    'controller' => SessionController::class,
                    'action' => [
                        'inscription-gestionnaire',
                    ],
                    'privileges' => [
                        SessionPrivileges::SESSION_INSCRIPTION_GESTIONNAIRE,
                    ],
                ],
                [
                    'controller' => SessionController::class,
                    'action' => [
                        'inscription-etudiant',
                    ],
                    'privileges' => [
                        SessionPrivileges::SESSION_INSCRIPTION_ETUDIANT,
                    ],
                ],
                [
                    'controller' => SessionController::class,
                    'action' => [
                        'generer-emargement',
                    ],
                    'privileges' => [
                        SessionPrivileges::SESSION_EMARGEMENT,
                    ],
                    'assertion' => SessionAssertion::class
                ],
            ],
        ],
    ],

    'router' => [
        'routes' => [
            'atelier' => [
                'child_routes' => [
                    'session' => [
                        'type' => Literal::class,
                        'options' => [
                            'route' => '/session',
                            'defaults' => [
                                /** @see SessionController::indexAction() */
                                'controller' => SessionController::class,
                                'action' => 'index',
                            ],
                        ],
                        'may_terminate' => true,
                        'child_routes' => [
                            'afficher' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/afficher/:session',
                                    'defaults' => [
                                        /** @see SessionController::afficherAction() */
                                        'action' => 'afficher',
                                    ],
                                ],
                            ],
                            'informations' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/informations/:session',
                                    'defaults' => [
                                        /** @see SessionController::informationsAction() */
                                        'action' => 'informations',
                                    ],
                                ],
                            ],
                            'ajouter' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/ajouter/:atelier',
                                    'defaults' => [
                                        /** @see SessionController::ajouterAction() */
                                        'action' => 'ajouter',
                                    ],
                                ],
                            ],
                            'ajouter-intervenant' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/ajouter-intervenant/:session',
                                    'defaults' => [
                                        /** @see SessionController::ajouterIntervenantAction() */
                                        'action' => 'ajouter-intervenant',
                                    ],
                                ],
                            ],
                            'retirer-intervenant' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/retrait-intervenant/:session/:intervenant',
                                    'defaults' => [
                                        /** @see SessionController::retirerIntervenantAction() */
                                        'action' => 'retirer-intervenant',
                                    ],
                                ],
                            ],
                            'modifier' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/modifier/:session',
                                    'defaults' => [
                                        /** @see SessionController::modifierAction() */
                                        'action' => 'modifier',
                                    ],
                                ],
                            ],
                            'historiser' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/historiser/:session',
                                    'defaults' => [
                                        /** @see SessionController::historiserAction() */
                                        'action' => 'historiser',
                                    ],
                                ],
                            ],
                            'restaurer' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/restaurer/:session',
                                    'defaults' => [
                                        /** @see SessionController::restaurerAction() */
                                        'action' => 'restaurer',
                                    ],
                                ],
                            ],
                            'supprimer' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/supprimer/:session',
                                    'defaults' => [
                                        /** @see SessionController::supprimerAction() */
                                        'action' => 'supprimer',
                                    ],
                                ],
                            ],
                            'rechercher' => [
                                'type' => Literal::class,
                                'options' => [
                                    'route' => '/rechercher',
                                    'defaults' => [
                                        /** @see SessionController::rechercherAction() */
                                        'action' => 'rechercher',
                                    ],
                                ],
                            ],
                            'ouvrir-inscription' => [
                                'type'  => Segment::class,
                                'options' => [
                                    'route'    => '/ouvrir-inscription/:session',
                                    'defaults' => [
                                        /** @see SessionController::ouvrirInscriptionAction() */
                                        'action'     => 'ouvrir-inscription',
                                    ],
                                ],
                            ],
                            'fermer-inscription' => [
                                'type'  => Segment::class,
                                'options' => [
                                    'route'    => '/fermer-inscription/:session',
                                    'defaults' => [
                                        /** @see SessionController::fermerInscriptionAction() */
                                        'action'     => 'fermer-inscription',
                                    ],
                                ],
                            ],
                            'envoyer-convocation' => [
                                'type'  => Segment::class,
                                'options' => [
                                    'route'    => '/envoyer-convocation/:session',
                                    'defaults' => [
                                        /** @see SessionController::envoyerConvocationAction() */
                                        'action'     => 'envoyer-convocation',
                                    ],
                                ],
                            ],
                            'demander-retour' => [
                                'type'  => Segment::class,
                                'options' => [
                                    'route'    => '/demander-retour/:session',
                                    'defaults' => [
                                        /** @see SessionController::demanderRetourAction() */
                                        'action'     => 'demander-retour',
                                    ],
                                ],
                            ],
                            'cloturer' => [
                                'type'  => Segment::class,
                                'options' => [
                                    'route'    => '/cloturer/:session',
                                    'defaults' => [
                                        /** @see SessionController::cloturerAction() */
                                        'action'     => 'cloturer',
                                    ],
                                ],
                            ],
                            'annuler' => [
                                'type'  => Segment::class,
                                'options' => [
                                    'route'    => '/annuler/:session',
                                    'defaults' => [
                                        /** @see SessionController::annulerAction() */
                                        'action'     => 'annuler',
                                    ],
                                ],
                            ],
                            'reouvrir' => [
                                'type'  => Segment::class,
                                'options' => [
                                    'route'    => '/reouvrir/:session',
                                    'defaults' => [
                                        /** @see SessionController::reouvrirAction() */
                                        'action'     => 'reouvrir',
                                    ],
                                ],
                            ],
                            'changer-etat' => [
                                'type'  => Segment::class,
                                'options' => [
                                    'route'    => '/changer-etat/:session',
                                    'defaults' => [
                                        /** @see SessionController::changerEtatAction() */
                                        'action'     => 'changer-etat',
                                    ],
                                ],
                            ],
                            'inscription-gestionnaire' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/inscription-gestionnaire/:session',
                                    'defaults' => [
                                        /** @see SessionController::inscriptionGestionnaireAction() */
                                        'action' => 'inscription-gestionnaire',
                                    ],
                                ],
                            ],
                            'inscription-etudiant' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/inscription-etudiant/:session/:etudiant',
                                    'defaults' => [
                                        /** @see SessionController::inscriptionEtudiantAction() */
                                        'action' => 'inscription-etudiant',
                                    ],
                                ],
                            ],
                            'generer-emargement' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/generer-emargement/:session[/:seance]',
                                    'defaults' => [
                                        /** @see SessionController::genererEmargementAction() */
                                        'action' => 'generer-emargement',
                                    ],
                                ],
                            ],
                            'enquete' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/enquete/:session',
                                    'defaults' => [
                                        /** @see SessionController::enqueteAction() */
                                        'action' => 'enquete',
                                    ],
                                ],
                            ],
                            'notifier-inscrit' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/notifier-inscrit/:session',
                                    'defaults' => [
                                        /** @see SessionController::notifierInscritAction() */
                                        'action' => 'notifier-inscrit',
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],

    'navigation' => [
        'default' => [
            'home' => [
                'pages' => [
                    'ressources' => [
                        'pages' => [
                            'session' => [
                                'label' => "Session",
                                'route' => 'atelier/session',
                                'resource' => PrivilegeController::getResourceId(SessionController::class, 'index'),
                                'order' => 1000 + 300,
                                'icon' => 'fas fa-angle-right',
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],

    'controllers' => [
        'factories' => [
            SessionController::class => SessionControllerFactory::class,
        ],
    ],
    'service_manager' => [
        'factories' => [
            SessionAssertion::class => SessionAssertionFactory::class,
            SessionService::class => SessionServiceFactory::class,

            //Evenement
            RetourSessionEvenement::class => RetourSessionEvenementFactory::class,
            CloturerSessionEvenement::class => CloturerSessionEvenementFactory::class,
            ConvoquerSessionEvenement::class => ConvoquerSessionEvenementFactory::class,
        ],
    ],
    'form_elements' => [
        'factories' => [
            SessionForm::class => SessionFormFactory::class,
        ],
    ],
    'hydrators' => [
        'factories' => [
            SessionHydrator::class => SessionHydratorFactory::class,
        ],
    ],
    'view_helpers' => [
        'invokables' => [
        ],
        'factories' => [
        ],
        'aliases' => [
        ],
    ],
];

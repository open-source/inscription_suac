<?php

namespace Atelier;

use Atelier\Controller\AtelierController;
use Atelier\Controller\AtelierControllerFactory;
use Atelier\Form\Atelier\AtelierForm;
use Atelier\Form\Atelier\AtelierFormFactory;
use Atelier\Form\Atelier\AtelierHydrator;
use Atelier\Form\Atelier\AtelierHydratorFactory;
use Atelier\Form\SelectionnerAtelier\SelectionnerAtelierForm;
use Atelier\Form\SelectionnerAtelier\SelectionnerAtelierFormFactory;
use Atelier\Form\SelectionnerAtelier\SelectionnerAtelierHydrator;
use Atelier\Form\SelectionnerAtelier\SelectionnerAtelierHydratorFactory;
use Atelier\Provider\Privilege\AtelierPrivileges;
use Atelier\Service\Atelier\AtelierService;
use Atelier\Service\Atelier\AtelierServiceFactory;
use Atelier\View\Helper\AtelierInformationViewHelper;
use Atelier\View\Helper\AtelierInformationViewHelperFactory;
use Atelier\View\Helper\AteliersViewHelper;
use Atelier\View\Helper\AteliersViewHelperFactory;
use Laminas\Router\Http\Literal;
use Laminas\Router\Http\Segment;
use UnicaenPrivilege\Guard\PrivilegeController;


return [
    'bjyauthorize' => [
        'guards' => [
            PrivilegeController::class => [
                [
                    'controller' => AtelierController::class,
                    'action' => [
                        'index',
                    ],
                    'privileges' => [
                        AtelierPrivileges::ATELIER_INDEX,
                    ],
                ],
                [
                    'controller' => AtelierController::class,
                    'action' => [
                        'afficher',
                        'fiche',
                        'afficher-programme',
                        'enquete',
                    ],
                    'privileges' => [
                        AtelierPrivileges::ATELIER_AFFICHER,
                    ],
                ],
                [
                    'controller' => AtelierController::class,
                    'action' => [
                        'ajouter',
                    ],
                    'privileges' => [
                        AtelierPrivileges::ATELIER_AJOUTER,
                    ],
                ],
                [
                    'controller' => AtelierController::class,
                    'action' => [
                        'modifier',
                        'televerser-image',
                        'supprimer-image',
                    ],
                    'privileges' => [
                        AtelierPrivileges::ATELIER_MODIFIER,
                    ],
                ],
                [
                    'controller' => AtelierController::class,
                    'action' => [
                        'historiser',
                        'restaurer',
                    ],
                    'privileges' => [
                        AtelierPrivileges::ATELIER_HISTORISER,
                    ],
                ],
                [
                    'controller' => AtelierController::class,
                    'action' => [
                        'supprimer',
                    ],
                    'privileges' => [
                        AtelierPrivileges::ATELIER_SUPPRIMER,
                    ],
                ],
            ],
        ],
    ],

    'router' => [
        'routes' => [
            'atelier' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/atelier',
                ],
                'may_terminate' => false,
                'child_routes' => [
                    'atelier' => [
                        'type' => Literal::class,
                        'options' => [
                            'route' => '/atelier',
                            'defaults' => [
                                /** @see AtelierController::indexAction() */
                                'controller' => AtelierController::class,
                                'action' => 'index',
                            ],
                        ],
                        'may_terminate' => true,
                        'child_routes' => [
                            'afficher' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/afficher/:atelier',
                                    'defaults' => [
                                        /** @see AtelierController::afficherAction() */
                                        'action' => 'afficher',
                                    ],
                                ],
                            ],
                            'fiche' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/fiche/:atelier',
                                    'defaults' => [
                                        /** @see AtelierController::ficheAction() */
                                        'action' => 'fiche',
                                    ],
                                ],
                            ],
                            'afficher-programme' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/afficher-programme/:atelier',
                                    'defaults' => [
                                        /** @see AtelierController::afficherProgrammeAction() */
                                        'action' => 'afficher-programme',
                                    ],
                                ],
                            ],
                            'ajouter' => [
                                'type' => Literal::class,
                                'options' => [
                                    'route' => '/ajouter',
                                    'defaults' => [
                                        /** @see AtelierController::ajouterAction() */
                                        'action' => 'ajouter',
                                    ],
                                ],
                            ],
                            'modifier' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/modifier/:atelier',
                                    'defaults' => [
                                        /** @see AtelierController::modifierAction() */
                                        'action' => 'modifier',
                                    ],
                                ],
                            ],
                            'televerser-image' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/televerser-image/:atelier',
                                    'defaults' => [
                                        /** @see AtelierController::televerserImageAction() */
                                        'action' => 'televerser-image',
                                    ],
                                ],
                            ],
                            'supprimer-image' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/supprimer-image/:atelier',
                                    'defaults' => [
                                        /** @see AtelierController::supprimerImageAction() */
                                        'action' => 'supprimer-image',
                                    ],
                                ],
                            ],
                            'historiser' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/historiser/:atelier',
                                    'defaults' => [
                                        /** @see AtelierController::historiserAction() */
                                        'action' => 'historiser',
                                    ],
                                ],
                            ],
                            'restaurer' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/restaurer/:atelier',
                                    'defaults' => [
                                        /** @see AtelierController::restaurerAction() */
                                        'action' => 'restaurer',
                                    ],
                                ],
                            ],
                            'supprimer' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/supprimer/:atelier',
                                    'defaults' => [
                                        /** @see AtelierController::supprimerAction() */
                                        'action' => 'supprimer',
                                    ],
                                ],
                            ],
                            'enquete' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/enquete/:atelier',
                                    'defaults' => [
                                        /** @see AtelierController::enqueteAction() */
                                        'action' => 'enquete',
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],

    'navigation' => [
        'default' => [
            'home' => [
                'pages' => [
                    'ressources' => [
                        'pages' => [
                            'atelier' => [
                                'label' => "Ateliers",
                                'route' => 'atelier/atelier',
                                'resource' => PrivilegeController::getResourceId(AtelierController::class, 'index'),
                                'order' => 1000 + 200,
                                'icon' => 'fas fa-angle-right',
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],

    'controllers' => [
        'factories' => [
            AtelierController::class => AtelierControllerFactory::class,
        ],
    ],
    'service_manager' => [
        'factories' => [
            AtelierService::class => AtelierServiceFactory::class,
        ],
    ],
    'form_elements' => [
        'factories' => [
            AtelierForm::class => AtelierFormFactory::class,
            SelectionnerAtelierForm::class => SelectionnerAtelierFormFactory::class,
        ],
    ],
    'hydrators' => [
        'factories' => [
            AtelierHydrator::class => AtelierHydratorFactory::class,
            SelectionnerAtelierHydrator::class => SelectionnerAtelierHydratorFactory::class,
        ],
    ],
    'view_helpers' => [
        'invokables' => [
        ],
        'factories' => [
            AtelierInformationViewHelper::class => AtelierInformationViewHelperFactory::class,
            AteliersViewHelper::class => AteliersViewHelperFactory::class,
        ],
        'aliases' => [
            'ateliers' => AteliersViewHelper::class,
            'atelierInformation' => AtelierInformationViewHelper::class,
        ],
    ],
];

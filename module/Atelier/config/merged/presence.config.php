<?php

namespace Formation;

use Atelier\Controller\PresenceController;
use Atelier\Controller\PresenceControllerFactory;
use Atelier\Provider\Privilege\SessionPrivileges;
use Atelier\Service\Presence\PresenceService;
use Atelier\Service\Presence\PresenceServiceFactory;
use Laminas\Router\Http\Literal;
use Laminas\Router\Http\Segment;
use UnicaenPrivilege\Guard\PrivilegeController;

return [
    'bjyauthorize' => [
        'guards' => [
            PrivilegeController::class => [
                [
                    'controller' => PresenceController::class,
                    'action' => [
                        'renseigner-presences',
                    ],
                    'privileges' => [
                        SessionPrivileges::SESSION_AFFICHER_PRESENCE,
                    ],
                ],
                [
                    'controller' => PresenceController::class,
                    'action' => [
                        'toggle-presence',
                        'toggle-presences',
                        'toggle-toutes-presences',
                    ],
                    'privileges' => [
                        SessionPrivileges::SESSION_GERER_PRESENCE,
                    ],
                ],
            ],
        ],
    ],

    'router'          => [
        'routes' => [
            'atelier' => [
                'child_routes' => [
                    'session' => [
                        'type' => Literal::class,
                        'options' => [
                            'route' => '/session',
                        ],
                        'may_terminate' => true,
                        'child_routes' => [
                            'renseigner-presences' => [
                                'type'  => Segment::class,
                                'options' => [
                                    'route'    => '/renseigner-presences/:session',
                                    'defaults' => [
                                        'controller' => PresenceController::class,
                                        'action'     => 'renseigner-presences',
                                    ],
                                ],
                            ],
                            'toggle-presence' => [
                                'type'  => Segment::class,
                                'options' => [
                                    'route'    => '/toggle-presence/:seance/:inscription',
                                    'defaults' => [
                                        /** @see PresenceController::togglePresenceAction() */
                                        'controller' => PresenceController::class,
                                        'action'     => 'toggle-presence',
                                    ],
                                ],
                            ],
                            'toggle-presences' => [
                                'type'  => Segment::class,
                                'options' => [
                                    'route'    => '/toggle-presences/:mode/:inscription',
                                    'defaults' => [
                                        /** @see PresenceController::togglePresencesAction() */
                                        'controller' => PresenceController::class,
                                        'action'     => 'toggle-presences',
                                    ],
                                ],
                            ],
                            'toggle-toutes-presences' => [
                                'type'  => Segment::class,
                                'options' => [
                                    'route'    => '/toggle-toutes-presences/:mode/:session',
                                    'defaults' => [
                                        /** @see PresenceController::toggleToutesPresencesAction() */
                                        'controller' => PresenceController::class,
                                        'action'     => 'toggle-toutes-presences',
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],

    'service_manager' => [
        'factories' => [
            PresenceService::class => PresenceServiceFactory::class,
        ],
    ],
    'controllers'     => [
        'factories' => [
            PresenceController::class => PresenceControllerFactory::class,
        ],
    ],
    'form_elements' => [
        'factories' => [],
    ],
    'hydrators' => [
        'factories' => [],
    ]

];
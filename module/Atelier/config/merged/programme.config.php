<?php

use Atelier\Controller\ProgrammeController;
use Atelier\Controller\ProgrammeControllerFactory;
use Atelier\Form\Programme\ProgrammeForm;
use Atelier\Form\Programme\ProgrammeFormFactory;
use Atelier\Form\Programme\ProgrammeHydrator;
use Atelier\Form\Programme\ProgrammeHydratorFactory;
use Atelier\Provider\Privilege\ProgrammePrivileges;
use Atelier\Service\Programme\ProgrammeService;
use Atelier\Service\Programme\ProgrammeServiceFactory;
use Laminas\Router\Http\Literal;
use Laminas\Router\Http\Segment;
use UnicaenPrivilege\Guard\PrivilegeController;


return [
    'bjyauthorize' => [
        'guards' => [
            PrivilegeController::class => [
                [
                    'controller' => ProgrammeController::class,
                    'action' => [
                        'index',
                    ],
                    'privileges' => [
                        ProgrammePrivileges::PROGRAMME_INDEX,
                    ],
                ],
                [
                    'controller' => ProgrammeController::class,
                    'action' => [
                        'courant'
                    ],
                    'privileges' => [
                        ProgrammePrivileges::PROGRAMME_ACTIONS_COURANTES,
                    ],
                ],
                [
                    'controller' => ProgrammeController::class,
                    'action' => [
                        'afficher',
                    ],
                    'privileges' => [
                        ProgrammePrivileges::PROGRAMME_AFFICHER,
                    ],
                ],
                [
                    'controller' => ProgrammeController::class,
                    'action' => [
                        'ajouter',
                    ],
                    'privileges' => [
                        ProgrammePrivileges::PROGRAMME_AJOUTER,
                    ],
                ],
                [
                    'controller' => ProgrammeController::class,
                    'action' => [
                        'modifier',
                    ],
                    'privileges' => [
                        ProgrammePrivileges::PROGRAMME_MODIFIER,
                    ],
                ],
                [
                    'controller' => ProgrammeController::class,
                    'action' => [
                        'historiser',
                        'restaurer',
                    ],
                    'privileges' => [
                        ProgrammePrivileges::PROGRAMME_HISTORISER,
                    ],
                ],
                [
                    'controller' => ProgrammeController::class,
                    'action' => [
                        'supprimer',
                    ],
                    'privileges' => [
                        ProgrammePrivileges::PROGRAMME_SUPPRIMER,
                    ],
                ],
                [
                    'controller' => ProgrammeController::class,
                    'action' => [
                        'gerer-ateliers',
                    ],
                    'privileges' => [
                        ProgrammePrivileges::PROGRAMME_GERER_ACTION,
                    ],
                ],
            ],
        ],
    ],

    'router' => [
        'routes' => [
            'atelier' => [
                'child_routes' => [
                    'programme' => [
                        'type' => Literal::class,
                        'options' => [
                            'route' => '/programme',
                            'defaults' => [
                                /** @see ProgrammeController::indexAction() */
                                'controller' => ProgrammeController::class,
                                'action' => 'index',
                            ],
                        ],
                        'may_terminate' => true,
                        'child_routes' => [
                            'courant' => [
                                'type' => Literal::class,
                                'options' => [
                                    'route' => '/courant',
                                    'defaults' => [
                                        /** @see ProgrammeController::courantAction() */
                                        'action' => 'courant',
                                    ],
                                ],
                            ],
                            'afficher' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/afficher/:programme',
                                    'defaults' => [
                                        /** @see ProgrammeController::afficherAction() */
                                        'action' => 'afficher',
                                    ],
                                ],
                            ],
                            'ajouter' => [
                                'type' => Literal::class,
                                'options' => [
                                    'route' => '/ajouter',
                                    'defaults' => [
                                        /** @see ProgrammeController::ajouterAction() */
                                        'action' => 'ajouter',
                                    ],
                                ],
                            ],
                            'modifier' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/modifier/:programme',
                                    'defaults' => [
                                        /** @see ProgrammeController::modifierAction() */
                                        'action' => 'modifier',
                                    ],
                                ],
                            ],
                            'historiser' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/historiser/:programme',
                                    'defaults' => [
                                        /** @see ProgrammeController::historiserAction() */
                                        'action' => 'historiser',
                                    ],
                                ],
                            ],
                            'restaurer' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/restaurer/:programme',
                                    'defaults' => [
                                        /** @see ProgrammeController::restaurerAction() */
                                        'action' => 'restaurer',
                                    ],
                                ],
                            ],
                            'supprimer' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/supprimer/:programme',
                                    'defaults' => [
                                        /** @see ProgrammeController::supprimerAction() */
                                        'action' => 'supprimer',
                                    ],
                                ],
                            ],
                            'gerer-ateliers' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/gerer-ateliers/:programme',
                                    'defaults' => [
                                        /** @see ProgrammeController::gererAteliersAction() */
                                        'action' => 'gerer-ateliers',
                                    ],
                                ],
                            ],
                            'ajouter-programme' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/ajouter-programme/:programme/:atelier',
                                    'defaults' => [
                                        /** @see ProgrammeController::ajouterProgrammeAction() */
                                        'action' => 'gerer-programme',
                                    ],
                                ],
                            ],
                            'retirer-programme' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/retirer-programme/:programme/:atelier',
                                    'defaults' => [
                                        /** @see ProgrammeController::retirerProgrammeAction() */
                                        'action' => 'retirer-programme',
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],

    'navigation' => [
        'default' => [
            'home' => [
                'pages' => [
                    'actions' => [
                        'label' => "Ateliers",
                        'route' => 'atelier/programme/courant',
                        'resource' => PrivilegeController::getResourceId(ProgrammeController::class, 'courant'),
                        'order' => 100,
                    ],
                    'ressources' => [
                        'pages' => [
                            'programme' => [
                                'label' => "Programme",
                                'route' => 'atelier/programme',
                                'resource' => PrivilegeController::getResourceId(ProgrammeController::class, 'index'),
                                'order' => 1000 + 400,
                                'icon' => 'fas fa-angle-right',
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],

    'controllers' => [
        'factories' => [
            ProgrammeController::class => ProgrammeControllerFactory::class,
        ],
    ],
    'service_manager' => [
        'factories' => [
            ProgrammeService::class => ProgrammeServiceFactory::class,
        ],
    ],
    'form_elements' => [
        'factories' => [
            ProgrammeForm::class => ProgrammeFormFactory::class,
        ],
    ],
    'hydrators' => [
        'factories' => [
            ProgrammeHydrator::class => ProgrammeHydratorFactory::class,
        ],
    ],
    'view_helpers' => [
        'invokables' => [
        ],
        'factories' => [
        ],
        'aliases' => [
        ],
    ],
];

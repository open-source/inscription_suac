<?php

use Atelier\Form\Notification\NotificationForm;
use Atelier\Form\Notification\NotificationFormFactory;
use Atelier\Form\Notification\NotificationHydrator;
use Atelier\Form\Notification\NotificationHydratorFactory;
use Doctrine\ORM\Mapping\Driver\XmlDriver;
use Doctrine\Persistence\Mapping\Driver\MappingDriverChain;
use Atelier\Form\Justification\JustificationForm;
use Atelier\Form\Justification\JustificationFormFactory;
use Atelier\Form\Justification\JustificationHydrator;
use Atelier\Form\Justification\JustificationHydratorFactory;
use Atelier\Provider\Identity\IdentityProvider;
use Atelier\Provider\Identity\IdentityProviderFactory;
use Atelier\Service\Macro\MacroService;
use Atelier\Service\Macro\MacroServiceFactory;
use Atelier\Service\Notification\NotificationService;
use Atelier\Service\Notification\NotificationServiceFactory;
use Atelier\Service\Url\UrlService;
use Atelier\Service\Url\UrlServiceFactory;

return array(
    'doctrine' => [
        'driver' => [
            'orm_default'            => [
                'class'   => MappingDriverChain::class,
                'drivers' => [
                    'Atelier\Entity\Db' => 'orm_default_xml_driver',
                ],
            ],
            'orm_default_xml_driver' => [
                'class' => XmlDriver::class,
                'cache' => 'apc',
                'paths' => [
                    __DIR__ . '/../src/Entity/Db/Mapping',
                ],
            ],
        ],
        'cache'  => [
            'apc' => [
                'namespace' => 'SUAC__' . __NAMESPACE__,
            ],
        ],
    ],

    'router' => [
        'routes' => [
        ],
    ],

    'service_manager' => [
        'factories' => [
            IdentityProvider::class => IdentityProviderFactory::class,
            NotificationService::class => NotificationServiceFactory::class,
            MacroService::class => MacroServiceFactory::class,
            UrlService::class => UrlServiceFactory::class,
        ],
    ],
    'form_elements'   => [
        'factories' => [
            JustificationForm::class => JustificationFormFactory::class,
            NotificationForm::class => NotificationFormFactory::class,
        ],
    ],
    'hydrators'       => [
        'factories' => [
            JustificationHydrator::class => JustificationHydratorFactory::class,
            NotificationHydrator::class => NotificationHydratorFactory::class,
        ],
    ],
    'controllers'     => [
        'factories' => [
        ],
    ],
    'view_helpers'    => [
        'invokables' => [
        ],
    ],

    'view_manager' => [
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
    ],
);

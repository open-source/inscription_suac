<?php

namespace Etudiant\Form\Etablissement;

trait EtablissementFormAwareTrait
{
    private EtablissementForm $etablissementForm;

    public function getEtablissementForm(): EtablissementForm
    {
        return $this->etablissementForm;
    }

    public function setEtablissementForm(EtablissementForm $etablissementForm): void
    {
        $this->etablissementForm = $etablissementForm;
    }
}
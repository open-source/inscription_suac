<?php

namespace Etudiant\Form\Etablissement;

use Laminas\Form\Element\Button;
use Laminas\Form\Element\Text;
use Laminas\Form\Form;
use Laminas\InputFilter\Factory;

class EtablissementForm extends Form
{
    public function init(): void
    {
        // libelle
        $this->add([
            'type' => Text::class,
            'name' => 'libelle',
            'options' => [
                'label' => "Libelle <span class='icon icon-obligatoire' title='Champ obligatoire'></span> :",
                'label_options' => ['disable_html_escape' => true],
            ],
            'attributes' => [
                'id' => 'libelle',
            ],
        ]);

        // sigle
        $this->add([
            'type' => Text::class,
            'name' => 'sigle',
            'options' => [
                'label' => "Sigle <span class='icon icon-obligatoire' title='Champ obligatoire'></span> :",
                'label_options' => ['disable_html_escape' => true],
            ],
            'attributes' => [
                'id' => 'sigle',
            ],
        ]);

        // bouton
        $this->add([
            'type' => Button::class,
            'name' => 'bouton',
            'options' => [
                'label' => '<i class="fas fa-save"></i> Enregistrer',
                'label_options' => ['disable_html_escape' => true],
            ],
            'attributes' => [
                'type' => 'submit',
                'class' => 'btn btn-primary',
            ],
        ]);

        // input filter
        $this->setInputFilter((new Factory())->createInputFilter([
            'libelle' => ['required' => true],
            'sigle' => ['required' => true],
        ]));
    }
}
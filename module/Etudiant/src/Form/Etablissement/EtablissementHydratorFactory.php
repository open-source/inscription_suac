<?php
namespace Etudiant\Form\Etablissement;

use Interop\Container\ContainerInterface;

class EtablissementHydratorFactory
{
    public function __invoke(ContainerInterface $container): EtablissementHydrator
    {
        return new EtablissementHydrator();
    }
}
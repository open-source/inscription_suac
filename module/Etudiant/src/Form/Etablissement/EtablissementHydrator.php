<?php

namespace Etudiant\Form\Etablissement;

use Etudiant\Entity\Db\Etablissement;
use Laminas\Hydrator\HydratorInterface;

class EtablissementHydrator implements HydratorInterface
{
    public function extract($object): array
    {
        /** @var Etablissement $object */
        return [
            'libelle' => $object->getLibelle() ?: null,
            'sigle' => $object->getSigle() ?: null,
         ];
    }

    public function hydrate(array $data, $object): object
    {
        $libelle = isset($data['libelle']) && trim($data['libelle']) !== '' ? trim($data['libelle']) : null;
        $sigle = isset($data['sigle']) && trim($data['sigle']) !== '' ? trim($data['sigle']) : null;

        /** @var Etablissement $object */
        $object->setLibelle($libelle);
        $object->setSigle($sigle);
        return $object;
    }
}
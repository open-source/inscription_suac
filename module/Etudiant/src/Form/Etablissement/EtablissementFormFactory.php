<?php
namespace Etudiant\Form\Etablissement;

use Interop\Container\ContainerInterface;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;

class EtablissementFormFactory
{
    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): EtablissementForm
    {
        /** @var EtablissementHydrator $hydrator */
        $hydrator = $container->get('HydratorManager')->get(EtablissementHydrator::class);

        $form = new EtablissementForm();
        $form->setHydrator($hydrator);
        return $form;
    }
}
<?php

namespace Etudiant\View\Helper;

use Etudiant\Entity\Db\Etudiant;
use Laminas\View\Helper\AbstractHelper;
use Laminas\View\Helper\Partial;
use Laminas\View\Renderer\PhpRenderer;
use Laminas\View\Resolver\TemplatePathStack;

class EtudiantInscriptionsAdministrativesViewHelper extends AbstractHelper
{

    public function __invoke(Etudiant $etudiant, ?bool $isAnneeEnCours = false): Partial|string
    {
        /** @var PhpRenderer $view */
        $view = $this->getView();
        $view->resolver()->attach(new TemplatePathStack(['script_paths' => [__DIR__ . "/partial"]]));

        return $view->partial('etudiant-inscriptions-administratives', ['etudiant' => $etudiant, 'isAnneeEnCours' => $isAnneeEnCours]);
    }
}
<?php

namespace Etudiant\View\Helper;

use Laminas\View\Helper\AbstractHelper;
use Laminas\View\Helper\Partial;
use Laminas\View\Renderer\PhpRenderer;
use Laminas\View\Resolver\TemplatePathStack;

class EtudiantsViewHelper extends AbstractHelper
{

    public function __invoke(?array $etudiants, array $options = []): Partial|string
    {
        /** @var PhpRenderer $view */
        $view = $this->getView();
        $view->resolver()->attach(new TemplatePathStack(['script_paths' => [__DIR__ . "/partial"]]));

        return $view->partial('etudiants-liste', ['etudiants' => $etudiants, 'options' => $options]);
    }
}
<?php

namespace Etudiant\Entity\Db;

class ComposanteIA
{
    private ?int $id = null;

    private ?string $composante = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getComposante(): ?string
    {
        return $this->composante;
    }
}
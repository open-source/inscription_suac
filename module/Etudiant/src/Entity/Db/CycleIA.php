<?php

namespace Etudiant\Entity\Db;

class CycleIA
{
    private ?int $id = null;

    private ?string $cycle = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCycle(): ?string
    {
        return $this->cycle;
    }
}
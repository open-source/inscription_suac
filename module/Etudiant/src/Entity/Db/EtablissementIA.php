<?php

namespace Etudiant\Entity\Db;

class EtablissementIA
{
    private ?int $id = null;

    private ?string $etablissement = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEtablissement(): ?string
    {
        return $this->etablissement;
    }
}
<?php

namespace Etudiant\Entity\Db;

class AnneeUnivIA
{
    private ?int $id = null;

    private ?string $annee_univ = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAnneeUniv(): ?string
    {
        return $this->annee_univ;
    }


}
<?php

namespace Etudiant\Entity\Db;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Atelier\Entity\Db\Inscription;
use Atelier\Provider\Etat\SessionEtats;
use Laminas\Permissions\Acl\Resource\ResourceInterface;
use PhpParser\Node\Scalar\String_;
use UnicaenSynchro\Entity\Db\IsSynchronisableInterface;
use UnicaenSynchro\Entity\Db\IsSynchronisableTrait;
use UnicaenUtilisateur\Entity\Db\AbstractUser;
use UnicaenValidation\Entity\HasValidationsInterface;
use UnicaenValidation\Entity\HasValidationsTrait;

class Etudiant implements
    HasValidationsInterface,
    ResourceInterface, IsSynchronisableInterface
{
    use HasValidationsTrait;
    use IsSynchronisableTrait;

    private ?string $id = null;
    private ?string $login = null;
    private ?string $nom = null;
    private ?string $prenom = null;
    private ?string $email = null;
    private ?AbstractUser $utilisateur = null;
    private ?string $numeroEtudiant = null;
    private Collection $inscriptions;
    private Collection $inscriptionsAdministratives;

    public function __construct()
    {
        $this->inscriptions = new ArrayCollection();
        $this->validations = new ArrayCollection();
        $this->inscriptionsAdministratives = new ArrayCollection();
    }

    public function getResourceId(): string
    {
        return 'Etudiant';
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function setId(?string $id): void
    {
        $this->id = $id;
    }

    public function getUtilisateur(): ?AbstractUser
    {
        return $this->utilisateur;
    }

    public function setUtilisateur(?AbstractUser $utilisateur): void
    {
        $this->utilisateur = $utilisateur;
    }

    public function getLogin(): ?string
    {
        return $this->login;
    }

    public function setLogin(?string $login): void
    {
        $this->login = $login;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(?string $nom): void
    {
        $this->nom = $nom;
    }

    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function setPrenom(?string $prenom): void
    {
        $this->prenom = $prenom;
    }

    public function getDenomination(bool $prenomFirst = true): ?string
    {
        if (!$prenomFirst) return $this->nom . " " . $this->prenom;
        return $this->prenom . " " . $this->nom;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): void
    {
        $this->email = $email;
    }

    public function getNumeroEtudiant(): ?string
    {
        return $this->numeroEtudiant;
    }



    public function setNumeroEtudiant(?string $numeroEtudiant): void
    {
        $this->numeroEtudiant = $numeroEtudiant;
    }

    public function getInscriptions(): Collection
    {
        return $this->inscriptions;
    }

    public function getInscriptionsAdministratives(bool $isAnneeEnCours = false ): Collection
    {
        if (!$isAnneeEnCours) return $this->inscriptionsAdministratives;
        $annee = Etudiant::getAnneeDate();
        $inscriptions = $this->getInscriptionAdministrativeByAnnee($annee);
        return $inscriptions;

    }

    public function getInscriptionAdministrativeByAnnee(int $annee): Collection
    {
        return $this->inscriptionsAdministratives->filter(function (InscriptionAdministrative $inscription) use ($annee) {
            return $inscription->getAnneeUniv() == $annee;
        });
    }

    /**
     * Composante de l'année en cours
     * @return String
     */
    public function getComposante(): string
    {
        $annee = Etudiant::getAnneeDate();
        $inscription = $this->getInscriptionAdministrativeByAnnee($annee)->first();
        return $inscription->getComposante();
    }

    static public function getAnneeDate(?DateTime $date = null): int
    {
        if ($date === null) $date = new DateTime();
        $year = (int)$date->format('Y');
        $month = (int)$date->format('m');
        if ($month >= 9) return $year;

        return ($year - 1);
    }

    /** FONCTIONS POUR LES MACROS *************************************************************************************/

    public function generateHistoriqueListe(): string
    {
        /** @var Inscription[] $inscriptions */
        $inscriptions = $this->getInscriptions()->toArray();
        $inscriptions = array_filter($inscriptions, function (Inscription $inscription) {
            return
                $inscription->estNonHistorise()
                and $inscription->getSession()->isEtatActif(SessionEtats::ETAT_CLOTURE_INSTANCE);
        }
        );
        usort($inscriptions, function (Inscription $inscription1, Inscription $inscription2) {
            return $inscription1->getSession()->getDebut(true) <=> $inscription2->getSession()->getDebut(true);
        });

        $texte = "";
        foreach ($inscriptions as $inscription) {
            $dureeSession = $inscription->getSession()->getDuree();
            $dureePresence = $inscription->toStringDureePresence();

            $texte .= "<div>";
            $texte .= $inscription->getSession()->getLibelle() . "(" . $inscription->getSession()->getPeriode() . ") <br>";
            $texte .= $dureePresence . " suivies sur " . $dureeSession;
            $texte .= "</div>";
        }
        return $texte;
    }
}
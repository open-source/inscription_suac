<?php

namespace Etudiant\Entity\Db;

use UnicaenSynchro\Entity\Db\IsSynchronisableInterface;
use UnicaenSynchro\Entity\Db\IsSynchronisableTrait;

class InscriptionAdministrative implements IsSynchronisableInterface
{
    use IsSynchronisableTrait;

    private ?string $id = null;

    private ?string $annee_univ = null;

    private ?string $formation = null;

    private ?string $diplome = null;

    private ?string $cycle = null;

    private ?string $composante = null;

    private ?string $etablissement = null;

    private ?Etudiant $etudiant;


    public function getId(): ?string
    {
        return $this->id;
    }

    public function getAnneeUniv(): ?string
    {
        return $this->annee_univ;
    }

    public function getFormation(): ?string
    {
        return $this->formation;
    }

    public function getDiplome(): ?string
    {
        return $this->diplome;
    }

    public function getCycle(): ?string
    {
        return $this->cycle;
    }

    public function getComposante(): ?string
    {
        return $this->composante;
    }

    public function getEtablissement(): ?string
    {
        return $this->etablissement;
    }

    public function getEtudiant(): ?Etudiant
    {
        return $this->etudiant;
    }

    public function setId(?string $id): void
    {
        $this->id = $id;
    }

    public function setAnneeUniv(?string $annee_univ): void
    {
        $this->annee_univ = $annee_univ;
    }

    public function setFormation(?string $formation): void
    {
        $this->formation = $formation;
    }

    public function setDiplome(?string $diplome): void
    {
        $this->diplome = $diplome;
    }

    public function setCycle(?string $cycle): void
    {
        $this->cycle = $cycle;
    }

    public function setComposante(?string $composante): void
    {
        $this->composante = $composante;
    }

    public function setEtablissement(?string $etablissement): void
    {
        $this->etablissement = $etablissement;
    }

    public function setEtudiant(?Etudiant $etudiant): void
    {
        $this->etudiant = $etudiant;
    }

    public function getAttribute($attribute): ?string
    {
        if (property_exists($this, $attribute)) {
            return $this->$attribute;
        }
        return null;
    }

}
<?php

namespace Etudiant\Provider\Identity;


use Etudiant\Provider\Role\RolesProvider;
use Etudiant\Service\Etudiant\EtudiantServiceAwareTrait;
use UnicaenUtilisateur\Entity\Db\RoleInterface;
use UnicaenUtilisateur\Entity\Db\User;
use UnicaenUtilisateur\Provider\Identity\AbstractIdentityProvider;
use UnicaenUtilisateur\Service\Role\RoleServiceAwareTrait;
use UnicaenUtilisateur\Service\User\UserServiceAwareTrait;

class IdentityProvider extends AbstractIdentityProvider
{
    use RoleServiceAwareTrait;
    use UserServiceAwareTrait;
    use EtudiantServiceAwareTrait;

    /**
     * @param string $code
     *
     * @return User[]|null
     */
    public function computeUsersAutomatiques(string $code): ?array
    {
        switch ($code) {
            case RolesProvider::ROLE_ETUDIANT :
                $users = $this->getEtudiantService()->getUserEtudiants();
                return $users;
        }

        return null;
    }



    /**
     * @param User|null $user
     *
     * @return string[]|RoleInterface[]
     */
    public function computeRolesAutomatiques(?User $user = null): array
    {

        $roles = [];

        if ($user === null) {
            $user = $this->getUserService()->getConnectedUser();
        }

        if ($user !== null) {
            $etudiant = $this->getEtudiantService()->getEtudiantByLogin(trim($user->getUsername()));
            if ($etudiant !== null) {
                $roleEtudiant = $this->getRoleService()->findByRoleId(RolesProvider::ROLE_ETUDIANT);
                $roles[]      = $roleEtudiant;
            }
        }

        return $roles;
    }

}
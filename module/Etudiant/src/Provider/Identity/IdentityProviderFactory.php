<?php

namespace Etudiant\Provider\Identity;

use Etudiant\Service\Etudiant\EtudiantService;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;
use UnicaenUtilisateur\Service\Role\RoleService;
use UnicaenUtilisateur\Service\User\UserService;

class IdentityProviderFactory
{
    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): IdentityProvider
    {
        $identityProvider = new IdentityProvider();

        $etudiantService = $container->get(EtudiantService::class);
        $userService     = $container->get(UserService::class);
        $roleService     = $container->get(RoleService::class);

        $identityProvider->setEtudiantService($etudiantService);
        $identityProvider->setUserService($userService);
        $identityProvider->setRoleService($roleService);

        return $identityProvider;
    }
}
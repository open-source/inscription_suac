<?php

namespace Etudiant\Provider\Privilege;

use UnicaenPrivilege\Provider\Privilege\Privileges;

class EtudiantPrivileges extends Privileges
{

    const ETUDIANT_INDEX = 'etudiant-etudiant_index';
    const ETUDIANT_AFFICHER = 'etudiant-etudiant_afficher';
    const ETUDIANT_IMPORTER = 'etudiant-etudiant_importer';
}
<?php

namespace Etudiant\Provider\Privilege;

use UnicaenPrivilege\Provider\Privilege\Privileges;

class EtablissementPrivileges extends Privileges
{
    const ETABLISSEMENT_INDEX = 'etablissement-etablissement_index';
    const ETABLISSEMENT_AFFICHER = 'etablissement-etablissement_afficher';
    const ETABLISSEMENT_AJOUTER = 'etablissement-etablissement_ajouter';
    const ETABLISSEMENT_MODIFIER = 'etablissement-etablissement_modifier';
    const ETABLISSEMENT_HISTORISER = 'etablissement-etablissement_historiser';
    const ETABLISSEMENT_SUPPRIMER = 'etablissement-etablissement_supprimer';
}
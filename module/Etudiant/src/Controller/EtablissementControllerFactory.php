<?php

namespace Etudiant\Controller;

use Etudiant\Form\Etablissement\EtablissementForm;
use Etudiant\Service\Etablissement\EtablissementService;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;

class EtablissementControllerFactory
{
    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): EtablissementController
    {
        $etablissementService = $container->get(EtablissementService::class);
        $etablissementForm = $container->get('FormElementManager')->get(EtablissementForm::class);

        $controller = new EtablissementController();
        $controller->setEtablissementService($etablissementService);
        $controller->setEtablissementForm($etablissementForm);

        return $controller;
    }
}
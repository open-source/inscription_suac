<?php

namespace Etudiant\Controller;

use Etudiant\Service\AnneeUnivIA\AnneeUnivIAService;
use Etudiant\Service\ComposanteIA\ComposanteIAService;
use Etudiant\Service\CycleIA\CycleIAService;
use Etudiant\Service\Etablissement\EtablissementService;
use Etudiant\Service\EtablissementIA\EtablissementIAService;
use Etudiant\Service\Etudiant\EtudiantService;
use Etudiant\Service\InscriptionAdministrative\InscriptionAdministrativeService;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;
use UnicaenParametre\Service\Parametre\ParametreService;

class EtudiantControllerFactory
{

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): EtudiantController
    {
        /**
         * @see ParametreService $parametreService
         */
        $anneeUnivIAService = $container->get(AnneeUnivIAService::class);
        $composanteIAService = $container->get(ComposanteIAService::class);
        $etablissementIAService = $container->get(EtablissementIAService::class);
        $etudiantService = $container->get(EtudiantService::class);
        $cycleIAService = $container->get(CycleIAService::class);
        $etablissementService = $container->get(EtablissementService::class);
        $parametreService = $container->get(ParametreService::class);

        $controller = new EtudiantController;
        $controller->setEtudiantService($etudiantService);
        $controller->setAnneeUnivIAService($anneeUnivIAService);
        $controller->setEtablissementIAService($etablissementIAService);
        $controller->setComposanteIAService($composanteIAService);
        $controller->setCycleIAService($cycleIAService);
        $controller->setEtablissementService($etablissementService);
        $controller->setParametreService($parametreService);

        return $controller;
    }
}


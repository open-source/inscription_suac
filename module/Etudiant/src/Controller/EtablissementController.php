<?php

namespace Etudiant\Controller;

use Etudiant\Entity\Db\Etablissement;
use Etudiant\Form\Etablissement\EtablissementFormAwareTrait;
use Etudiant\Service\Etablissement\EtablissementServiceAwareTrait;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\View\Model\ViewModel;

class EtablissementController extends AbstractActionController
{
    use EtablissementServiceAwareTrait;
    use EtablissementFormAwareTrait;

    public function indexAction(): ViewModel
    {
        $etablissements = $this->getEtablissementService()->getEtablissements();
        return new ViewModel([
            'etablissements' => $etablissements,
        ]);
    }

    public function afficherAction(): ViewModel
    {
        $etablissement = $this->getEtablissementService()->getRequestedEtablissement($this);
        return new ViewModel([
            'etablissement' => $etablissement,
        ]);
    }

    public function ajouterAction(): ViewModel
    {
        $etablissement = new Etablissement();
        $form = $this->getEtablissementForm();
        $form->setAttribute('action', $this->url()->fromRoute('etudiant/etablissement/ajouter'));
        $form->setAttribute('method', 'post');
        $form->bind($etablissement);

        if ($this->getRequest()->isPost()) {
            $form->setData($this->getRequest()->getPost());
            if ($form->isValid()) {
                $this->getEtablissementService()->create($etablissement);
                exit();
            }
        }

        $vm = new ViewModel();
        $vm->setTemplate('default/default-form');
        $vm->setVariables([
            'title' => 'Ajouter un établissement',
            'form' => $form,
        ]);
        return $vm;
    }

    public function modifierAction(): ViewModel
    {
        $etablissement = $this->getEtablissementService()->getRequestedEtablissement($this);
        $form = $this->getEtablissementForm();
        $form->setAttribute('action', $this->url()->fromRoute('etudiant/etablissement/modifier', ['etablissement' => $etablissement->getId()]));
        $form->setAttribute('method', 'post');
        $form->bind($etablissement);

        if ($this->getRequest()->isPost()) {
            $form->setData($this->getRequest()->getPost());
            if ($form->isValid()) {
                $this->getEtablissementService()->update($etablissement);
                exit();
            }
        }

        $vm = new ViewModel();
        $vm->setTemplate('default/default-form');
        $vm->setVariables([
            'title' => 'Modifier un établissement',
            'form' => $form,
        ]);
        return $vm;
    }

    public function supprimerAction(): ViewModel
    {
        $etablissement = $this->getEtablissementService()->getRequestedEtablissement($this);

        if ($this->getRequest()->isPost()) {
            $data = $this->getRequest()->getPost();
            if ($data["reponse"] === "oui") $this->getEtablissementService()->delete($etablissement);
            exit();
        }

        $vm = new ViewModel();
        $vm->setTemplate('default/confirmation');
        $vm->setVariables([
            'title' => "Suppression de l'établissement [" . $etablissement->getLibelle() . "]",
            'text' => "La suppression est définitive êtes-vous sûr&middot;e de vouloir continuer ?",
            'action' => $this->url()->fromRoute('etudiant/etablissement/supprimer', ["etablissement" => $etablissement->getId()], [], true),
        ]);
        return $vm;
    }
}
<?php

namespace Etudiant\Controller;

use Application\Provider\Parametre\CharteParametres;
use Application\Provider\Validation\CharteValidations;
use Etudiant\Entity\Db\Etudiant;
use Etudiant\Service\AnneeUnivIA\AnneeUnivIAServiceAwareTrait;
use Etudiant\Service\ComposanteIA\ComposanteIAServiceAwareTrait;
use Etudiant\Service\CycleIA\CycleIAServiceAwareTrait;
use Etudiant\Service\Etablissement\EtablissementServiceAwareTrait;
use Etudiant\Service\EtablissementIA\EtablissementIAServiceAwareTrait;
use Etudiant\Service\Etudiant\EtudiantServiceAwareTrait;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\View\Model\JsonModel;
use Laminas\View\Model\ViewModel;
use UnicaenParametre\Service\Parametre\ParametreServiceAwareTrait;


/**
 * Description of EtudiantController
 *
 */
class EtudiantController extends AbstractActionController
{
    use AnneeUnivIAServiceAwareTrait;
    use ComposanteIAServiceAwareTrait;
    use CycleIAServiceAwareTrait;
    use EtablissementServiceAwareTrait;
    use EtablissementIAServiceAwareTrait;
    use EtudiantServiceAwareTrait;
    use ParametreServiceAwareTrait;

    public function indexAction(): ViewModel
    {

        $anneeUnivIAasOptions = $this->getAnneeUnivIAService()->getAnneeUnivIAasOptions();
        $etablissementIAasOptions = $this->getEtablissementIAService()->getEtablissementIAasOptions();
        $composanteIAasOptions = $this->getComposanteIAService()->getComposanteIAasOptions();
        $cycleIAasOptions = $this->getCycleIAService()->getCycleIAasOptions();
        $params = $this->params()->fromQuery();

        $paramsEtudiantsIA = null;
        if (array_key_exists('annee', $params)) {
            $anneeUniv = $params['annee'] ? $this->getAnneeUnivIAService()->getAnneeUnivIA($params['annee']) : null;
            if ($anneeUniv) {
                $paramsEtudiantsIA['annee_univ'] = $anneeUniv->getAnneeUniv();
            }
        }

        if (array_key_exists('etablissement', $params)) {
            $etablissement = $params['etablissement'] ? $this->getEtablissementIAService()->getEtablissementIA($params['etablissement']) : null;
            if ($etablissement) {
                $paramsEtudiantsIA['etablissement'] = $etablissement->getEtablissement();
            }
        }

        if (array_key_exists('composante', $params)) {
            $composante = $params['composante'] ? $this->getComposanteIAService()->getComposanteIA($params['composante']) : null;
            if ($composante) {
                $paramsEtudiantsIA['composante'] = $composante?->getComposante();
            }
        }

        if (array_key_exists('cycle', $params)) {
            $cycle = $params['cycle'] ? $this->getCycleIAService()->getCycleIA($params['cycle']) : null;
            if ($cycle) {
                $paramsEtudiantsIA['cycle'] = $cycle->getCycle();
            }
        }

        if (array_key_exists('etudiant', $params)) {
            if (array_key_exists('id', $params['etudiant'])) {
                $paramsEtudiantsIA['id'] = $params['etudiant']['id'];
            }
        }

        $etudiants = null;
        if ($paramsEtudiantsIA !== null) {
            $etudiants = $this->getEtudiantService()->getEtudiantsWithFiltre($paramsEtudiantsIA);
        }
        $options = ['isAnneeEnCours' => true];

        return new ViewModel([
            'etudiants' => $etudiants,
            'params' => $params,
            'anneeUnivIAasOptions' => $anneeUnivIAasOptions,
            'etablissementIAasOptions' => $etablissementIAasOptions,
            'composanteIAasOptions' => $composanteIAasOptions,
            'cycleIAasOptions' => $cycleIAasOptions,
            'options' => $options,
        ]);
    }

    public function afficherAction(): ViewModel
    {
        $etudiant = $this->getEtudiantService()->getRequestedEtudiant($this);

        $charteActive = $this->getParametreService()->getValeurForParametre(CharteParametres::TYPE, CharteParametres::CHARTE_ACTIVE);
        $validation = null;
        if ($charteActive) {
            $instance = $etudiant->getValidationActiveByTypeCode(CharteValidations::CHARTE_SIGNEE);
            $validation = $instance??false;
        }

        return new ViewModel([
            'title' => "Affichage d'un·e Étudiant·e",
            'etudiant' => $etudiant,
            'charte' => $validation,
        ]);
    }

    /** FONCTION DE RECHERCHE *****************************************************************************************/

    public function rechercherAction(): JsonModel
    {
        if (($term = $this->params()->fromQuery('term'))) {
            $etudiants = $this->getEtudiantService()->findEtudiantByTerm($term);
            foreach ($etudiants as $etudiant) {
                $result[] = [
                    'id' => $etudiant->getId(),
                    'label' => $etudiant->getDenomination(),
                    'extra' => "<span class='badge' style='background-color: slategray;'>" . $etudiant->getNumeroEtudiant() . "</span>",
                ];
            }
            usort($result, function ($a, $b) {
                return strcmp($a['label'], $b['label']);
            });

            return new JsonModel($result);
        }
        exit;
    }

    public function importerEtudiantAction(): ViewModel
    {
        $url = $this->url()->fromRoute('etudiant/importer-etudiant', [], []);

        $error = [];
        $warning = [];
        $messages = [];

        $etablissement = null;

        $etablissementAsOptions = $this->getEtablissementService()->getEtablissementAsOptions();

        $request = $this->getRequest();
        if ($request->isPost()) {

            $data = $request->getPost();
            $file = $request->getFiles();

            $etablissement = (isset($data['etablissement']) and $data['etablissement'] != '') ? $this->getEtablissementService()->getEtablissement($data['etablissement']) : null;

            $fichier_path = $file['file']['tmp_name'];

            //reading
            $array = [];
            if ($etablissement == null) {
                $error[] = "Aucun établissement sélectionné !";
            } elseif ($fichier_path == null or $fichier_path == '') {
                $error[] = "Aucun fichier !";
            } else {

                $all = file_get_contents($fichier_path);
                $encoding = mb_detect_encoding($all, 'UTF-8, ISO-8859-1');
                $handle = fopen($fichier_path, "r");

                while ($content = fgetcsv($handle, 0, ";")) {
                    $array[] = array_map(function (string $a) use ($encoding) {
                        //Note les backquote ne passe pas dans la fonction de convertion ...
                        $a = str_replace(chr(63), '\'', $a);
                        $a = mb_convert_encoding($a, 'UTF-8', $encoding);
                        return $a;
                    }, $content);
                }

                $messages [] = "Traitement du fichier " . $file['file']['name'];
                $messages [] = sizeof($array) . " lignes lues.";

            }

            if (empty($error)) {
                if (sizeof($array) < 2) {
                    $error[] = "Le fichier ne contient pas de données.";
                } else {

                    // champs obligatoires
                    $champs = ['nom', 'prenom', 'email', 'cycle', 'diplome'];
                    $champs = array_map('strtolower', $champs);
                    $array[0] = array_map('trim', $array[0]);
                    $array[0] = array_map(['\UnicaenApp\Util', 'stripAccents'], $array[0]);
                    $array[0] = array_map('strtolower', $array[0]);
                    $ind_champs = [];
                    foreach ($champs as $champ) {
                        if ($array[0][array_search($champ, $array[0])] !== $champ) {
                            $error[] = "Le fichier ne contient pas le champ [" . $champ . "].";
                        } else {
                            $ind_champs[$champ] = array_search($champ, $array[0]);
                        }
                    }

                    if (empty($error)) {
                        $etudiants = [];
                        foreach (array_slice($array, 1) as $line) {
                            $etudiant = [];
                            foreach ($champs as $champ) {
                                $etudiant [$champ] = $line[$ind_champs[$champ]];
                            }
                            $etudiant['source_id'] = $etablissement->getSigle();
                            $etudiant['id'] = $etudiant['source_id'] . '_' . $etudiant['email'];
                            $etudiant['numero_etudiant'] = '';
                            $etudiant['login'] = '';
                            $etudiant['annee'] = Etudiant::getAnneeDate();
                            $etudiant['etablissement'] = $etablissement->getSigle() . ' - ' . $etablissement->getLibelle();
                            $etudiant['composante'] = '';
                            $etudiant['formation'] = '';

                            $etudiants[] = $etudiant;
                        }
                        foreach ($this->getEtudiantService()->importEtudiants($etudiants) as $message) {
                            $messages[] = $message;
                        }
                    }
                }
            }

        }

        $vm = new ViewModel([
            'title' => "Importer des étudiants",
            'etablissement' => $etablissement,
            'etablissementAsOptions' => $etablissementAsOptions,
            'error' => $error,
            'warning' => $warning,
            'messages' => $messages,
        ]);

        return $vm;
    }
}


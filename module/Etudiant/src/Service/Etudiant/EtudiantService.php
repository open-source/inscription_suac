<?php

namespace Etudiant\Service\Etudiant;


use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\QueryBuilder;
use DoctrineModule\Persistence\ProvidesObjectManager;
use Etudiant\Entity\Db\Etudiant;
use Etudiant\Entity\Db\InscriptionAdministrative;
use Etudiant\Service\InscriptionAdministrative\InscriptionAdministrativeServiceAwareTrait;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\View\Model\ViewModel;
use UnicaenApp\Exception\RuntimeException;
use UnicaenUtilisateur\Entity\Db\User;
use UnicaenUtilisateur\Entity\Db\UserInterface;
use UnicaenUtilisateur\Service\User\UserService;
use UnicaenUtilisateur\Service\User\UserServiceAwareTrait;

/**
 * Description of EtudiantService
 *
 */
class EtudiantService
{
    use ProvidesObjectManager;
    use UserServiceAwareTrait;
    use InscriptionAdministrativeServiceAwareTrait;

    const MAX_RESULTS = 1000; // résultats max en comptant les jointures intermédiaires

    /** GESTION DES ENTITÉS *******************************************************************************************/
    public function create(Etudiant $etudiant): Etudiant
    {
        $this->getObjectManager()->persist($etudiant);
        $this->getObjectManager()->flush($etudiant);
        return $etudiant;
    }

    public function update(Etudiant $etudiant): Etudiant
    {
        $this->getObjectManager()->flush($etudiant);
        return $etudiant;
    }

    public function delete(Etudiant $etudiant): void
    {
        $this->getObjectManager()->remove($etudiant);
        $this->getObjectManager()->flush($etudiant);
    }


    /** REQUÊTAGE  ****************************************************************************************************/

    public function createQueryBuilder(): QueryBuilder
    {
        $qb = $this->getObjectManager()->getRepository(Etudiant::class)->createQueryBuilder('etudiant');
        return $qb;
    }

    public function getEtudiant(?string $id): ?Etudiant
    {
        $qb = $this->createQueryBuilder()
            ->andWhere('etudiant.id = :id')
            ->setParameter('id', $id);
        try {
            $result = $qb->getQuery()->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            throw new RuntimeException("Plusieurs Etudiant partagent la même id [" . $id . "]", 0, $e);
        }

        return $result;
    }

    public function getRequestedEtudiant(AbstractActionController $controller, string $param = 'etudiant'): ?Etudiant
    {
        $id = $controller->params()->fromRoute($param);
        return $this->getEtudiant($id);
    }

    public function getEtudiants(?string $champ = null, string $ordre = 'ASC'): array
    {
        $qb = $this->createQueryBuilder();

        if ($champ === null) {
            $qb = $qb->orderBy('etudiant.nom,etudiant.prenom,etudiant.login', $ordre);
        } else {
            $qb = $qb->orderBy('etudiant.' . $champ, $ordre);
        }
        $result = $qb->getQuery()->getResult();

        return $result;
    }

    public function getEtudiantByLogin(string $login): ?Etudiant
    {
        $qb = $this->createQueryBuilder()
            ->andWhere('etudiant.login = :login')
            ->setParameter('login', $login);
        try {
            $result = $qb->getQuery()->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            throw new RuntimeException("Plusieurs Etudiant partagent le même username [" . $login . "]", 0, $e);
        }

        return $result;
    }

    /**
     * @return User[]
     */
    public function getUserEtudiants(): array
    {
        $qb = $this->createQueryBuilder()
            ->Join("etudiant.utilisateur", "utilisateur")
            ->addSelect("utilisateur");

        $result = $qb->getQuery()->getResult();
        $users = [];
        /** @var Etudiant $item */
        foreach ($result as $item) {
            $users[$item->getLogin()] = $item->getUtilisateur();
        }

        return $users;
    }

    public function getConnectedEtudiant(): ?Etudiant
    {
        $user = $this->getUserService()->getConnectedUser();
        if ($user === null) {
            throw new RuntimeException("Aucun·e utilisateur·trice de connecté·e", 0);
        }
        return $this->getEtudiantByLogin($user->getUsername());
    }

    /** @return Etudiant[] */
    public function findEtudiantByTerm(string $term): array
    {
        $qb = $this->createQueryBuilder()
            ->andWhere("LOWER(CONCAT(etudiant.prenom, ' ', etudiant.nom)) like :search OR LOWER(CONCAT(etudiant.nom, ' ', etudiant.prenom)) like :search")
            ->setParameter('search', '%' . strtolower($term) . '%');
        $result = $qb->getQuery()->getResult();

        $etudiants = [];
        /** @var  $item */
        foreach ($result as $item) {
            $etudiants[$item->getId()] = $item;
        }

        return $etudiants;
    }

    public function getEtudiantByUser(?UserInterface $user, bool $histo = false): ?Etudiant
    {
        if ($user === null) return null;
        $qb = $this->createQueryBuilder()
            ->andWhere('etudiant.utilisateur = :user')->setParameter('user', $user);
        if (!$histo) $qb = $qb->andWhere('etudiant.deletedOn IS NULL');
        try {
            $result = $qb->getQuery()->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            throw new RuntimeException("Plusieurs [" . Etudiant::class . "] partagent le même utilisateur [" . $user->getId() . "|" . $user->getUsername() . "]", 0, $e);
        }
        return $result;
    }

    public function getEtudiantsWithFiltre(array $params = [], string $champ = 'id', string $ordre = 'ASC'): array
    {
        foreach ($params as $key => $value) {
            if ($value == null) {
                unset($params[$key]);
            }
        }

        $keysIA = ['annee_univ', 'etablissement', 'composante', 'cycle'];
        $keysEtudiant = ['id'];

        $qb = $this->createQueryBuilder();
        $qb->leftJoin('etudiant.inscriptionsAdministratives', 'ia');

        foreach ($params as $key => $value) {
            if (in_array($key, $keysIA)) {
                $qb = $qb->andWhere('ia.' . $key . ' = :' . $key)->setParameter($key, $value);
            }
            if (in_array($key, $keysEtudiant)) {
                $qb = $qb->andWhere('etudiant.' . $key . ' = :' . $key)->setParameter($key, $value);
            }

        }

        $qb = $qb->orderBy('etudiant.' . $champ, $ordre);
        $qb->setFirstResult(0);
        $qb->setMaxResults(self::MAX_RESULTS);
        $result = $qb->getQuery()->getResult();
        return $result;
    }

    /** IMPORT manuel (CSV) */

    /**
     * @param array $etudiants
     * @return string[]
     */
    public function importEtudiants(array $etudiants): array
    {
        $messages = [];

        $nbCreationEtudiant = 0;
        $nbMajEtudiant = 0;
        $nbCreationIA = 0;
        $nbMajIA = 0;

        foreach ($etudiants as $etudiant) {
            $e = $this->getEtudiant($etudiant['id']);
            if ($e === null) {
                $e = new Etudiant();
                $e->setId($etudiant['id']);
                $e->setSourceId($etudiant['source_id']);
                $e->setLogin($etudiant['login']);
                $e->setNom($etudiant['nom']);
                $e->setPrenom($etudiant['prenom']);
                $e->setEmail($etudiant['email']);
                $e->setNumeroEtudiant($etudiant['numero_etudiant']);
                $e->setInsertedOn(new \DateTime());
                $this->create($e);
                $nbCreationEtudiant++;

                // inscription administrative
                $ia = new InscriptionAdministrative();
                $nbCreationIA++;
                $ia->setId(uniqid($etudiant['source_id'] . '_'));
                $ia->setAnneeUniv($etudiant['annee']);
                $ia->setEtablissement($etudiant['etablissement']);
                $ia->setComposante($etudiant['composante']);
                $ia->setCycle($etudiant['cycle']);
                $ia->setDiplome($etudiant['diplome']);
                $ia->setFormation($etudiant['formation']);
                $ia->setSourceId($etudiant['source_id']);
                $ia->setInsertedOn(new \DateTime());

                $ia->setEtudiant($e);

                $this->getInscriptionAdministrativeService()->create($ia);


            } else {
                $isMaj = false;
                if ($etudiant['source_id'] != $e->getSourceId()) {
                    $e->setSourceId($etudiant['source_id']);
                    $isMaj = true;
                }
                if ($etudiant['login'] != $e->getLogin()) {
                    $e->setLogin($etudiant['login']);
                    $isMaj = true;
                }
                if ($etudiant['nom'] != $e->getNom()) {
                    $e->setNom($etudiant['nom']);
                    $isMaj = true;
                }
                if ($etudiant['prenom'] != $e->getPrenom()) {
                    $e->setPrenom($etudiant['prenom']);
                    $isMaj = true;
                }
                if ($etudiant['email'] != $e->getEmail()) {
                    $e->setEmail($etudiant['email']);
                    $isMaj = true;
                }
                if ($etudiant['numero_etudiant'] != $e->getNumeroEtudiant()) {
                    $e->setNumeroEtudiant($etudiant['numero_etudiant']);
                    $isMaj = true;
                }
                if ($isMaj) {
                    $e->setUpdatedOn(new \DateTime());
                    $this->update($e);
                    $nbMajEtudiant++;
                }

                // inscription administrative
                $IAs = $e->getInscriptionsAdministratives();
                $isNewIA = true;
                foreach ($IAs as $ia) {
                    /** @var InscriptionAdministrative $ia */
                    if ($ia->getAnneeUniv() == $etudiant['annee'] && $ia->getEtablissement() == $etudiant['etablissement']) {
                        $isMajIA = false;
                        $isNewIA = false;
                        if ($etudiant['source_id'] != $ia->getSourceId()) {
                            $ia->setSourceId($etudiant['source_id']);
                            $isMajIA = true;
                        }
                        if ($etudiant['composante'] != $ia->getComposante()) {
                            $ia->setComposante($etudiant['composante']);
                            $isMajIA = true;
                        }
                        if ($etudiant['cycle'] != $ia->getCycle()) {
                            $ia->setCycle($etudiant['cycle']);
                            $isMajIA = true;
                        }
                        if ($etudiant['diplome'] != $ia->getDiplome()) {
                            $ia->setDiplome($etudiant['diplome']);
                            $isMajIA = true;
                        }
                        if ($etudiant['formation'] != $ia->getFormation()) {
                            $ia->setFormation($etudiant['formation']);
                            $isMajIA = true;
                        }


                        if ($isMajIA) {
                            $ia->setUpdatedOn(new \DateTime());
                            $this->getInscriptionAdministrativeService()->update($ia);
                            $nbMajIA++;
                        }
                    }
                }
                if ($isNewIA) {
                    $ia = new InscriptionAdministrative();
                    $nbCreationIA++;
                    $ia->setId(uniqid($etudiant['source_id'] . '_'));
                    $ia->setAnneeUniv($etudiant['annee']);
                    $ia->setEtablissement($etudiant['etablissement']);
                    $ia->setComposante($etudiant['composante']);
                    $ia->setCycle($etudiant['cycle']);
                    $ia->setDiplome($etudiant['diplome']);
                    $ia->setFormation($etudiant['formation']);
                    $ia->setSourceId($etudiant['source_id']);
                    $ia->setInsertedOn(new \DateTime());

                    $ia->setEtudiant($e);

                    $this->getInscriptionAdministrativeService()->create($ia);
                }
            }

            // création du user s'il nexiste pas
            $this->creerEtudiantUser($e);
        }
        $messages[] = "Création de " . $nbCreationEtudiant . " étudiant(s)";
        $messages[] = "Mise à jour de " . $nbMajEtudiant . " étudiant(s)";
        $messages[] = "Création de " . $nbCreationIA . " inscription(s) administrative(s)";
        $messages[] = "Mise à jour de " . $nbMajIA . " inscription(s) administrative(s)";

        return $messages;
    }

    public function creerEtudiantUser(Etudiant $etudiant): User
    {
        // le mail est l'element discriminant dans le cas d'import
        $user = $this->getUserService()->getRepo()->findOneBy(['email' => $etudiant->getEmail()]);

        //creation si non existant
        if ($user === null) {
            $user = new User();
            $user->setEmail($etudiant->getEmail());
            $user->setPassword('db');
            $user->setDisplayName($etudiant->getDenomination());
            $login = \UnicaenApp\Util::reduce($etudiant->getNom() . $this->getUserService()->getMaxId() + 1);
            $user->setUsername($login);
            $user->setState(1);
            $this->getUserService()->createLocal($user);
        }

        //lien entre user <=> etudiant
        if ($etudiant->getLogin() !== $user->getUsername()) {
            $etudiant->setLogin($user->getUsername());
            $this->update($etudiant);
        }

        return $user;
    }
}


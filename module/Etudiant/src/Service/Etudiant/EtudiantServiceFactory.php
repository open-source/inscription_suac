<?php

namespace Etudiant\Service\Etudiant;

use Doctrine\ORM\EntityManager;
use Etudiant\Service\InscriptionAdministrative\InscriptionAdministrativeService;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;
use UnicaenUtilisateur\Service\User\UserService;

class EtudiantServiceFactory {

    /**
     * @param ContainerInterface $container
     * @return EtudiantService
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container) : EtudiantService
    {
        /**
         * @var EntityManager $entityManager
         * @var UserService $userService
         */
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        $userService = $container->get(UserService::class);
        $inscriptionAdministrativeService = $container->get(InscriptionAdministrativeService::class);

        $service = new EtudiantService();
        $service->setObjectManager($entityManager);
        $service->setUserService($userService);
        $service->setInscriptionAdministrativeService($inscriptionAdministrativeService);

        return $service;
    }
}
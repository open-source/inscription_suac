<?php

namespace Etudiant\Service\Etudiant;

trait EtudiantServiceAwareTrait
{
    private EtudiantService $etudiantService;

    public function getEtudiantService(): EtudiantService
    {
        return $this->etudiantService;
    }

    public function setEtudiantService(EtudiantService $etudiantService): void
    {
        $this->etudiantService = $etudiantService;
    }
}


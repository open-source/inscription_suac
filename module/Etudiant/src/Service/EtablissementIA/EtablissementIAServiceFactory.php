<?php

namespace Etudiant\Service\EtablissementIA;

use Doctrine\ORM\EntityManager;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;

class EtablissementIAServiceFactory
{
    public function __invoke(ContainerInterface $container) : EtablissementIAService
    {
        $entityManager = $container->get('doctrine.entitymanager.orm_default');

        $service = new EtablissementIAService();
        $service->setObjectManager($entityManager);
        return $service;
    }
}
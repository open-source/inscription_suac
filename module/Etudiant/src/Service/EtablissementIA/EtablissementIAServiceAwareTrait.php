<?php

namespace Etudiant\Service\EtablissementIA;

trait EtablissementIAServiceAwareTrait
{
    private EtablissementIAService $etablissementIAService;

    public function getEtablissementIAService(): EtablissementIAService
    {
        return $this->etablissementIAService;
    }

    public function setEtablissementIAService(EtablissementIAService $etablissementIAService): void
    {
        $this->etablissementIAService = $etablissementIAService;
    }
}
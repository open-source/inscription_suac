<?php

namespace Etudiant\Service\EtablissementIA;

use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\QueryBuilder;
use DoctrineModule\Persistence\ProvidesObjectManager;
use Etudiant\Entity\Db\EtablissementIA;
use Laminas\Mvc\Controller\AbstractActionController;
use RuntimeException;

class EtablissementIAService
{
    use ProvidesObjectManager;

    public function createQueryBuilder(): QueryBuilder
    {
        $qb = $this->getObjectManager()->getRepository(EtablissementIA::class)->createQueryBuilder('etablissementIA');
        return $qb;
    }

    public function getEtablissementIA(?int $id): ?EtablissementIA
    {
        $qb = $this->createQueryBuilder()
            ->andWhere('etablissementIA.id = :id')
            ->setParameter('id', $id);
        try {
            $result = $qb->getQuery()->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            throw new RuntimeException("Plusieurs EtablissementIA partagent la même id [" . $id . "]", 0, $e);
        }

        return $result;
    }

    public function getRequestedEtablissementIA(AbstractActionController $controller, string $param = 'etablissement-ia'): ?EtablissementIA
    {
        $id = $controller->params()->fromRoute($param);
        return $this->getEtablissementIA($id);
    }

    public function getEtablissementIAs(?string $champ = null, string $ordre = 'ASC'): array
    {
        $qb = $this->createQueryBuilder();

        if ($champ === null) {
            $qb->orderBy('etablissementIA.id', $ordre);
        } else {
            $qb->orderBy('etablissementIA.' . $champ, $ordre);
        }
        $result = $qb->getQuery()->getResult();

        return $result;
    }

    public function getEtablissementIAasOptions() : array
    {
        $etablissementIAs = $this->getEtablissementIAs('etablissement');
        $options = [];
        foreach ($etablissementIAs as $etablissementIA) {
            $options[$etablissementIA->getId()] = $etablissementIA->getEtablissement();
        }
        return $options;
    }
}
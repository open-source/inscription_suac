<?php

namespace Etudiant\Service\CycleIA;

use Doctrine\ORM\EntityManager;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;

class CycleIAServiceFactory
{
    public function __invoke(ContainerInterface $container) : CycleIAService
    {
        $entityManager = $container->get('doctrine.entitymanager.orm_default');

        $service = new CycleIAService();
        $service->setObjectManager($entityManager);
        return $service;
    }
}
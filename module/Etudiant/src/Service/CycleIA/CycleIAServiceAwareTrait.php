<?php

namespace Etudiant\Service\CycleIA;

trait CycleIAServiceAwareTrait
{
    private CycleIAService $cycleIAService;

    public function getCycleIAService(): CycleIAService
    {
        return $this->cycleIAService;
    }

    public function setCycleIAService(CycleIAService $cycleIAService): void
    {
        $this->cycleIAService = $cycleIAService;
    }
}
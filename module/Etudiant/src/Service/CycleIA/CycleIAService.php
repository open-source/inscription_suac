<?php

namespace Etudiant\Service\CycleIA;

use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\QueryBuilder;
use DoctrineModule\Persistence\ProvidesObjectManager;
use Etudiant\Entity\Db\CycleIA;
use Laminas\Mvc\Controller\AbstractActionController;
use RuntimeException;

class CycleIAService
{
    use ProvidesObjectManager;

    public function createQueryBuilder(): QueryBuilder
    {
        $qb = $this->getObjectManager()->getRepository(CycleIA::class)->createQueryBuilder('cycleIA');
        return $qb;
    }

    public function getCycleIA(?int $id): ?CycleIA
    {
        $qb = $this->createQueryBuilder()
            ->andWhere('cycleIA.id = :id')
            ->setParameter('id', $id);
        try {
            $result = $qb->getQuery()->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            throw new RuntimeException("Plusieurs CycleIA partagent la même id [" . $id . "]", 0, $e);
        }

        return $result;
    }

    public function getRequestedCycleIA(AbstractActionController $controller, string $param = 'cycle-ia'): ?CycleIA
    {
        $id = $controller->params()->fromRoute($param);
        return $this->getCycleIA($id);
    }

    public function getCycleIAs(?string $champ = null, string $ordre = 'ASC'): array
    {
        $qb = $this->createQueryBuilder();

        if ($champ === null) {
            $qb->orderBy('cycleIA.id', $ordre);
        } else {
            $qb->orderBy('cycleIA.' . $champ, $ordre);
        }
        $result = $qb->getQuery()->getResult();

        return $result;
    }

    public function getCycleIAasOptions() : array
    {
        $cycleIAs = $this->getCycleIAs('cycle');
        $options = [];
        foreach ($cycleIAs as $cycleIA) {
            $options[$cycleIA->getId()] = $cycleIA->getCycle();
        }
        return $options;
    }
}
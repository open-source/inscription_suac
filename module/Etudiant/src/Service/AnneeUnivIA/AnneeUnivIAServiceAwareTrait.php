<?php

namespace Etudiant\Service\AnneeUnivIA;

trait AnneeUnivIAServiceAwareTrait
{
    private AnneeUnivIAService $anneeUnivIAService;

    public function getAnneeUnivIAService(): AnneeUnivIAService
    {
        return $this->anneeUnivIAService;
    }

    public function setAnneeUnivIAService(AnneeUnivIAService $anneeUnivIAService): void
    {
        $this->anneeUnivIAService = $anneeUnivIAService;
    }
}
<?php

namespace Etudiant\Service\AnneeUnivIA;

use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\QueryBuilder;
use DoctrineModule\Persistence\ProvidesObjectManager;
use Etudiant\Entity\Db\AnneeUnivIA;
use Laminas\Mvc\Controller\AbstractActionController;
use RuntimeException;

class AnneeUnivIAService
{
    use ProvidesObjectManager;

    /** GESTION DES ENTITÉS *******************************************************************************************/

    /** REQUÊTAGE  ****************************************************************************************************/
    public function createQueryBuilder(): QueryBuilder
    {
        $qb = $this->getObjectManager()->getRepository(AnneeUnivIA::class)->createQueryBuilder('anneeUnivIA');
        return $qb;
    }

    public function getAnneeUnivIA(?int $id): ?AnneeUnivIA
    {
        $qb = $this->createQueryBuilder()
            ->andWhere('anneeUnivIA.id = :id')
            ->setParameter('id', $id);
        try {
            $result = $qb->getQuery()->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            throw new RuntimeException("Plusieurs AnneeUnivIA partagent la même id [" . $id . "]", 0, $e);
        }

        return $result;
    }

    public function getAnneeUnivIAByAnneeUniv(string $anneeUniv): ?AnneeUnivIA
    {
        $qb = $this->createQueryBuilder()
            ->andWhere('anneeUnivIA.anneeUniv = :anneeUniv')
            ->setParameter('anneeUniv', $anneeUniv);
        try {
            $result = $qb->getQuery()->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            throw new RuntimeException("Plusieurs AnneeUnivIA partagent la même anneeUniv [" . $anneeUniv . "]", 0, $e);
        }

        return $result;
    }

    public function getRequestedAnneeUnivIA(AbstractActionController $controller, string $param = 'annee-univ-ia'): ?AnneeUnivIA
    {
        $id = $controller->params()->fromRoute($param);
        return $this->getAnneeUnivIA($id);
    }

    /**
     * @param string|null $champ
     * @param string $ordre
     * @return AnneeUnivIA []
     */
    public function getAnneeUnivIAs(?string $champ = null, string $ordre = 'ASC'): array
    {
        $qb = $this->createQueryBuilder();

        if ($champ === null) {
            $qb->orderBy('anneeUnivIA.id', $ordre);
        } else {
            $qb->orderBy('anneeUnivIA.' . $champ, $ordre);
        }
        $result = $qb->getQuery()->getResult();

        return $result;
    }

    /**
     * @return string[]
     */
    public function getAnneeUnivIAasOptions() : array
    {
        $anneeUnivIAs = $this->getAnneeUnivIAs('annee_univ');
        $options = [];
        foreach ($anneeUnivIAs as $anneeUnivIA) {
            $options[$anneeUnivIA->getId()] = $anneeUnivIA->getAnneeUniv();
        }
        return $options;
    }
}
<?php

namespace Etudiant\Service\AnneeUnivIA;

use Doctrine\ORM\EntityManager;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;

class AnneeUnivIAServiceFactory
{

    /**
     * @param ContainerInterface $container
     * @return AnneeUnivIAService
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container) : AnneeUnivIAService
    {
        /**
         * @var EntityManager $entityManager
         */
        $entityManager = $container->get('doctrine.entitymanager.orm_default');

        $service = new AnneeUnivIAService();
        $service->setObjectManager($entityManager);
        return $service;
    }
}
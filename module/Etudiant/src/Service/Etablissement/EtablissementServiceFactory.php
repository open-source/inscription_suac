<?php

namespace Etudiant\Service\Etablissement;

use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;

class EtablissementServiceFactory
{
    /**
     * @param ContainerInterface $container
     * @return EtablissementService
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): EtablissementService
    {
        /**
         * @var EntityManager $entityManager
         */
        $entityManager = $container->get('doctrine.entitymanager.orm_default');

        $service = new EtablissementService();
        $service->setObjectManager($entityManager);
        return $service;
    }
}
<?php

namespace Etudiant\Service\Etablissement;

use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\QueryBuilder;
use DoctrineModule\Persistence\ProvidesObjectManager;
use Etudiant\Entity\Db\Etablissement;
use Laminas\Mvc\Controller\AbstractActionController;
use UnicaenApp\Exception\RuntimeException;

class EtablissementService
{
    use ProvidesObjectManager;

    public function create(Etablissement $etablissement): Etablissement
    {
        $this->getObjectManager()->persist($etablissement);
        $this->getObjectManager()->flush($etablissement);
        return $etablissement;
    }

    public function update(Etablissement $etablissement): Etablissement
    {
        $this->getObjectManager()->flush($etablissement);
        return $etablissement;
    }

    public function delete(Etablissement $etablissement): Etablissement
    {
        $this->getObjectManager()->remove($etablissement);
        $this->getObjectManager()->flush($etablissement);
        return $etablissement;
    }

    public function createQueryBuilder(): QueryBuilder
    {
        return $this->getObjectManager()->getRepository(Etablissement::class)->createQueryBuilder('etablissement');
    }

    public function getEtablissements(string $champ = 'libelle', string $ordre = 'ASC'): array
    {
        $qb = $this->createQueryBuilder()
            ->orderBy('etablissement.' . $champ, $ordre);
        return $qb->getQuery()->getResult();
    }

    public function getEtablissement(?int $id): ?Etablissement
    {
        if ($id === null) return null;
        $qb = $this->createQueryBuilder()
            ->andWhere('etablissement.id = :id')
            ->setParameter('id', $id);

        try {
            return $qb->getQuery()->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            throw new RuntimeException("Multiple [".Etablissement::class."] entities found with the same ID [" . $id . "]", $e);
        }
    }

    public function getRequestedEtablissement(AbstractActionController $controller, string $paramName = 'etablissement'): ?Etablissement
    {
        $id = $controller->params()->fromRoute($paramName);
        return $this->getEtablissement($id);
    }

    public function getEtablissementAsOptions() : array
    {
        $etablissements = $this->getEtablissements();
        $options = [];
        foreach ($etablissements as $etablissement) {
            /** @var Etablissement $etablissement */
            $options[$etablissement->getId()] = $etablissement->getSigle() . ' - ' . $etablissement->getLibelle();
        }
        return $options;
    }
}
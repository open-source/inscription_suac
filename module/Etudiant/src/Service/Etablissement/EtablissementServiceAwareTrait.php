<?php

namespace Etudiant\Service\Etablissement;

trait EtablissementServiceAwareTrait
{
    private EtablissementService $importEtablissementService;

    public function getEtablissementService(): EtablissementService
    {
        return $this->importEtablissementService;
    }

    public function setEtablissementService(EtablissementService $importEtablissementService): void
    {
        $this->importEtablissementService = $importEtablissementService;
    }
}
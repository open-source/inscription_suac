<?php

namespace Etudiant\Service\InscriptionAdministrative;

trait InscriptionAdministrativeServiceAwareTrait
{
    private InscriptionAdministrativeService $inscriptionAdministrativeService;

    public function getInscriptionAdministrativeService(): InscriptionAdministrativeService
    {
        return $this->inscriptionAdministrativeService;
    }

    public function setInscriptionAdministrativeService(InscriptionAdministrativeService $inscriptionAdministrativeService): void
    {
        $this->inscriptionAdministrativeService = $inscriptionAdministrativeService;
    }


}
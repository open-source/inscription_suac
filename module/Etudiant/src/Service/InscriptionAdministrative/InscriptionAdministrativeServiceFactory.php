<?php

namespace Etudiant\Service\InscriptionAdministrative;

use Doctrine\ORM\EntityManager;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;

class InscriptionAdministrativeServiceFactory
{

    public function __invoke(ContainerInterface $container) : InscriptionAdministrativeService
    {
        /**
         * @var EntityManager $entityManager
         */

        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        $service = new InscriptionAdministrativeService();
        $service->setObjectManager($entityManager);
        return $service;
    }
}
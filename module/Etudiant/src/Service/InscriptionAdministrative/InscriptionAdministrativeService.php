<?php

namespace Etudiant\Service\InscriptionAdministrative;

use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\QueryBuilder;
use DoctrineModule\Persistence\ProvidesObjectManager;
use Etudiant\Entity\Db\Etudiant;
use Etudiant\Entity\Db\InscriptionAdministrative;

class InscriptionAdministrativeService
{
    use ProvidesObjectManager;

    /** GESTION DES ENTITÉS *******************************************************************************************/

    public function create(InscriptionAdministrative $inscriptionAdministrative): InscriptionAdministrative
    {
        $this->getObjectManager()->persist($inscriptionAdministrative);
        $this->getObjectManager()->flush($inscriptionAdministrative);
        return $inscriptionAdministrative;
    }

    public function update(InscriptionAdministrative $inscriptionAdministrative): InscriptionAdministrative
    {
        $this->getObjectManager()->flush($inscriptionAdministrative);
        return $inscriptionAdministrative;
    }

    public function delete(InscriptionAdministrative $inscriptionAdministrative): void
    {
        $this->getObjectManager()->remove($inscriptionAdministrative);
        $this->getObjectManager()->flush($inscriptionAdministrative);
    }

    /** REQUÊTAGE  ****************************************************************************************************/

    public function createQueryBuilder(): QueryBuilder
    {
        $qb = $this->getObjectManager()->getRepository(InscriptionAdministrative::class)->createQueryBuilder('inscriptionAdministrative');
        return $qb;
    }

    public function getInscriptionAdministrative(?string $id): ?InscriptionAdministrative
    {
        $qb = $this->createQueryBuilder()
            ->andWhere('inscriptionAdministrative.id = :id')
            ->setParameter('id', $id);

        try {
            return $qb->getQuery()->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }

    public function getInscriptionsAdministrativesEtudiant(Etudiant $etudiant): array
    {
        $qb = $this->createQueryBuilder();
        $qb->andWhere('inscriptionAdministrative.etudiant = :etudiant')
            ->setParameter('etudiant', $etudiant);
        return $qb->getQuery()->getResult();
    }

    public function getInscriptionAdministrativeEtudiantAnneUniv(Etudiant $etudiant, string $anneeUniv): array
    {
        $qb = $this->createQueryBuilder();
        $qb->andWhere('inscriptionAdministrative.etudiant = :etudiant')
            ->andWhere('inscriptionAdministrative.annee_univ = :anneeUniv')
            ->setParameter('etudiant', $etudiant)
            ->setParameter('anneeUniv', $anneeUniv);
        return $qb->getQuery()->getResult();
    }



}
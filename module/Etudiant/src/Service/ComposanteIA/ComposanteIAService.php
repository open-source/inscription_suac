<?php

namespace Etudiant\Service\ComposanteIA;

use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\QueryBuilder;
use DoctrineModule\Persistence\ProvidesObjectManager;
use Etudiant\Entity\Db\ComposanteIA;
use Laminas\Mvc\Controller\AbstractActionController;
use RuntimeException;

class ComposanteIAService
{
    use ProvidesObjectManager;

    public function createQueryBuilder(): QueryBuilder
    {
        $qb = $this->getObjectManager()->getRepository(ComposanteIA::class)->createQueryBuilder('composanteIA');
        return $qb;
    }

    public function getComposanteIA(?int $id): ?ComposanteIA
    {
        $qb = $this->createQueryBuilder()
            ->andWhere('composanteIA.id = :id')
            ->setParameter('id', $id);
        try {
            $result = $qb->getQuery()->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            throw new RuntimeException("Plusieurs ComposanteIA partagent la même id [" . $id . "]", 0, $e);
        }

        return $result;
    }

    public function getRequestedComposanteIA(AbstractActionController $controller, string $param = 'composante-ia'): ?ComposanteIA
    {
        $id = $controller->params()->fromRoute($param);
        return $this->getComposanteIA($id);
    }

    public function getComposanteIAs(?string $champ = null, string $ordre = 'ASC'): array
    {
        $qb = $this->createQueryBuilder();

        if ($champ === null) {
            $qb->orderBy('composanteIA.id', $ordre);
        } else {
            $qb->orderBy('composanteIA.' . $champ, $ordre);
        }
        $result = $qb->getQuery()->getResult();

        return $result;
    }

    public function getComposanteIAasOptions() : array
    {
        $composanteIAs = $this->getComposanteIAs('composante');
        $options = [];
        foreach ($composanteIAs as $composanteIA) {
            $options[$composanteIA->getId()] = $composanteIA->getComposante();
        }
        return $options;
    }
}
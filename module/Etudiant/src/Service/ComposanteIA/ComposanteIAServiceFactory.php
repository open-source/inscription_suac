<?php

namespace Etudiant\Service\ComposanteIA;

use Doctrine\ORM\EntityManager;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;

class ComposanteIAServiceFactory
{
    public function __invoke(ContainerInterface $container) : ComposanteIAService
    {
        $entityManager = $container->get('doctrine.entitymanager.orm_default');

        $service = new ComposanteIAService();
        $service->setObjectManager($entityManager);
        return $service;
    }
}
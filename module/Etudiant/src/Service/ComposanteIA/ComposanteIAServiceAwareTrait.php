<?php

namespace Etudiant\Service\ComposanteIA;

trait ComposanteIAServiceAwareTrait
{
    private ComposanteIAService $composanteIAService;

    public function getComposanteIAService(): ComposanteIAService
    {
        return $this->composanteIAService;
    }

    public function setComposanteIAService(ComposanteIAService $composanteIAService): void
    {
        $this->composanteIAService = $composanteIAService;
    }
}
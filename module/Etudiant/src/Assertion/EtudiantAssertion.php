<?php

namespace Etudiant\Assertion;

use Application\Provider\Role\RolesProvider as RoleAppProvider;
use Etudiant\Entity\Db\Etudiant;
use Etudiant\Provider\Privilege\EtudiantPrivileges;
use Etudiant\Provider\Role\RolesProvider as RoleEtudiantProvider;
use Etudiant\Service\Etudiant\EtudiantServiceAwareTrait;
use Laminas\Permissions\Acl\Resource\ResourceInterface;
use UnicaenPrivilege\Assertion\AbstractAssertion;
use UnicaenPrivilege\Service\Privilege\PrivilegeCategorieServiceAwareTrait;
use UnicaenPrivilege\Service\Privilege\PrivilegeServiceAwareTrait;
use UnicaenUtilisateur\Entity\Db\RoleInterface;
use UnicaenUtilisateur\Entity\Db\UserInterface;
use UnicaenUtilisateur\Service\User\UserServiceAwareTrait;

class EtudiantAssertion extends AbstractAssertion
{
    use PrivilegeServiceAwareTrait;
    use PrivilegeCategorieServiceAwareTrait;
    use UserServiceAwareTrait;

    use EtudiantServiceAwareTrait;


    //TODO mettre à un plus haut niveau car dupliqué
    public function isScopeCompatible(?Etudiant $etudiant, UserInterface $user, ?RoleInterface $role): bool
    {
        if (!$role) {
            return false;
        }
        $isConnectedEtudiant = false;
        if ($etudiant !== null and $role->getRoleId() === RoleEtudiantProvider::ROLE_ETUDIANT) {
            $isConnectedEtudiant = $etudiant->getLogin() === $user->getUsername();
        }

        $value = match ($role->getRoleId()) {
            RoleAppProvider::ROLE_ADMIN_TEC,
            RoleAppProvider::ROLE_ADMIN_FONC,
            RoleAppProvider::ROLE_OBSERVATEUR
                    => true,
            RoleEtudiantProvider::ROLE_ETUDIANT => $isConnectedEtudiant,
            default => false,
        };

        return $value;
    }



    protected function assertEntity(ResourceInterface $entity = null, $privilege = null): bool
    {
        /** @var Etudiant|null $entity */
        if (!$entity instanceof Etudiant) {
            return false;
        }

        return $this->computeAssertion($entity, $privilege);
    }



    /**
     * @param Etudiant|null $entity
     * @param string        $privilege
     *
     * @return bool
     */
    private function computeAssertion(?Etudiant $entity, string $privilege): bool
    {

        $user = $this->getUserService()->getConnectedUser();
        /**@var $role RoleInterface|null */
        $role = $this->getUserService()->getConnectedRole();

        if (!$this->getPrivilegeService()->checkPrivilege($privilege, $role)) return false;

        switch ($privilege) {
            case EtudiantPrivileges::ETUDIANT_AFFICHER:
            case EtudiantPrivileges::ETUDIANT_INDEX:
                return $this->isScopeCompatible($entity, $user, $role);
        }

        return true;
    }



    protected function assertController($controller, $action = null, $privilege = null): bool
    {
        /** @var Etudiant|null $entity */
        $etudiantId = (($this->getMvcEvent()->getRouteMatch()->getParam('etudiant')));
        $entity = $this->getEtudiantService()->getEtudiant($etudiantId);

        return match ($action) {
            'afficher'
                => $this->computeAssertion($entity, EtudiantPrivileges::ETUDIANT_AFFICHER),
            'rechercher',
            'index'
                => $this->computeAssertion($entity, EtudiantPrivileges::ETUDIANT_INDEX),
            default => true,
        };

    }
}
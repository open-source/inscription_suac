<?php

namespace Etudiant\Assertion;

use Etudiant\Service\Etudiant\EtudiantService;
use Interop\Container\ContainerInterface;
use Laminas\Mvc\Application;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use UnicaenPrivilege\Service\Privilege\PrivilegeCategorieService;
use UnicaenPrivilege\Service\Privilege\PrivilegeService;
use UnicaenUtilisateur\Service\User\UserService;

class EtudiantAssertionFactory
{

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): EtudiantAssertion
    {
        /**
         * @var PrivilegeService          $privilegeService
         * @var PrivilegeCategorieService $privilegeCategorieService
         * @var UserService               $userService
         * @var EtudiantService           $etudiantService
         */
        $privilegeService          = $container->get(PrivilegeService::class);
        $privilegeCategorieService = $container->get(PrivilegeCategorieService::class);
        $userService               = $container->get(UserService::class);
        $etudiantService           = $container->get(EtudiantService::class);

        $assertion = new EtudiantAssertion();
        $assertion->setPrivilegeService($privilegeService);
        $assertion->setPrivilegeCategorieService($privilegeCategorieService);
        $assertion->setUserService($userService);
        $assertion->setEtudiantService($etudiantService);

        /* @var $application Application */
        $application = $container->get('Application');
        $mvcEvent    = $application->getMvcEvent();
        $assertion->setMvcEvent($mvcEvent);

        return $assertion;
    }
}
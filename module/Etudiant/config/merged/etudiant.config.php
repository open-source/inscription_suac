<?php

use Etudiant\Assertion\EtudiantAssertion;
use Etudiant\Assertion\EtudiantAssertionFactory;
use Etudiant\Controller\EtudiantController;
use Etudiant\Controller\EtudiantControllerFactory;
use Etudiant\Provider\Identity\IdentityProvider;
use Etudiant\Provider\Identity\IdentityProviderFactory;
use Etudiant\Provider\Privilege\EtudiantPrivileges;
use Etudiant\Service\AnneeUnivIA\AnneeUnivIAService;
use Etudiant\Service\AnneeUnivIA\AnneeUnivIAServiceFactory;
use Etudiant\Service\ComposanteIA\ComposanteIAService;
use Etudiant\Service\ComposanteIA\ComposanteIAServiceFactory;
use Etudiant\Service\CycleIA\CycleIAService;
use Etudiant\Service\CycleIA\CycleIAServiceFactory;
use Etudiant\Service\EtablissementIA\EtablissementIAService;
use Etudiant\Service\EtablissementIA\EtablissementIAServiceFactory;
use Etudiant\Service\Etudiant\EtudiantService;
use Etudiant\Service\Etudiant\EtudiantServiceFactory;
use Etudiant\Service\InscriptionAdministrative\InscriptionAdministrativeService;
use Etudiant\Service\InscriptionAdministrative\InscriptionAdministrativeServiceFactory;
use Etudiant\View\Helper\EtudiantInscriptionsAdministrativesViewHelper;
use Etudiant\View\Helper\EtudiantsViewHelper;
use Laminas\Router\Http\Literal;
use Laminas\Router\Http\Segment;
use UnicaenPrivilege\Guard\PrivilegeController;
use UnicaenPrivilege\Provider\Rule\PrivilegeRuleProvider;


return [
    'bjyauthorize' => [
        'resource_providers' => [
            'BjyAuthorize\Provider\Resource\Config' => [
                'Etudiant' => [],
            ],
        ],
        'rule_providers' => [
            PrivilegeRuleProvider::class => [
                'allow' => [
                    [
                        'privileges' => [
                            EtudiantPrivileges::ETUDIANT_AFFICHER,
                            EtudiantPrivileges::ETUDIANT_INDEX,
                        ],
                        'resources' => ['Etudiant'],
                        'assertion' => EtudiantAssertion::class,
                    ],
                ],
            ],
        ],
        'guards' => [
            PrivilegeController::class => [
                [
                    'controller' => EtudiantController::class,
                    'action' => [
                        'index',
                    ],
                    'privileges' => [
                        EtudiantPrivileges::ETUDIANT_INDEX,
                    ],
                ],
                [
                    'controller' => EtudiantController::class,
                    'action' => [
                        'afficher',
                        'rechercher',
                    ],
                    'privileges' => [
                        EtudiantPrivileges::ETUDIANT_AFFICHER,
                    ],
                    'assertion' => EtudiantAssertion::class,

                ],
                [
                    'controller' => EtudiantController::class,
                    'action' => [
                        'importerEtudiant',
                    ],
                    'privileges' => [
                        EtudiantPrivileges::ETUDIANT_IMPORTER,
                    ],
                ],
            ],
        ],
    ],

    'router' => [
        'routes' => [
            'etudiant' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/etudiant',
                    'defaults' => [
                        /** @see EtudiantController::indexAction() */
                        'controller' => EtudiantController::class,
                        'action' => 'index',
                    ],
                ],
                'may_terminate' => true,
                'child_routes' => [
                    'afficher' => [
                        'type' => Segment::class,
                        'options' => [
                            'route' => '/afficher/:etudiant',
                            'defaults' => [
                                /** @see EtudiantController::afficherAction() */
                                'controller' => EtudiantController::class,
                                'action' => 'afficher',
                            ],
                        ],
                    ],
                    'rechercher' => [
                        'type' => Literal::class,
                        'options' => [
                            'route' => '/rechercher',
                            'defaults' => [
                                'controller' => EtudiantController::class,
                                'action' => 'rechercher',
                            ],
                        ],
                    ],
                    'importer-etudiant' => [
                        'type' => Literal::class,
                        'options' => [
                            'route' => '/importer-etudiant',
                            'defaults' => [
                                /** @see EtudiantController::importerEtudiantAction() */
                                'controller' => EtudiantController::class,
                                'action' => 'importerEtudiant',
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],

    'navigation' => [
        'default' => [
            'home' => [
                'pages' => [
                    'gestion' => [
                        'pages' => [
                            'etudiant' => [
                                'label' => "Gestion des étudiants",
                                'route' => 'etudiant',
                                'resource' => PrivilegeController::getResourceId(EtudiantController::class, 'index'),
                                'order' => 1000 + 200,
                                'icon' => 'fas fa-angle-right',
                            ],
                            'importer-etudiant' => [
                                'label' => "Importer des étudiants",
                                'route' => 'etudiant/importer-etudiant',
                                'resource' => PrivilegeController::getResourceId(EtudiantController::class, 'importerEtudiant'),
                                'order' => 1000 + 300,
                                'icon' => 'fas fa-angle-right',
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],

    'controllers' => [
        'factories' => [
            EtudiantController::class => EtudiantControllerFactory::class,
        ],
    ],
    'service_manager' => [
        'factories' => [
            IdentityProvider::class => IdentityProviderFactory::class,
            EtudiantService::class => EtudiantServiceFactory::class,
            EtudiantAssertion::class => EtudiantAssertionFactory::class,
            AnneeUnivIAService::class => AnneeUnivIAServiceFactory::class,
            EtablissementIAService::class => EtablissementIAServiceFactory::class,
            ComposanteIAService::class => ComposanteIAServiceFactory::class,
            CycleIAService::class => CycleIAServiceFactory::class,
            InscriptionAdministrativeService::class => InscriptionAdministrativeServiceFactory::class,
        ],
    ],
    'form_elements' => [
        'factories' => [
        ],
    ],
    'hydrators' => [
        'factories' => [
        ],
    ],
    'view_helpers' => [
        'invokables' => [
            'etudiants' => EtudiantsViewHelper::class,
            'etudiantInscriptionsAdministratives' => EtudiantInscriptionsAdministrativesViewHelper::class,
        ],
        'factories' => [
        ],
        'aliases' => [
        ],
    ],
];

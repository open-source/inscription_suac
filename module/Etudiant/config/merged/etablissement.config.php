<?php

use Etudiant\Controller\EtablissementController;
use Etudiant\Controller\EtablissementControllerFactory;
use Etudiant\Form\Etablissement\EtablissementForm;
use Etudiant\Form\Etablissement\EtablissementFormFactory;
use Etudiant\Form\Etablissement\EtablissementHydrator;
use Etudiant\Form\Etablissement\EtablissementHydratorFactory;
use Etudiant\Provider\Privilege\EtablissementPrivileges;
use Etudiant\Service\Etablissement\EtablissementService;
use Etudiant\Service\Etablissement\EtablissementServiceFactory;
use Laminas\Router\Http\Literal;
use Laminas\Router\Http\Segment;
use UnicaenPrivilege\Guard\PrivilegeController;

return [
    'bjyauthorize' => [
        'guards' => [
            PrivilegeController::class => [
                [
                    'controller' => EtablissementController::class,
                    'action' => [
                        'index',
                    ],
                    'privileges' => [
                        EtablissementPrivileges::ETABLISSEMENT_INDEX,
                    ],
                ],
                [
                    'controller' => EtablissementController::class,
                    'action' => [
                        'afficher',
                    ],
                    'privileges' => [
                        EtablissementPrivileges::ETABLISSEMENT_AFFICHER,
                    ],
                ],
                [
                    'controller' => EtablissementController::class,
                    'action' => [
                        'ajouter',
                    ],
                    'privileges' => [
                        EtablissementPrivileges::ETABLISSEMENT_AJOUTER,
                    ],
                ],
                [
                    'controller' => EtablissementController::class,
                    'action' => [
                        'modifier',
                    ],
                    'privileges' => [
                        EtablissementPrivileges::ETABLISSEMENT_MODIFIER,
                    ],
                ],
                [
                    'controller' => EtablissementController::class,
                    'action' => [
                        'supprimer',
                    ],
                    'privileges' => [
                        EtablissementPrivileges::ETABLISSEMENT_SUPPRIMER,
                    ],
                ],
            ],
        ],
    ],

    'router' => [
        'routes' => [
            'etudiant' => [
                'child_routes' => [
                    'etablissement' => [
                        'type' => Literal::class,
                        'options' => [
                            'route' => '/etablissement',
                            'defaults' => [
                                /** @see EtablissementController::indexAction() */
                                'controller' => EtablissementController::class,
                                'action' => 'index',
                            ],
                        ],
                        'may_terminate' => true,
                        'child_routes' => [
                            'afficher' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/afficher/:etablissement',
                                    'defaults' => [
                                        /** @see EtablissementController::afficherAction() */
                                        'action' => 'afficher',
                                    ],
                                ],
                            ],
                            'ajouter' => [
                                'type' => Literal::class,
                                'options' => [
                                    'route' => '/ajouter',
                                    'defaults' => [
                                        /** @see EtablissementController::ajouterAction() */
                                        'action' => 'ajouter',
                                    ],
                                ],
                            ],
                            'modifier' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/modifier/:etablissement',
                                    'defaults' => [
                                        /** @see EtablissementController::modifierAction() */
                                        'action' => 'modifier',
                                    ],
                                ],
                            ],
                            'supprimer' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/supprimer/:etablissement',
                                    'defaults' => [
                                        /** @see EtablissementController::supprimerAction() */
                                        'action' => 'supprimer',
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],

    'navigation' => [
        'default' => [
            'home' => [
                'pages' => [
                    'gestion' => [
                        'pages' => [
                            'etablissement' => [
                                'label' => "Gestion des établissements",
                                'route' => 'etudiant/etablissement',
                                'resource' => PrivilegeController::getResourceId(EtablissementController::class, 'index'),
                                'order' => 1000 + 400,
                                'icon' => 'fas fa-angle-right',
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],

    'controllers' => [
        'factories' => [
            EtablissementController::class => EtablissementControllerFactory::class,
        ],
    ],
    'service_manager' => [
        'factories' => [
            EtablissementService::class => EtablissementServiceFactory::class,
        ],
    ],
    'form_elements' => [
        'factories' => [
            EtablissementForm::class => EtablissementFormFactory::class,
        ],
    ],
    'hydrators' => [
        'factories' => [
            EtablissementHydrator::class => EtablissementHydratorFactory::class,
        ],
    ],
    'view_helpers' => [
        'invokables' => [
        ],
        'factories' => [
        ],
        'aliases' => [
        ],
    ],
];
